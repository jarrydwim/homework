import agi.foundation.time.*;
import agi.foundation.*;
import agi.foundation.navigation.*;
import agi.foundation.navigation.datareaders.*;
import agi.foundation.geometry.*;
import agi.foundation.coordinates.*;
import agi.foundation.celestial.*;
import agi.foundation.celestial.earthcentralbody.*;
import agi.foundation.celestial.centralbodiesfacet.*;
import agi.foundation.geometry.discrete.*;
import agi.foundation.access.constraints.*;
import agi.foundation.access.*;
import agi.foundation.platforms.*;
import agi.foundation.propagators.*;
import agi.foundation.access.constraints.*;

%% Receiver data
NumberOfChannels = 12;
MaskAngle = 20.0; % degrees above the horizon - below which no GPS satellites will be tracked
ReceiverNoise = 1.0; % 1 meter of constant noise on each receiving channel (for each SV)

%% GpsReceiver Position data (Colorado Springs)
Latitude = 39.0;
Longitude = -104.77;
Height = 1910;

%% GpsReceiver Start Time
StartTime = GregorianDate(2012,11,24, 11, 03, 45);

%% Earth reference for Navigation and Access calculations
facet = CentralBodiesFacet.getFromContext();
earth = facet.getEarth();


%% Setting up the GpsReceiver
% The receiver stores many properties and has a defined location.  This location
% is the point of reference for visibility calculations.
receiver = GpsReceiver();

% Set the receiver properties 
% Be sure to convert your angles to Radians!
minimumAngle = Trig.degreesToRadians(MaskAngle);
constraints = receiver.getReceiverConstraints();
constraints.clear();
constraints.add(ElevationAngleConstraint(earth, minimumAngle));
receiver.setNumberOfChannels(NumberOfChannels);
% this is where you model your own receiver's noise.  Here, we're
% defaulting to 1.0 meter of error
receiver.setNoiseModel(ConstantGpsReceiverNoiseModel(ReceiverNoise));

% The receiver's methods of reducing the number of visible satellites to the limit imposed by the number of channels
receiver.setReceiverSolutionType(GpsReceiverSolutionType.ALL_IN_VIEW);

% Create a new location for the receiver by using the Cartographic type from AGI.Foundation.Coordinates
% again, remember to convert from degrees to Radians! (except the height of course)
position = Cartographic(Trig.degreesToRadians(Longitude),Trig.degreesToRadians(Latitude), Height);

% Now create an antenna for the GPS receiver.  We specify the location of the antenna by assigning a
% PointCartographic instance to the receiver's locationPoint.  We specify that the antenna should be oriented
% according to the typically-used East-North-Up axes by assigning an instance of AxesEastNorthUp to
% the receiver's orientation axes.  While the orientation of the antenna won't affect which satellites are visible
% or tracked in this case, it will affect the DOP values.  For example, the EDOP value can be found in
% DilutionOfPrecision.X, but only if we've configured the antenna to use East-North-Up axes.
antenna = Platform();
antenna.setLocationPoint(PointCartographic(earth, position));
antenna.setOrientationAxes(AxesEastNorthUp(earth, antenna.getLocationPoint()));
receiver.setAntenna(antenna);

%% Now get the satellites to use, and other navigation data
almanac = SemAlmanac.readFrom('GPSAlmanac.al3',1);
pafFile = PerformanceAssessmentFile.readFrom('2012_329_v02.paf');
psfFile = PredictionSupportFile.readFrom('2012_329_234500_v01.psf');
sofFile = SatelliteOutageFile.readFrom('2012_326_152025_v01.sof');

% Add Satellites and their outages
gpsConstellation = almanac.createSatelliteCollection();
% Add satellite outages to the constellation
sofFile.populateConstellationOutageData(gpsConstellation);
receiver.setNavigationSatellites(gpsConstellation);

%% Get Evaluators for different metrics
% Using an Evaluator group for like evaluations can speed things up quite a
% bit
group = EvaluatorGroup();
% get a DilutionOfPrecisionEvaluator
dopEval = receiver.getDilutionOfPrecisionEvaluator(group);
% Get an Assessed Accuracy Evaluator, using the PAF data
AssdAccEval = receiver.getNavigationAccuracyAssessedEvaluator(pafFile,AccuracyCalculationPersona.DILIGENT,group);
% Get a Predicted Accuracy Evaluator
PredAccEval = receiver.getNavigationAccuracyPredictedEvaluator(psfFile,group);
% Get the satellite index evaluator - returns an set of indices into the
% platformcollection for the constellation for those SVs tracked
SatelliteEval = receiver.getSatelliteTrackingIndexEvaluator(group);

%% Evaluate at StartTime
% Evaluate which satellites were used
satelliteIndices = SatelliteEval.evaluate(JulianDate(StartTime));
disp('Number of Satellites used in this calculation:')
length(satelliteIndices)
%Evaluate the DOP at the start Time
dop = dopEval.evaluate(JulianDate(StartTime));
disp('East Dop: ');
dop.getX()
disp('North Dop: ');
dop.getY()
disp('Vertical Dop: ');
dop.getZ()
disp('Horizontal Dop: ');
dop.getXY()
disp('Position Dop: ');
dop.getPosition()
disp('Time Dop: ');
dop.getTime()
% Evaluate the Assessed Accuracy at the Start Time
AssdAcc = AssdAccEval.evaluate(JulianDate(StartTime));
disp('East Assessed Navigation Error: (meters)');
% The Total errors include errors from the ReceiverNoiseModel above
% you can also use AssdAcc.getXSignalInSpace() to see navigation errors due to the
% constellation only (PAF file errors)
AssdAcc.getXTotal()
disp('North Assessed Navigation Error: (meters)');
AssdAcc.getYTotal()
disp('Vertical Assessed Navigation Error: (meters)');
AssdAcc.getZTotal()
disp('Horizontal Assessed Navigation Error: (meters)');
AssdAcc.getXYTotal()
disp('Position Assessed Navigation Error: (meters)');
AssdAcc.getPositionTotal()
disp('Time Assessed Navigation Error: (seconds)');
AssdAcc.getTimeTotal()

%Evaluate the predicted Navigation Accuracy at the Start Time
PredAcc = PredAccEval.evaluate(JulianDate(StartTime));
disp('East Predicted 1 Sigma Navigation Error: (meters ~68% confidence)');
PredAcc.getXTotal()
disp('North Predicted 1 Sigma Navigation Error: (meters ~68% confidence)');
PredAcc.getYTotal()
disp('Vertical Predicted 1 Sigma Navigation Error: (meters ~68% confidence)');
PredAcc.getZTotal()
disp('Horizontal Predicted 1 Sigma Navigation Error: (meters ~37% confidence)');
PredAcc.getXYTotal()
disp('Position Predicted 1 Sigma Navigation Error: (meters ~20% confidence)');
PredAcc.getPositionTotal()
disp('Time Predicted 1 Sigma Navigation Error: (seconds ~68% confidence)');
PredAcc.getTimeTotal()
%% Convert Predicted Results
%Convert the 1-Sigma Predicted navigation errors above to a standard 95% confidence
ci = ConfidenceInterval;
disp('Total Position error - Predicted 95% confidence(meters): ');
ci.convertToGlobalPositioningSystemConfidence(PredAcc.getPositionTotal(),95,ConfidenceIntervalVariableDimension.THREE)
disp('Total Horizontal error -  Predicted 95% confidence(meters): ');
ci.convertToGlobalPositioningSystemConfidence(PredAcc.getXYTotal(),95,ConfidenceIntervalVariableDimension.TWO)
disp('Total Vertical error - Predicted 95% confidence(meters): ');
ci.convertToGlobalPositioningSystemConfidence(PredAcc.getZTotal(),95,ConfidenceIntervalVariableDimension.ONE)
