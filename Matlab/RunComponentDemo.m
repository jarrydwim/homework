%% Load the Components - be sure your license data and paths are set
%% correctly!
LoadComponents;

%% Run the Navigation Accuracy Example
NavigationExample;

%% Run the DGL Access Example
DGLExample;