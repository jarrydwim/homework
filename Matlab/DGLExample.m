%import the java classes we'll use in this example
import agi.foundation.time.*;
import agi.foundation.*;
import agi.foundation.geometry.*;
import agi.foundation.coordinates.*;
import agi.foundation.celestial.*;
import agi.foundation.celestial.earthcentralbody.*;
import agi.foundation.celestial.centralbodiesfacet.*;
import agi.foundation.access.*;
import agi.foundation.platforms.*;
import agi.foundation.propagators.*;
import agi.foundation.access.constraints.*;

%% Simple Access
%Pick a TLE set that we know will work here for the dates & facility location
tle = '1 20813U 90084A   07157.52757149 -.00000978  00000-0  10000-3 0  8139 2 20813  62.3585 177.5907 7234421 261.2958  18.3008  2.01001663122427';

% Set latitude and longitude values for the facility and the minimum
% elevation angle
latitude = 40.0;
longitude = -76.0;
minimumElevationAngle = 5.0;

% Propagate the TLE set with an SGP4 propagator and use the result to
% define the time-varying location of a Platform object representing the satellite
propagator = Sgp4Propagator(TwoLineElementSet(tle));
satellite = Platform;
satellite.setLocationPoint(propagator.createPoint());
satellite.setOrientationAxes(Axes.getRoot());

% Create a platform object representing a facility, and position it
% with angular values converted to radians
facility = Platform;
facility.setLocationPoint(PointCartographic(earth,...
    Cartographic(Trig.degreesToRadians(longitude),...
                 Trig.degreesToRadians(latitude),...
                 0.0)));
facility.setOrientationAxes(Axes.getRoot());

% Create an elevation angle constraint and convert the angle to radians
elevationAngleConstraint = ElevationAngleConstraint;
elevationAngleConstraint.setMinimumValue(Trig.degreesToRadians(minimumElevationAngle));

% Use the two Platform objects and the constraint to define
% an access computation. Note that it doesn't matter which is the receiver
% and which is the transmitter, but the constraint must be assigned to the
% one representing the facility
access = AccessComputation;
access.setTransmitter(satellite);
access.setReceiver(facility);
access.getReceiverConstraints().add(elevationAngleConstraint);

%Set the start and end dates for the time period during which access is computed
startDate = GregorianDate.parse('06/01/2007 12:00');
endDate = GregorianDate.parse('06/03/2007 12:00');            

% Convert the start and end dates for the access computation period
% to Julian dates
AccessStart = JulianDate(startDate);
AccessEnd = JulianDate(endDate);

% Compute access intervals for the specified time period
accessResult = access.computeIntervals(AccessStart, AccessEnd);
intervals = accessResult.getOverallSatisfactionIntervals();
% The access intervals can now be used to generate schedules, 
% highlight satellite ground tracks, etc.

% Let's see how many access opportunities there are
disp('Number of Accesses:');
disp(intervals.size());

% Let's print out the Access Times (note the non-matlab indexing!)
for i = 0:intervals.size() - 1
    fprintf(1,'Access %i:\n',i);
    disp('Start:');
    % One way to get a date
    disp(intervals.get(i).getStart().toGregorianDate().toString());
    disp('End:');
    % another way to get a date
    disp(intervals.get(i).getStop().toDateTime.toString());
    disp('Period:');
    % Java Period type
    disp(intervals.get(i).toDuration().toPeriod().toString());
end
