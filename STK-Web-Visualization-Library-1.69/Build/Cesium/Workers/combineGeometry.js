/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["./when-283ff6c4","./Check-5faa3364","./Math-6bc5bd67","./Cartesian2-5374041e","./Transforms-05b6f507","./RuntimeError-73ca5813","./WebGLConstants-cf9e59a1","./ComponentDatatype-9728ab6a","./GeometryAttribute-3cde56b0","./GeometryAttributes-7ab0fc8a","./AttributeCompression-fb133ccf","./GeometryPipeline-96353589","./EncodedCartesian3-27431b61","./IndexDatatype-462102f2","./IntersectionTests-bb4b429d","./Plane-fcb3bbc4","./PrimitivePipeline-32674400","./WebMercatorProjection-d77429b4","./createTaskProcessorWorker"],function(e,t,i,r,n,a,o,b,c,s,m,f,P,p,u,d,y,C,l){"use strict";return l(function(e,t){var i=y.PrimitivePipeline.unpackCombineGeometryParameters(e),r=y.PrimitivePipeline.combineGeometry(i);return y.PrimitivePipeline.packCombineGeometryResults(r,t)})});
