/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["exports","./when-283ff6c4","./Check-5faa3364","./Math-6bc5bd67"],function(e,h,t,r){"use strict";var c=r.CesiumMath.EPSILON10;e.arrayRemoveDuplicates=function(e,t,r){if(h.defined(e)){r=h.defaultValue(r,!1);var f,n,i,a=e.length;if(a<2)return e;for(f=1;f<a&&!t(n=e[f-1],i=e[f],c);++f);if(f===a)return r&&t(e[0],e[e.length-1],c)?e.slice(1):e;for(var u=e.slice(0,f);f<a;++f)t(n,i=e[f],c)||(u.push(i),n=i);return r&&1<u.length&&t(u[0],u[u.length-1],c)&&u.shift(),u}}});
