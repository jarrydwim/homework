/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["./when-283ff6c4","./Check-5faa3364","./Math-6bc5bd67","./Cartesian2-5374041e","./Transforms-05b6f507","./RuntimeError-73ca5813","./WebGLConstants-cf9e59a1","./ComponentDatatype-9728ab6a","./GeometryAttribute-3cde56b0","./GeometryAttributes-7ab0fc8a","./IndexDatatype-462102f2","./VertexFormat-de1e6064"],function(z,e,t,g,T,a,r,L,O,P,k,n){"use strict";var R=new g.Cartesian3;return function(e){var t,a,r=e._vertexFormat,n=e._radius,o=z.defined(e._perDirectionRadius)&&e._perDirectionRadius,i=e._directions;i[0].clock<i[1].clock&&i.reverse();var s,p,m,y,u,c=0,f=new P.GeometryAttributes,b=[],A=[],C=i.length;for(s=0;s<C;s++)y=g.Cartesian3.fromSpherical(i[s]),0!==s&&(g.Cartesian3.equals(b[s-1],y)||s===C-1&&g.Cartesian3.equals(b[0],y))||(b.push(y),A.push(g.Cartesian3.normalize(y,new g.Cartesian3)));if(C=b.length,r.position){u=2*(C+1)*3;var l=new Float64Array(u);for(s=p=0;s<C;s++){l[p++]=0,l[p++]=0,l[p++]=0,y=A[s];var v=o?g.Cartesian3.magnitude(b[s]):n;l[p++]=y.x*v,l[p++]=y.y*v,l[p++]=y.z*v,c=Math.max(c,v)}l[p++]=l[0],l[p++]=l[1],l[p++]=l[2],l[p++]=l[3],l[p++]=l[4],l[p++]=l[5],f.position=new O.GeometryAttribute({componentDatatype:L.ComponentDatatype.DOUBLE,componentsPerAttribute:3,values:l})}if(r.normal){var d;for(u=2*(C+1)*3,t=new Float32Array(u),s=p=0;s<C;s++)y=b[s],d=s+1===C?b[0]:b[s+1],R=g.Cartesian3.normalize(g.Cartesian3.cross(y,d,R),R),t[p++]=R.x,t[p++]=R.y,t[p++]=R.z,t[p++]=R.x,t[p++]=R.y,t[p++]=R.z;t[p++]=t[0],t[p++]=t[1],t[p++]=t[2],t[p++]=t[3],t[p++]=t[4],t[p++]=t[5],f.normal=new O.GeometryAttribute({componentDatatype:L.ComponentDatatype.FLOAT,componentsPerAttribute:3,values:t})}if(r.bitangent){for(u=2*(C+1)*3,a=new Float32Array(u),s=p=0;s<C;s++)y=A[s],a[p++]=y.x,a[p++]=y.y,a[p++]=y.z,a[p++]=y.x,a[p++]=y.y,a[p++]=y.z;a[p++]=a[0],a[p++]=a[1],a[p++]=a[2],a[p++]=a[3],a[p++]=a[4],a[p++]=a[5],f.bitangent=new O.GeometryAttribute({componentDatatype:L.ComponentDatatype.FLOAT,componentsPerAttribute:3,values:a})}if(r.tangent){u=2*(C+1)*3;var w=new Float32Array(u);for(s=p=0;s<u;s+=6){var D=g.Cartesian3.unpack(t,s),x=g.Cartesian3.unpack(a,s),h=g.Cartesian3.normalize(g.Cartesian3.cross(x,D,R),R);w[p++]=h.x,w[p++]=h.y,w[p++]=h.z,w[p++]=h.x,w[p++]=h.y,w[p++]=h.z}f.tangent=new O.GeometryAttribute({componentDatatype:L.ComponentDatatype.FLOAT,componentsPerAttribute:3,values:w})}if(r.st){u=2*(C+1)*2;var F=new Float32Array(u);for(s=p=0;s<C;s++)m=1-s/(C+1),F[p++]=m,F[p++]=0,F[p++]=m,F[p++]=1;m=1-s/(C+1),F[p++]=m,F[p++]=0,F[p++]=m,F[p++]=1,f.st=new O.GeometryAttribute({componentDatatype:L.ComponentDatatype.FLOAT,componentsPerAttribute:2,values:F})}s=p=0,u=2*(C+1)*3;for(var G=k.IndexDatatype.createTypedArray(u/3,u);p<u-6;)G[p++]=s,G[p++]=s+3,G[p++]=s+1,G[p++]=s,G[p++]=s+2,G[p++]=s+3,s+=2;return G[p++]=s,G[p++]=1,G[p++]=s+1,G[p++]=s,G[p++]=0,G[p++]=1,new O.Geometry({attributes:f,indices:G,primitiveType:O.PrimitiveType.TRIANGLES,boundingSphere:new T.BoundingSphere(g.Cartesian3.ZERO,c)})}});
