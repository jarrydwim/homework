/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(['./when-283ff6c4', './Check-5faa3364', './Math-4fd79cbb', './Cartesian2-8ffc48fc', './Transforms-93159586', './RuntimeError-73ca5813', './WebGLConstants-cf9e59a1', './ComponentDatatype-d832f265', './GeometryAttribute-9dadb4c5', './GeometryAttributes-7ab0fc8a', './AttributeCompression-16a40fd9', './GeometryPipeline-10c90962', './EncodedCartesian3-9c63e7b7', './IndexDatatype-82708eb8', './IntersectionTests-244b4523', './Plane-2da0baf9', './GeometryOffsetAttribute-2d2108a6', './VertexFormat-6f16a8a8', './EllipseGeometryLibrary-adf27809', './GeometryInstance-0983b1f6', './EllipseGeometry-a779a464'], function (when, Check, _Math, Cartesian2, Transforms, RuntimeError, WebGLConstants, ComponentDatatype, GeometryAttribute, GeometryAttributes, AttributeCompression, GeometryPipeline, EncodedCartesian3, IndexDatatype, IntersectionTests, Plane, GeometryOffsetAttribute, VertexFormat, EllipseGeometryLibrary, GeometryInstance, EllipseGeometry) { 'use strict';

  function createEllipseGeometry(ellipseGeometry, offset) {
    if (when.defined(offset)) {
      ellipseGeometry = EllipseGeometry.EllipseGeometry.unpack(ellipseGeometry, offset);
    }
    ellipseGeometry._center = Cartesian2.Cartesian3.clone(ellipseGeometry._center);
    ellipseGeometry._ellipsoid = Cartesian2.Ellipsoid.clone(ellipseGeometry._ellipsoid);
    return EllipseGeometry.EllipseGeometry.createGeometry(ellipseGeometry);
  }

  return createEllipseGeometry;

});
//# sourceMappingURL=createEllipseGeometry.js.map
