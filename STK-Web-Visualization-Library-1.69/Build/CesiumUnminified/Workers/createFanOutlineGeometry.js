/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(['./when-283ff6c4', './Check-5faa3364', './Math-4fd79cbb', './Cartesian2-8ffc48fc', './Transforms-93159586', './RuntimeError-73ca5813', './WebGLConstants-cf9e59a1', './ComponentDatatype-d832f265', './GeometryAttribute-9dadb4c5', './GeometryAttributes-7ab0fc8a', './IndexDatatype-82708eb8', './VertexFormat-6f16a8a8'], function (when, Check, _Math, Cartesian2, Transforms, RuntimeError, WebGLConstants, ComponentDatatype, GeometryAttribute, GeometryAttributes, IndexDatatype, VertexFormat) { 'use strict';

  var scratchCartesian = new Cartesian2.Cartesian3();

  /**
   * Describes the outline of a {@link FanGeometry}.
   *
   * @alias FanOutlineGeometry
   * @ionsdk
   * @constructor
   *
   * @param {Spherical[]} options.directions The directions, pointing outward from the origin, that defined the fan.
   * @param {Number} options.radius The radius at which to draw the fan.
   * @param {Boolean} options.perDirectionRadius When set to true, the magnitude of each direction is used in place of a constant radius.
   * @param {Number} [options.numberOfRings=6] The number of outline rings to draw, starting from the outer edge and equidistantly spaced towards the center.
   * @param {VertexFormat} [options.vertexFormat=VertexFormat.DEFAULT] The vertex attributes to be computed.
   */
  function FanOutlineGeometry(options) {
    options = when.defaultValue(options, when.defaultValue.EMPTY_OBJECT);

    //>>includeStart('debug', pragmas.debug);
    if (!when.defined(options.directions)) {
      throw new Check.DeveloperError("options.directions is required");
    }
    if (!options.perDirectionRadius && !when.defined(options.radius)) {
      throw new Check.DeveloperError(
        "options.radius is required when options.perDirectionRadius is undefined or false."
      );
    }
    //>>includeEnd('debug');

    this._radius = options.radius;
    this._directions = options.directions;
    this._perDirectionRadius = options.perDirectionRadius;
    this._numberOfRings = when.defaultValue(options.numberOfRings, 6);
    this._vertexFormat = when.defaultValue(options.vertexFormat, VertexFormat.VertexFormat.DEFAULT);
    this._workerName = "createFanOutlineGeometry";
  }

  /**
   * Computes the geometric representation of a fan outline, including its vertices, indices, and a bounding sphere.
   *
   * @param {FanOutlineGeometry} fanGeometry A description of the fan.
   * @returns {Geometry} The computed vertices and indices.
   */
  FanOutlineGeometry.createGeometry = function (fanGeometry) {
    //>>includeStart('debug', pragmas.debug);
    if (!when.defined(fanGeometry)) {
      throw new Check.DeveloperError("fanGeometry is required");
    }
    //>>includeEnd('debug');

    var radius = fanGeometry._radius;
    var perDirectionRadius =
      when.defined(fanGeometry._perDirectionRadius) && fanGeometry._perDirectionRadius;
    var directions = fanGeometry._directions;
    var vertexFormat = fanGeometry._vertexFormat;
    var numberOfRings = fanGeometry._numberOfRings;

    var i;
    var x;
    var ring;
    var length;
    var maxRadius = 0;
    var indices;
    var positions;
    var directionsLength = directions.length;
    var attributes = new GeometryAttributes.GeometryAttributes();

    if (vertexFormat.position) {
      x = 0;
      length = directionsLength * 3 * numberOfRings;
      positions = new Float64Array(length);

      for (ring = 0; ring < numberOfRings; ring++) {
        for (i = 0; i < directionsLength; i++) {
          scratchCartesian = Cartesian2.Cartesian3.fromSpherical(
            directions[i],
            scratchCartesian
          );
          var currentRadius = perDirectionRadius
            ? Cartesian2.Cartesian3.magnitude(scratchCartesian)
            : radius;
          var ringRadius = (currentRadius / numberOfRings) * (ring + 1);
          scratchCartesian = Cartesian2.Cartesian3.normalize(
            scratchCartesian,
            scratchCartesian
          );

          positions[x++] = scratchCartesian.x * ringRadius;
          positions[x++] = scratchCartesian.y * ringRadius;
          positions[x++] = scratchCartesian.z * ringRadius;
          maxRadius = Math.max(maxRadius, currentRadius);
        }
      }

      attributes.position = new GeometryAttribute.GeometryAttribute({
        componentDatatype: ComponentDatatype.ComponentDatatype.DOUBLE,
        componentsPerAttribute: 3,
        values: positions,
      });
    }

    x = 0;
    length = directionsLength * 2 * numberOfRings;
    indices = IndexDatatype.IndexDatatype.createTypedArray(length / 3, length);

    for (ring = 0; ring < numberOfRings; ring++) {
      var offset = ring * directionsLength;
      for (i = 0; i < directionsLength - 1; i++) {
        indices[x++] = i + offset;
        indices[x++] = i + 1 + offset;
      }
      indices[x++] = i + offset;
      indices[x++] = 0 + offset;
    }

    return new GeometryAttribute.Geometry({
      attributes: attributes,
      indices: indices,
      primitiveType: GeometryAttribute.PrimitiveType.LINES,
      boundingSphere: new Transforms.BoundingSphere(Cartesian2.Cartesian3.ZERO, maxRadius),
    });
  };

  var createFanOutlineGeometry = FanOutlineGeometry.createGeometry;

  return createFanOutlineGeometry;

});
//# sourceMappingURL=createFanOutlineGeometry.js.map
