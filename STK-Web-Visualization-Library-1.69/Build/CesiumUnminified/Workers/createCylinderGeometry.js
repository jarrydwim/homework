/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(['./when-283ff6c4', './Check-5faa3364', './Math-4fd79cbb', './Cartesian2-8ffc48fc', './Transforms-93159586', './RuntimeError-73ca5813', './WebGLConstants-cf9e59a1', './ComponentDatatype-d832f265', './GeometryAttribute-9dadb4c5', './GeometryAttributes-7ab0fc8a', './IndexDatatype-82708eb8', './GeometryOffsetAttribute-2d2108a6', './VertexFormat-6f16a8a8', './CylinderGeometryLibrary-44089058', './CylinderGeometry-88ab5e75'], function (when, Check, _Math, Cartesian2, Transforms, RuntimeError, WebGLConstants, ComponentDatatype, GeometryAttribute, GeometryAttributes, IndexDatatype, GeometryOffsetAttribute, VertexFormat, CylinderGeometryLibrary, CylinderGeometry) { 'use strict';

  function createCylinderGeometry(cylinderGeometry, offset) {
    if (when.defined(offset)) {
      cylinderGeometry = CylinderGeometry.CylinderGeometry.unpack(cylinderGeometry, offset);
    }
    return CylinderGeometry.CylinderGeometry.createGeometry(cylinderGeometry);
  }

  return createCylinderGeometry;

});
//# sourceMappingURL=createCylinderGeometry.js.map
