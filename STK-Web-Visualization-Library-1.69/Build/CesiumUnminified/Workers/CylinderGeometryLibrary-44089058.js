/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(['exports', './Math-4fd79cbb'], function (exports, _Math) { 'use strict';

  /**
   * @private
   */
  var CylinderGeometryLibrary = {};

  /**
   * @private
   */
  CylinderGeometryLibrary.computePositions = function (
    length,
    topRadius,
    bottomRadius,
    slices,
    fill
  ) {
    var topZ = length * 0.5;
    var bottomZ = -topZ;

    var twoSlice = slices + slices;
    var size = fill ? 2 * twoSlice : twoSlice;
    var positions = new Float64Array(size * 3);
    var i;
    var index = 0;
    var tbIndex = 0;
    var bottomOffset = fill ? twoSlice * 3 : 0;
    var topOffset = fill ? (twoSlice + slices) * 3 : slices * 3;

    for (i = 0; i < slices; i++) {
      var angle = (i / slices) * _Math.CesiumMath.TWO_PI;
      var x = Math.cos(angle);
      var y = Math.sin(angle);
      var bottomX = x * bottomRadius;
      var bottomY = y * bottomRadius;
      var topX = x * topRadius;
      var topY = y * topRadius;

      positions[tbIndex + bottomOffset] = bottomX;
      positions[tbIndex + bottomOffset + 1] = bottomY;
      positions[tbIndex + bottomOffset + 2] = bottomZ;

      positions[tbIndex + topOffset] = topX;
      positions[tbIndex + topOffset + 1] = topY;
      positions[tbIndex + topOffset + 2] = topZ;
      tbIndex += 3;
      if (fill) {
        positions[index++] = bottomX;
        positions[index++] = bottomY;
        positions[index++] = bottomZ;
        positions[index++] = topX;
        positions[index++] = topY;
        positions[index++] = topZ;
      }
    }

    return positions;
  };

  exports.CylinderGeometryLibrary = CylinderGeometryLibrary;

});
//# sourceMappingURL=CylinderGeometryLibrary-44089058.js.map
