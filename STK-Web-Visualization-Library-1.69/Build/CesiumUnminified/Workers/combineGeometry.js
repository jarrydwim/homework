/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(['./when-283ff6c4', './Check-5faa3364', './Math-4fd79cbb', './Cartesian2-8ffc48fc', './Transforms-93159586', './RuntimeError-73ca5813', './WebGLConstants-cf9e59a1', './ComponentDatatype-d832f265', './GeometryAttribute-9dadb4c5', './GeometryAttributes-7ab0fc8a', './AttributeCompression-16a40fd9', './GeometryPipeline-10c90962', './EncodedCartesian3-9c63e7b7', './IndexDatatype-82708eb8', './IntersectionTests-244b4523', './Plane-2da0baf9', './PrimitivePipeline-9413490c', './WebMercatorProjection-a49f5927', './createTaskProcessorWorker'], function (when, Check, _Math, Cartesian2, Transforms, RuntimeError, WebGLConstants, ComponentDatatype, GeometryAttribute, GeometryAttributes, AttributeCompression, GeometryPipeline, EncodedCartesian3, IndexDatatype, IntersectionTests, Plane, PrimitivePipeline, WebMercatorProjection, createTaskProcessorWorker) { 'use strict';

  function combineGeometry(packedParameters, transferableObjects) {
    var parameters = PrimitivePipeline.PrimitivePipeline.unpackCombineGeometryParameters(
      packedParameters
    );
    var results = PrimitivePipeline.PrimitivePipeline.combineGeometry(parameters);
    return PrimitivePipeline.PrimitivePipeline.packCombineGeometryResults(
      results,
      transferableObjects
    );
  }
  var combineGeometry$1 = createTaskProcessorWorker(combineGeometry);

  return combineGeometry$1;

});
//# sourceMappingURL=combineGeometry.js.map
