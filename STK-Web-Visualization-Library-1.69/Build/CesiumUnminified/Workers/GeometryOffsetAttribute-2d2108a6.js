/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(['exports', './when-283ff6c4', './Check-5faa3364'], function (exports, when, Check) { 'use strict';

  /**
   * Fill an array or a portion of an array with a given value.
   *
   * @param {Array} array The array to fill.
   * @param {*} value The value to fill the array with.
   * @param {Number} [start=0] The index to start filling at.
   * @param {Number} [end=array.length] The index to end stop at.
   *
   * @returns {Array} The resulting array.
   * @private
   */
  function arrayFill(array, value, start, end) {
    //>>includeStart('debug', pragmas.debug);
    Check.Check.defined("array", array);
    Check.Check.defined("value", value);
    if (when.defined(start)) {
      Check.Check.typeOf.number("start", start);
    }
    if (when.defined(end)) {
      Check.Check.typeOf.number("end", end);
    }
    //>>includeEnd('debug');

    if (typeof array.fill === "function") {
      return array.fill(value, start, end);
    }

    var length = array.length >>> 0;
    var relativeStart = when.defaultValue(start, 0);
    // If negative, find wrap around position
    var k =
      relativeStart < 0
        ? Math.max(length + relativeStart, 0)
        : Math.min(relativeStart, length);
    var relativeEnd = when.defaultValue(end, length);
    // If negative, find wrap around position
    var last =
      relativeEnd < 0
        ? Math.max(length + relativeEnd, 0)
        : Math.min(relativeEnd, length);

    // Fill array accordingly
    while (k < last) {
      array[k] = value;
      k++;
    }
    return array;
  }

  /**
   * Represents which vertices should have a value of `true` for the `applyOffset` attribute
   * @private
   */
  var GeometryOffsetAttribute = {
    NONE: 0,
    TOP: 1,
    ALL: 2,
  };
  var GeometryOffsetAttribute$1 = Object.freeze(GeometryOffsetAttribute);

  exports.GeometryOffsetAttribute = GeometryOffsetAttribute$1;
  exports.arrayFill = arrayFill;

});
//# sourceMappingURL=GeometryOffsetAttribute-2d2108a6.js.map
