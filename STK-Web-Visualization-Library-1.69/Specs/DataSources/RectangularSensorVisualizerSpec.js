import { RectangularSensorVisualizer } from "../../Source/Cesium.js";
import { BoundingSphere } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { JulianDate } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";
import { Matrix3 } from "../../Source/Cesium.js";
import { Matrix4 } from "../../Source/Cesium.js";
import { Quaternion } from "../../Source/Cesium.js";
import { BoundingSphereState } from "../../Source/Cesium.js";
import { ColorMaterialProperty } from "../../Source/Cesium.js";
import { ConstantProperty } from "../../Source/Cesium.js";
import { EntityCollection } from "../../Source/Cesium.js";
import { RectangularSensorGraphics } from "../../Source/Cesium.js";
import { SensorVolumePortionToDisplay } from "../../Source/Cesium.js";
import createScene from "../createScene.js";

describe(
  "DataSources/RectangularSensorVisualizer",
  function () {
    var scene;
    var visualizer;

    beforeAll(function () {
      scene = createScene();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    afterEach(function () {
      visualizer = visualizer && visualizer.destroy();
    });

    it("constructor throws if no scene is passed.", function () {
      expect(function () {
        return new RectangularSensorVisualizer();
      }).toThrowDeveloperError();
    });

    it("update throws if no time specified.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);
      expect(function () {
        visualizer.update();
      }).toThrowDeveloperError();
    });

    it("isDestroy returns false until destroyed.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);
      expect(visualizer.isDestroyed()).toEqual(false);
      visualizer.destroy();
      expect(visualizer.isDestroyed()).toEqual(true);
      visualizer = undefined;
    });

    it("object with no rectangularSensor does not create a primitive.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      visualizer.update(JulianDate.now());
      expect(scene.primitives.length).toEqual(0);
    });

    it("object with no position does not create a primitive.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var rectangularSensor = (testObject.rectangularSensor = new RectangularSensorGraphics());
      rectangularSensor.xHalfAngle = new ConstantProperty(0.1);
      rectangularSensor.yHalfAngle = new ConstantProperty(0.2);
      visualizer.update(JulianDate.now());
      expect(scene.primitives.length).toEqual(0);
    });

    it("A RectangularSensorGraphics causes a sensor to be created and updated.", function () {
      var time = JulianDate.now();
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(
        new Quaternion(
          0,
          0,
          Math.sin(CesiumMath.PI_OVER_FOUR),
          Math.cos(CesiumMath.PI_OVER_FOUR)
        )
      );

      var rectangularSensor = (testObject.rectangularSensor = new RectangularSensorGraphics());
      rectangularSensor.xHalfAngle = new ConstantProperty(0.1);
      rectangularSensor.yHalfAngle = new ConstantProperty(0.2);
      rectangularSensor.intersectionColor = new ConstantProperty(
        new Color(0.1, 0.2, 0.3, 0.4)
      );
      rectangularSensor.intersectionWidth = new ConstantProperty(0.5);
      rectangularSensor.showIntersection = new ConstantProperty(true);
      rectangularSensor.showThroughEllipsoid = new ConstantProperty(true);
      rectangularSensor.radius = new ConstantProperty(123.5);
      rectangularSensor.show = new ConstantProperty(true);
      rectangularSensor.lateralSurfaceMaterial = new ColorMaterialProperty(
        Color.WHITE
      );
      rectangularSensor.showLateralSurfaces = new ConstantProperty(true);
      rectangularSensor.ellipsoidHorizonSurfaceMaterial = new ColorMaterialProperty(
        Color.RED
      );
      rectangularSensor.showEllipsoidHorizonSurfaces = new ConstantProperty(
        true
      );
      rectangularSensor.domeSurfaceMaterial = new ColorMaterialProperty(
        Color.BLUE
      );
      rectangularSensor.showDomeSurfaces = new ConstantProperty(true);
      rectangularSensor.ellipsoidSurfaceMaterial = new ColorMaterialProperty(
        Color.BLUE
      );
      rectangularSensor.showEllipsoidSurfaces = new ConstantProperty(true);
      rectangularSensor.portionToDisplay = new ConstantProperty(
        SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON
      );
      rectangularSensor.environmentConstraint = new ConstantProperty(true);
      rectangularSensor.showEnvironmentOcclusion = new ConstantProperty(true);
      rectangularSensor.environmentOcclusionMaterial = new ColorMaterialProperty();
      rectangularSensor.showEnvironmentIntersection = new ConstantProperty(
        true
      );
      rectangularSensor.environmentIntersectionColor = new ConstantProperty(
        Color.WHITE
      );
      rectangularSensor.environmentIntersectionWidth = new ConstantProperty(1);
      visualizer.update(time);

      expect(scene.primitives.length).toEqual(1);
      var p = scene.primitives.get(0);
      expect(p.intersectionColor).toEqual(
        testObject.rectangularSensor.intersectionColor.getValue(time)
      );
      expect(p.intersectionWidth).toEqual(
        testObject.rectangularSensor.intersectionWidth.getValue(time)
      );
      expect(p.showThroughEllipsoid).toEqual(
        testObject.rectangularSensor.showThroughEllipsoid.getValue(time)
      );
      expect(p.showIntersection).toEqual(
        testObject.rectangularSensor.showIntersection.getValue(time)
      );
      expect(p.radius).toEqual(
        testObject.rectangularSensor.radius.getValue(time)
      );
      expect(p.modelMatrix).toEqual(
        Matrix4.fromRotationTranslation(
          Matrix3.fromQuaternion(testObject.orientation.getValue(time)),
          testObject.position.getValue(time)
        )
      );
      expect(p.show).toEqual(testObject.rectangularSensor.show.getValue(time));
      expect(p.lateralSurfaceMaterial.uniforms).toEqual(
        testObject.rectangularSensor.lateralSurfaceMaterial.getValue(time)
      );
      expect(p.showLateralSurfaces).toEqual(
        testObject.rectangularSensor.showLateralSurfaces.getValue(time)
      );
      expect(p.ellipsoidHorizonSurfaceMaterial.uniforms).toEqual(
        testObject.rectangularSensor.ellipsoidHorizonSurfaceMaterial.getValue(
          time
        )
      );
      expect(p.showEllipsoidHorizonSurfaces).toEqual(
        testObject.rectangularSensor.showEllipsoidHorizonSurfaces.getValue(time)
      );
      expect(p.domeSurfaceMaterial.uniforms).toEqual(
        testObject.rectangularSensor.domeSurfaceMaterial.getValue(time)
      );
      expect(p.showDomeSurfaces).toEqual(
        testObject.rectangularSensor.showDomeSurfaces.getValue(time)
      );
      expect(p.ellipsoidSurfaceMaterial.uniforms).toEqual(
        testObject.rectangularSensor.ellipsoidSurfaceMaterial.getValue(time)
      );
      expect(p.showEllipsoidSurfaces).toEqual(
        testObject.rectangularSensor.showEllipsoidSurfaces.getValue(time)
      );
      expect(p.portionToDisplay).toEqual(
        SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON
      );
      expect(p.environmentConstraint).toEqual(
        rectangularSensor.environmentConstraint.getValue(time)
      );
      expect(p.showEnvironmentOcclusion).toEqual(
        rectangularSensor.showEnvironmentOcclusion.getValue(time)
      );
      expect(p.environmentOcclusionMaterial.uniforms).toEqual(
        rectangularSensor.environmentOcclusionMaterial.getValue(time)
      );
      expect(p.showEnvironmentIntersection).toEqual(
        rectangularSensor.showEnvironmentIntersection.getValue(time)
      );
      expect(p.environmentIntersectionColor).toEqual(
        rectangularSensor.environmentIntersectionColor.getValue(time)
      );
      expect(p.environmentIntersectionWidth).toEqual(
        rectangularSensor.environmentIntersectionWidth.getValue(time)
      );

      rectangularSensor.show.setValue(false);
      visualizer.update(time);
      expect(p.show).toEqual(false);
    });

    it("clear removes rectangularSensors.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var rectangularSensor = (testObject.rectangularSensor = new RectangularSensorGraphics());
      rectangularSensor.xHalfAngle = new ConstantProperty(0.1);
      rectangularSensor.yHalfAngle = new ConstantProperty(0.2);

      var time = JulianDate.now();
      expect(scene.primitives.length).toEqual(0);
      visualizer.update(time);
      expect(scene.primitives.length).toEqual(1);
      expect(scene.primitives.get(0).show).toEqual(true);
      entityCollection.removeAll();
      visualizer.update(time);
      expect(scene.primitives.length).toEqual(0);
    });

    it("Visualizer sets entity property.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var rectangularSensor = (testObject.rectangularSensor = new RectangularSensorGraphics());
      rectangularSensor.xHalfAngle = new ConstantProperty(0.1);
      rectangularSensor.yHalfAngle = new ConstantProperty(0.2);

      var time = JulianDate.now();
      visualizer.update(time);
      expect(scene.primitives.get(0).id).toEqual(testObject);
    });

    it("Computes bounding sphere.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);

      var time = JulianDate.now();
      var testObject = entityCollection.getOrCreateEntity("test");
      var sensor = new RectangularSensorGraphics();
      sensor.radius = 2000.0;
      testObject.rectangularSensor = sensor;

      var center = new Cartesian3(5678, 1234, 1101112);
      testObject.position = new ConstantProperty(center);
      visualizer.update(time);

      var sensorPrimitive = scene.primitives.get(0);
      var result = new BoundingSphere();
      var state = visualizer.getBoundingSphere(testObject, result);
      expect(state).toBe(BoundingSphereState.DONE);

      var expected = new BoundingSphere(center, sensorPrimitive.radius);
      expect(result).toEqual(expected);
    });

    it("Computes bounding sphere with infinite radius", function () {
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);

      var time = JulianDate.now();
      var testObject = entityCollection.getOrCreateEntity("test");
      var sensor = new RectangularSensorGraphics();
      testObject.rectangularSensor = sensor;

      var center = new Cartesian3(5678, 1234, 1101112);
      testObject.position = new ConstantProperty(center);
      visualizer.update(time);

      var sensorPrimitive = scene.primitives.get(0);
      var result = new BoundingSphere();
      var state = visualizer.getBoundingSphere(testObject, result);
      expect(state).toBe(BoundingSphereState.DONE);

      var expected = new BoundingSphere(center, sensorPrimitive.radius);
      expect(result.center).toEqual(expected.center);
      expect(result.radius).toBeGreaterThan(0.0);
      expect(result.radius).toBeLessThan(expected.radius);
    });

    it("Compute bounding sphere throws without entity.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);
      var result = new BoundingSphere();
      expect(function () {
        visualizer.getBoundingSphere(undefined, result);
      }).toThrowDeveloperError();
    });

    it("Compute bounding sphere throws without result.", function () {
      var entityCollection = new EntityCollection();
      var testObject = entityCollection.getOrCreateEntity("test");
      visualizer = new RectangularSensorVisualizer(scene, entityCollection);
      expect(function () {
        visualizer.getBoundingSphere(testObject, undefined);
      }).toThrowDeveloperError();
    });
  },
  "WebGL"
);
