import { FanGraphics } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { ColorMaterialProperty } from "../../Source/Cesium.js";
import { ConstantProperty } from "../../Source/Cesium.js";
import { ShadowMode } from "../../Source/Cesium.js";

describe("DataSources/FanGraphics", function () {
  it("creates expected instance from raw assignment and construction", function () {
    var options = {
      show: true,
      radius: 1,
      perDirectionRadius: false,
      directions: [],
      material: Color.BLUE,
      fill: false,
      outline: false,
      outlineColor: Color.RED,
      outlineWidth: 2,
      numberOfRings: 3,
      shadows: ShadowMode.DISABLED,
    };

    var fan = new FanGraphics(options);
    expect(fan.show).toBeInstanceOf(ConstantProperty);
    expect(fan.radius).toBeInstanceOf(ConstantProperty);
    expect(fan.perDirectionRadius).toBeInstanceOf(ConstantProperty);
    expect(fan.directions).toBeInstanceOf(ConstantProperty);
    expect(fan.material).toBeInstanceOf(ColorMaterialProperty);
    expect(fan.fill).toBeInstanceOf(ConstantProperty);
    expect(fan.outline).toBeInstanceOf(ConstantProperty);
    expect(fan.outlineColor).toBeInstanceOf(ConstantProperty);
    expect(fan.outlineWidth).toBeInstanceOf(ConstantProperty);
    expect(fan.numberOfRings).toBeInstanceOf(ConstantProperty);
    expect(fan.shadows).toBeInstanceOf(ConstantProperty);

    expect(fan.show.getValue()).toEqual(options.show);
    expect(fan.radius.getValue()).toEqual(options.radius);
    expect(fan.perDirectionRadius.getValue()).toEqual(
      options.perDirectionRadius
    );
    expect(fan.directions.getValue()).toEqual(options.directions);
    expect(fan.material.color.getValue()).toEqual(options.material);
    expect(fan.fill.getValue()).toEqual(options.fill);
    expect(fan.outline.getValue()).toEqual(options.outline);
    expect(fan.outlineColor.getValue()).toEqual(options.outlineColor);
    expect(fan.outlineWidth.getValue()).toEqual(options.outlineWidth);
    expect(fan.shadows.getValue()).toEqual(options.shadows);
  });

  it("merge assigns unassigned properties", function () {
    var source = new FanGraphics();
    source.material = new ColorMaterialProperty();
    source.show = new ConstantProperty();
    source.radius = new ConstantProperty();
    source.perDirectionRadius = new ConstantProperty();
    source.directions = new ConstantProperty();
    source.fill = new ConstantProperty();
    source.outline = new ConstantProperty();
    source.outlineColor = new ConstantProperty();
    source.outlineWidth = new ConstantProperty();
    source.numberOfRings = new ConstantProperty();
    source.shadows = new ConstantProperty(ShadowMode.ENABLED);

    var target = new FanGraphics();
    target.merge(source);

    expect(target.material).toBe(source.material);
    expect(target.show).toBe(source.show);
    expect(target.radius).toBe(source.radius);
    expect(target.perDirectionRadius).toBe(source.perDirectionRadius);
    expect(target.directions).toBe(source.directions);
    expect(target.fill).toBe(source.fill);
    expect(target.outline).toBe(source.outline);
    expect(target.outlineColor).toBe(source.outlineColor);
    expect(target.outlineWidth).toBe(source.outlineWidth);
    expect(target.numberOfRings).toBe(source.numberOfRings);
    expect(target.shadows).toBe(source.shadows);
  });

  it("merge does not assign assigned properties", function () {
    var source = new FanGraphics();
    source.material = new ColorMaterialProperty();
    source.show = new ConstantProperty();

    var material = new ColorMaterialProperty();
    var show = new ConstantProperty();
    var radius = new ConstantProperty();
    var perDirectionRadius = new ConstantProperty();
    var directions = new ConstantProperty();
    var fill = new ConstantProperty();
    var outline = new ConstantProperty();
    var outlineColor = new ConstantProperty();
    var outlineWidth = new ConstantProperty();
    var numberOfRings = new ConstantProperty();
    var shadows = new ConstantProperty();

    var target = new FanGraphics();
    target.material = material;
    target.show = show;
    target.radius = radius;
    target.perDirectionRadius = perDirectionRadius;
    target.directions = directions;
    target.fill = fill;
    target.outline = outline;
    target.outlineColor = outlineColor;
    target.outlineWidth = outlineWidth;
    target.numberOfRings = numberOfRings;
    target.shadows = shadows;

    target.merge(source);

    expect(target.material).toBe(material);
    expect(target.show).toBe(show);
    expect(target.radius).toBe(radius);
    expect(target.perDirectionRadius).toBe(perDirectionRadius);
    expect(target.directions).toBe(directions);
    expect(target.fill).toBe(fill);
    expect(target.outline).toBe(outline);
    expect(target.outlineColor).toBe(outlineColor);
    expect(target.outlineWidth).toBe(outlineWidth);
    expect(target.numberOfRings).toBe(numberOfRings);
    expect(target.shadows).toBe(shadows);
  });

  it("clone works", function () {
    var source = new FanGraphics();
    source.material = new ColorMaterialProperty();
    source.show = new ConstantProperty();
    source.radius = new ConstantProperty();
    source.perDirectionRadius = new ConstantProperty();
    source.directions = new ConstantProperty();
    source.fill = new ConstantProperty();
    source.outline = new ConstantProperty();
    source.outlineColor = new ConstantProperty();
    source.outlineWidth = new ConstantProperty();
    source.numberOfRings = new ConstantProperty();
    source.shadows = new ConstantProperty();

    var result = source.clone();
    expect(result.material).toBe(source.material);
    expect(result.show).toBe(source.show);
    expect(result.radius).toBe(source.radius);
    expect(result.perDirectionRadius).toBe(source.perDirectionRadius);
    expect(result.directions).toBe(source.directions);
    expect(result.fill).toBe(source.fill);
    expect(result.outline).toBe(source.outline);
    expect(result.outlineColor).toBe(source.outlineColor);
    expect(result.outlineWidth).toBe(source.outlineWidth);
    expect(result.numberOfRings).toBe(source.numberOfRings);
    expect(result.shadows).toBe(source.shadows);
  });

  it("merge throws if source undefined", function () {
    var target = new FanGraphics();
    expect(function () {
      target.merge(undefined);
    }).toThrowDeveloperError();
  });
});
