import { VectorGraphics } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { ConstantProperty } from "../../Source/Cesium.js";

describe("DataSources/VectorGraphics", function () {
  it("creates expected instance from raw assignment and construction", function () {
    var options = {
      color: Color.RED,
      length: 1,
      minimumLengthInPixels: 2,
      direction: new Cartesian3(),
      show: true,
    };

    var vector = new VectorGraphics(options);
    expect(vector.color).toBeInstanceOf(ConstantProperty);
    expect(vector.length).toBeInstanceOf(ConstantProperty);
    expect(vector.minimumLengthInPixels).toBeInstanceOf(ConstantProperty);
    expect(vector.direction).toBeInstanceOf(ConstantProperty);
    expect(vector.show).toBeInstanceOf(ConstantProperty);

    expect(vector.color.getValue()).toEqual(options.color);
    expect(vector.length.getValue()).toEqual(options.length);
    expect(vector.minimumLengthInPixels.getValue()).toEqual(
      options.minimumLengthInPixels
    );
    expect(vector.direction.getValue()).toEqual(options.direction);
    expect(vector.show.getValue()).toEqual(options.show);
  });

  it("merge assigns unassigned properties", function () {
    var source = new VectorGraphics();
    source.color = new ConstantProperty(Color.WHITE);
    source.length = new ConstantProperty(2);
    source.minimumLengthInPixels = new ConstantProperty(3);
    source.direction = new ConstantProperty(new Cartesian3(1, 0, 0));
    source.show = new ConstantProperty(true);

    var target = new VectorGraphics();
    target.merge(source);
    expect(target.color).toBe(source.color);
    expect(target.length).toBe(source.length);
    expect(target.minimumLengthInPixels).toBe(source.minimumLengthInPixels);
    expect(target.direction).toBe(source.direction);
    expect(target.show).toBe(source.show);
  });

  it("merge does not assign assigned properties", function () {
    var source = new VectorGraphics();
    source.color = new ConstantProperty(Color.WHITE);
    source.length = new ConstantProperty(2);
    source.minimumLengthInPixels = new ConstantProperty(3);
    source.direction = new ConstantProperty(new Cartesian3(1, 0, 0));
    source.show = new ConstantProperty(true);

    var color = new ConstantProperty(Color.WHITE);
    var length = new ConstantProperty(2);
    var minimumLengthInPixels = new ConstantProperty(3);
    var direction = new ConstantProperty(new Cartesian3(1, 0, 0));
    var show = new ConstantProperty(true);

    var target = new VectorGraphics();
    target.color = color;
    target.length = length;
    target.minimumLengthInPixels = minimumLengthInPixels;
    target.direction = direction;
    target.show = show;

    target.merge(source);
    expect(target.color).toBe(color);
    expect(target.length).toBe(length);
    expect(target.minimumLengthInPixels).toBe(minimumLengthInPixels);
    expect(target.direction).toBe(direction);
    expect(target.show).toBe(show);
  });

  it("clone works", function () {
    var source = new VectorGraphics();
    source.color = new ConstantProperty(Color.WHITE);
    source.length = new ConstantProperty(2);
    source.minimumLengthInPixels = new ConstantProperty(3);
    source.direction = new ConstantProperty(new Cartesian3(1, 0, 0));
    source.show = new ConstantProperty(true);

    var result = source.clone();
    expect(result.color).toBe(source.color);
    expect(result.length).toBe(source.length);
    expect(result.minimumLengthInPixels).toBe(source.minimumLengthInPixels);
    expect(result.direction).toBe(source.direction);
    expect(result.show).toBe(source.show);
  });

  it("merge throws if source undefined", function () {
    var target = new VectorGraphics();
    expect(function () {
      target.merge(undefined);
    }).toThrowDeveloperError();
  });
});
