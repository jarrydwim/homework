import { ConicSensorVisualizer } from "../../Source/Cesium.js";
import { BoundingSphere } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { JulianDate } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";
import { Matrix3 } from "../../Source/Cesium.js";
import { Matrix4 } from "../../Source/Cesium.js";
import { Quaternion } from "../../Source/Cesium.js";
import { BoundingSphereState } from "../../Source/Cesium.js";
import { ColorMaterialProperty } from "../../Source/Cesium.js";
import { ConicSensorGraphics } from "../../Source/Cesium.js";
import { ConstantProperty } from "../../Source/Cesium.js";
import { EntityCollection } from "../../Source/Cesium.js";
import { SensorVolumePortionToDisplay } from "../../Source/Cesium.js";
import createScene from "../createScene.js";

describe(
  "DataSources/ConicSensorVisualizer",
  function () {
    var scene;
    var visualizer;

    beforeAll(function () {
      scene = createScene();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    afterEach(function () {
      visualizer = visualizer && visualizer.destroy();
    });

    it("constructor throws if no scene is passed.", function () {
      expect(function () {
        return new ConicSensorVisualizer();
      }).toThrowDeveloperError();
    });

    it("update throws if no time specified.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);
      expect(function () {
        visualizer.update();
      }).toThrowDeveloperError();
    });

    it("isDestroy returns false until destroyed.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);
      expect(visualizer.isDestroyed()).toEqual(false);
      visualizer.destroy();
      expect(visualizer.isDestroyed()).toEqual(true);
      visualizer = undefined;
    });

    it("object with no conicSensor does not create a primitive.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      visualizer.update(JulianDate.now());
      expect(scene.primitives.length).toEqual(0);
    });

    it("object with no position does not create a primitive.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var conicSensor = (testObject.conicSensor = new ConicSensorGraphics());
      conicSensor.maximumClockAngle = new ConstantProperty(1);
      conicSensor.outerHalfAngle = new ConstantProperty(1);
      visualizer.update(JulianDate.now());
      expect(scene.primitives.length).toEqual(0);
    });

    it("A ConicSensorGraphics causes a ConicSensor to be created and updated.", function () {
      var time = JulianDate.now();
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);
      visualizer._hasFragmentDepth = true; //Force ConicSensor path for testing

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(
        new Quaternion(
          0,
          0,
          Math.sin(CesiumMath.PI_OVER_FOUR),
          Math.cos(CesiumMath.PI_OVER_FOUR)
        )
      );

      var conicSensor = (testObject.conicSensor = new ConicSensorGraphics());
      conicSensor.minimumClockAngle = new ConstantProperty(0.1);
      conicSensor.maximumClockAngle = new ConstantProperty(0.2);
      conicSensor.innerHalfAngle = new ConstantProperty(0.3);
      conicSensor.outerHalfAngle = new ConstantProperty(0.4);
      conicSensor.intersectionColor = new ConstantProperty(
        new Color(0.1, 0.2, 0.3, 0.4)
      );
      conicSensor.intersectionWidth = new ConstantProperty(0.5);
      conicSensor.showIntersection = new ConstantProperty(true);
      conicSensor.showThroughEllipsoid = new ConstantProperty(true);
      conicSensor.radius = new ConstantProperty(123.5);
      conicSensor.show = new ConstantProperty(true);
      conicSensor.lateralSurfaceMaterial = new ColorMaterialProperty(
        Color.WHITE
      );
      conicSensor.showLateralSurfaces = new ConstantProperty(true);
      conicSensor.ellipsoidHorizonSurfaceMaterial = new ColorMaterialProperty(
        Color.RED
      );
      conicSensor.showEllipsoidHorizonSurfaces = new ConstantProperty(true);
      conicSensor.domeSurfaceMaterial = new ColorMaterialProperty(Color.BLUE);
      conicSensor.showDomeSurfaces = new ConstantProperty(true);
      conicSensor.ellipsoidSurfaceMaterial = new ColorMaterialProperty(
        Color.BLUE
      );
      conicSensor.showEllipsoidSurfaces = new ConstantProperty(true);
      conicSensor.portionToDisplay = new ConstantProperty(
        SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON
      );
      conicSensor.environmentConstraint = new ConstantProperty(true);
      conicSensor.showEnvironmentOcclusion = new ConstantProperty(true);
      conicSensor.environmentOcclusionMaterial = new ColorMaterialProperty();
      conicSensor.showEnvironmentIntersection = new ConstantProperty(true);
      conicSensor.environmentIntersectionColor = new ConstantProperty(
        Color.WHITE
      );
      conicSensor.environmentIntersectionWidth = new ConstantProperty(1);

      visualizer.update(time);
      expect(scene.primitives.length).toEqual(1);

      var c = scene.primitives.get(0);
      expect(c.minimumClockAngle).toEqual(
        testObject.conicSensor.minimumClockAngle.getValue(time)
      );
      expect(c.maximumClockAngle).toEqual(
        testObject.conicSensor.maximumClockAngle.getValue(time)
      );
      expect(c.innerHalfAngle).toEqual(
        testObject.conicSensor.innerHalfAngle.getValue(time)
      );
      expect(c.outerHalfAngle).toEqual(
        testObject.conicSensor.outerHalfAngle.getValue(time)
      );
      expect(c.intersectionColor).toEqual(
        testObject.conicSensor.intersectionColor.getValue(time)
      );
      expect(c.intersectionWidth).toEqual(
        testObject.conicSensor.intersectionWidth.getValue(time)
      );
      expect(c.showIntersection).toEqual(
        testObject.conicSensor.showIntersection.getValue(time)
      );
      expect(c.showThroughEllipsoid).toEqual(
        testObject.conicSensor.showThroughEllipsoid.getValue(time)
      );
      expect(c.radius).toEqual(testObject.conicSensor.radius.getValue(time));
      expect(c.modelMatrix).toEqual(
        Matrix4.fromRotationTranslation(
          Matrix3.fromQuaternion(testObject.orientation.getValue(time)),
          testObject.position.getValue(time)
        )
      );
      expect(c.show).toEqual(testObject.conicSensor.show.getValue(time));
      expect(c.lateralSurfaceMaterial.uniforms).toEqual(
        testObject.conicSensor.lateralSurfaceMaterial.getValue(time)
      );
      expect(c.showLateralSurfaces).toEqual(
        testObject.conicSensor.showLateralSurfaces.getValue(time)
      );
      expect(c.ellipsoidHorizonSurfaceMaterial.uniforms).toEqual(
        conicSensor.ellipsoidHorizonSurfaceMaterial.getValue(time)
      );
      expect(c.showEllipsoidHorizonSurfaces).toEqual(
        testObject.conicSensor.showEllipsoidHorizonSurfaces.getValue(time)
      );
      expect(c.domeSurfaceMaterial.uniforms).toEqual(
        conicSensor.domeSurfaceMaterial.getValue(time)
      );
      expect(c.showDomeSurfaces).toEqual(
        testObject.conicSensor.showDomeSurfaces.getValue(time)
      );
      expect(c.ellipsoidSurfaceMaterial.uniforms).toEqual(
        conicSensor.ellipsoidSurfaceMaterial.getValue(time)
      );
      expect(c.showEllipsoidSurfaces).toEqual(
        testObject.conicSensor.showEllipsoidSurfaces.getValue(time)
      );
      expect(c.portionToDisplay).toEqual(
        SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON
      );
      expect(c.environmentConstraint).toEqual(
        conicSensor.environmentConstraint.getValue(time)
      );
      expect(c.showEnvironmentOcclusion).toEqual(
        conicSensor.showEnvironmentOcclusion.getValue(time)
      );
      expect(c.environmentOcclusionMaterial.uniforms).toEqual(
        conicSensor.environmentOcclusionMaterial.getValue(time)
      );
      expect(c.showEnvironmentIntersection).toEqual(
        conicSensor.showEnvironmentIntersection.getValue(time)
      );
      expect(c.environmentIntersectionColor).toEqual(
        conicSensor.environmentIntersectionColor.getValue(time)
      );
      expect(c.environmentIntersectionWidth).toEqual(
        conicSensor.environmentIntersectionWidth.getValue(time)
      );

      conicSensor.show.setValue(false);
      visualizer.update(time);
      expect(c.show).toEqual(false);
    });

    it("IntersectionColor is set correctly with multiple conicSensors.", function () {
      var time = JulianDate.now();
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));

      var testObject2 = entityCollection.getOrCreateEntity("test2");
      testObject2.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject2.orientation = new ConstantProperty(
        new Quaternion(0, 0, 0, 1)
      );

      var conicSensor = (testObject.conicSensor = new ConicSensorGraphics());
      conicSensor.intersectionColor = new ConstantProperty(
        new Color(0.1, 0.2, 0.3, 0.4)
      );

      var conicSensor2 = (testObject2.conicSensor = new ConicSensorGraphics());
      conicSensor2.intersectionColor = new ConstantProperty(
        new Color(0.4, 0.3, 0.2, 0.1)
      );

      visualizer.update(time);

      expect(scene.primitives.length).toEqual(2);
      var c = scene.primitives.get(0);
      expect(c.intersectionColor).toEqual(
        testObject.conicSensor.intersectionColor.getValue(time)
      );

      c = scene.primitives.get(1);
      expect(c.intersectionColor).toEqual(
        testObject2.conicSensor.intersectionColor.getValue(time)
      );
    });

    it("An empty ConicSensorGraphics causes a ConicSensor to be created with expected defaults.", function () {
      var time = JulianDate.now();
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);
      visualizer._hasFragmentDepth = true; //Force ConicSensor path for testing

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));

      testObject.conicSensor = new ConicSensorGraphics();
      visualizer.update(time);

      expect(scene.primitives.length).toEqual(1);
      var c = scene.primitives.get(0);
      expect(c.minimumClockAngle).toEqual(0.0);
      expect(c.maximumClockAngle).toEqual(CesiumMath.TWO_PI);
      expect(c.innerHalfAngle).toEqual(0);
      expect(c.outerHalfAngle).toEqual(Math.PI);
      expect(isFinite(c.radius)).toEqual(false);
      expect(c.show).toEqual(true);
    });

    it("clear removed primitives.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var conicSensor = (testObject.conicSensor = new ConicSensorGraphics());
      conicSensor.maximumClockAngle = new ConstantProperty(1);
      conicSensor.outerHalfAngle = new ConstantProperty(1);

      var time = JulianDate.now();
      expect(scene.primitives.length).toEqual(0);
      visualizer.update(time);
      expect(scene.primitives.length).toEqual(1);
      expect(scene.primitives.get(0).show).toEqual(true);
      entityCollection.removeAll();
      visualizer.update(time);
      expect(scene.primitives.length).toEqual(0);
    });

    it("Visualizer sets id property.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var conicSensor = (testObject.conicSensor = new ConicSensorGraphics());
      conicSensor.maximumClockAngle = new ConstantProperty(1);
      conicSensor.outerHalfAngle = new ConstantProperty(1);

      var time = JulianDate.now();
      visualizer.update(time);
      expect(scene.primitives.get(0).id).toBe(testObject);
    });

    it("Computes bounding sphere.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      var center = new Cartesian3(1234, 5678, 9101112);
      testObject.position = new ConstantProperty(center);
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var conicSensor = (testObject.conicSensor = new ConicSensorGraphics());
      conicSensor.maximumClockAngle = new ConstantProperty(1);
      conicSensor.outerHalfAngle = new ConstantProperty(1);
      conicSensor.radius = 2000.0;

      var time = JulianDate.now();
      visualizer.update(time);

      var sensorPrimitive = scene.primitives.get(0);
      var result = new BoundingSphere();
      var state = visualizer.getBoundingSphere(testObject, result);
      expect(state).toBe(BoundingSphereState.DONE);

      var expected = new BoundingSphere(center, sensorPrimitive.radius);
      expect(result).toEqual(expected);
    });

    it("Computes bounding sphere with infinite radius", function () {
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      var center = new Cartesian3(1234, 5678, 9101112);
      testObject.position = new ConstantProperty(center);
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var conicSensor = (testObject.conicSensor = new ConicSensorGraphics());
      conicSensor.maximumClockAngle = new ConstantProperty(1);
      conicSensor.outerHalfAngle = new ConstantProperty(1);

      var time = JulianDate.now();
      visualizer.update(time);

      var sensorPrimitive = scene.primitives.get(0);
      var result = new BoundingSphere();
      var state = visualizer.getBoundingSphere(testObject, result);
      expect(state).toBe(BoundingSphereState.DONE);

      var expected = new BoundingSphere(center, sensorPrimitive.radius);
      expect(result.center).toEqual(expected.center);
      expect(result.radius).toBeGreaterThan(0.0);
      expect(result.radius).toBeLessThan(expected.radius);
    });

    it("Compute bounding sphere throws without entity.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new ConicSensorVisualizer(scene, entityCollection);
      var result = new BoundingSphere();
      expect(function () {
        visualizer.getBoundingSphere(undefined, result);
      }).toThrowDeveloperError();
    });

    it("Compute bounding sphere throws without result.", function () {
      var entityCollection = new EntityCollection();
      var testObject = entityCollection.getOrCreateEntity("test");
      visualizer = new ConicSensorVisualizer(scene, entityCollection);
      expect(function () {
        visualizer.getBoundingSphere(testObject, undefined);
      }).toThrowDeveloperError();
    });
  },
  "WebGL"
);
