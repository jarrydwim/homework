import { CustomPatternSensorVisualizer } from "../../Source/Cesium.js";
import { BoundingSphere } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { JulianDate } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";
import { Matrix3 } from "../../Source/Cesium.js";
import { Matrix4 } from "../../Source/Cesium.js";
import { Quaternion } from "../../Source/Cesium.js";
import { Spherical } from "../../Source/Cesium.js";
import { BoundingSphereState } from "../../Source/Cesium.js";
import { ColorMaterialProperty } from "../../Source/Cesium.js";
import { ConstantProperty } from "../../Source/Cesium.js";
import { CustomPatternSensorGraphics } from "../../Source/Cesium.js";
import { EntityCollection } from "../../Source/Cesium.js";
import { SensorVolumePortionToDisplay } from "../../Source/Cesium.js";
import createScene from "../createScene.js";

describe(
  "DataSources/CustomPatternSensorVisualizer",
  function () {
    var scene;
    var visualizer;

    beforeAll(function () {
      scene = createScene();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    afterEach(function () {
      visualizer = visualizer && visualizer.destroy();
    });

    it("constructor throws if no scene is passed.", function () {
      expect(function () {
        return new CustomPatternSensorVisualizer();
      }).toThrowDeveloperError();
    });

    it("update throws if no time specified.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);
      expect(function () {
        visualizer.update();
      }).toThrowDeveloperError();
    });

    it("isDestroy returns false until destroyed.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);
      expect(visualizer.isDestroyed()).toEqual(false);
      visualizer.destroy();
      expect(visualizer.isDestroyed()).toEqual(true);
      visualizer = undefined;
    });

    it("object with no customPatternSensor does not create a primitive.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      visualizer.update(JulianDate.now());
      expect(scene.primitives.length).toEqual(0);
    });

    it("object with no position does not create a primitive.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var customPatternSensor = (testObject.customPatternSensor = new CustomPatternSensorGraphics());
      customPatternSensor.directions = new ConstantProperty([
        new Spherical(0, 0, 0),
        new Spherical(1, 0, 0),
        new Spherical(2, 0, 0),
        new Spherical(3, 0, 0),
      ]);
      visualizer.update(JulianDate.now());
      expect(scene.primitives.length).toEqual(0);
    });

    it("object with no orientation does not create a primitive.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      var customPatternSensor = (testObject.customPatternSensor = new CustomPatternSensorGraphics());
      customPatternSensor.directions = new ConstantProperty([
        new Spherical(0, 0, 0),
        new Spherical(1, 0, 0),
        new Spherical(2, 0, 0),
        new Spherical(3, 0, 0),
      ]);
      visualizer.update(JulianDate.now());
      expect(scene.primitives.length).toEqual(0);
    });

    it("A CustomPatternSensorGraphics causes a CustomSensor to be created and updated.", function () {
      var time = JulianDate.now();
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(
        new Quaternion(
          0,
          0,
          Math.sin(CesiumMath.PI_OVER_FOUR),
          Math.cos(CesiumMath.PI_OVER_FOUR)
        )
      );

      var customPatternSensor = (testObject.customPatternSensor = new CustomPatternSensorGraphics());
      customPatternSensor.directions = new ConstantProperty([
        new Spherical(0, 1, 1),
        new Spherical(0, 2, 2),
        new Spherical(0, 3, 1),
        new Spherical(0, 4, 1),
      ]);
      customPatternSensor.intersectionColor = new ConstantProperty(
        new Color(0.1, 0.2, 0.3, 0.4)
      );
      customPatternSensor.intersectionWidth = new ConstantProperty(0.5);
      customPatternSensor.showIntersection = new ConstantProperty(true);
      customPatternSensor.showThroughEllipsoid = new ConstantProperty(true);
      customPatternSensor.radius = new ConstantProperty(123.5);
      customPatternSensor.show = new ConstantProperty(true);
      customPatternSensor.lateralSurfaceMaterial = new ColorMaterialProperty(
        Color.WHITE
      );
      customPatternSensor.showLateralSurfaces = new ConstantProperty(true);
      customPatternSensor.ellipsoidHorizonSurfaceMaterial = new ColorMaterialProperty(
        Color.RED
      );
      customPatternSensor.showEllipsoidHorizonSurfaces = new ConstantProperty(
        true
      );
      customPatternSensor.domeSurfaceMaterial = new ColorMaterialProperty(
        Color.BLUE
      );
      customPatternSensor.showDomeSurfaces = new ConstantProperty(true);
      customPatternSensor.ellipsoidSurfaceMaterial = new ColorMaterialProperty(
        Color.BLUE
      );
      customPatternSensor.showEllipsoidSurfaces = new ConstantProperty(true);
      customPatternSensor.portionToDisplay = new ConstantProperty(
        SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON
      );
      customPatternSensor.environmentConstraint = new ConstantProperty(true);
      customPatternSensor.showEnvironmentOcclusion = new ConstantProperty(true);
      customPatternSensor.environmentOcclusionMaterial = new ColorMaterialProperty();
      customPatternSensor.showEnvironmentIntersection = new ConstantProperty(
        true
      );
      customPatternSensor.environmentIntersectionColor = new ConstantProperty(
        Color.WHITE
      );
      customPatternSensor.environmentIntersectionWidth = new ConstantProperty(
        1
      );
      visualizer.update(time);

      expect(scene.primitives.length).toEqual(1);
      var p = scene.primitives.get(0);
      expect(p.intersectionColor).toEqual(
        testObject.customPatternSensor.intersectionColor.getValue(time)
      );
      expect(p.intersectionWidth).toEqual(
        testObject.customPatternSensor.intersectionWidth.getValue(time)
      );
      expect(p.showIntersection).toEqual(
        testObject.customPatternSensor.showIntersection.getValue(time)
      );
      expect(p.showThroughEllipsoid).toEqual(
        testObject.customPatternSensor.showThroughEllipsoid.getValue(time)
      );
      expect(p.radius).toEqual(
        testObject.customPatternSensor.radius.getValue(time)
      );
      expect(p.modelMatrix).toEqual(
        Matrix4.fromRotationTranslation(
          Matrix3.fromQuaternion(testObject.orientation.getValue(time)),
          testObject.position.getValue(time)
        )
      );
      expect(p.show).toEqual(
        testObject.customPatternSensor.show.getValue(time)
      );
      expect(p.lateralSurfaceMaterial.uniforms).toEqual(
        testObject.customPatternSensor.lateralSurfaceMaterial.getValue(time)
      );
      expect(p.showLateralSurfaces).toEqual(
        testObject.customPatternSensor.showLateralSurfaces.getValue(time)
      );
      expect(p.ellipsoidHorizonSurfaceMaterial.uniforms).toEqual(
        testObject.customPatternSensor.ellipsoidHorizonSurfaceMaterial.getValue(
          time
        )
      );
      expect(p.showEllipsoidHorizonSurfaces).toEqual(
        testObject.customPatternSensor.showEllipsoidHorizonSurfaces.getValue(
          time
        )
      );
      expect(p.domeSurfaceMaterial.uniforms).toEqual(
        testObject.customPatternSensor.domeSurfaceMaterial.getValue(time)
      );
      expect(p.showDomeSurfaces).toEqual(
        testObject.customPatternSensor.showDomeSurfaces.getValue(time)
      );
      expect(p.ellipsoidSurfaceMaterial.uniforms).toEqual(
        testObject.customPatternSensor.ellipsoidSurfaceMaterial.getValue(time)
      );
      expect(p.showEllipsoidSurfaces).toEqual(
        testObject.customPatternSensor.showEllipsoidSurfaces.getValue(time)
      );
      expect(p.portionToDisplay).toEqual(
        SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON
      );
      expect(p.environmentConstraint).toEqual(
        customPatternSensor.environmentConstraint.getValue(time)
      );
      expect(p.showEnvironmentOcclusion).toEqual(
        customPatternSensor.showEnvironmentOcclusion.getValue(time)
      );
      expect(p.environmentOcclusionMaterial.uniforms).toEqual(
        customPatternSensor.environmentOcclusionMaterial.getValue(time)
      );
      expect(p.showEnvironmentIntersection).toEqual(
        customPatternSensor.showEnvironmentIntersection.getValue(time)
      );
      expect(p.environmentIntersectionColor).toEqual(
        customPatternSensor.environmentIntersectionColor.getValue(time)
      );
      expect(p.environmentIntersectionWidth).toEqual(
        customPatternSensor.environmentIntersectionWidth.getValue(time)
      );

      customPatternSensor.show.setValue(false);
      visualizer.update(time);
      expect(p.show).toEqual(false);
    });

    it("clear removes customPatternSensors.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var customPatternSensor = (testObject.customPatternSensor = new CustomPatternSensorGraphics());
      customPatternSensor.directions = new ConstantProperty([
        new Spherical(0, 1, 1),
        new Spherical(0, 2, 2),
        new Spherical(0, 3, 3),
        new Spherical(0, 4, 4),
      ]);

      var time = JulianDate.now();
      expect(scene.primitives.length).toEqual(0);
      visualizer.update(time);
      expect(scene.primitives.length).toEqual(1);
      expect(scene.primitives.get(0).show).toEqual(true);
      entityCollection.removeAll();
      visualizer.update(time);
      expect(scene.primitives.length).toEqual(0);
    });

    it("Visualizer sets entity property.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var customPatternSensor = (testObject.customPatternSensor = new CustomPatternSensorGraphics());
      customPatternSensor.directions = new ConstantProperty([
        new Spherical(0, 1, 1),
        new Spherical(0, 2, 2),
        new Spherical(0, 3, 3),
        new Spherical(0, 4, 4),
      ]);

      var time = JulianDate.now();
      visualizer.update(time);
      expect(scene.primitives.get(0).id).toEqual(testObject);
    });

    it("Computes bounding sphere.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);

      var time = JulianDate.now();
      var testObject = entityCollection.getOrCreateEntity("test");
      var center = new Cartesian3(5678, 1234, 1101112);
      testObject.position = new ConstantProperty(center);
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var customPatternSensor = (testObject.customPatternSensor = new CustomPatternSensorGraphics());
      customPatternSensor.radius = 2000.0;
      customPatternSensor.directions = new ConstantProperty([
        new Spherical(0, 1, 1),
        new Spherical(0, 2, 2),
        new Spherical(0, 3, 3),
        new Spherical(0, 4, 4),
      ]);

      visualizer.update(time);

      var sensorPrimitive = scene.primitives.get(0);
      var result = new BoundingSphere();
      var state = visualizer.getBoundingSphere(testObject, result);
      expect(state).toBe(BoundingSphereState.DONE);

      var expected = new BoundingSphere(center, sensorPrimitive.radius);
      expect(result).toEqual(expected);
    });

    it("Computes bounding sphere with infinite radius", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);

      var time = JulianDate.now();
      var testObject = entityCollection.getOrCreateEntity("test");
      var center = new Cartesian3(5678, 1234, 1101112);
      testObject.position = new ConstantProperty(center);
      testObject.orientation = new ConstantProperty(new Quaternion(0, 0, 0, 1));
      var customPatternSensor = (testObject.customPatternSensor = new CustomPatternSensorGraphics());
      customPatternSensor.directions = new ConstantProperty([
        new Spherical(0, 1, 1),
        new Spherical(0, 2, 2),
        new Spherical(0, 3, 3),
        new Spherical(0, 4, 4),
      ]);

      visualizer.update(time);

      var sensorPrimitive = scene.primitives.get(0);
      var result = new BoundingSphere();
      var state = visualizer.getBoundingSphere(testObject, result);
      expect(state).toBe(BoundingSphereState.DONE);

      var expected = new BoundingSphere(center, sensorPrimitive.radius);
      expect(result.center).toEqual(expected.center);
      expect(result.radius).toBeGreaterThan(0.0);
      expect(result.radius).toBeLessThan(expected.radius);
    });

    it("Compute bounding sphere throws without entity.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);
      var result = new BoundingSphere();
      expect(function () {
        visualizer.getBoundingSphere(undefined, result);
      }).toThrowDeveloperError();
    });

    it("Compute bounding sphere throws without result.", function () {
      var entityCollection = new EntityCollection();
      var testObject = entityCollection.getOrCreateEntity("test");
      visualizer = new CustomPatternSensorVisualizer(scene, entityCollection);
      expect(function () {
        visualizer.getBoundingSphere(testObject, undefined);
      }).toThrowDeveloperError();
    });
  },
  "WebGL"
);
