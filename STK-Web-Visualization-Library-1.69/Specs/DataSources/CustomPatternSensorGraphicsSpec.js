import { CustomPatternSensorGraphics } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { ColorMaterialProperty } from "../../Source/Cesium.js";
import { ConstantProperty } from "../../Source/Cesium.js";
import { SensorVolumePortionToDisplay } from "../../Source/Cesium.js";

describe("DataSources/CustomPatternSensorGraphics", function () {
  it("creates expected instance from raw assignment and construction", function () {
    var options = {
      lateralSurfaceMaterial: Color.WHITE,
      showLateralSurfaces: true,
      ellipsoidHorizonSurfaceMaterial: Color.BLACK,
      showEllipsoidHorizonSurfaces: false,
      domeSurfaceMaterial: Color.BLUE,
      showDomeSurfaces: true,
      ellipsoidSurfaceMaterial: Color.RED,
      showEllipsoidSurfaces: false,
      portionToDisplay: SensorVolumePortionToDisplay.COMPLETE,
      directions: [],
      intersectionColor: Color.ORANGE,
      radius: 5,
      show: true,
      showIntersection: false,
      intersectionWidth: 6,
      showThroughEllipsoid: true,
      environmentConstraint: true,
      showEnvironmentOcclusion: true,
      environmentOcclusionMaterial: Color.YELLOW,
      showEnvironmentIntersection: true,
      environmentIntersectionColor: Color.GREEN,
      environmentIntersectionWidth: 50.0,
    };

    var sensor = new CustomPatternSensorGraphics(options);
    expect(sensor.lateralSurfaceMaterial).toBeInstanceOf(ColorMaterialProperty);
    expect(sensor.showLateralSurfaces).toBeInstanceOf(ConstantProperty);
    expect(sensor.ellipsoidHorizonSurfaceMaterial).toBeInstanceOf(
      ColorMaterialProperty
    );
    expect(sensor.showEllipsoidHorizonSurfaces).toBeInstanceOf(
      ConstantProperty
    );
    expect(sensor.domeSurfaceMaterial).toBeInstanceOf(ColorMaterialProperty);
    expect(sensor.showDomeSurfaces).toBeInstanceOf(ConstantProperty);
    expect(sensor.ellipsoidSurfaceMaterial).toBeInstanceOf(
      ColorMaterialProperty
    );
    expect(sensor.showEllipsoidSurfaces).toBeInstanceOf(ConstantProperty);
    expect(sensor.portionToDisplay).toBeInstanceOf(ConstantProperty);
    expect(sensor.directions).toBeInstanceOf(ConstantProperty);
    expect(sensor.intersectionColor).toBeInstanceOf(ConstantProperty);
    expect(sensor.radius).toBeInstanceOf(ConstantProperty);
    expect(sensor.show).toBeInstanceOf(ConstantProperty);
    expect(sensor.showIntersection).toBeInstanceOf(ConstantProperty);
    expect(sensor.intersectionWidth).toBeInstanceOf(ConstantProperty);
    expect(sensor.showThroughEllipsoid).toBeInstanceOf(ConstantProperty);
    expect(sensor.environmentConstraint).toBeInstanceOf(ConstantProperty);
    expect(sensor.showEnvironmentOcclusion).toBeInstanceOf(ConstantProperty);
    expect(sensor.environmentOcclusionMaterial).toBeInstanceOf(
      ColorMaterialProperty
    );
    expect(sensor.showEnvironmentIntersection).toBeInstanceOf(ConstantProperty);
    expect(sensor.environmentIntersectionColor).toBeInstanceOf(
      ConstantProperty
    );
    expect(sensor.environmentIntersectionWidth).toBeInstanceOf(
      ConstantProperty
    );

    expect(sensor.lateralSurfaceMaterial.color.getValue()).toEqual(
      options.lateralSurfaceMaterial
    );
    expect(sensor.showLateralSurfaces.getValue()).toEqual(
      options.showLateralSurfaces
    );
    expect(sensor.ellipsoidHorizonSurfaceMaterial.color.getValue()).toEqual(
      options.ellipsoidHorizonSurfaceMaterial
    );
    expect(sensor.showEllipsoidHorizonSurfaces.getValue()).toEqual(
      options.showEllipsoidHorizonSurfaces
    );
    expect(sensor.domeSurfaceMaterial.color.getValue()).toEqual(
      options.domeSurfaceMaterial
    );
    expect(sensor.showDomeSurfaces.getValue()).toEqual(
      options.showDomeSurfaces
    );
    expect(sensor.ellipsoidSurfaceMaterial.color.getValue()).toEqual(
      options.ellipsoidSurfaceMaterial
    );
    expect(sensor.showEllipsoidSurfaces.getValue()).toEqual(
      options.showEllipsoidSurfaces
    );
    expect(sensor.portionToDisplay.getValue()).toEqual(
      options.portionToDisplay
    );
    expect(sensor.directions.getValue()).toEqual(options.directions);
    expect(sensor.intersectionColor.getValue()).toEqual(
      options.intersectionColor
    );
    expect(sensor.radius.getValue()).toEqual(options.radius);
    expect(sensor.show.getValue()).toEqual(options.show);
    expect(sensor.showIntersection.getValue()).toEqual(
      options.showIntersection
    );
    expect(sensor.intersectionWidth.getValue()).toEqual(
      options.intersectionWidth
    );
    expect(sensor.showThroughEllipsoid.getValue()).toEqual(
      options.showThroughEllipsoid
    );
    expect(sensor.environmentConstraint.getValue()).toEqual(
      options.environmentConstraint
    );
    expect(sensor.showEnvironmentOcclusion.getValue()).toEqual(
      options.showEnvironmentOcclusion
    );
    expect(sensor.environmentOcclusionMaterial.color.getValue()).toEqual(
      options.environmentOcclusionMaterial
    );
    expect(sensor.showEnvironmentIntersection.getValue()).toEqual(
      options.showEnvironmentIntersection
    );
    expect(sensor.environmentIntersectionColor.getValue()).toEqual(
      options.environmentIntersectionColor
    );
    expect(sensor.environmentIntersectionWidth.getValue()).toEqual(
      options.environmentIntersectionWidth
    );
  });

  it("merge assigns unassigned properties", function () {
    var source = new CustomPatternSensorGraphics();
    source.lateralSurfaceMaterial = new ColorMaterialProperty();
    source.showLateralSurfaces = new ConstantProperty(true);
    source.ellipsoidHorizonSurfaceMaterial = new ColorMaterialProperty();
    source.showEllipsoidHorizonSurfaces = new ConstantProperty(true);
    source.domeSurfaceMaterial = new ColorMaterialProperty();
    source.showDomeSurfaces = new ConstantProperty(true);
    source.ellipsoidSurfaceMaterial = new ColorMaterialProperty();
    source.showEllipsoidSurfaces = new ConstantProperty(true);
    source.directions = new ConstantProperty([]);
    source.intersectionColor = new ConstantProperty(Color.WHITE);
    source.radius = new ConstantProperty(1);
    source.show = new ConstantProperty(true);
    source.showIntersection = new ConstantProperty(true);
    source.intersectionWidth = new ConstantProperty(1);
    source.showThroughEllipsoid = new ConstantProperty(true);
    source.portionToDisplay = new ConstantProperty(
      SensorVolumePortionToDisplay.COMPLETE
    );
    source.environmentConstraint = new ConstantProperty(true);
    source.showEnvironmentOcclusion = new ConstantProperty(true);
    source.environmentOcclusionMaterial = new ColorMaterialProperty();
    source.showEnvironmentIntersection = new ConstantProperty(true);
    source.environmentIntersectionColor = new ConstantProperty(Color.WHITE);
    source.environmentIntersectionWidth = new ConstantProperty(1);

    var target = new CustomPatternSensorGraphics();
    target.merge(source);

    expect(target.lateralSurfaceMaterial).toBe(source.lateralSurfaceMaterial);
    expect(target.showLateralSurfaces).toBe(source.showLateralSurfaces);
    expect(target.ellipsoidHorizonSurfaceMaterial).toBe(
      source.ellipsoidHorizonSurfaceMaterial
    );
    expect(target.showEllipsoidHorizonSurfaces).toBe(
      source.showEllipsoidHorizonSurfaces
    );
    expect(target.domeSurfaceMaterial).toBe(source.domeSurfaceMaterial);
    expect(target.showDomeSurfaces).toBe(source.showDomeSurfaces);
    expect(target.ellipsoidSurfaceMaterial).toBe(
      source.ellipsoidSurfaceMaterial
    );
    expect(target.showEllipsoidSurfaces).toBe(source.showEllipsoidSurfaces);
    expect(target.directions).toBe(source.directions);
    expect(target.intersectionColor).toBe(source.intersectionColor);
    expect(target.radius).toBe(source.radius);
    expect(target.show).toBe(source.show);
    expect(target.showIntersection).toBe(source.showIntersection);
    expect(target.intersectionWidth).toBe(source.intersectionWidth);
    expect(target.showThroughEllipsoid).toBe(source.showThroughEllipsoid);
    expect(target.portionToDisplay).toBe(source.portionToDisplay);
    expect(target.environmentConstraint).toBe(source.environmentConstraint);
    expect(target.showEnvironmentOcclusion).toBe(
      source.showEnvironmentOcclusion
    );
    expect(target.environmentOcclusionMaterial).toBe(
      source.environmentOcclusionMaterial
    );
    expect(target.showEnvironmentIntersection).toBe(
      source.showEnvironmentIntersection
    );
    expect(target.environmentIntersectionColor).toBe(
      source.environmentIntersectionColor
    );
    expect(target.environmentIntersectionWidth).toBe(
      source.environmentIntersectionWidth
    );
  });

  it("merge does not assign assigned properties", function () {
    var source = new CustomPatternSensorGraphics();
    source.lateralSurfaceMaterial = new ColorMaterialProperty();
    source.showLateralSurfaces = new ConstantProperty(true);
    source.ellipsoidHorizonSurfaceMaterial = new ColorMaterialProperty();
    source.showEllipsoidHorizonSurfaces = new ConstantProperty(true);
    source.domeSurfaceMaterial = new ColorMaterialProperty();
    source.showDomeSurfaces = new ConstantProperty(true);
    source.ellipsoidSurfaceMaterial = new ColorMaterialProperty();
    source.showEllipsoidSurfaces = new ConstantProperty(true);
    source.directions = new ConstantProperty([]);
    source.intersectionColor = new ConstantProperty(Color.WHITE);
    source.radius = new ConstantProperty(1);
    source.show = new ConstantProperty(true);
    source.showIntersection = new ConstantProperty(true);
    source.intersectionWidth = new ConstantProperty(1);
    source.showThroughEllipsoid = new ConstantProperty(true);
    source.portionToDisplay = new ConstantProperty(
      SensorVolumePortionToDisplay.COMPLETE
    );
    source.environmentConstraint = new ConstantProperty(true);
    source.showEnvironmentOcclusion = new ConstantProperty(true);
    source.environmentOcclusionMaterial = new ColorMaterialProperty();
    source.showEnvironmentIntersection = new ConstantProperty(true);
    source.environmentIntersectionColor = new ConstantProperty(Color.WHITE);
    source.environmentIntersectionWidth = new ConstantProperty(1);

    var lateralSurfaceMaterial = new ColorMaterialProperty();
    var showLateralSurfaces = new ConstantProperty(true);
    var ellipsoidHorizonSurfaceMaterial = new ColorMaterialProperty();
    var showEllipsoidHorizonSurfaces = new ConstantProperty(true);
    var domeSurfaceMaterial = new ColorMaterialProperty();
    var showDomeSurfaces = new ConstantProperty(true);
    var ellipsoidSurfaceMaterial = new ColorMaterialProperty();
    var showEllipsoidSurfaces = new ConstantProperty(true);
    var directions = new ConstantProperty([]);
    var intersectionColor = new ConstantProperty(Color.WHITE);
    var radius = new ConstantProperty(1);
    var show = new ConstantProperty(true);
    var showIntersection = new ConstantProperty(true);
    var intersectionWidth = new ConstantProperty(1);
    var showThroughEllipsoid = new ConstantProperty(true);
    var portionToDisplay = new ConstantProperty(
      SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON
    );
    var environmentConstraint = new ConstantProperty(true);
    var showEnvironmentOcclusion = new ConstantProperty(true);
    var environmentOcclusionMaterial = new ColorMaterialProperty();
    var showEnvironmentIntersection = new ConstantProperty(true);
    var environmentIntersectionColor = new ConstantProperty(Color.WHITE);
    var environmentIntersectionWidth = new ConstantProperty(1);

    var target = new CustomPatternSensorGraphics();
    target.lateralSurfaceMaterial = lateralSurfaceMaterial;
    target.showLateralSurfaces = showLateralSurfaces;
    target.ellipsoidHorizonSurfaceMaterial = ellipsoidHorizonSurfaceMaterial;
    target.showEllipsoidHorizonSurfaces = showEllipsoidHorizonSurfaces;
    target.domeSurfaceMaterial = domeSurfaceMaterial;
    target.showDomeSurfaces = showDomeSurfaces;
    target.ellipsoidSurfaceMaterial = ellipsoidSurfaceMaterial;
    target.showEllipsoidSurfaces = showEllipsoidSurfaces;
    target.directions = directions;
    target.intersectionColor = intersectionColor;
    target.radius = radius;
    target.show = show;
    target.showIntersection = showIntersection;
    target.intersectionWidth = intersectionWidth;
    target.showThroughEllipsoid = showThroughEllipsoid;
    target.portionToDisplay = portionToDisplay;
    target.environmentConstraint = environmentConstraint;
    target.showEnvironmentOcclusion = showEnvironmentOcclusion;
    target.environmentOcclusionMaterial = environmentOcclusionMaterial;
    target.showEnvironmentIntersection = showEnvironmentIntersection;
    target.environmentIntersectionColor = environmentIntersectionColor;
    target.environmentIntersectionWidth = environmentIntersectionWidth;

    target.merge(source);

    expect(target.lateralSurfaceMaterial).toBe(lateralSurfaceMaterial);
    expect(target.showLateralSurfaces).toBe(showLateralSurfaces);
    expect(target.ellipsoidHorizonSurfaceMaterial).toBe(
      ellipsoidHorizonSurfaceMaterial
    );
    expect(target.showEllipsoidHorizonSurfaces).toBe(
      showEllipsoidHorizonSurfaces
    );
    expect(target.domeSurfaceMaterial).toBe(domeSurfaceMaterial);
    expect(target.showDomeSurfaces).toBe(showDomeSurfaces);
    expect(target.ellipsoidSurfaceMaterial).toBe(ellipsoidSurfaceMaterial);
    expect(target.showEllipsoidSurfaces).toBe(showEllipsoidSurfaces);
    expect(target.directions).toBe(directions);
    expect(target.intersectionColor).toBe(intersectionColor);
    expect(target.radius).toBe(radius);
    expect(target.show).toBe(show);
    expect(target.showIntersection).toBe(showIntersection);
    expect(target.intersectionWidth).toBe(intersectionWidth);
    expect(target.showThroughEllipsoid).toBe(showThroughEllipsoid);
    expect(target.portionToDisplay).toBe(portionToDisplay);
    expect(target.environmentConstraint).toBe(environmentConstraint);
    expect(target.showEnvironmentOcclusion).toBe(showEnvironmentOcclusion);
    expect(target.environmentOcclusionMaterial).toBe(
      environmentOcclusionMaterial
    );
    expect(target.showEnvironmentIntersection).toBe(
      showEnvironmentIntersection
    );
    expect(target.environmentIntersectionColor).toBe(
      environmentIntersectionColor
    );
    expect(target.environmentIntersectionWidth).toBe(
      environmentIntersectionWidth
    );
  });

  it("clone works", function () {
    var source = new CustomPatternSensorGraphics();
    source.lateralSurfaceMaterial = new ColorMaterialProperty();
    source.showLateralSurfaces = new ConstantProperty(true);
    source.ellipsoidHorizonSurfaceMaterial = new ColorMaterialProperty();
    source.showEllipsoidHorizonSurfaces = new ConstantProperty(true);
    source.domeSurfaceMaterial = new ColorMaterialProperty();
    source.showDomeSurfaces = new ConstantProperty(true);
    source.ellipsoidSurfaceMaterial = new ColorMaterialProperty();
    source.showEllipsoidSurfaces = new ConstantProperty(true);
    source.directions = new ConstantProperty([]);
    source.intersectionColor = new ConstantProperty(Color.WHITE);
    source.radius = new ConstantProperty(1);
    source.show = new ConstantProperty(true);
    source.showIntersection = new ConstantProperty(true);
    source.intersectionWidth = new ConstantProperty(1);
    source.showThroughEllipsoid = new ConstantProperty(true);
    source.portionToDisplay = new ConstantProperty(
      SensorVolumePortionToDisplay.COMPLETE
    );
    source.environmentConstraint = new ConstantProperty(true);
    source.showEnvironmentOcclusion = new ConstantProperty(true);
    source.environmentOcclusionMaterial = new ColorMaterialProperty();
    source.showEnvironmentIntersection = new ConstantProperty(true);
    source.environmentIntersectionColor = new ConstantProperty(Color.WHITE);
    source.environmentIntersectionWidth = new ConstantProperty(1);

    var result = source.clone();
    expect(result.lateralSurfaceMaterial).toBe(source.lateralSurfaceMaterial);
    expect(result.showLateralSurfaces).toBe(source.showLateralSurfaces);
    expect(result.ellipsoidHorizonSurfaceMaterial).toBe(
      source.ellipsoidHorizonSurfaceMaterial
    );
    expect(result.showEllipsoidHorizonSurfaces).toBe(
      source.showEllipsoidHorizonSurfaces
    );
    expect(result.domeSurfaceMaterial).toBe(source.domeSurfaceMaterial);
    expect(result.showDomeSurfaces).toBe(source.showDomeSurfaces);
    expect(result.ellipsoidSurfaceMaterial).toBe(
      source.ellipsoidSurfaceMaterial
    );
    expect(result.showEllipsoidSurfaces).toBe(source.showEllipsoidSurfaces);
    expect(result.directions).toBe(source.directions);
    expect(result.intersectionColor).toBe(source.intersectionColor);
    expect(result.radius).toBe(source.radius);
    expect(result.show).toBe(source.show);
    expect(result.showIntersection).toBe(source.showIntersection);
    expect(result.intersectionWidth).toBe(source.intersectionWidth);
    expect(result.showThroughEllipsoid).toBe(source.showThroughEllipsoid);
    expect(result.portionToDisplay).toBe(source.portionToDisplay);
    expect(result.environmentConstraint).toBe(source.environmentConstraint);
    expect(result.showEnvironmentOcclusion).toBe(
      source.showEnvironmentOcclusion
    );
    expect(result.environmentOcclusionMaterial).toBe(
      source.environmentOcclusionMaterial
    );
    expect(result.showEnvironmentIntersection).toBe(
      source.showEnvironmentIntersection
    );
    expect(result.environmentIntersectionColor).toBe(
      source.environmentIntersectionColor
    );
    expect(result.environmentIntersectionWidth).toBe(
      source.environmentIntersectionWidth
    );
  });

  it("merge throws if source undefined", function () {
    var target = new CustomPatternSensorGraphics();
    expect(function () {
      target.merge(undefined);
    }).toThrowDeveloperError();
  });
});
