import { VectorVisualizer } from "../../Source/Cesium.js";
import { BoundingSphere } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { JulianDate } from "../../Source/Cesium.js";
import { BoundingSphereState } from "../../Source/Cesium.js";
import { ConstantPositionProperty } from "../../Source/Cesium.js";
import { ConstantProperty } from "../../Source/Cesium.js";
import { EntityCollection } from "../../Source/Cesium.js";
import { VectorGraphics } from "../../Source/Cesium.js";
import createScene from "../createScene.js";

describe(
  "DataSources/VectorVisualizer",
  function () {
    var scene;
    var visualizer;

    beforeAll(function () {
      scene = createScene();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    afterEach(function () {
      visualizer = visualizer && visualizer.destroy();
    });

    it("constructor throws if no scene is passed.", function () {
      expect(function () {
        return new VectorVisualizer();
      }).toThrowDeveloperError();
    });

    it("update throws if no time specified.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new VectorVisualizer(scene, entityCollection);
      expect(function () {
        visualizer.update();
      }).toThrowDeveloperError();
    });

    it("object with no vector does not create one.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new VectorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantProperty(
        new Cartesian3(1234, 5678, 9101112)
      );
      visualizer.update(JulianDate.now());
      expect(scene.primitives.length).toEqual(0);
    });

    it("object with no position does not create a vector.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new VectorVisualizer(scene, entityCollection);

      var testObject = entityCollection.getOrCreateEntity("test");
      var vector = (testObject.vector = new VectorGraphics());
      vector.show = new ConstantProperty(true);

      visualizer.update(JulianDate.now());
      expect(scene.primitives.length).toEqual(0);
    });

    it("A VectorGraphics causes a primtive to be created and updated.", function () {
      var time = JulianDate.now();

      var entityCollection = new EntityCollection();
      visualizer = new VectorVisualizer(scene, entityCollection);

      expect(scene.primitives.length).toEqual(0);

      var testObject = entityCollection.getOrCreateEntity("test");
      testObject.position = new ConstantPositionProperty(
        new Cartesian3(1234, 5678, 9101112)
      );

      var vector = (testObject.vector = new VectorGraphics());
      vector.show = new ConstantProperty(true);
      vector.color = new ConstantProperty(new Color(0.8, 0.7, 0.6, 0.5));
      vector.length = new ConstantProperty(13.5);
      vector.minimumLengthInPixels = new ConstantProperty(3.0);
      vector.direction = new ConstantProperty(new Cartesian3(1, 2, 3));

      visualizer.update(time);

      expect(scene.primitives.length).toEqual(1);

      var primitive = scene.primitives.get(0);
      visualizer.update(time);
      expect(primitive.show).toEqual(testObject.vector.show.getValue(time));
      expect(primitive.position).toEqual(testObject.position.getValue(time));
      expect(primitive.color).toEqual(testObject.vector.color.getValue(time));

      testObject.position = new ConstantProperty(
        new Cartesian3(5678, 1234, 1101112)
      );
      vector.color = new ConstantProperty(new Color(0.1, 0.2, 0.3, 0.4));

      visualizer.update(time);
      expect(primitive.show).toEqual(testObject.vector.show.getValue(time));
      expect(primitive.position).toEqual(testObject.position.getValue(time));
      expect(primitive.color).toEqual(testObject.vector.color.getValue(time));

      var sphere = new BoundingSphere();
      expect(visualizer.getBoundingSphere(testObject, sphere)).toEqual(
        BoundingSphereState.DONE
      );
      expect(sphere.center).toEqual(primitive.position);
      expect(sphere.radius).toEqual(primitive.length);

      vector.show = new ConstantProperty(false);
      visualizer.update(time);
      expect(primitive.show).toEqual(testObject.vector.show.getValue(time));
    });

    it("clear hides primitives.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new VectorVisualizer(scene, entityCollection);
      expect(scene.primitives.length).toEqual(0);
      var testObject = entityCollection.getOrCreateEntity("test");
      var time = JulianDate.now();

      testObject.position = new ConstantProperty(
        new Cartesian3(5678, 1234, 1101112)
      );
      var vector = (testObject.vector = new VectorGraphics());
      vector.show = new ConstantProperty(true);
      vector.color = new ConstantProperty(new Color(0.8, 0.7, 0.6, 0.5));
      vector.length = new ConstantProperty(13.5);
      vector.minimumLengthInPixels = new ConstantProperty(3.0);
      vector.direction = new ConstantProperty(new Cartesian3(1, 2, 3));
      visualizer.update(time);

      expect(scene.primitives.length).toEqual(1);

      entityCollection.removeAll();
      expect(scene.primitives.length).toEqual(0);
    });

    it("Visualizer sets id property.", function () {
      var entityCollection = new EntityCollection();
      visualizer = new VectorVisualizer(scene, entityCollection);

      expect(scene.primitives.length).toEqual(0);

      var testObject = entityCollection.getOrCreateEntity("test");

      var time = JulianDate.now();
      var vector = (testObject.vector = new VectorGraphics());

      testObject.position = new ConstantProperty(
        new Cartesian3(5678, 1234, 1101112)
      );
      vector.show = new ConstantProperty(true);
      vector.color = new ConstantProperty(new Color(0.8, 0.7, 0.6, 0.5));
      vector.length = new ConstantProperty(13.5);
      vector.minimumLengthInPixels = new ConstantProperty(3.0);
      vector.direction = new ConstantProperty(new Cartesian3(1, 2, 3));

      visualizer.update(time);
      var primitive = scene.primitives.get(0);
      expect(primitive.id).toEqual(testObject);
    });
  },
  "WebGL"
);
