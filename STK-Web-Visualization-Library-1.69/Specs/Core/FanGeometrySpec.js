import { FanGeometry } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";
import { Spherical } from "../../Source/Cesium.js";
import { VertexFormat } from "../../Source/Cesium.js";

describe("Core/FanGeometry", function () {
  var directionData = [0, 1, 100, 0, 2, 200, 0, 3, 300];
  var directions = [];
  for (var i = 0; i < directionData.length; i += 3) {
    directions.push(
      new Spherical(
        directionData[i],
        directionData[i + 1],
        directionData[i + 2]
      )
    );
  }

  it("constructor throws without directions", function () {
    expect(function () {
      return new FanGeometry({
        vertexFormat: VertexFormat.DEFAULT,
        directions: undefined,
        radius: 10000,
      });
    }).toThrowDeveloperError();
  });

  it("constructor throws without radius", function () {
    expect(function () {
      return new FanGeometry({
        vertexFormat: VertexFormat.DEFAULT,
        directions: directions,
        radius: undefined,
      });
    }).toThrowDeveloperError();
  });

  it("constructor computes all vertex attributes with constant radius", function () {
    var m = FanGeometry.createGeometry(
      new FanGeometry({
        vertexFormat: VertexFormat.ALL,
        directions: directions,
        radius: 10000,
      })
    );

    var directionsLength = directions.length;
    expect(m.attributes.position.values.length).toEqual(
      (directionsLength + 1) * 2 * 3
    );
    expect(m.attributes.normal.values.length).toEqual(
      (directionsLength + 1) * 2 * 3
    );
    expect(m.attributes.tangent.values.length).toEqual(
      (directionsLength + 1) * 2 * 3
    );
    expect(m.attributes.bitangent.values.length).toEqual(
      (directionsLength + 1) * 2 * 3
    );
    expect(m.attributes.st.values.length).toEqual(
      (directionsLength + 1) * 2 * 2
    );

    expect(m.indices.length).toEqual((directionsLength + 1) * 2 * 3);

    expect(m.boundingSphere.center).toEqual(Cartesian3.ZERO);
    expect(m.boundingSphere.radius).toEqual(10000);
  });

  it("constructor computes all vertex attributes with perDirectionRadius", function () {
    var m = FanGeometry.createGeometry(
      new FanGeometry({
        vertexFormat: VertexFormat.ALL,
        directions: directions,
        perDirectionRadius: true,
      })
    );

    var directionsLength = directions.length;
    expect(m.attributes.position.values.length).toEqual(
      (directionsLength + 1) * 2 * 3
    );
    expect(m.attributes.normal.values.length).toEqual(
      (directionsLength + 1) * 2 * 3
    );
    expect(m.attributes.tangent.values.length).toEqual(
      (directionsLength + 1) * 2 * 3
    );
    expect(m.attributes.bitangent.values.length).toEqual(
      (directionsLength + 1) * 2 * 3
    );
    expect(m.attributes.st.values.length).toEqual(
      (directionsLength + 1) * 2 * 2
    );

    expect(m.indices.length).toEqual((directionsLength + 1) * 2 * 3);

    expect(m.boundingSphere.center).toEqual(Cartesian3.ZERO);
    expect(m.boundingSphere.radius).toEqualEpsilon(300, CesiumMath.EPSILON12);
  });

  it("constructor throws with undefined directions", function () {
    expect(function () {
      return new FanGeometry({
        directions: undefined,
        radius: 10000,
      });
    }).toThrowDeveloperError();
  });

  it("constructor throws with undefined radius when perDirectionRadius is false", function () {
    expect(function () {
      return new FanGeometry({
        directions: directions,
        radius: undefined,
        perDirectionRadius: false,
      });
    }).toThrowDeveloperError();
  });

  it("createGeometry throws with undefined parmaeter", function () {
    expect(function () {
      FanGeometry.createGeometry(undefined);
    }).toThrowDeveloperError();
  });
});
