import { SphericalPolygon } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";

describe("Core/SphericalPolygon", function () {
  it("default constructor has undefined properties", function () {
    var s = new SphericalPolygon();
    expect(s.isConvex).toEqual(undefined);
    expect(s.vertices).toEqual([]);
    expect(s.convexHull.length).toEqual(0);
    expect(s.referenceAxis).toEqual(undefined);
    expect(s.referenceDistance).toEqual(undefined);
  });

  it("constructor sets expected properties", function () {
    var vertices = [];
    vertices.push({
      clock: CesiumMath.toRadians(-135.0),
      cone: CesiumMath.toRadians(60.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(-45.0),
      cone: CesiumMath.toRadians(60.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(45.0),
      cone: CesiumMath.toRadians(60.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(135.0),
      cone: CesiumMath.toRadians(60.0),
    });

    var s = new SphericalPolygon(vertices);
    expect(s.isConvex).toEqual(true);
    expect(s.vertices).toEqual(vertices);
    expect(s.convexHull.length).toEqual(4);
    expect(s.convexHull[0]).toEqual(0);
    expect(s.convexHull[1]).toEqual(1);
    expect(s.convexHull[2]).toEqual(2);
    expect(s.convexHull[3]).toEqual(3);
    expect(s.referenceAxis).toEqual(Cartesian3.UNIT_Z);
    expect(s.referenceDistance).toEqualEpsilon(0.5, CesiumMath.EPSILON15);
  });

  it("changing vertices causes properties to be recomputed", function () {
    var vertices = [];
    vertices.push({
      clock: CesiumMath.toRadians(-135.0),
      cone: CesiumMath.toRadians(60.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(-45.0),
      cone: CesiumMath.toRadians(60.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(45.0),
      cone: CesiumMath.toRadians(60.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(135.0),
      cone: CesiumMath.toRadians(60.0),
    });

    var s = new SphericalPolygon(vertices);
    expect(s.isConvex).toEqual(true);
    expect(s.vertices).toEqual(vertices);
    expect(s.convexHull.length).toEqual(4);
    expect(s.convexHull[0]).toEqual(0);
    expect(s.convexHull[1]).toEqual(1);
    expect(s.convexHull[2]).toEqual(2);
    expect(s.convexHull[3]).toEqual(3);
    expect(s.referenceAxis).toEqualEpsilon(
      Cartesian3.UNIT_Z,
      CesiumMath.EPSILON15
    );
    expect(s.referenceDistance).toEqualEpsilon(0.5, CesiumMath.EPSILON15);

    vertices.length = 0;
    vertices.push({
      clock: CesiumMath.toRadians(135.0),
      cone: CesiumMath.toRadians(135.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(-135.0),
      cone: CesiumMath.toRadians(150.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(-45.0),
      cone: CesiumMath.toRadians(135.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(-135.0),
      cone: CesiumMath.toRadians(135.0),
    });
    s.vertices = vertices;
    expect(s.isConvex).toEqual(false);
    expect(s.vertices).toEqual(vertices);
    expect(s.convexHull.length).toEqual(3);
    expect(s.convexHull[0]).toEqual(0);
    expect(s.convexHull[1]).toEqual(2);
    expect(s.convexHull[2]).toEqual(3);
    expect(s.referenceAxis).toEqualEpsilon(
      Cartesian3.negate(Cartesian3.UNIT_Z, new Cartesian3()),
      CesiumMath.EPSILON15
    );
    expect(s.referenceDistance).toEqualEpsilon(
      1.0 / Math.sqrt(2.0),
      CesiumMath.EPSILON7
    );
  });

  it("foot sensor produces correct results", function () {
    var vertices = [];
    vertices.push({
      clock: CesiumMath.toRadians(0.0),
      cone: CesiumMath.toRadians(14.03624347),
    });
    vertices.push({
      clock: CesiumMath.toRadians(74.3),
      cone: CesiumMath.toRadians(32.98357092),
    });
    vertices.push({
      clock: CesiumMath.toRadians(80.54),
      cone: CesiumMath.toRadians(37.23483398),
    });
    vertices.push({
      clock: CesiumMath.toRadians(90.0),
      cone: CesiumMath.toRadians(37.77568431),
    });
    vertices.push({
      clock: CesiumMath.toRadians(101.31),
      cone: CesiumMath.toRadians(37.41598862),
    });
    vertices.push({
      clock: CesiumMath.toRadians(116.56),
      cone: CesiumMath.toRadians(29.20519037),
    });
    vertices.push({
      clock: CesiumMath.toRadians(180.0),
      cone: CesiumMath.toRadians(14.03624347),
    });
    vertices.push({
      clock: CesiumMath.toRadians(247.2),
      cone: CesiumMath.toRadians(34.11764003),
    });
    vertices.push({
      clock: CesiumMath.toRadians(251.56),
      cone: CesiumMath.toRadians(44.67589157),
    });
    vertices.push({
      clock: CesiumMath.toRadians(253.96),
      cone: CesiumMath.toRadians(46.12330271),
    });
    vertices.push({
      clock: CesiumMath.toRadians(259.63),
      cone: CesiumMath.toRadians(46.19202903),
    });
    vertices.push({
      clock: CesiumMath.toRadians(262.87),
      cone: CesiumMath.toRadians(45.21405547),
    });
    vertices.push({
      clock: CesiumMath.toRadians(262.4),
      cone: CesiumMath.toRadians(37.09839406),
    });
    vertices.push({
      clock: CesiumMath.toRadians(266.47),
      cone: CesiumMath.toRadians(45.42651157),
    });
    vertices.push({
      clock: CesiumMath.toRadians(270.0),
      cone: CesiumMath.toRadians(45.42651157),
    });
    vertices.push({
      clock: CesiumMath.toRadians(270.97),
      cone: CesiumMath.toRadians(36.40877457),
    });
    vertices.push({
      clock: CesiumMath.toRadians(272.79),
      cone: CesiumMath.toRadians(45.70731937),
    });
    vertices.push({
      clock: CesiumMath.toRadians(278.33),
      cone: CesiumMath.toRadians(45.98533395),
    });
    vertices.push({
      clock: CesiumMath.toRadians(281.89),
      cone: CesiumMath.toRadians(36.0358894),
    });
    vertices.push({
      clock: CesiumMath.toRadians(282.99),
      cone: CesiumMath.toRadians(45.0),
    });
    vertices.push({
      clock: CesiumMath.toRadians(286.99),
      cone: CesiumMath.toRadians(43.26652934),
    });
    vertices.push({
      clock: CesiumMath.toRadians(291.8),
      cone: CesiumMath.toRadians(33.97011942),
    });
    vertices.push({
      clock: CesiumMath.toRadians(293.3),
      cone: CesiumMath.toRadians(41.50882857),
    });
    vertices.push({
      clock: CesiumMath.toRadians(300.96),
      cone: CesiumMath.toRadians(36.0826946),
    });
    vertices.push({
      clock: CesiumMath.toRadians(319.18),
      cone: CesiumMath.toRadians(19.9888568),
    });

    var s = new SphericalPolygon(vertices);
    expect(s.isConvex).toEqual(false);
    expect(s.vertices).toEqual(vertices);
    expect(s.convexHull.length).toEqual(12);
    expect(s.convexHull[0]).toEqual(1);
    expect(s.convexHull[1]).toEqual(2);
    expect(s.convexHull[2]).toEqual(3);
    expect(s.convexHull[3]).toEqual(4);
    expect(s.convexHull[4]).toEqual(5);
    expect(s.convexHull[5]).toEqual(8);
    expect(s.convexHull[6]).toEqual(9);
    expect(s.convexHull[7]).toEqual(10);
    expect(s.convexHull[8]).toEqual(17);
    expect(s.convexHull[9]).toEqual(19);
    expect(s.convexHull[10]).toEqual(22);
    expect(s.convexHull[11]).toEqual(23);
    expect(s.convexHull.holes.length).toEqual(5);

    var hole = s.convexHull.holes[0];
    expect(hole.length).toEqual(4);
    expect(hole[0]).toEqual(5);
    expect(hole[1]).toEqual(6);
    expect(hole[2]).toEqual(7);
    expect(hole[3]).toEqual(8);
    expect(hole.holes).toEqual(undefined);

    hole = s.convexHull.holes[1];
    expect(hole.length).toEqual(4);
    expect(hole[0]).toEqual(10);
    expect(hole[1]).toEqual(12);
    expect(hole[2]).toEqual(15);
    expect(hole[3]).toEqual(17);
    expect(hole.holes.length).toEqual(3);

    var hull = hole.holes[0];
    expect(hull.length).toEqual(3);
    expect(hull[0]).toEqual(10);
    expect(hull[1]).toEqual(11);
    expect(hull[2]).toEqual(12);
    expect(hull.holes).toEqual(undefined);

    hull = hole.holes[1];
    expect(hull.length).toEqual(4);
    expect(hull[0]).toEqual(12);
    expect(hull[1]).toEqual(13);
    expect(hull[2]).toEqual(14);
    expect(hull[3]).toEqual(15);
    expect(hull.holes).toEqual(undefined);

    hull = hole.holes[2];
    expect(hull.length).toEqual(3);
    expect(hull[0]).toEqual(15);
    expect(hull[1]).toEqual(16);
    expect(hull[2]).toEqual(17);
    expect(hull.holes).toEqual(undefined);

    hole = s.convexHull.holes[2];
    expect(hole.length).toEqual(3);
    expect(hole[0]).toEqual(17);
    expect(hole[1]).toEqual(18);
    expect(hole[2]).toEqual(19);
    expect(hole.holes).toEqual(undefined);

    hole = s.convexHull.holes[3];
    expect(hole.length).toEqual(3);
    expect(hole[0]).toEqual(19);
    expect(hole[1]).toEqual(21);
    expect(hole[2]).toEqual(22);
    expect(hole.holes.length).toEqual(1);

    hull = hole.holes[0];
    expect(hull.length).toEqual(3);
    expect(hull[0]).toEqual(19);
    expect(hull[1]).toEqual(20);
    expect(hull[2]).toEqual(21);
    expect(hull.holes).toEqual(undefined);

    hole = s.convexHull.holes[4];
    expect(hole.length).toEqual(3);
    expect(hole[0]).toEqual(23);
    expect(hole[1]).toEqual(24);
    expect(hole[2]).toEqual(1);
    expect(hole.holes.length).toEqual(1);

    hull = hole.holes[0];
    expect(hull.length).toEqual(3);
    expect(hull[0]).toEqual(24);
    expect(hull[1]).toEqual(0);
    expect(hull[2]).toEqual(1);
    expect(hull.holes).toEqual(undefined);

    var axis = new Cartesian3(
      -0.011600696329179922,
      -0.07289265444780975,
      0.9972723222732247
    );

    expect(s.referenceAxis).toEqualEpsilon(axis, CesiumMath.EPSILON7);
    expect(s.referenceDistance).toEqualEpsilon(
      0.7436070769796277,
      CesiumMath.EPSILON7
    );
  });
});
