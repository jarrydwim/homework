import { BoundingSphere } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { Ellipsoid } from "../../../Source/Cesium.js";
import { Globe } from "../../../Source/Cesium.js";
import { HeadingPitchRoll } from "../../../Source/Cesium.js";
import { Matrix4 } from "../../../Source/Cesium.js";
import { SceneTransforms } from "../../../Source/Cesium.js";
import { TransformEditorViewModel } from "../../../Source/Cesium.js";
import { Transforms } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";

describe(
  "Widgets/TransformEditor/TransformEditorViewModel",
  function () {
    var scene;
    var ellipsoid = Ellipsoid.WGS84;
    var globe = new Globe(ellipsoid);
    beforeAll(function () {
      scene = createScene();
      scene.globe = globe;
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("constructor sets default values", function () {
      var position = Cartesian3.fromDegrees(0.0, 0.0);
      var hpr = HeadingPitchRoll.fromDegrees(15, 30, -35);
      var scale = new Cartesian3(2.0, 2.0, 2.0);
      var transform = Transforms.headingPitchRollToFixedFrame(position, hpr);
      transform = Matrix4.setScale(transform, scale, transform);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      expect(viewModel.editorMode).toBeUndefined();
      expect(viewModel.enableNonUniformScaling).toBe(false);
      expect(viewModel.position).toEqualEpsilon(position, CesiumMath.EPSILON8);
      expect(viewModel.headingPitchRoll).toEqualEpsilon(
        hpr,
        CesiumMath.EPSILON8
      );
      expect(viewModel.scale).toEqualEpsilon(scale, CesiumMath.EPSILON8);
      expect(viewModel.menuExpanded).toBe(false);
      expect(viewModel.left).toBe("0");
      expect(viewModel.top).toBe("0");
      expect(viewModel.active).toBe(false);
      expect(viewModel.originOffset).toEqual(Cartesian3.ZERO);
    });

    it("constructor propagates pixelSize and maximumSizeInMeters to translation / rotation / scale editors as an uniform Cartesian2", function () {
      var position = Cartesian3.fromDegrees(24.0, -120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var offset = new Cartesian3(235.0, 33.0, -25.0);
      var pixelSize = 512;
      var maximumSizeInMeters = 1024;
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
        originOffset: offset,
        pixelSize: pixelSize,
        maximumSizeInMeters: maximumSizeInMeters,
      });

      expect(viewModel._translationEditor.pixelSize).toEqual(pixelSize);
      expect(viewModel._scaleEditor.pixelSize).toEqual(pixelSize);
      expect(viewModel._rotationEditor.pixelSize).toEqual(pixelSize);
      expect(viewModel._translationEditor.maximumSizeInMeters).toEqual(
        maximumSizeInMeters
      );
      expect(viewModel._scaleEditor.maximumSizeInMeters).toEqual(
        maximumSizeInMeters
      );
      expect(viewModel._rotationEditor.maximumSizeInMeters).toEqual(
        maximumSizeInMeters
      );
    });

    it("constructor uses originOffset", function () {
      var position = Cartesian3.fromDegrees(24.0, -120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var offset = new Cartesian3(235.0, 33.0, -25.0);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
        originOffset: offset,
      });
      expect(viewModel.originOffset).toEqual(offset);
    });

    it("constructor uses non uniform scaling if the transform has non-uniorm scaling", function () {
      var position = Cartesian3.fromDegrees(24.0, -120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var scale = new Cartesian3(2.0, 4.0, 6.0);
      transform = Matrix4.setScale(transform, scale, transform);
      var boundingSphere = new BoundingSphere(position, 10);
      var offset = new Cartesian3(235.0, 33.0, -25.0);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
        originOffset: offset,
      });
      expect(viewModel.enableNonUniformScaling).toBe(true);
      viewModel.enableNonUniformScaling = false;
      expect(viewModel.scale).toEqualEpsilon(
        new Cartesian3(2.0, 2.0, 2.0),
        CesiumMath.EPSILON8
      );
    });

    it("sets editorMode", function () {
      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      viewModel.editorMode = TransformEditorViewModel.EditorMode.TRANSLATION;
      expect(viewModel.editorMode).toEqual(
        TransformEditorViewModel.EditorMode.TRANSLATION
      );
      expect(viewModel._translationEditor.active).toBe(true);
      expect(viewModel._rotationEditor.active).toBe(false);
      expect(viewModel._scaleEditor.active).toBe(false);

      viewModel.editorMode = TransformEditorViewModel.EditorMode.ROTATION;
      expect(viewModel.editorMode).toEqual(
        TransformEditorViewModel.EditorMode.ROTATION
      );
      expect(viewModel._translationEditor.active).toBe(false);
      expect(viewModel._rotationEditor.active).toBe(true);
      expect(viewModel._scaleEditor.active).toBe(false);

      viewModel.editorMode = TransformEditorViewModel.EditorMode.SCALE;
      expect(viewModel.editorMode).toEqual(
        TransformEditorViewModel.EditorMode.SCALE
      );
      expect(viewModel._translationEditor.active).toBe(false);
      expect(viewModel._rotationEditor.active).toBe(false);
      expect(viewModel._scaleEditor.active).toBe(true);
    });

    it("sets position", function () {
      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      expect(viewModel.position).toEqualEpsilon(position, CesiumMath.EPSILON8);

      var newPosition = Cartesian3.fromDegrees(22.0, 10.5);
      viewModel.position = newPosition;
      expect(viewModel.position).toEqual(newPosition);
      expect(
        Matrix4.getTranslation(transform, new Cartesian3())
      ).toEqualEpsilon(newPosition, CesiumMath.EPSILON8);
    });

    it("sets headingPitchRoll", function () {
      var position = Cartesian3.fromDegrees(0.0, 0.0);
      var hpr = HeadingPitchRoll.fromDegrees(15, 30, -35);
      var scale = new Cartesian3(2.0, 2.0, 2.0);
      var transform = Transforms.headingPitchRollToFixedFrame(position, hpr);
      transform = Matrix4.setScale(transform, scale, transform);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      expect(viewModel.headingPitchRoll).toEqualEpsilon(
        hpr,
        CesiumMath.EPSILON8
      );

      var newHPR = HeadingPitchRoll.fromDegrees(-0.23, 25, 90);
      viewModel.headingPitchRoll = newHPR;
      expect(viewModel.headingPitchRoll).toEqual(newHPR);
      expect(
        Transforms.fixedFrameToHeadingPitchRoll(
          transform,
          Ellipsoid.WGS84,
          undefined,
          new HeadingPitchRoll()
        )
      ).toEqualEpsilon(newHPR, CesiumMath.EPSILON8);
    });

    it("sets scale", function () {
      var position = Cartesian3.fromDegrees(0.0, 0.0);
      var hpr = HeadingPitchRoll.fromDegrees(15, 30, -35);
      var scale = new Cartesian3(2.0, 2.0, 2.0);
      var transform = Transforms.headingPitchRollToFixedFrame(position, hpr);
      transform = Matrix4.setScale(transform, scale, transform);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      expect(viewModel.scale).toEqualEpsilon(scale, CesiumMath.EPSILON8);

      var newScale = new Cartesian3(1.0, 1.2, 1.0);
      viewModel.scale = newScale;
      expect(viewModel.scale).toEqual(newScale);
      expect(Matrix4.getScale(transform, new Cartesian3())).toEqualEpsilon(
        newScale,
        CesiumMath.EPSILON8
      );
    });

    it("sets originOffset", function () {
      var position = Cartesian3.fromDegrees(24.0, -120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var offset = new Cartesian3(235.0, 33.0, -25.0);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      expect(viewModel.originOffset).toEqual(Cartesian3.ZERO);
      viewModel.originOffset = offset;
      expect(viewModel.originOffset).toEqual(offset);
    });

    it("setOriginPosition sets originOffset from a position in world space", function () {
      var position = Cartesian3.fromDegrees(24.0, -120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      var originPosition = Cartesian3.fromDegrees(24.0, -120.0, 300);
      var expectedOffset = new Cartesian3(0.0, 0.0, 300.0);

      expect(viewModel.originOffset).toEqual(Cartesian3.ZERO);
      viewModel.setOriginPosition(originPosition);
      expect(viewModel.originOffset).toEqualEpsilon(
        expectedOffset,
        CesiumMath.EPSILON8
      );
    });

    it("setOriginPosition throws with no position", function () {
      var position = Cartesian3.fromDegrees(24.0, -120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });

      expect(function () {
        viewModel.setOriginPosition();
      }).toThrowDeveloperError();
    });

    it("activates and deactivates", function () {
      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });

      viewModel.activate();
      expect(viewModel.active).toBe(true);
      expect(viewModel.editorMode).toEqual(
        TransformEditorViewModel.EditorMode.TRANSLATION
      );
      expect(viewModel._translationEditor.active).toBe(true);
      expect(viewModel._rotationEditor.active).toBe(false);
      expect(viewModel._scaleEditor.active).toBe(false);

      viewModel.deactivate();
      expect(viewModel.active).toBe(false);
      expect(viewModel._translationEditor.active).toBe(false);
      expect(viewModel._rotationEditor.active).toBe(false);
      expect(viewModel._scaleEditor.active).toBe(false);
    });

    it("expandMenu expands the menu", function () {
      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      expect(viewModel.menuExpanded).toBe(false);
      viewModel.expandMenu();
      expect(viewModel.menuExpanded).toBe(true);
    });

    it("setModeTranslation activates translation mode", function () {
      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      viewModel.setModeTranslation();
      expect(viewModel.editorMode).toEqual(
        TransformEditorViewModel.EditorMode.TRANSLATION
      );
      expect(viewModel._translationEditor.active).toBe(true);
    });

    it("setModeRotation activates rotation mode", function () {
      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      viewModel.setModeRotation();
      expect(viewModel.editorMode).toEqual(
        TransformEditorViewModel.EditorMode.ROTATION
      );
      expect(viewModel._rotationEditor.active).toBe(true);
    });

    it("setModeScale activates scale mode", function () {
      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      viewModel.setModeScale();
      expect(viewModel.editorMode).toEqual(
        TransformEditorViewModel.EditorMode.SCALE
      );
      expect(viewModel._scaleEditor.active).toBe(true);
    });

    it("toggleNonUniformScaling toggles enableNonUniformScaling", function () {
      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      expect(viewModel.enableNonUniformScaling).toBe(false);
      viewModel.toggleNonUniformScaling();
      expect(viewModel.enableNonUniformScaling).toBe(true);
      viewModel.toggleNonUniformScaling();
      expect(viewModel.enableNonUniformScaling).toBe(false);
    });

    it("update sets the top and left position", function () {
      spyOn(SceneTransforms, "wgs84ToWindowCoordinates").and.returnValue(
        new Cartesian2(50.2, 253.7)
      );

      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });

      viewModel.activate();
      viewModel._update();
      expect(viewModel.left).toEqual("37px");
      expect(viewModel.top).toEqual("253px");
    });

    it("destroy", function () {
      var position = Cartesian3.fromDegrees(-24.0, 120.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var boundingSphere = new BoundingSphere(position, 10);
      var viewModel = new TransformEditorViewModel({
        scene: scene,
        transform: transform,
        boundingSphere: boundingSphere,
      });
      expect(viewModel.isDestroyed()).toBe(false);
      viewModel.destroy();
      expect(viewModel.isDestroyed()).toBe(true);
    });

    it("throws if scene is undefined", function () {
      expect(function () {
        return new TransformEditorViewModel({
          scene: undefined,
          transform: new Matrix4(),
          boundingSphere: new BoundingSphere(),
        });
      }).toThrowDeveloperError();
    });

    it("throws if transform is undefined", function () {
      expect(function () {
        return new TransformEditorViewModel({
          scene: scene,
          transform: undefined,
          boundingSphere: new BoundingSphere(),
        });
      }).toThrowDeveloperError();
    });

    it("throws if boundingSphere is undefined", function () {
      expect(function () {
        return new TransformEditorViewModel({
          scene: scene,
          transform: new Matrix4(),
          boundingSphere: undefined,
        });
      }).toThrowDeveloperError();
    });
  },
  "WebGL"
);
