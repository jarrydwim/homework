import { AxisLinePrimitive } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Color } from "../../../Source/Cesium.js";
import { ColorGeometryInstanceAttribute } from "../../../Source/Cesium.js";
import { Matrix4 } from "../../../Source/Cesium.js";
import { PolylineColorAppearance } from "../../../Source/Cesium.js";
import { PolylineMaterialAppearance } from "../../../Source/Cesium.js";
import { Primitive } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";

describe(
  "Widgets/TransformEditor/AxisLinePrimitive",
  function () {
    var scene;

    beforeAll(function () {
      scene = createScene();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("throws with no color option", function () {
      expect(function () {
        return new AxisLinePrimitive({
          color: undefined,
          positions: [],
        });
      }).toThrowDeveloperError();
    });

    it("throws with no positions option", function () {
      expect(function () {
        return new AxisLinePrimitive({
          color: Color.RED,
          positions: undefined,
        });
      }).toThrowDeveloperError();
    });

    it("creates and destroys", function () {
      var line = new AxisLinePrimitive({
        color: Color.RED,
        positions: [],
      });
      expect(line.show).toBe(true);
      expect(line.id).toBeUndefined();
      expect(line.color).toEqual(Color.RED);
      expect(line.positions).toEqual([]);
      expect(line.width).toBe(8);
      expect(line.modelMatrix).toEqual(Matrix4.IDENTITY);
      expect(line.boundingVolume).toBeDefined();
      expect(line.isDestroyed()).toBe(false);
      line.destroy();
      expect(line.isDestroyed()).toBe(true);
    });

    it("creates with options", function () {
      var line = new AxisLinePrimitive({
        color: Color.LIME,
        positions: [new Cartesian3(), new Cartesian3()],
        show: false,
        id: "my id",
        loop: true,
        arrow: true,
        width: 2,
        depthFail: false,
      });

      expect(line.show).toBe(false);
      expect(line.id).toBe("my id");
      expect(line.positions.length).toBe(3);
    });

    it("creates poyline and updates", function () {
      spyOn(Primitive.prototype, "update");
      var mockFrameState = {};
      var line = new AxisLinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -122, 40, -122, 41]),
      });
      line.update(mockFrameState);
      var primitive = line._primitive;
      expect(primitive).toBeDefined();
      expect(Primitive.prototype.update).toHaveBeenCalledWith(mockFrameState);

      var instance = primitive.geometryInstances;
      expect(instance).toBeDefined();
      expect(instance.attributes.color).toEqual(
        ColorGeometryInstanceAttribute.fromColor(Color.RED)
      );
      expect(instance.attributes.depthFailColor).toEqual(
        ColorGeometryInstanceAttribute.fromColor(Color.RED.withAlpha(0.3))
      );

      expect(primitive.appearance).toBeInstanceOf(PolylineColorAppearance);
      expect(primitive.depthFailAppearance).toBeInstanceOf(
        PolylineColorAppearance
      );
    });

    it("creates arrow polyline and updates", function () {
      spyOn(Primitive.prototype, "update");
      var mockFrameState = {};
      var line = new AxisLinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -122, 40, -122, 41]),
        arrow: true,
      });
      line.update(mockFrameState);
      var primitive = line._primitive;
      expect(primitive).toBeDefined();
      expect(Primitive.prototype.update).toHaveBeenCalledWith(mockFrameState);

      var instance = primitive.geometryInstances;
      expect(instance).toBeDefined();
      expect(instance.attributes.color).toEqual(
        ColorGeometryInstanceAttribute.fromColor(Color.RED)
      );
      expect(instance.attributes.depthFailColor).toEqual(
        ColorGeometryInstanceAttribute.fromColor(Color.RED.withAlpha(0.3))
      );

      expect(primitive.appearance).toBeInstanceOf(PolylineMaterialAppearance);
      expect(primitive.depthFailAppearance).toBeInstanceOf(
        PolylineMaterialAppearance
      );
    });

    it("creates polyline without depthfail and updates", function () {
      spyOn(Primitive.prototype, "update");
      var mockFrameState = {};
      var line = new AxisLinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -122, 40, -122, 41]),
        depthFail: false,
      });
      line.update(mockFrameState);
      var primitive = line._primitive;
      expect(primitive).toBeDefined();
      expect(Primitive.prototype.update).toHaveBeenCalledWith(mockFrameState);

      expect(primitive.appearance).toBeInstanceOf(PolylineColorAppearance);
      expect(primitive.depthFailAppearance).toBeUndefined();
    });

    it("creates polyline with modelMatrix and updates", function () {
      spyOn(Primitive.prototype, "update");
      var mockFrameState = {};
      var line = new AxisLinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -122, 40, -122, 41]),
        depthFail: false,
      });
      line.modelMatrix = Matrix4.fromTranslation(new Cartesian3(1, 2, 3));
      line.update(mockFrameState);
      var primitive = line._primitive;
      expect(primitive).toBeDefined();
      expect(Primitive.prototype.update).toHaveBeenCalledWith(mockFrameState);
      var instance = primitive.geometryInstances;
      expect(instance.modelMatrix).toEqual(line.modelMatrix);
    });

    it("hides", function () {
      var line = new AxisLinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -122, 40, -122, 41]),
        show: false,
      });
      line.positions = Cartesian3.fromDegreesArray([
        -123,
        40,
        -122,
        40,
        -122,
        41,
      ]);
      line.update();
      var primitive = line._primitive;
      expect(primitive).toBeUndefined();
    });

    it("updates when positions changes", function () {
      var line = new AxisLinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -125, 40, -14, 41]),
      });
      scene.primitives.add(line);
      scene.renderForSpecs();

      var primitive1 = line._primitive;
      expect(primitive1).toBeDefined();

      line.positions = Cartesian3.fromDegreesArray([
        -123,
        42,
        -122,
        42,
        -122,
        41,
      ]);
      scene.renderForSpecs();

      var primitive2 = line._primitive;
      expect(primitive2).toBeDefined();
      expect(primitive2).not.toEqual(primitive1);
      expect(primitive1.isDestroyed()).toBe(true);
    });

    it("updates when modelMatrix changes", function () {
      var line = new AxisLinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -125, 40, -14, 41]),
      });
      scene.primitives.add(line);
      scene.renderForSpecs();

      var primitive1 = line._primitive;
      expect(primitive1).toBeDefined();

      line.modelMatrix = Matrix4.fromTranslation(new Cartesian3(5, 55, -55));
      scene.renderForSpecs();

      var primitive2 = line._primitive;
      expect(primitive2).toBeDefined();
      expect(primitive2).not.toEqual(primitive1);
      expect(primitive1.isDestroyed()).toBe(true);
    });

    it("does not make a new primitive when show changes", function () {
      var line = new AxisLinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -122, 40, -122, 41]),
      });
      scene.primitives.add(line);
      scene.renderForSpecs();

      var primitive1 = line._primitive;
      expect(primitive1).toBeDefined();

      line.show = false;
      scene.renderForSpecs();

      var primitive2 = line._primitive;
      expect(primitive2).toBeDefined();
      expect(primitive2).toBe(primitive1);
    });
  },
  "WebGL"
);
