import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { HeadingPitchRoll } from "../../../Source/Cesium.js";
import { IntersectionTests } from "../../../Source/Cesium.js";
import { Matrix3 } from "../../../Source/Cesium.js";
import { Matrix4 } from "../../../Source/Cesium.js";
import { Plane } from "../../../Source/Cesium.js";
import { RotationEditor } from "../../../Source/Cesium.js";
import { Transforms } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";

describe("RotationEditor", function () {
  var scene;

  beforeEach(function () {
    scene = createScene();
  });

  afterEach(function () {
    scene.destroyForSpecs();
  });

  function getOptions() {
    return {
      scene: scene,
      transform: Transforms.eastNorthUpToFixedFrame(
        Cartesian3.fromDegrees(0, 0)
      ),
      originOffset: new Cartesian3(),
      setPosition: jasmine.createSpy(),
      setHeadingPitchRoll: jasmine.createSpy(),
      radius: 1,
    };
  }

  it("has expected values", function () {
    var options = getOptions();
    var editor = new RotationEditor(options);

    expect(scene.primitives.length).toBe(5);
    expect(editor.active).toBe(false);
    expect(editor.originOffset).toEqual(options.originOffset);
  });

  it("activates and deactivates", function () {
    var editor = new RotationEditor(getOptions());
    editor.active = true;
    expect(editor._polylineX.show).toBe(true);
    expect(editor._polylineY.show).toBe(true);
    expect(editor._polylineZ.show).toBe(true);
    editor.active = false;
    expect(editor._polylineX.show).toBe(false);
    expect(editor._polylineY.show).toBe(false);
    expect(editor._polylineZ.show).toBe(false);
  });

  it("rotation editor with pixel sizing is different than non pixel-sized scale editor", function () {
    var options = getOptions();
    var noPixelSizing = new RotationEditor(options);
    options.pixelSize = 256;
    var pixelSizing = new RotationEditor(options);
    var identicalMatrices = Matrix4.equalsEpsilon(
      pixelSizing._modelMatrix,
      noPixelSizing._modelMatrix,
      CesiumMath.EPSILON10
    );
    expect(identicalMatrices).toBe(false);
  });

  it("maximumSizeInMeters is respected when pixel sizing is enabled", function () {
    var options = getOptions();
    options.pixelSize = 256;
    var pixelSizingNoMaximum = new RotationEditor(options);
    options.maximumSizeInMeters = 128;
    var pixelsizingWithMaximum = new RotationEditor(options);

    var scalingFactorNoMaximum = Matrix4.getScale(
      pixelSizingNoMaximum._modelMatrix,
      new Cartesian3()
    );
    var scalingFactorWithMaximum = Matrix4.getScale(
      pixelsizingWithMaximum._modelMatrix,
      new Cartesian3()
    );
    var maximumNoLimit = Cartesian3.maximumComponent(scalingFactorNoMaximum);
    var maximumLimited = Cartesian3.maximumComponent(scalingFactorWithMaximum);
    expect(maximumNoLimit).toBeGreaterThan(maximumLimited);
  });

  it("modified the circle model matrix on update", function () {
    var options = getOptions();
    var editor = new RotationEditor(options);
    var oldModelMatrix = editor._modelMatrix.clone();
    options.transform = Matrix4.clone(
      Transforms.eastNorthUpToFixedFrame(Cartesian3.fromDegrees(50, 0)),
      options.transform
    );
    editor.update();
    expect(editor._modelMatrix).not.toEqual(oldModelMatrix);
  });

  it("finds the picked axis on pointer down", function () {
    spyOn(scene, "drillPick").and.returnValue([{ id: "X" }]);
    var editor = new RotationEditor(getOptions());
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      editor._rotationStartPoint
    );
    editor.handleLeftDown(new Cartesian2(220, 220));
    expect(editor._rotationAxis).toBe(Cartesian3.UNIT_X);
    expect(editor._dragging).toBe(true);
  });

  it("sets heading on pointer move for Z axis rotation", function () {
    var options = getOptions();
    var editor = new RotationEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(6378137, 10.315120040369635, 7.92468462711544)
    );

    editor._dragging = true;
    editor._modelOrigin = Cartesian3.fromDegrees(0, 0);
    editor._startTransform = Transforms.eastNorthUpToFixedFrame(
      editor._modelOrigin
    );
    editor._startRotation = Matrix4.getMatrix3(
      editor._startTransform,
      new Matrix3()
    );
    editor._rotationAxis = Cartesian3.UNIT_Z;
    editor._rotationStartPoint = new Cartesian3(
      6378137,
      0.6202380102481623,
      16.340161990495105
    );
    editor._rotaitonPlane = Plane.fromPointNormal(
      editor._modelOrigin,
      editor._rotationAxis
    );

    editor.handleMouseMove(new Cartesian2(220, 220));

    expect(options.setHeadingPitchRoll).toHaveBeenCalled();
    expect(options.setHeadingPitchRoll.calls.allArgs()[0][0]).toEqualEpsilon(
      new HeadingPitchRoll(0.8777717819875748, 0.0, 0.0),
      CesiumMath.EPSILON10
    );

    expect(options.setPosition).toHaveBeenCalled();
  });

  it("sets pitch on pointer move for Y axis rotation", function () {
    var options = getOptions();
    var editor = new RotationEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(6378137.412678664, 16.376663665930963, 0)
    );

    editor._dragging = true;
    editor._modelOrigin = Cartesian3.fromDegrees(0, 0);
    editor._startTransform = Transforms.eastNorthUpToFixedFrame(
      editor._modelOrigin
    );
    editor._startRotation = Matrix4.getMatrix3(
      editor._startTransform,
      new Matrix3()
    );
    editor._rotationAxis = Cartesian3.UNIT_Y;
    editor._rotationStartPoint = new Cartesian3(
      6378150.263215737,
      9.190182774505512,
      0
    );
    editor._rotaitonPlane = Plane.fromPointNormal(
      editor._modelOrigin,
      editor._rotationAxis
    );

    editor.handleMouseMove(new Cartesian2(220, 220));

    expect(options.setHeadingPitchRoll).toHaveBeenCalled();
    expect(options.setHeadingPitchRoll.calls.allArgs()[0][0]).toEqualEpsilon(
      new HeadingPitchRoll(0.0, -0.2841083271723956, 0.0),
      CesiumMath.EPSILON10
    );

    expect(options.setPosition).toHaveBeenCalled();
  });

  it("sets roll on pointer move for X axis rotation", function () {
    var options = getOptions();
    var editor = new RotationEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(6378139.432499991, 0, 21.968016067364836)
    );

    editor._dragging = true;
    editor._modelOrigin = Cartesian3.fromDegrees(0, 0);
    editor._startTransform = Transforms.eastNorthUpToFixedFrame(
      editor._modelOrigin
    );
    editor._startRotation = Matrix4.getMatrix3(
      editor._startTransform,
      new Matrix3()
    );
    editor._rotationAxis = Cartesian3.UNIT_X;
    editor._rotationStartPoint = new Cartesian3(
      6378152.62886797,
      0,
      4.982178924371966
    );
    editor._rotaitonPlane = Plane.fromPointNormal(
      editor._modelOrigin,
      editor._rotationAxis
    );

    editor.handleMouseMove(new Cartesian2(220, 220));

    expect(options.setHeadingPitchRoll).toHaveBeenCalled();
    expect(options.setHeadingPitchRoll.calls.allArgs()[0][0]).toEqualEpsilon(
      new HeadingPitchRoll(0.0, 0.0, -0.6438411345764213),
      CesiumMath.EPSILON10
    );

    expect(options.setPosition).toHaveBeenCalled();
  });

  it("computes the rotation angle on the XY plane", function () {
    var transform = Matrix4.IDENTITY;
    var axis = Cartesian3.UNIT_Z;
    var start = new Cartesian3(1, 0, 0);
    var end = new Cartesian3(0, 1, 0);
    var angle = RotationEditor._getRotationAngle(
      transform,
      Cartesian3.ZERO,
      axis,
      start,
      end
    );
    expect(angle).toEqual(CesiumMath.PI_OVER_TWO);
    angle = RotationEditor._getRotationAngle(
      transform,
      Cartesian3.ZERO,
      axis,
      end,
      start
    );
    expect(angle).toEqual(-CesiumMath.PI_OVER_TWO);
  });

  it("computes the rotation angle on the XZ plane", function () {
    var transform = Matrix4.IDENTITY;
    var axis = Cartesian3.UNIT_Y;
    var start = new Cartesian3(1, 0, 0);
    var end = new Cartesian3(0, 0, 1);
    var angle = RotationEditor._getRotationAngle(
      transform,
      Cartesian3.ZERO,
      axis,
      start,
      end
    );
    expect(angle).toEqual(-CesiumMath.PI_OVER_TWO);
    angle = RotationEditor._getRotationAngle(
      transform,
      Cartesian3.ZERO,
      axis,
      end,
      start
    );
    expect(angle).toEqual(CesiumMath.PI_OVER_TWO);
  });

  it("computes the rotation angle on the YZ plane", function () {
    var transform = Matrix4.IDENTITY;
    var axis = Cartesian3.UNIT_X;
    var start = new Cartesian3(0, 1, 0);
    var end = new Cartesian3(0, 0, 1);
    var angle = RotationEditor._getRotationAngle(
      transform,
      Cartesian3.ZERO,
      axis,
      start,
      end
    );
    expect(angle).toEqual(CesiumMath.PI_OVER_TWO);
    angle = RotationEditor._getRotationAngle(
      transform,
      Cartesian3.ZERO,
      axis,
      end,
      start
    );
    expect(angle).toEqual(-CesiumMath.PI_OVER_TWO);
  });

  it("stops dragging on pointer up", function () {
    var editor = new RotationEditor(getOptions());
    editor._dragging = true;
    editor.handleLeftUp();
    expect(editor._dragging).toBe(false);
  });

  it("destroys the editor", function () {
    var editor = new RotationEditor(getOptions());
    expect(editor.isDestroyed()).toBe(false);
    editor.destroy();
    expect(scene.primitives.length).toBe(0);
    expect(editor.isDestroyed()).toBe(true);
  });
});
