import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { IntersectionTests } from "../../../Source/Cesium.js";
import { knockout } from "../../../Source/Cesium.js";
import { Matrix4 } from "../../../Source/Cesium.js";
import { Plane } from "../../../Source/Cesium.js";
import { ScaleEditor } from "../../../Source/Cesium.js";
import { Transforms } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";

describe("ScaleEditor", function () {
  var scene;

  beforeEach(function () {
    scene = createScene();
  });

  afterEach(function () {
    scene.destroyForSpecs();
  });

  function getOptions() {
    return {
      scene: scene,
      transform: Transforms.eastNorthUpToFixedFrame(
        Cartesian3.fromDegrees(0, 0)
      ),
      originOffset: new Cartesian3(),
      enableNonUniformScaling: knockout.observable(false),
      setPosition: jasmine.createSpy(),
      setScale: jasmine.createSpy(),
      radius: 1,
    };
  }

  it("has expected values", function () {
    var options = getOptions();
    var editor = new ScaleEditor(options);

    expect(scene.primitives.length).toBe(4);
    expect(editor.active).toBe(false);
    expect(editor.originOffset).toEqual(options.originOffset);
  });

  it("activates and deactivates", function () {
    var editor = new ScaleEditor(getOptions());
    editor.active = true;
    expect(editor._polylineX.show).toBe(true);
    expect(editor._polylineY.show).toBe(true);
    expect(editor._polylineZ.show).toBe(true);
    editor.active = false;
    expect(editor._polylineX.show).toBe(false);
    expect(editor._polylineY.show).toBe(false);
    expect(editor._polylineZ.show).toBe(false);
  });

  it("modifies the model matrix on update", function () {
    var options = getOptions();
    var editor = new ScaleEditor(options);
    var oldModelMatrix = editor._modelMatrix.clone();
    options.transform = Matrix4.clone(
      Transforms.eastNorthUpToFixedFrame(Cartesian3.fromDegrees(50, 0)),
      options.transform
    );
    editor.update();
    expect(editor._modelMatrix).not.toEqual(oldModelMatrix);
  });

  it("scale editor with pixel sizing is different than non pixel-sized scale editor", function () {
    var options = getOptions();
    var noPixelSizing = new ScaleEditor(options);
    options.pixelSize = 256;
    var pixelSizing = new ScaleEditor(options);
    var identicalMatrices = Matrix4.equalsEpsilon(
      pixelSizing._modelMatrix,
      noPixelSizing._modelMatrix,
      CesiumMath.EPSILON10
    );
    expect(identicalMatrices).toBe(false);
  });

  it("maximumSizeInMeters is respected when pixelSizing is enabled", function () {
    var options = getOptions();
    options.pixelSize = 256;
    var pixelSizingNoMaximum = new ScaleEditor(options);
    options.maximumSizeInMeters = 128;
    var pixelsizingWithMaximum = new ScaleEditor(options);

    var scalingFactorNoMaximum = Matrix4.getScale(
      pixelSizingNoMaximum._modelMatrix,
      new Cartesian3()
    );
    var scalingFactorWithMaximum = Matrix4.getScale(
      pixelsizingWithMaximum._modelMatrix,
      new Cartesian3()
    );
    var maximumNoLimit = Cartesian3.maximumComponent(scalingFactorNoMaximum);
    var maximumLimited = Cartesian3.maximumComponent(scalingFactorWithMaximum);
    expect(maximumNoLimit).toBeGreaterThan(maximumLimited);
  });

  it("finds the picked axis on pointer down", function () {
    spyOn(scene, "drillPick").and.returnValue([{ id: "X" }]);
    spyOn(scene.camera, "getPickRay");
    var editor = new ScaleEditor(getOptions());
    spyOn(IntersectionTests, "rayPlane").and.returnValue(editor._startPosition);
    editor.handleLeftDown(new Cartesian2(220, 220));
    expect(editor._dragAlongVector).toBe(Cartesian3.UNIT_X);
    expect(editor._dragging).toBe(true);
  });

  it("sets uniform scale on pointer move", function () {
    var options = getOptions();
    var editor = new ScaleEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(6378127.056702078, 1.0761568596238362, 130.4702645402034)
    );

    editor._dragging = true;
    editor._startPosition = new Cartesian3(
      6378137.360150858,
      -0.038978900068741495,
      19.220881382994925
    );
    editor._offsetVector = new Cartesian3(
      0.3601508578285575,
      -0.038978900068741495,
      19.220881382994925
    );
    editor._pickedAxis = "Y";
    editor._dragAlongVector = Cartesian3.UNIT_Y;
    editor._pickingPlane = Plane.fromPointNormal(
      Cartesian3.fromDegrees(0, 0),
      Cartesian3.UNIT_Z
    );
    editor._startValue = 20;
    editor._startScale = new Cartesian3(20, 20, 20);

    editor.handleMouseMove(new Cartesian2(220, 220));

    var expectedScale = 94.16625543813899;
    expect(options.setScale).toHaveBeenCalled();
    expect(options.setScale.calls.allArgs()[0][0]).toEqualEpsilon(
      new Cartesian3(expectedScale, expectedScale, expectedScale),
      CesiumMath.EPSILON8
    );

    expect(options.setPosition).toHaveBeenCalled();
  });

  it("sets scale X on pointer move", function () {
    var options = getOptions();
    options.enableNonUniformScaling(true);
    var editor = new ScaleEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(6378136.994803323, 1.956614929857687, -0.08483043916781305)
    );

    editor._dragging = true;
    editor._startPosition = new Cartesian3(
      6378136.99974326,
      0.9897186019719069,
      -0.004191020353937036
    );
    editor._offsetVector = new Cartesian3(
      -0.0002567395567893982,
      0.9897186019719069,
      -0.004191020353937036
    );
    editor._dragAlongVector = Cartesian3.UNIT_X;
    editor._pickedAxis = "X";
    editor._pickingPlane = Plane.fromPointNormal(
      Cartesian3.fromDegrees(0, 0),
      Cartesian3.UNIT_Z
    );
    editor._startValue = 1;
    editor._startScale = new Cartesian3(1, 1, 1);

    editor.handleMouseMove(new Cartesian2(220, 220));

    expect(options.setScale).toHaveBeenCalled();
    expect(options.setScale.calls.allArgs()[0][0]).toEqualEpsilon(
      new Cartesian3(1.6445975519238534, 1.0, 1.0),
      CesiumMath.EPSILON8
    );

    expect(options.setPosition).toHaveBeenCalled();
  });

  it("sets scale Y on pointer move", function () {
    var options = getOptions();
    options.enableNonUniformScaling(true);
    var editor = new ScaleEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(
        6378136.999999977,
        0.08358721612789188,
        0.38772332164574996
      )
    );

    editor._dragging = true;
    editor._startPosition = new Cartesian3(
      6378136.999999996,
      0.012938326092264418,
      0.9605390892344715
    );
    editor._offsetVector = new Cartesian3(
      0,
      0.012938326092264418,
      0.9605390892344715
    );
    editor._dragAlongVector = Cartesian3.UNIT_Y;
    editor._pickedAxis = "Y";
    editor._pickingPlane = Plane.fromPointNormal(
      Cartesian3.fromDegrees(0, 0),
      Cartesian3.UNIT_Z
    );
    editor._startValue = 1;
    editor._startScale = new Cartesian3(1, 1, 1);

    editor.handleMouseMove(new Cartesian2(220, 220));

    expect(options.setScale).toHaveBeenCalled();
    expect(options.setScale.calls.allArgs()[0][0]).toEqualEpsilon(
      new Cartesian3(1.0, 0.618122821607519, 1.0),
      CesiumMath.EPSILON8
    );

    expect(options.setPosition).toHaveBeenCalled();
  });

  it("sets scale Z on pointer move", function () {
    var options = getOptions();
    options.enableNonUniformScaling(true);
    var editor = new ScaleEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(
        6378139.371025945,
        -0.06275585414012497,
        0.23541056078984646
      )
    );

    editor._dragging = true;
    editor._startPosition = new Cartesian3(
      6378137.985142435,
      -0.01289929814805646,
      0.04838801180283436
    );
    editor._offsetVector = new Cartesian3(
      0.9851424349471927,
      -0.01289929814805646,
      0.04838801180283436
    );
    editor._dragAlongVector = Cartesian3.UNIT_Z;
    editor._pickedAxis = "Z";
    editor._pickingPlane = Plane.fromPointNormal(
      Cartesian3.fromDegrees(0, 0),
      Cartesian3.UNIT_Y
    );
    editor._startValue = 1;
    editor._startScale = new Cartesian3(1, 1, 1);

    editor.handleMouseMove(new Cartesian2(220, 220));

    expect(options.setScale).toHaveBeenCalled();
    expect(options.setScale.calls.allArgs()[0][0]).toEqualEpsilon(
      new Cartesian3(1.0, 1.0, 1.9239223400751748),
      CesiumMath.EPSILON8
    );

    expect(options.setPosition).toHaveBeenCalled();
  });

  it("stops dragging on pointer up", function () {
    var editor = new ScaleEditor(getOptions());
    editor._dragging = true;
    editor.handleLeftUp();
    expect(editor._dragging).toBe(false);
  });

  it("destroys the editor", function () {
    var editor = new ScaleEditor(getOptions());
    expect(editor.isDestroyed()).toBe(false);
    editor.destroy();
    expect(scene.primitives.length).toBe(0);
    expect(editor.isDestroyed()).toBe(true);
  });
});
