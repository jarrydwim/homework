import { BoundingSphere } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";
import { Matrix4 } from "../../../Source/Cesium.js";
import { TransformEditor } from "../../../Source/Cesium.js";

describe(
  "Widgets/TransformEditor/TransformEditor",
  function () {
    var scene;
    beforeAll(function () {
      scene = createScene();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("constructor sets expected values", function () {
      var transformEditor = new TransformEditor({
        container: document.body,
        scene: scene,
        transform: new Matrix4(),
        boundingSphere: new BoundingSphere(),
      });
      expect(transformEditor.container).toBe(document.body);
      expect(transformEditor.viewModel).toBeDefined();
      transformEditor.destroy();
    });

    it("constructor propagates pixelSize and maximumSizeInMeters to ViewModel", function () {
      var pixelSize = 512;
      var maximumSizeInMeters = 1024;

      var transformEditor = new TransformEditor({
        container: document.body,
        scene: scene,
        transform: new Matrix4(),
        boundingSphere: new BoundingSphere(),
        pixelSize: pixelSize,
        maximumSizeInMeters: maximumSizeInMeters,
      });

      expect(transformEditor.viewModel.pixelSize).toEqual(pixelSize);
      expect(transformEditor.viewModel.maximumSizeInMeters).toEqual(
        maximumSizeInMeters
      );
    });

    it("constructor works with string id container", function () {
      var testElement = document.createElement("span");
      testElement.id = "testElement";
      document.body.appendChild(testElement);
      var transformEditor = new TransformEditor({
        container: "testElement",
        scene: scene,
        transform: new Matrix4(),
        boundingSphere: new BoundingSphere(),
      });
      expect(transformEditor.container).toBe(testElement);
      document.body.removeChild(testElement);
      transformEditor.destroy();
    });

    it("throws if container is undefined", function () {
      expect(function () {
        return new TransformEditor({
          container: undefined,
          scene: scene,
          transform: new Matrix4(),
          boundingSphere: new BoundingSphere(),
        });
      }).toThrowDeveloperError();
    });

    it("throws if scene is undefined", function () {
      expect(function () {
        return new TransformEditor({
          container: document.body,
          scene: undefined,
          transform: new Matrix4(),
          boundingSphere: new BoundingSphere(),
        });
      }).toThrowDeveloperError();
    });

    it("throws if transform is undefined", function () {
      expect(function () {
        return new TransformEditor({
          container: document.body,
          scene: scene,
          transform: undefined,
          boundingSphere: new BoundingSphere(),
        });
      }).toThrowDeveloperError();
    });

    it("throws if boundingSphere is undefined", function () {
      expect(function () {
        return new TransformEditor({
          container: document.body,
          scene: scene,
          transform: new Matrix4(),
          boundingSphere: undefined,
        });
      }).toThrowDeveloperError();
    });
  },
  "WebGL"
);
