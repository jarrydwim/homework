import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { IntersectionTests } from "../../../Source/Cesium.js";
import { Matrix4 } from "../../../Source/Cesium.js";
import { Plane } from "../../../Source/Cesium.js";
import { Transforms } from "../../../Source/Cesium.js";
import { TranslationEditor } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";

describe("TranslationEditor", function () {
  var scene;

  beforeEach(function () {
    scene = createScene();
  });

  afterEach(function () {
    scene.destroyForSpecs();
  });

  function getOptions() {
    return {
      scene: scene,
      originOffset: new Cartesian3(),
      setPosition: jasmine.createSpy(),
      transform: Transforms.eastNorthUpToFixedFrame(
        Cartesian3.fromDegrees(0, 0)
      ),
      radius: 3,
    };
  }

  it("has expected values", function () {
    var options = getOptions();
    var editor = new TranslationEditor(options);

    expect(editor.active).toBe(false);
    expect(editor.originOffset).toEqual(options.originOffset);
    expect(scene.primitives.length).toBe(3);
    expect(editor.active).toBe(false);
  });

  it("activates and deactivates", function () {
    var editor = new TranslationEditor(getOptions());
    editor.active = true;
    expect(editor._polylineX.show).toBe(true);
    expect(editor._polylineY.show).toBe(true);
    expect(editor._polylineZ.show).toBe(true);
    editor.active = false;
    expect(editor._polylineX.show).toBe(false);
    expect(editor._polylineY.show).toBe(false);
    expect(editor._polylineZ.show).toBe(false);
  });

  it("rotation editor with pixel sizing is different than non pixel-sized scale editor", function () {
    var options = getOptions();
    var noPixelSizing = new TranslationEditor(options);
    options.pixelSize = 256;
    var pixelSizing = new TranslationEditor(options);
    var identicalMatrices = Matrix4.equalsEpsilon(
      pixelSizing._modelMatrix,
      noPixelSizing._modelMatrix,
      CesiumMath.EPSILON10
    );
    expect(identicalMatrices).toBe(false);
  });

  it("maximumSizeInMeters is respected when pixelSizing is enabled", function () {
    var options = getOptions();
    options.pixelSize = 256;
    var pixelSizingNoMaximum = new TranslationEditor(options);
    options.maximumSizeInMeters = 128;
    var pixelsizingWithMaximum = new TranslationEditor(options);

    var scalingFactorNoMaximum = Matrix4.getScale(
      pixelSizingNoMaximum._modelMatrix,
      new Cartesian3()
    );
    var scalingFactorWithMaximum = Matrix4.getScale(
      pixelsizingWithMaximum._modelMatrix,
      new Cartesian3()
    );
    var maximumNoLimit = Cartesian3.maximumComponent(scalingFactorNoMaximum);
    var maximumLimited = Cartesian3.maximumComponent(scalingFactorWithMaximum);
    expect(maximumNoLimit).toBeGreaterThan(maximumLimited);
  });

  it("modifies the model matrix on update", function () {
    var options = getOptions();
    var editor = new TranslationEditor(options);
    var oldModelMatrix = editor._modelMatrix.clone();
    options.transform = Matrix4.clone(
      Transforms.eastNorthUpToFixedFrame(Cartesian3.fromDegrees(50, 0)),
      options.transform
    );
    editor.update();
    expect(editor._modelMatrix).not.toEqual(oldModelMatrix);
  });

  it("finds the picked axis on pointer down", function () {
    spyOn(scene, "drillPick").and.returnValue([{ id: "X" }]);
    spyOn(scene.camera, "getPickRay");
    var editor = new TranslationEditor(getOptions());
    spyOn(IntersectionTests, "rayPlane").and.returnValue(editor._offsetVector);
    editor.handleLeftDown(new Cartesian2(20, 25));
    expect(editor._dragAlongVector).toBe(Cartesian3.UNIT_X);
    expect(editor._dragging).toBe(true);
  });

  it("sets longitude on X axis drag", function () {
    var options = getOptions(Cartesian3.fromDegrees(0, 0));
    var editor = new TranslationEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(6378133.5634705955, 63.86437043883924, -3.436334614717083)
    );

    editor._dragging = true;
    editor._offsetVector = new Cartesian3(
      0.2046475512906909,
      7.651613949293852,
      0.20463595211859342
    );
    editor._dragAlongVector = Cartesian3.UNIT_X;
    editor._pickingPlane = Plane.fromPointNormal(
      Cartesian3.fromDegrees(0, 0),
      Cartesian3.UNIT_Z
    );

    editor.handleMouseMove(new Cartesian2(220, 220));

    expect(options.setPosition).toHaveBeenCalled();
    expect(options.setPosition.calls.allArgs()[0][0]).toEqualEpsilon(
      Cartesian3.fromDegrees(0.0005049677831586523, 0.0, 0.0),
      CesiumMath.EPSILON8
    );
  });

  it("sets latitude on Y axis drag", function () {
    var options = getOptions();
    var editor = new TranslationEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(6378136.811657146, 6.283511914037014, 138.07853690683916),
      CesiumMath.EPSILON8
    );

    editor._dragging = true;
    editor._offsetVector = new Cartesian3(
      0.0024780798703432083,
      -0.0826739368136415,
      8.167564073598841
    );
    editor._dragAlongVector = Cartesian3.UNIT_Y;
    editor._pickingPlane = Plane.fromPointNormal(
      Cartesian3.fromDegrees(0, 0),
      Cartesian3.UNIT_Z
    );

    editor.handleMouseMove(new Cartesian2(220, 220));

    expect(options.setPosition).toHaveBeenCalled();
    expect(options.setPosition.calls.allArgs()[0][0]).toEqualEpsilon(
      Cartesian3.fromDegrees(0.0, 0.001174875185476718, 0.0),
      CesiumMath.EPSILON8
    );
  });

  it("sets height on Z axis drag", function () {
    var options = getOptions();
    var editor = new TranslationEditor(options);
    spyOn(scene.camera, "getPickRay");
    spyOn(IntersectionTests, "rayPlane").and.returnValue(
      new Cartesian3(
        6378204.676321564,
        0.0645687367328378,
        0.491702432736701676
      )
    );

    editor._dragging = true;
    editor._offsetVector = new Cartesian3(
      6.232351469807327,
      0.13702037211027118,
      1.0434345429409504
    );
    editor._dragAlongVector = Cartesian3.UNIT_Z;
    editor._pickingPlane = Plane.fromPointNormal(
      Cartesian3.fromDegrees(0, 0),
      Cartesian3.UNIT_Y
    );

    editor.handleMouseMove(new Cartesian2(220, 220));

    expect(options.setPosition).toHaveBeenCalled();
    expect(options.setPosition.calls.allArgs()[0][0]).toEqualEpsilon(
      Cartesian3.fromDegrees(0.0, 0.0, 61.44397009443492),
      CesiumMath.EPSILON8
    );
  });

  it("stops dragging on pointer up", function () {
    var editor = new TranslationEditor(getOptions());
    editor._dragging = true;
    editor.handleLeftUp();
    expect(editor._dragging).toBe(false);
  });

  it("destroys the editor", function () {
    var editor = new TranslationEditor(getOptions());
    expect(editor.isDestroyed()).toBe(false);
    editor.destroy();
    expect(scene.primitives.length).toBe(0);
    expect(editor.isDestroyed()).toBe(true);
  });
});
