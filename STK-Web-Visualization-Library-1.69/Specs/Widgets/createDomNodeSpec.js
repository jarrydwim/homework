import { createDomNode } from "../../Source/Cesium.js";

describe("Widgets/createDomNode", function () {
  it("throws without html", function () {
    expect(function () {
      return createDomNode(undefined);
    }).toThrowDeveloperError();
  });

  it("returns a single html dom element", function () {
    var html = "<div>text content</div>";
    var result = createDomNode(html);
    expect(result.textContent).toBe("text content");
    expect(result.children.length).toBe(0);
    expect(result.tagName).toEqual("DIV");
  });

  it("returns a single html dom element from text", function () {
    var html = "text content";
    var result = createDomNode(html);
    expect(result.textContent).toBe("text content");
    expect(result.children.length).toBe(0);
    expect(result.tagName).toEqual("DIV");
  });

  it("returns a single html span element", function () {
    var html = "<span>text content</span>";
    var result = createDomNode(html);
    expect(result.textContent).toBe("text content");
    expect(result.children.length).toBe(0);
    expect(result.tagName).toEqual("SPAN");
  });

  it("returns nested elements", function () {
    var html = "<div>" + "    <div></div>" + "    <div></div>" + "</div>";
    var result = createDomNode(html);
    expect(result.children.length).toBe(2);
    expect(result.tagName).toEqual("DIV");
  });

  it("returns wraps siblings and returns one element", function () {
    var html = "<div></div><div></div>";
    var result = createDomNode(html);
    expect(result.children.length).toBe(2);
    expect(result.tagName).toEqual("DIV");
  });
});
