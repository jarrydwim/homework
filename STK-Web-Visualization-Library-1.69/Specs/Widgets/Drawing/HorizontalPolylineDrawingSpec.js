import { HorizontalPolylineDrawing } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import { PolylineDrawing } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe("Widgets/Drawing/HorizontalPolylineDrawing", function () {
  var points;
  var primitives;
  var scene;

  beforeAll(function () {
    scene = createScene();
    primitives = new PrimitiveCollection();
    points = new PointPrimitiveCollection();
    scene.globe = createGlobe();
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("can create and destroy", function () {
    var drawing = new HorizontalPolylineDrawing({
      scene: scene,
      points: points,
      primitives: primitives,
    });
    expect(drawing.isDestroyed()).toBe(false);

    drawing.destroy();

    expect(drawing.isDestroyed()).toBe(true);
  });

  it("throws with no scene", function () {
    expect(function () {
      return new HorizontalPolylineDrawing({
        points: points,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("works with no globe", function () {
    var globe = scene.globe;
    scene.globe = undefined;
    expect(function () {
      return new HorizontalPolylineDrawing({
        scene: scene,
        points: points,
        primitives: primitives,
      });
    }).not.toThrowDeveloperError();
    scene.globe = globe;
  });

  it("handle click does nothing if click does not return a position", function () {
    spyOn(PolylineDrawing, "_getWorldPosition");
    var drawing = new HorizontalPolylineDrawing({
      scene: scene,
      points: points,
      primitives: primitives,
    });

    drawing.handleClick(new Cartesian2(), false);

    expect(drawing._positions.length).toBe(0);

    drawing.destroy();
  });

  it("handleMouseMove does nothing if drawing is not active", function () {
    var drawing = new HorizontalPolylineDrawing({
      scene: scene,
      points: points,
      primitives: primitives,
    });

    expect(drawing._positions).toEqual([]);
    drawing.handleMouseMove(new Cartesian2(), false);
    expect(drawing._positions).toEqual([]);

    drawing.destroy();
  });

  it("handleMouseMove draws drawing at current mouse position", function () {
    spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
      scene,
      cart2,
      primitives,
      result
    ) {
      return Cartesian3.fromDegrees(
        cart2.x,
        cart2.y,
        undefined,
        undefined,
        result
      );
    });
    var drawing = new HorizontalPolylineDrawing({
      scene: scene,
      points: points,
      primitives: primitives,
    });

    drawing.handleClick(new Cartesian2(10, 10), false);

    expect(drawing._positions.length).toEqual(1);
    drawing.handleMouseMove(new Cartesian2(12, 10), false);
    expect(drawing._polyline.positions.length).toEqual(2);
    expect(drawing._polyline.positions[1]).not.toEqual(drawing._positions[0]);

    drawing.destroy();
  });
});
