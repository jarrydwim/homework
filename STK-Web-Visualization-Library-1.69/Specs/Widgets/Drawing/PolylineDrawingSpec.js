import { PolylineDrawing } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe(
  "Widgets/Drawing/PolylineDrawing",
  function () {
    var points;
    var primitives;
    var scene;

    beforeAll(function () {
      scene = createScene();
      scene.globe = createGlobe();
      primitives = new PrimitiveCollection();
      points = new PointPrimitiveCollection();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("can create and destroy", function () {
      var drawing = new PolylineDrawing({
        scene: scene,
        points: points,
        primitives: primitives,
      });
      expect(drawing.isDestroyed()).toBe(false);
      expect(drawing._pointCollection).toBe(points);
      expect(drawing._primitives).toBe(primitives);

      drawing.destroy();

      expect(drawing.isDestroyed()).toBe(true);
    });

    it("can create and destroy with default options", function () {
      var drawing = new PolylineDrawing({
        scene: scene,
      });
      expect(drawing.isDestroyed()).toBe(false);
      expect(drawing._pointCollection).toBeDefined();
      expect(drawing._primitives).toBe(scene.primitives);

      drawing.destroy();

      expect(drawing.isDestroyed()).toBe(true);
    });

    it("throws with no scene", function () {
      expect(function () {
        return new PolylineDrawing();
      }).toThrowDeveloperError();
    });

    it("works with no globe", function () {
      var globe = scene.globe;
      scene.globe = undefined;
      expect(function () {
        return new PolylineDrawing({
          scene: scene,
        });
      }).not.toThrowDeveloperError();
      scene.globe = globe;
    });

    it("handleClick adds points on mouse click", function () {
      var position = Cartesian3.fromDegrees(-120, 30);
      spyOn(PolylineDrawing, "_getWorldPosition").and.returnValue(position);
      var drawing = new PolylineDrawing({
        scene: scene,
        points: points,
        primitives: primitives,
      });

      spyOn(drawing, "addPoint");

      drawing.handleClick(new Cartesian2());
      expect(drawing.addPoint).toHaveBeenCalledWith(position);

      drawing.destroy();
    });

    it("handleClick does not add the same point twice", function () {
      spyOn(PolylineDrawing, "_getWorldPosition").and.returnValue(
        Cartesian3.fromDegrees(-120, 30)
      );
      var drawing = new PolylineDrawing({
        scene: scene,
        points: points,
        primitives: primitives,
      });

      drawing.handleClick(new Cartesian2());
      drawing.handleClick(new Cartesian2());
      expect(drawing._positions.length).toBe(1);

      drawing.destroy();
    });

    it("handle click does nothing if click does not return a position", function () {
      spyOn(PolylineDrawing, "_getWorldPosition");
      var drawing = new PolylineDrawing({
        scene: scene,
        points: points,
        primitives: primitives,
      });

      spyOn(drawing, "addPoint");

      drawing.handleClick(new Cartesian2());
      expect(drawing.addPoint).not.toHaveBeenCalled();

      drawing.destroy();
    });

    it("handleMouseMove does nothing if drawing is not active", function () {
      var drawing = new PolylineDrawing({
        scene: scene,
        points: points,
        primitives: primitives,
      });

      expect(drawing._polyline.positions.length).toBe(0);
      drawing.handleMouseMove(new Cartesian2());
      expect(drawing._polyline.positions.length).toBe(0);

      drawing.destroy();
    });

    it("handleMouseMove draws drawing at current mouse position", function () {
      spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
        scene,
        cart2
      ) {
        return Cartesian3.fromDegrees(cart2.x, cart2.y);
      });
      var drawing = new PolylineDrawing({
        scene: scene,
        points: points,
        primitives: primitives,
      });

      drawing.handleClick(new Cartesian2(10, 10));
      drawing.handleClick(new Cartesian2(10, 30));

      expect(drawing._polyline.positions.length).toBe(2);
      drawing.handleMouseMove(new Cartesian2(12, 10));
      expect(drawing._polyline.positions.length).toBe(3);

      drawing.destroy();
    });

    it("handleDoubleClick stops drawing", function () {
      spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
        scene,
        cart2
      ) {
        return Cartesian3.fromDegrees(cart2.x, cart2.y);
      });

      var drawing = new PolylineDrawing({
        scene: scene,
        points: points,
        primitives: primitives,
      });

      drawing.handleClick(new Cartesian2(10, 10));
      drawing.handleClick(new Cartesian2(10, 20));

      drawing.handleMouseMove(new Cartesian2(12, 10));

      drawing.handleClick(new Cartesian2(12, 10));
      drawing.handleDoubleClick();

      expect(drawing._polyline.positions.length).toBe(3);

      drawing.handleMouseMove(new Cartesian2(14, 10));
      expect(drawing._polyline.positions.length).toBe(3);

      drawing.destroy();
    });
  },
  "WebGL"
);
