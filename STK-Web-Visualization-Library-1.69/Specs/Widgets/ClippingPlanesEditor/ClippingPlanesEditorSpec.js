import { ClippingPlanesEditor } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { ScreenSpaceEventType } from "../../../Source/Cesium.js";
import { Transforms } from "../../../Source/Cesium.js";
import { ClippingPlane } from "../../../Source/Cesium.js";
import { ClippingPlaneCollection } from "../../../Source/Cesium.js";
import { Globe } from "../../../Source/Cesium.js";
import { Model } from "../../../Source/Cesium.js";
import { when } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";
import Cesium3DTilesTester from "../../Cesium3DTilesTester.js";
import pollToPromise from "../../pollToPromise.js";

describe(
  "Widgets/ClippingPlanesEditor/ClippingPlanesEditor",
  function () {
    var tilesetUrl = "./Data/Cesium3DTiles/Tilesets/Tileset/tileset.json";
    var scene;
    beforeAll(function () {
      scene = createScene();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("throws without scene", function () {
      expect(function () {
        return new ClippingPlanesEditor({
          scene: undefined,
          clippingPlanes: new ClippingPlaneCollection(),
        });
      }).toThrowDeveloperError();
    });

    it("throws without clippingPlanes", function () {
      expect(function () {
        return new ClippingPlanesEditor({
          scene: scene,
          clippingPlanes: undefined,
        });
      }).toThrowDeveloperError();
    });

    it("constructs and destroys", function () {
      var clippingPlanes = new ClippingPlaneCollection();
      var tool = new ClippingPlanesEditor({
        scene: scene,
        clippingPlanes: clippingPlanes,
      });
      expect(tool.active).toBe(false);
      expect(tool.scene).toBe(scene);
      expect(tool.clippingPlanes).toBe(clippingPlanes);
      expect(tool.isDestroyed()).toBe(false);

      tool.destroy();

      expect(tool.isDestroyed()).toBe(true);
    });

    it("activates and deactivates", function () {
      var tool = new ClippingPlanesEditor({
        scene: scene,
        clippingPlanes: new ClippingPlaneCollection(),
      });
      var sseh = tool._sseh;

      expect(
        sseh.getInputAction(ScreenSpaceEventType.LEFT_DOWN)
      ).toBeUndefined();
      expect(
        sseh.getInputAction(ScreenSpaceEventType.MOUSE_MOVE)
      ).toBeUndefined();
      expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_UP)).toBeUndefined();

      tool.activate();

      expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_DOWN)).toBeDefined();
      expect(
        sseh.getInputAction(ScreenSpaceEventType.MOUSE_MOVE)
      ).toBeDefined();
      expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_UP)).toBeDefined();
      expect(tool.active).toBe(true);

      tool.deactivate();

      expect(
        sseh.getInputAction(ScreenSpaceEventType.LEFT_DOWN)
      ).toBeUndefined();
      expect(
        sseh.getInputAction(ScreenSpaceEventType.MOUSE_MOVE)
      ).toBeUndefined();
      expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_UP)).toBeUndefined();
      expect(tool.active).toBe(false);
    });

    it("works for clipping planes on a Cesium3DTilest", function () {
      var clippingPlanes = new ClippingPlaneCollection({
        planes: [new ClippingPlane(Cartesian3.UNIT_Z, -100000000.0)],
      });
      return Cesium3DTilesTester.loadTileset(scene, tilesetUrl).then(function (
        tileset
      ) {
        tileset.clippingPlanes = clippingPlanes;

        var tool = new ClippingPlanesEditor({
          scene: scene,
          clippingPlanes: clippingPlanes,
        });

        tool.activate();

        expect(tool._primitives.length).toEqual(clippingPlanes.length);

        tool.destroy();
        scene.primitives.remove(tileset);
      });
    });

    it("works for clipping planes on a Model", function () {
      var model = scene.primitives.add(
        Model.fromGltf({
          modelMatrix: Transforms.eastNorthUpToFixedFrame(
            Cartesian3.fromDegrees(0.0, 0.0, 100.0)
          ),
          url: "./Data/Models/Box/CesiumBoxTest.gltf",
          show: false,
        })
      );

      return pollToPromise(
        function () {
          // Render scene to progressively load the model
          scene.renderForSpecs();
          return model.ready;
        },
        { timeout: 10000 }
      )
        .then(function () {
          var clippingPlanes = new ClippingPlaneCollection({
            planes: [
              new ClippingPlane(Cartesian3.UNIT_X, 0.0),
              new ClippingPlane(Cartesian3.UNIT_Y, 0.0),
            ],
          });
          model.clippingPlanes = clippingPlanes;

          var tool = new ClippingPlanesEditor({
            scene: scene,
            clippingPlanes: clippingPlanes,
          });

          tool.activate();

          expect(tool._primitives.length).toEqual(clippingPlanes.length);

          tool.destroy();
          scene.primitives.remove(model);
        })
        .otherwise(function () {
          return when.reject(model);
        });
    });

    it("works for clipping planes on the globe", function () {
      scene.globe = new Globe();
      var clippingPlanes = new ClippingPlaneCollection({
        modelMatrix: Transforms.eastNorthUpToFixedFrame(
          Cartesian3.fromDegrees(-110, 30)
        ),
        planes: [
          new ClippingPlane(Cartesian3.UNIT_X, 0.0),
          new ClippingPlane(Cartesian3.UNIT_Y, 0.0),
        ],
      });
      scene.globe.clippingPlanes = clippingPlanes;

      var tool = new ClippingPlanesEditor({
        scene: scene,
        clippingPlanes: clippingPlanes,
      });

      tool.activate();

      expect(tool._primitives.length).toEqual(clippingPlanes.length);

      scene.globe = undefined;
    });
  },
  "WebGL"
);
