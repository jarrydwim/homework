import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { ClippingPlane } from "../../../Source/Cesium.js";
import { ClippingPlanePrimitive } from "../../../Source/Cesium.js";
import { HeadingPitchRange } from "../../../Source/Cesium.js";
import { Matrix4 } from "../../../Source/Cesium.js";
import { Transforms } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";

describe(
  "Widgets/ClippingPlanesEditor/ClippingPlanePrimitive",
  function () {
    var scene;

    beforeAll(function () {
      scene = createScene();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("throws with no clippingPlane", function () {
      expect(function () {
        return new ClippingPlanePrimitive({
          clippingPlane: undefined,
          transform: new Matrix4(),
          size: 30,
        });
      }).toThrowDeveloperError();
    });

    it("throws with no transform", function () {
      expect(function () {
        return new ClippingPlanePrimitive({
          clippingPlane: new ClippingPlane(Cartesian3.UNIT_X, 0.0),
          transform: undefined,
          size: 30,
        });
      }).toThrowDeveloperError();
    });

    it("throws with no size", function () {
      expect(function () {
        return new ClippingPlanePrimitive({
          clippingPlane: new ClippingPlane(Cartesian3.UNIT_X, 0.0),
          transform: new Matrix4(),
          size: undefined,
        });
      }).toThrowDeveloperError();
    });

    it("creates and destroys", function () {
      var clippingPlane = new ClippingPlane(Cartesian3.UNIT_X, 0.0);
      var position = Cartesian3.fromDegrees(30, -25);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var size = new Cartesian2(50, 50);
      var planePrimitive = new ClippingPlanePrimitive({
        clippingPlane: clippingPlane,
        transform: transform,
        size: size,
      });
      expect(planePrimitive.frontColor).toBeDefined();
      expect(planePrimitive.backColor).toBeDefined();
      expect(planePrimitive.pixelSize.x).toBeGreaterThan(0);
      expect(planePrimitive.pixelSize.y).toBeGreaterThan(0);
      expect(planePrimitive.maximumSizeInMeters.x).toBe(Infinity);
      expect(planePrimitive.maximumSizeInMeters.y).toBe(Infinity);
      expect(planePrimitive.size).toEqual(size);
      expect(planePrimitive.centerPosition).toBeDefined();
      expect(planePrimitive.transform).toEqual(transform);
      expect(planePrimitive.clippingPlane).toBe(clippingPlane);
      expect(planePrimitive.show).toBe(true);
      expect(planePrimitive.isDestroyed()).toBe(false);

      planePrimitive.destroy();

      expect(planePrimitive.isDestroyed()).toBe(true);
    });

    it("creates with options", function () {
      var clippingPlane = new ClippingPlane(Cartesian3.UNIT_X, 0.0);
      var transform = Transforms.eastNorthUpToFixedFrame(
        Cartesian3.fromDegrees(30, -25)
      );
      var size = new Cartesian2(50, 50);
      var planePrimitive = new ClippingPlanePrimitive({
        show: false,
        size: size,
        transform: transform,
        clippingPlane: clippingPlane,
      });

      expect(planePrimitive.show).toBe(false);
      expect(planePrimitive.size).toEqual(size);
      expect(planePrimitive.transform).toEqual(transform);
    });

    it("creates geometry and updates", function () {
      var size = new Cartesian2(22, 22);
      var planePrimitive = new ClippingPlanePrimitive({
        transform: Transforms.eastNorthUpToFixedFrame(
          Cartesian3.fromDegrees(30, -25)
        ),
        size: size,
        clippingPlane: new ClippingPlane(Cartesian3.UNIT_X, 0.0),
      });
      scene.primitives.add(planePrimitive);
      scene.renderForSpecs();

      expect(planePrimitive._frontPrimitive).toBeDefined();
      expect(planePrimitive._backPrimitive).toBeDefined();
      expect(planePrimitive._outlinePrimitive).toBeDefined();
      expect(scene.frameState.commandList.length).toBeGreaterThan(0);
    });

    it("hides", function () {
      var size = new Cartesian2(22, 22);
      var planePrimitive = new ClippingPlanePrimitive({
        transform: Transforms.eastNorthUpToFixedFrame(
          Cartesian3.fromDegrees(30, -25)
        ),
        size: size,
        clippingPlane: new ClippingPlane(Cartesian3.UNIT_X, 0.0),
        show: false,
      });
      planePrimitive.update();
      expect(planePrimitive._frontPrimitive).toBeUndefined();
      expect(planePrimitive._backPrimitive).toBeUndefined();
    });

    it("updates when clipping plane distance changes", function () {
      var size = new Cartesian2(22, 22);
      var clippingPlane = new ClippingPlane(Cartesian3.UNIT_X, 0.0);
      var planePrimitive = new ClippingPlanePrimitive({
        transform: Transforms.eastNorthUpToFixedFrame(
          Cartesian3.fromDegrees(30, -25)
        ),
        size: size,
        clippingPlane: clippingPlane,
      });
      scene.primitives.add(planePrimitive);
      scene.renderForSpecs();

      var primitiveFront1 = planePrimitive._frontPrimitive;
      var primitiveBack1 = planePrimitive._backPrimitive;
      expect(primitiveFront1).toBeDefined();
      expect(primitiveBack1).toBeDefined();

      clippingPlane.distance = 30.0;
      scene.renderForSpecs();

      var primitiveFront2 = planePrimitive._frontPrimitive;
      var primitiveBack2 = planePrimitive._backPrimitive;
      expect(primitiveFront2).toBeDefined();
      expect(primitiveBack2).toBeDefined();
      expect(primitiveFront2).not.toEqual(primitiveFront1);
      expect(primitiveBack2).not.toEqual(primitiveBack1);
      expect(primitiveFront1.isDestroyed()).toBe(true);
      expect(primitiveBack1.isDestroyed()).toBe(true);
    });

    it("does not make a new primitive when show changes", function () {
      var size = new Cartesian2(22, 22);
      var planePrimitive = new ClippingPlanePrimitive({
        transform: Transforms.eastNorthUpToFixedFrame(
          Cartesian3.fromDegrees(30, -25)
        ),
        size: size,
        clippingPlane: new ClippingPlane(Cartesian3.UNIT_X, 0.0),
      });
      scene.primitives.add(planePrimitive);
      scene.renderForSpecs();

      var primitive1 = planePrimitive._backPrimitive;
      expect(primitive1).toBeDefined();

      planePrimitive.show = false;
      scene.renderForSpecs();

      planePrimitive.show = true;
      scene.renderForSpecs();

      var primitive2 = planePrimitive._backPrimitive;
      expect(primitive2).toBeDefined();
      expect(primitive2).toBe(primitive1);
    });

    it("scales modelMatrix when pixelSize is enabled", function () {
      var position = Cartesian3.fromDegrees(30, -25);
      var size = new Cartesian2(22, 22);
      var clippingPlane = new ClippingPlane(Cartesian3.UNIT_X, 0.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);

      var beforePrimitive = new ClippingPlanePrimitive({
        transform: transform,
        size: size,
        clippingPlane: clippingPlane,
        pixelSize: new Cartesian3(0, 0),
      });
      var afterPrimitive = new ClippingPlanePrimitive({
        transform: transform,
        size: size,
        clippingPlane: clippingPlane,
        pixelSize: new Cartesian3(100, 100),
      });
      scene.primitives.add(beforePrimitive);
      scene.primitives.add(afterPrimitive);

      var cameraOffset = new HeadingPitchRange(0, 0, 10);
      scene.camera.lookAt(position, cameraOffset);
      scene.renderForSpecs();

      var beforeMatrix = beforePrimitive._frontPrimitive.modelMatrix;
      var afterMatrix = afterPrimitive._frontPrimitive.modelMatrix;
      var matricesSame = Matrix4.equalsEpsilon(
        beforeMatrix,
        afterMatrix,
        CesiumMath.EPSILON10
      );
      expect(matricesSame).toBeFalsy();
    });

    it("limits scale of matrix when maximumSizeInMeters is defined", function () {
      var position = Cartesian3.fromDegrees(30, -25);
      var size = new Cartesian2(22, 22);
      var clippingPlane = new ClippingPlane(Cartesian3.UNIT_X, 0.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var pixelSize = new Cartesian3(1000, 1000);

      var beforePrimitive = new ClippingPlanePrimitive({
        transform: transform,
        size: size,
        clippingPlane: clippingPlane,
        pixelSize: pixelSize,
      });
      var afterPrimitive = new ClippingPlanePrimitive({
        transform: transform,
        size: size,
        clippingPlane: clippingPlane,
        pixelSize: pixelSize,
        maximumSizeInMeters: new Cartesian3(50, 50),
      });
      scene.primitives.add(beforePrimitive);
      scene.primitives.add(afterPrimitive);

      var cameraOffset = new HeadingPitchRange(0, 0, 10);
      scene.camera.lookAt(position, cameraOffset);
      scene.renderForSpecs();

      var beforeMatrix = beforePrimitive._frontPrimitive.modelMatrix;
      var afterMatrix = afterPrimitive._frontPrimitive.modelMatrix;
      var beforeScale = Matrix4.getScale(beforeMatrix, new Cartesian3());
      var afterScale = Matrix4.getScale(afterMatrix, new Cartesian3());
      var beforeMaxScale = Cartesian3.maximumComponent(beforeScale);
      var afterMaxScale = Cartesian3.maximumComponent(afterScale);
      expect(beforeMaxScale).toBeGreaterThan(afterMaxScale);
    });

    it("scales modelMatrix non-uniformly", function () {
      var position = Cartesian3.fromDegrees(30, -25);
      var size = new Cartesian2(22, 22);
      var clippingPlane = new ClippingPlane(Cartesian3.UNIT_X, 0.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);

      var planePrimitive = new ClippingPlanePrimitive({
        transform: transform,
        size: size,
        clippingPlane: clippingPlane,
        pixelSize: new Cartesian3(1000, 10),
      });
      scene.primitives.add(planePrimitive);

      var cameraOffset = new HeadingPitchRange(0, 0, 10);
      scene.camera.lookAt(position, cameraOffset);
      scene.renderForSpecs();

      var modelMatrix = planePrimitive._frontPrimitive.modelMatrix;
      var scale = Matrix4.getScale(modelMatrix, new Cartesian3());
      expect(scale.x).toBeGreaterThan(scale.y);
    });

    it("applies maximumSizeInMeters non-uniformly", function () {
      var position = Cartesian3.fromDegrees(30, -25);
      var size = new Cartesian2(22, 22);
      var clippingPlane = new ClippingPlane(Cartesian3.UNIT_X, 0.0);
      var transform = Transforms.eastNorthUpToFixedFrame(position);
      var pixelSize = new Cartesian3(1000, 1000);

      var beforePrimitive = new ClippingPlanePrimitive({
        transform: transform,
        size: size,
        clippingPlane: clippingPlane,
        pixelSize: pixelSize,
      });
      var afterPrimitive = new ClippingPlanePrimitive({
        transform: transform,
        size: size,
        clippingPlane: clippingPlane,
        pixelSize: pixelSize,
        maximumSizeInMeters: new Cartesian3(100, 1),
      });
      scene.primitives.add(beforePrimitive);
      scene.primitives.add(afterPrimitive);

      var cameraOffset = new HeadingPitchRange(0, 0, 10);
      scene.camera.lookAt(position, cameraOffset);
      scene.renderForSpecs();

      var beforeMatrix = beforePrimitive._frontPrimitive.modelMatrix;
      var afterMatrix = afterPrimitive._frontPrimitive.modelMatrix;
      var beforeScale = Matrix4.getScale(beforeMatrix, new Cartesian3());
      var afterScale = Matrix4.getScale(afterMatrix, new Cartesian3());

      expect(beforeScale.x).toBeGreaterThan(afterScale.x);
      expect(beforeScale.y).toBeGreaterThan(afterScale.y);
      expect(afterScale.x).toBeGreaterThan(afterScale.y);
    });
  },
  "WebGL"
);
