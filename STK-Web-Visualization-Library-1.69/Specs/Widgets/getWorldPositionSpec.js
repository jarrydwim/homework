import { getWorldPosition } from "../../Source/Cesium.js";
import { Cartesian2 } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { BillboardCollection } from "../../Source/Cesium.js";
import { Cesium3DTileFeature } from "../../Source/Cesium.js";
import { Cesium3DTileset } from "../../Source/Cesium.js";
import createGlobe from "../createGlobe.js";
import createScene from "../createScene.js";

describe("Widgets/getWorldPosition", function () {
  var scene;
  beforeAll(function () {
    scene = createScene();
    scene.globe = createGlobe();
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("throws without scene", function () {
    expect(function () {
      return getWorldPosition(undefined, new Cartesian2(), new Cartesian3());
    }).toThrowDeveloperError();
  });

  it("throws without mousePosition", function () {
    expect(function () {
      return getWorldPosition(scene, undefined, new Cartesian3());
    }).toThrowDeveloperError();
  });

  it("throws without result", function () {
    expect(function () {
      return getWorldPosition(scene, new Cartesian2(), undefined);
    }).toThrowDeveloperError();
  });

  it("picks the globe", function () {
    var expected = new Cartesian3(1.0, 2.0, 3.0);
    spyOn(scene, "pick");
    spyOn(scene.globe, "pick").and.returnValue(expected);

    var result = new Cartesian3();
    var returned = getWorldPosition(scene, new Cartesian2(), result);
    expect(scene.globe.pick).toHaveBeenCalled();
    expect(result).toBe(returned);
    expect(result).toEqual(expected);
  });

  it("picks a Cesium3DTileFeature", function () {
    if (!scene.pickPositionSupported) {
      return;
    }
    var mousePosition = new Cartesian2();
    var expected = new Cartesian3(1.0, 2.0, 3.0);
    var mockFeature = new Cesium3DTileFeature({}, 3);
    spyOn(scene, "pick").and.returnValue(mockFeature);
    spyOn(scene, "pickPosition").and.returnValue(expected);

    var result = new Cartesian3();
    var returned = getWorldPosition(scene, mousePosition, result);
    expect(scene.pick).toHaveBeenCalledWith(mousePosition, 1, 1);
    expect(scene.pickPosition).toHaveBeenCalled();
    expect(result).toBe(returned);
    expect(result).toEqual(expected);
  });

  it("picks a Cesium3DTileset", function () {
    if (!scene.pickPositionSupported) {
      return;
    }

    var mousePosition = new Cartesian2();
    var expected = new Cartesian3(1.0, 2.0, 3.0);
    var tileset = new Cesium3DTileset({
      url: "invalid.json",
    });
    spyOn(scene, "pick").and.returnValue({
      primitive: tileset,
    });
    spyOn(scene, "pickPosition").and.returnValue(expected);

    var result = new Cartesian3();
    var returned = getWorldPosition(scene, mousePosition, result);
    expect(scene.pick).toHaveBeenCalledWith(mousePosition, 1, 1);
    expect(scene.pickPosition).toHaveBeenCalled();
    expect(result).toBe(returned);
    expect(result).toEqual(expected);
  });

  it("does not pick random primitive", function () {
    if (!scene.pickPositionSupported) {
      return;
    }

    var mousePosition = new Cartesian2();
    var expected = new Cartesian3(1.0, 2.0, 3.0);
    var collection = new BillboardCollection();
    var billboard = collection.add();
    spyOn(scene, "pick").and.returnValue({
      collection: collection,
      primitive: billboard,
    });
    spyOn(scene, "pickPosition").and.returnValue(expected);
    spyOn(scene.globe, "pick").and.returnValue(expected);

    var result = new Cartesian3();
    var returned = getWorldPosition(scene, mousePosition, result);
    expect(scene.pick).toHaveBeenCalledWith(mousePosition, 1, 1);
    expect(scene.pickPosition).not.toHaveBeenCalled();
    expect(scene.globe.pick).toHaveBeenCalled();
    expect(result).toBe(returned);
    expect(result).toEqual(expected);
  });
});
