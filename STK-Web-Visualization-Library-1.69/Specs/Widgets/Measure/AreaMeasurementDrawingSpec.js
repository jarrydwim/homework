import { AreaUnits } from "../../../Source/Cesium.js";
import { AreaMeasurementDrawing } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { LabelCollection } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import { SceneTransforms } from "../../../Source/Cesium.js";
import { PolygonDrawing } from "../../../Source/Cesium.js";
import { MeasureUnits } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe(
  "Widgets/Measure/AreaMeasurementDrawing",
  function () {
    var labels;
    var points;
    var primitives;
    var scene;

    // corner points of half olympic sized swimming pool
    var positions = [
      new Cartesian3(
        1222099.0051609897,
        -4740070.1605999395,
        4075158.6725652968
      ),
      new Cartesian3(
        1222095.6646850954,
        -4740086.906038893,
        4075140.3203022922
      ),
      new Cartesian3(
        1222108.6632761091,
        -4740085.962928512,
        4075137.5378793897
      ),
      new Cartesian3(1222111.3487180157, -4740069.32747863, 4075155.9588378966),
    ];

    var metersUnits = new MeasureUnits({
      areaUnits: AreaUnits.SQUARE_METERS,
    });

    beforeAll(function () {
      scene = createScene();
      scene.globe = createGlobe();
      primitives = new PrimitiveCollection();
      points = new PointPrimitiveCollection();
      labels = new LabelCollection();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("can create and destroy", function () {
      var drawing = new AreaMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
      expect(drawing.isDestroyed()).toBe(false);

      drawing.destroy();

      expect(drawing.isDestroyed()).toBe(true);
    });

    it("throws with no scene", function () {
      expect(function () {
        return new AreaMeasurementDrawing({
          units: metersUnits,
          points: points,
          labels: labels,
          primitives: primitives,
        });
      }).toThrowDeveloperError();
    });

    it("works with no globe", function () {
      var globe = scene.globe;
      scene.globe = undefined;
      expect(function () {
        return new AreaMeasurementDrawing({
          scene: scene,
          units: metersUnits,
          points: points,
          labels: labels,
          primitives: primitives,
        });
      }).not.toThrowDeveloperError();
      scene.globe = globe;
    });

    it("throws with no units", function () {
      expect(function () {
        return new AreaMeasurementDrawing({
          scene: scene,
          points: points,
          labels: labels,
          primitives: primitives,
        });
      }).toThrowDeveloperError();
    });

    it("throws with no points", function () {
      expect(function () {
        return new AreaMeasurementDrawing({
          scene: scene,
          units: metersUnits,
          labels: labels,
          primitives: primitives,
        });
      }).toThrowDeveloperError();
    });

    it("throws with no labels", function () {
      expect(function () {
        return new AreaMeasurementDrawing({
          scene: scene,
          units: metersUnits,
          points: points,
          primitives: primitives,
        });
      }).toThrowDeveloperError();
    });

    it("throws with no primitives", function () {
      expect(function () {
        return new AreaMeasurementDrawing({
          scene: scene,
          units: metersUnits,
          points: points,
          labels: labels,
        });
      }).toThrowDeveloperError();
    });

    it("updates area label text", function () {
      var drawing = new AreaMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });

      for (var i = 0; i < positions.length; i++) {
        drawing.addPoint(positions[i]);
      }

      expect(drawing._area).toEqualEpsilon(325.077465, CesiumMath.EPSILON6);
      expect(drawing._label.text).toEqual("325.08 m²");

      drawing.destroy();
    });

    it("updates area label text feet", function () {
      var drawing = new AreaMeasurementDrawing({
        scene: scene,
        units: new MeasureUnits({
          areaUnits: AreaUnits.SQUARE_FEET,
        }),
        points: points,
        labels: labels,
        primitives: primitives,
        locale: "en",
      });

      for (var i = 0; i < positions.length; i++) {
        drawing.addPoint(positions[i]);
      }

      expect(drawing._area).toEqualEpsilon(325.077465, CesiumMath.EPSILON6);
      expect(drawing._label.text).toEqual("3,499.10 sq ft");

      drawing.destroy();
    });

    it("refreshes label text on unit change", function () {
      var drawing = new AreaMeasurementDrawing({
        scene: scene,
        units: new MeasureUnits({
          areaUnits: AreaUnits.SQUARE_FEET,
        }),
        points: points,
        labels: labels,
        primitives: primitives,
        locale: "en",
      });

      for (var i = 0; i < positions.length; i++) {
        drawing.addPoint(positions[i]);
      }

      expect(drawing._area).toEqualEpsilon(325.077465, CesiumMath.EPSILON6);
      expect(drawing._label.text).toEqual("3,499.10 sq ft");

      drawing._selectedUnits.areaUnits = AreaUnits.SQUARE_METERS;
      drawing._refreshLabels();

      expect(drawing._area).toEqualEpsilon(325.077465, CesiumMath.EPSILON6);
      expect(drawing._label.text).toEqual("325.08 m²");

      drawing.destroy();
    });

    it("updateLabel sets the label position to the top position in screen coordinates", function () {
      var expectedPosition = positions[2];
      spyOn(SceneTransforms, "wgs84ToWindowCoordinates").and.callFake(function (
        scene,
        position,
        result
      ) {
        if (position === expectedPosition) {
          result.y = 30;
        } else {
          result.y = 50;
        }
        return result;
      });

      var drawing = new AreaMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });

      for (var i = 0; i < positions.length; i++) {
        drawing.addPoint(positions[i]);
      }

      drawing.updateLabel();

      expect(drawing._label.position).toEqual(expectedPosition);

      drawing.destroy();
    });

    it("reset hides the drawing", function () {
      spyOn(PolygonDrawing, "_getWorldPosition").and.callFake(function (
        scene,
        mouse,
        primitives,
        result
      ) {
        return positions[mouse.x].clone(result);
      });

      var drawing = new AreaMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });

      drawing.handleClick(new Cartesian2(0, 10));
      drawing.handleMouseMove(new Cartesian2(1, 100));
      drawing.handleClick(new Cartesian2(1, 100));
      drawing.handleMouseMove(new Cartesian2(2, 50));
      drawing.handleClick(new Cartesian2(2, 50));

      expect(drawing._polyline.show).toBe(true);
      expect(drawing._polygon.show).toBe(true);
      expect(drawing._label.show).toBe(true);
      expect(drawing._pointCollection.length).toBe(3);
      expect(drawing._points.length).toBe(3);

      drawing.reset();

      expect(drawing._polyline.show).toBe(false);
      expect(drawing._polygon.show).toBe(false);
      expect(drawing._label.show).toBe(false);
      expect(drawing._pointCollection.length).toBe(0);
      expect(drawing._points.length).toBe(0);

      drawing.destroy();
    });
  },
  "WebGL"
);
