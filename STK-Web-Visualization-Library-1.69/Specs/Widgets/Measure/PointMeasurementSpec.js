import { DistanceUnits } from "../../../Source/Cesium.js";
import { PointMeasurement } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { LabelCollection } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import { MeasureUnits } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe("Widgets/Measure/PointMeasurement", function () {
  var labels;
  var points;
  var primitives;
  var scene;

  var metersUnits = new MeasureUnits({
    distanceUnits: DistanceUnits.METERS,
  });

  var feetUnits = new MeasureUnits({
    distanceUnits: DistanceUnits.FEET,
  });

  beforeAll(function () {
    scene = createScene();
    primitives = new PrimitiveCollection();
    points = new PointPrimitiveCollection();
    labels = new LabelCollection();
    scene.globe = createGlobe();
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("can create and destroy", function () {
    var measurement = new PointMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });
    expect(measurement.icon).toBeDefined();
    expect(measurement.type).toBe("Point coordinates");
    expect(measurement.instructions).toBeDefined();
    expect(measurement.id).toBe("pointMeasurement");
    expect(measurement.isDestroyed()).toBe(false);

    measurement.destroy();

    expect(measurement.isDestroyed()).toBe(true);
  });

  it("throws with no scene", function () {
    expect(function () {
      return new PointMeasurement({
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("works with no globe", function () {
    var globe = scene.globe;
    scene.globe = undefined;
    expect(function () {
      return new PointMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).not.toThrowDeveloperError();
    scene.globe = globe;
  });

  it("throws with no units", function () {
    expect(function () {
      return new PointMeasurement({
        scene: scene,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no points", function () {
    expect(function () {
      return new PointMeasurement({
        scene: scene,
        units: metersUnits,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no labels", function () {
    expect(function () {
      return new PointMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no primitives", function () {
    expect(function () {
      return new PointMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
      });
    }).toThrowDeveloperError();
  });

  it("updates label text", function () {
    spyOn(PointMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return Cartesian3.fromDegrees(20, 0, 100, undefined, result);
    });
    spyOn(PointMeasurement, "_getSlope").and.returnValue(0);
    spyOn(PointMeasurement.prototype, "_pickPositionSupported").and.returnValue(
      true
    );
    spyOn(scene.globe, "getHeight").and.returnValue(50);

    var measurement = new PointMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleMouseMove(new Cartesian2(0, 10));

    expect(measurement._label.text).toEqual(
      "lon: 20° 0' 0.00\"\nlat: 0° 0' 0.00\"\nheight: 50.00 m\nslope: 0.000°"
    );
    expect(measurement._point.show).toBe(true);
    expect(measurement._label.show).toBe(true);

    measurement.destroy();
  });

  it("updates label text feet", function () {
    spyOn(PointMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return Cartesian3.fromDegrees(20, 0, 100, undefined, result);
    });
    spyOn(PointMeasurement, "_getSlope").and.returnValue(0);
    spyOn(PointMeasurement.prototype, "_pickPositionSupported").and.returnValue(
      true
    );
    spyOn(scene.globe, "getHeight").and.returnValue(50);

    var measurement = new PointMeasurement({
      scene: scene,
      units: feetUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleMouseMove(new Cartesian2(0, 10));

    expect(measurement._label.text).toEqual(
      "lon: 20° 0' 0.00\"\nlat: 0° 0' 0.00\"\nheight: 164.04 ft\nslope: 0.000°"
    );

    measurement.destroy();
  });

  it("refreshes label text on unit change", function () {
    spyOn(PointMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return Cartesian3.fromDegrees(20, 0, 100, undefined, result);
    });
    spyOn(PointMeasurement, "_getSlope").and.returnValue(0);
    spyOn(PointMeasurement.prototype, "_pickPositionSupported").and.returnValue(
      true
    );
    spyOn(scene.globe, "getHeight").and.returnValue(50);

    var measurement = new PointMeasurement({
      scene: scene,
      units: feetUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleMouseMove(new Cartesian2(0, 10));

    expect(measurement._label.text).toEqual(
      "lon: 20° 0' 0.00\"\nlat: 0° 0' 0.00\"\nheight: 164.04 ft\nslope: 0.000°"
    );

    measurement.selectedUnits.distanceUnits = DistanceUnits.METERS;
    measurement._refreshLabels();

    expect(measurement._label.text).toEqual(
      "lon: 20° 0' 0.00\"\nlat: 0° 0' 0.00\"\nheight: 50.00 m\nslope: 0.000°"
    );

    measurement.destroy();
  });

  it("handle click does nothing if mouse move does not return a position", function () {
    spyOn(PointMeasurement, "_getWorldPosition");
    var measurement = new PointMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleMouseMove(new Cartesian2());

    expect(measurement._point.show).toBe(false);
    expect(measurement._label.show).toBe(false);

    measurement.destroy();
  });

  it("reset hides the drawing", function () {
    spyOn(PointMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return Cartesian3.fromDegrees(0, 0, 100, undefined, result);
    });

    var measurement = new PointMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleMouseMove(new Cartesian2());

    expect(measurement._label.show).toBe(true);

    measurement.reset();

    expect(measurement._label.show).toBe(false);

    measurement.destroy();
  });
});
