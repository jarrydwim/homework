import { AreaUnits } from "../../../Source/Cesium.js";
import { DistanceUnits } from "../../../Source/Cesium.js";
import { VolumeUnits } from "../../../Source/Cesium.js";
import { MeasureViewModel } from "../../../Source/Cesium.js";
import { Ellipsoid } from "../../../Source/Cesium.js";
import { MeasureUnits } from "../../../Source/Cesium.js";
import { Globe } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";

describe(
  "Widgets/Measure/MeasureViewModel",
  function () {
    var scene;
    var ellipsoid = Ellipsoid.WGS84;
    var globe = new Globe(ellipsoid);
    beforeAll(function () {
      scene = createScene();
      scene.globe = globe;
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("constructor sets default values", function () {
      var viewModel = new MeasureViewModel({
        scene: scene,
      });
      expect(viewModel.scene).toBe(scene);
      expect(viewModel.units).toEqual(new MeasureUnits());

      expect(viewModel.expanded).toBe(false);
      expect(viewModel.instructionsVisible).toBe(false);
      expect(viewModel.selectedMeasurement).toBe(undefined);
      expect(viewModel.measurements).toBeDefined();

      expect(viewModel.isDestroyed()).toEqual(false);
      viewModel.destroy();
      expect(viewModel.isDestroyed()).toEqual(true);
    });

    it("constructor sets values", function () {
      var viewModel = new MeasureViewModel({
        scene: scene,
        units: new MeasureUnits({
          distanceUnits: DistanceUnits.FEET,
          areaUnits: AreaUnits.ACRES,
        }),
      });
      expect(viewModel.scene).toBe(scene);
      expect(viewModel.units.distanceUnits).toBe(DistanceUnits.FEET);
      expect(viewModel.units.areaUnits).toBe(AreaUnits.ACRES);
      expect(viewModel.units.volumeUnits).toBe(VolumeUnits.CUBIC_METERS);
    });

    it("throws if scene is undefined", function () {
      expect(function () {
        return new MeasureViewModel();
      }).toThrowDeveloperError();
    });

    it("activates and deactivates", function () {
      var viewModel = new MeasureViewModel({
        scene: scene,
      });
      expect(viewModel.expanded).toBe(false);
      viewModel.toggleActive();
      expect(viewModel.expanded).toBe(true);
      viewModel.toggleActive();
      expect(viewModel.expanded).toBe(false);
    });

    it("toggles instructions", function () {
      var viewModel = new MeasureViewModel({
        scene: scene,
      });
      expect(viewModel.instructionsVisible).toBe(false);
      viewModel.toggleInstructions();
      expect(viewModel.instructionsVisible).toBe(true);
      viewModel.toggleInstructions();
      expect(viewModel.instructionsVisible).toBe(false);
    });

    it("resets", function () {
      var viewModel = new MeasureViewModel({
        scene: scene,
      });
      viewModel.toggleInstructions();
      expect(viewModel.instructionsVisible).toBe(true);

      viewModel.reset();
      expect(viewModel.instructionsVisible).toBe(false);
    });

    it("sets selected measurement", function () {
      var viewModel = new MeasureViewModel({
        scene: scene,
      });
      viewModel.selectedMeasurement = viewModel.measurements[0];
      expect(viewModel._mouseHandler.selectedMeasurement).toBe(
        viewModel.measurements[0]
      );

      viewModel.selectedMeasurement = viewModel.measurements[1];
      expect(viewModel._mouseHandler.selectedMeasurement).toBe(
        viewModel.measurements[1]
      );
    });
  },
  "WebGL"
);
