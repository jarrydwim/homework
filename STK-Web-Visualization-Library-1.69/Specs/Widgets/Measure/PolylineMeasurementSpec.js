import { DistanceUnits } from "../../../Source/Cesium.js";
import { PolylineMeasurement } from "../../../Source/Cesium.js";
import { LabelCollection } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import { MeasureUnits } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe("Widgets/Measure/PolylineMeasurement", function () {
  var labels;
  var points;
  var primitives;
  var scene;

  var metersUnits = new MeasureUnits({
    distanceUnits: DistanceUnits.METERS,
  });

  beforeAll(function () {
    scene = createScene();
    scene.globe = createGlobe();
    primitives = new PrimitiveCollection();
    points = new PointPrimitiveCollection();
    labels = new LabelCollection();
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("can create and destroy", function () {
    var measurement = new PolylineMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });
    expect(measurement.icon).toBeDefined();
    expect(measurement.type).toBe("Polyline Distance");
    expect(measurement.instructions).toBeDefined();
    expect(measurement.id).toBe("polylineMeasurement");
    expect(measurement._drawing).toBeDefined();
    expect(measurement.isDestroyed()).toBe(false);

    measurement.destroy();

    expect(measurement.isDestroyed()).toBe(true);
  });

  it("throws with no scene", function () {
    expect(function () {
      return new PolylineMeasurement({
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("works with no globe", function () {
    var globe = scene.globe;
    scene.globe = undefined;
    expect(function () {
      return new PolylineMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).not.toThrowDeveloperError();
    scene.globe = globe;
  });

  it("throws with no units", function () {
    expect(function () {
      return new PolylineMeasurement({
        scene: scene,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no points", function () {
    expect(function () {
      return new PolylineMeasurement({
        scene: scene,
        units: metersUnits,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no labels", function () {
    expect(function () {
      return new PolylineMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no primitives", function () {
    expect(function () {
      return new PolylineMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
      });
    }).toThrowDeveloperError();
  });
});
