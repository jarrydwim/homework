import { Material } from "../../../Source/Cesium.js";
import { ClassificationType } from "../../../Source/Cesium.js";
import { MeasurementSettings } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Color } from "../../../Source/Cesium.js";
import { Ellipsoid } from "../../../Source/Cesium.js";
import { HorizontalOrigin } from "../../../Source/Cesium.js";
import { VerticalOrigin } from "../../../Source/Cesium.js";

describe("Widgets/Measure/MeasurementSettings", function () {
  it("creates default polyline", function () {
    var polyline = MeasurementSettings.getPolylineOptions();
    expect(polyline.width).toBe(3);
    expect(polyline.color).toBe(MeasurementSettings.color);
  });

  it("creates polyline with settings", function () {
    var defaultColor = MeasurementSettings.color;
    var newColor = Color.RED;
    MeasurementSettings.color = newColor;
    var positions = [new Cartesian3(), new Cartesian3()];

    var polyline = MeasurementSettings.getPolylineOptions({
      show: false,
      ellipsoid: Ellipsoid.UNIT_SPHERE,
      width: 5,
      id: 10,
      positions: positions,
      materialType: Material.PolylineDashType,
      loop: true,
      clampToGround: true,
      classificationType: ClassificationType.CESIUM_3D_TILE,
      allowPicking: true,
    });

    expect(polyline.show).toBe(false);
    expect(polyline.ellipsoid).toBe(Ellipsoid.UNIT_SPHERE);
    expect(polyline.width).toBe(5);
    expect(polyline.color).toEqual(newColor);
    expect(polyline.id).toBe(10);
    expect(polyline.loop).toBe(true);
    expect(polyline.materialType).toBe(Material.PolylineDashType);
    expect(polyline.positions).toEqual(positions);
    expect(polyline.clampToGround).toBe(true);
    expect(polyline.classificationType).toBe(ClassificationType.CESIUM_3D_TILE);
    expect(polyline.allowPicking).toBe(true);

    MeasurementSettings.color = defaultColor;
  });

  it("creates default polygon", function () {
    var polyline = MeasurementSettings.getPolygonOptions();
    expect(polyline.color).toBe(MeasurementSettings.color);
    expect(polyline.depthFailColor).toBe(MeasurementSettings.color);
  });

  it("creates polygon with settings", function () {
    var defaultColor = MeasurementSettings.color;
    var newColor = Color.RED;
    MeasurementSettings.color = newColor;
    var positions = [new Cartesian3(), new Cartesian3(), new Cartesian3()];

    var polygon = MeasurementSettings.getPolygonOptions({
      show: false,
      ellipsoid: Ellipsoid.UNIT_SPHERE,
      depthFailColor: Color.GREEN,
      id: 10,
      positions: positions,
      clampToGround: true,
      classificationType: ClassificationType.CESIUM_3D_TILE,
      allowPicking: true,
    });

    expect(polygon.show).toBe(false);
    expect(polygon.ellipsoid).toBe(Ellipsoid.UNIT_SPHERE);
    expect(polygon.color).toEqual(newColor);
    expect(polygon.depthFailColor).toBe(Color.GREEN);
    expect(polygon.id).toBe(10);
    expect(polygon.positions).toEqual(positions);
    expect(polygon.clampToGround).toBe(true);
    expect(polygon.classificationType).toBe(ClassificationType.CESIUM_3D_TILE);
    expect(polygon.allowPicking).toBe(true);

    MeasurementSettings.color = defaultColor;
  });

  it("creates default point", function () {
    var point = MeasurementSettings.getPointOptions();
    expect(point.pixelSize).toBe(10);
    expect(point.color).toBe(MeasurementSettings.color);
    expect(point.position).toEqual(Cartesian3.ZERO);
    expect(point.show).toBe(false);
  });

  it("creates point with settings", function () {
    var defaultColor = MeasurementSettings.color;
    var newColor = Color.RED;
    MeasurementSettings.color = newColor;

    var point = MeasurementSettings.getPointOptions();
    expect(point.pixelSize).toBe(10);
    expect(point.color).toBe(newColor);
    expect(point.position).toEqual(Cartesian3.ZERO);
    expect(point.show).toBe(false);

    MeasurementSettings.color = defaultColor;
  });

  it("creates default label", function () {
    var label = MeasurementSettings.getLabelOptions();
    expect(label.show).toBe(false);
    expect(label.font).toBe(MeasurementSettings.labelFont);
    expect(label.scale).toBe(1.0);
    expect(label.showBackground).toBe(true);
    expect(label.horizontalOrigin).toBe(HorizontalOrigin.CENTER);
    expect(label.verticalOrigin).toBe(VerticalOrigin.BOTTOM);
    expect(label.pixelOffset).toEqual(new Cartesian2(0, -9));
    expect(label.disableDepthTestDistance).toEqual(Number.POSITIVE_INFINITY);
    expect(label.position).toEqual(Cartesian3.ZERO);
  });

  it("creates label with settings", function () {
    var defaultFont = MeasurementSettings.labelFont;
    var newFont = "14pt Helvetica";
    MeasurementSettings.labelFont = newFont;

    var label = MeasurementSettings.getLabelOptions({
      scale: 0.5,
      horizontalOrigin: HorizontalOrigin.LEFT,
      verticalOrigin: VerticalOrigin.TOP,
      pixelOffset: new Cartesian2(10, 10),
    });

    expect(label.show).toBe(false);
    expect(label.font).toBe(newFont);
    expect(label.scale).toBe(0.5);
    expect(label.showBackground).toBe(true);
    expect(label.horizontalOrigin).toBe(HorizontalOrigin.LEFT);
    expect(label.verticalOrigin).toBe(VerticalOrigin.TOP);
    expect(label.pixelOffset).toEqual(new Cartesian2(10, 10));
    expect(label.disableDepthTestDistance).toEqual(Number.POSITIVE_INFINITY);
    expect(label.position).toEqual(Cartesian3.ZERO);

    MeasurementSettings.labelFont = defaultFont;
  });
});
