import { MeasureUnits } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { DistanceUnits } from "../../../Source/Cesium.js";
import { AreaUnits } from "../../../Source/Cesium.js";
import { VolumeUnits } from "../../../Source/Cesium.js";
import { AngleUnits } from "../../../Source/Cesium.js";

describe("Widgets/Measure/MeasureUnits", function () {
  function testDistanceConversion(valueA, valueB, unitA, unitB) {
    var resultB = MeasureUnits.convertDistance(valueA, unitA, unitB);
    expect(resultB).toEqualEpsilon(valueB, CesiumMath.EPSILON7);

    var resultA = MeasureUnits.convertDistance(valueB, unitB, unitA);
    expect(resultA).toEqualEpsilon(valueA, CesiumMath.EPSILON7);
  }

  function testAreaConversion(valueA, valueB, unitA, unitB) {
    var resultB = MeasureUnits.convertArea(valueA, unitA, unitB);
    expect(resultB).toEqualEpsilon(valueB, CesiumMath.EPSILON7);

    var resultA = MeasureUnits.convertArea(valueB, unitB, unitA);
    expect(resultA).toEqualEpsilon(valueA, CesiumMath.EPSILON7);
  }

  function testVolumeConversion(valueA, valueB, unitA, unitB) {
    var resultB = MeasureUnits.convertVolume(valueA, unitA, unitB);
    expect(resultB).toEqualEpsilon(valueB, CesiumMath.EPSILON7);

    var resultA = MeasureUnits.convertVolume(valueB, unitB, unitA);
    expect(resultA).toEqualEpsilon(valueA, CesiumMath.EPSILON7);
  }

  it("converts distance between meters and meters", function () {
    testDistanceConversion(
      1947.8475,
      1947.8475,
      DistanceUnits.METERS,
      DistanceUnits.METERS
    );
  });

  it("converts distance between meters and centimeters", function () {
    testDistanceConversion(
      1.9475,
      194.75,
      DistanceUnits.METERS,
      DistanceUnits.CENTIMETERS
    );
  });

  it("converts distance between meters and kilometers", function () {
    testDistanceConversion(
      1947.8475,
      1.9478475,
      DistanceUnits.METERS,
      DistanceUnits.KILOMETERS
    );
  });

  it("converts distance between meters and feet", function () {
    testDistanceConversion(
      1947.8475,
      6390.575787401574,
      DistanceUnits.METERS,
      DistanceUnits.FEET
    );
  });

  it("converts distance between meters and US Survey Feet", function () {
    testDistanceConversion(
      1947.8475,
      6390.563006250001,
      DistanceUnits.METERS,
      DistanceUnits.US_SURVEY_FEET
    );
  });

  it("converts distance between meters and inches", function () {
    testDistanceConversion(
      1.9475,
      76.6732283,
      DistanceUnits.METERS,
      DistanceUnits.INCHES
    );
  });

  it("converts distance between meters and yards", function () {
    testDistanceConversion(
      1947.8475,
      2130.19192913,
      DistanceUnits.METERS,
      DistanceUnits.YARDS
    );
  });

  it("converts distance between meters and miles", function () {
    testDistanceConversion(
      1947.5234048,
      1.21013494,
      DistanceUnits.METERS,
      DistanceUnits.MILES
    );
  });

  it("converts area between square meters and square meters", function () {
    testAreaConversion(
      1947.8475,
      1947.8475,
      AreaUnits.SQUARE_METERS,
      AreaUnits.SQUARE_METERS
    );
  });

  it("converts area between square meters and square centimeters", function () {
    testAreaConversion(
      1.0,
      10000.0,
      AreaUnits.SQUARE_METERS,
      AreaUnits.SQUARE_CENTIMETERS
    );
  });

  it("converts area between square meters and square kilometers", function () {
    testAreaConversion(
      1000000.0,
      1.0,
      AreaUnits.SQUARE_METERS,
      AreaUnits.SQUARE_KILOMETERS
    );
  });

  it("converts area between square meters and square feet", function () {
    testAreaConversion(
      1947.8475,
      20966.455995411987,
      AreaUnits.SQUARE_METERS,
      AreaUnits.SQUARE_FEET
    );
  });

  it("converts area between square meters and square inches", function () {
    testAreaConversion(
      1.0,
      1550.0031,
      AreaUnits.SQUARE_METERS,
      AreaUnits.SQUARE_INCHES
    );
  });

  it("converts area between square meters and square yards", function () {
    testAreaConversion(
      1947.8475,
      2329.6062217,
      AreaUnits.SQUARE_METERS,
      AreaUnits.SQUARE_YARDS
    );
  });

  it("converts area between square meters and square miles", function () {
    testAreaConversion(
      52841927.3205308,
      20.4023822,
      AreaUnits.SQUARE_METERS,
      AreaUnits.SQUARE_MILES
    );
  });

  it("converts area between square meters and acres", function () {
    testAreaConversion(
      23416.32531647,
      5.7863,
      AreaUnits.SQUARE_METERS,
      AreaUnits.ACRES
    );
  });

  it("converts area between square meters and hectares", function () {
    testAreaConversion(
      10000.0,
      1.0,
      AreaUnits.SQUARE_METERS,
      AreaUnits.HECTARES
    );
  });

  it("converts volume between cubic meters and cubic meters", function () {
    testVolumeConversion(
      1947.8475,
      1947.8475,
      VolumeUnits.CUBIC_METERS,
      VolumeUnits.CUBIC_METERS
    );
  });

  it("converts volume between cubic meters and cubic centimeters", function () {
    testVolumeConversion(
      1.0,
      1000000.0,
      VolumeUnits.CUBIC_METERS,
      VolumeUnits.CUBIC_CENTIMETERS
    );
  });

  it("converts volume between cubic meters and cubic kilometers", function () {
    testVolumeConversion(
      1000000000.0,
      1.0,
      VolumeUnits.CUBIC_METERS,
      VolumeUnits.CUBIC_KILOMETERS
    );
  });

  it("converts volume between cubic meters and cubic feet", function () {
    testVolumeConversion(
      1947.8475,
      68787.58528678473,
      VolumeUnits.CUBIC_METERS,
      VolumeUnits.CUBIC_FEET
    );
  });

  it("converts volume between cubic meters and cubic inches", function () {
    testVolumeConversion(
      1.0,
      61023.74409473229,
      VolumeUnits.CUBIC_METERS,
      VolumeUnits.CUBIC_INCHES
    );
  });

  it("converts volume between cubic meters and cubic yards", function () {
    testVolumeConversion(
      23.524015,
      30.76825,
      VolumeUnits.CUBIC_METERS,
      VolumeUnits.CUBIC_YARDS
    );
  });

  it("converts volume between cubic miles and cubic meters", function () {
    testVolumeConversion(
      4168181825.44058,
      1.0,
      VolumeUnits.CUBIC_METERS,
      VolumeUnits.CUBIC_MILES
    );
  });

  it("throws when converting distance to invalid unit", function () {
    expect(function () {
      return MeasureUnits.convertDistance(
        1947.8475,
        DistanceUnits.METERS,
        undefined
      );
    }).toThrowDeveloperError();
  });

  it("throws when converting area to invalid unit", function () {
    expect(function () {
      return MeasureUnits.convertDistance(
        1947.8475,
        AreaUnits.SQUARE_METERS,
        undefined
      );
    }).toThrowDeveloperError();
  });

  it("throws when converting volume to invalid unit", function () {
    expect(function () {
      return MeasureUnits.convertVolume(
        1947.8475,
        VolumeUnits.CUBIC_METERS,
        undefined
      );
    }).toThrowDeveloperError();
  });

  it("converts angle in radians to radians", function () {
    var radians = MeasureUnits.convertAngle(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.RADIANS,
      AngleUnits.RADIANS
    );
    expect(radians).toEqual(CesiumMath.PI_OVER_FOUR);
  });

  it("converts angle in radians to degrees", function () {
    var degrees = MeasureUnits.convertAngle(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.RADIANS,
      AngleUnits.DEGREES
    );
    expect(degrees).toEqual(45);
  });

  it("converts angle in radians to grade", function () {
    var grade = MeasureUnits.convertAngle(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.RADIANS,
      AngleUnits.GRADE
    );
    expect(grade).toEqualEpsilon(100, CesiumMath.EPSILON7);
  });

  it("clamps grade below 0 degrees", function () {
    var grade = MeasureUnits.convertAngle(
      -CesiumMath.PI_OVER_TWO,
      AngleUnits.RADIANS,
      AngleUnits.GRADE
    );
    expect(grade).toBe(0);
  });

  it("clamps grade above 90 degrees", function () {
    var grade = MeasureUnits.convertAngle(
      CesiumMath.PI,
      AngleUnits.RADIANS,
      AngleUnits.GRADE
    );
    expect(grade).toBe(Number.POSITIVE_INFINITY);
  });

  it("converts angle in radians to ratio", function () {
    var ratio = MeasureUnits.convertAngle(
      CesiumMath.PI_OVER_SIX,
      AngleUnits.RADIANS,
      AngleUnits.RATIO
    );
    expect(ratio).toEqualEpsilon(0.577350269, CesiumMath.EPSILON7);
  });

  it("converts angle in degrees to radians", function () {
    var radians = MeasureUnits.convertAngle(
      45,
      AngleUnits.DEGREES,
      AngleUnits.RADIANS
    );
    expect(radians).toEqual(CesiumMath.PI_OVER_FOUR);
  });

  it("converts angle in grade to radians", function () {
    var radians = MeasureUnits.convertAngle(
      100,
      AngleUnits.GRADE,
      AngleUnits.RADIANS
    );
    expect(radians).toEqual(CesiumMath.PI_OVER_FOUR);
  });

  it("converts angle in ratio to radians", function () {
    var radians = MeasureUnits.convertAngle(
      0.577350269,
      AngleUnits.RATIO,
      AngleUnits.RADIANS
    );
    expect(radians).toEqualEpsilon(CesiumMath.PI_OVER_SIX, CesiumMath.EPSILON7);
  });

  it("converts angle in degrees minutes seconds to radians", function () {
    var radians = MeasureUnits.convertAngle(
      "30° 15' 50.54\"",
      AngleUnits.DEGREES_MINUTES_SECONDS,
      AngleUnits.RADIANS
    );
    expect(radians).toEqualEpsilon(0.5282071235627174, CesiumMath.EPSILON7);
  });

  it("converts negative angle in degrees minutes seconds to radians", function () {
    var radians = MeasureUnits.convertAngle(
      "-30° 15' 50.54\"",
      AngleUnits.DEGREES_MINUTES_SECONDS,
      AngleUnits.RADIANS
    );
    expect(radians).toEqualEpsilon(-0.5282071235627174, CesiumMath.EPSILON7);
  });

  it("converts angle in degrees minutes seconds and west direction to radians", function () {
    var radians = MeasureUnits.convertAngle(
      "30° 15' 50.54\" W",
      AngleUnits.DEGREES_MINUTES_SECONDS,
      AngleUnits.RADIANS
    );
    expect(radians).toEqualEpsilon(-0.5282071235627174, CesiumMath.EPSILON7);
  });

  it("converts angle in degrees minutes seconds and east direction to radians", function () {
    var radians = MeasureUnits.convertAngle(
      "30° 15' 50.54\" E",
      AngleUnits.DEGREES_MINUTES_SECONDS,
      AngleUnits.RADIANS
    );
    expect(radians).toEqualEpsilon(0.5282071235627174, CesiumMath.EPSILON7);
  });

  it("converts angle in degrees minutes seconds and south direction to radians", function () {
    var radians = MeasureUnits.convertAngle(
      "30° 15' 50.54\" S",
      AngleUnits.DEGREES_MINUTES_SECONDS,
      AngleUnits.RADIANS
    );
    expect(radians).toEqualEpsilon(-0.5282071235627174, CesiumMath.EPSILON7);
  });

  it("converts angle in degrees minutes seconds and north direction to radians", function () {
    var radians = MeasureUnits.convertAngle(
      "30° 15' 50.54\" N",
      AngleUnits.DEGREES_MINUTES_SECONDS,
      AngleUnits.RADIANS
    );
    expect(radians).toEqualEpsilon(0.5282071235627174, CesiumMath.EPSILON7);
  });

  it("throws when converting degrees minutes seconds to radians when value is invalid", function () {
    expect(function () {
      return MeasureUnits.convertAngle(
        '-30.0"',
        AngleUnits.DEGREES_MINUTES_SECONDS,
        AngleUnits.RADIANS
      );
    }).toThrowRuntimeError();
  });

  it("converts angle in grade to degrees", function () {
    var degrees = MeasureUnits.convertAngle(
      100,
      AngleUnits.GRADE,
      AngleUnits.DEGREES
    );
    expect(degrees).toEqualEpsilon(45, CesiumMath.EPSILON7);
  });

  it("throws when converting angle to invalid unit", function () {
    expect(function () {
      return MeasureUnits.convertAngle(
        CesiumMath.PI_OVER_FOUR,
        AngleUnits.RADIANS,
        undefined
      );
    }).toThrowDeveloperError();
  });

  it("formats numbers", function () {
    var string = MeasureUnits.numberToString(1947.8475, "en");
    expect(string).toBe("1,947.85");
  });

  it("formats very small negative numbers as non-negative zero", function () {
    var string = MeasureUnits.numberToString(-0.001, "en");
    expect(string).toBe("0.00");

    string = MeasureUnits.numberToString(-0.0001, "en", 3);
    expect(string).toBe("0.000");

    var formatOptions = { maximumFractionDigits: 4, minimumFractionDigits: 3 };
    string = MeasureUnits.distanceToString(
      -0.00001,
      DistanceUnits.METERS,
      "en",
      formatOptions
    );
    expect(string).toBe("0.000 m");
  });

  it("formats numbers with locale", function () {
    var string = MeasureUnits.numberToString(1947.8475, "de-DE");
    expect(string).toBe("1.947,85");
  });

  it("formats numbers with options as number", function () {
    var string = MeasureUnits.numberToString(1947.8475, "en", 5);
    expect(string).toBe("1,947.84750");
  });

  it("formats numbers with options as object", function () {
    var options = { maximumFractionDigits: 5, minimumFractionDigits: 0 };
    var string = MeasureUnits.numberToString(1947.843, "en", options);
    expect(string).toBe("1,947.843");
  });

  it("formats numbers with options as function", function () {
    var formatter = function (number) {
      if (number <= 10) {
        return { maximumFractionDigits: 2, minimumFractionDigits: 2 };
      }
      return { maximumFractionDigits: 0 };
    };

    var string = MeasureUnits.numberToString(1947.8421, "en", formatter);
    expect(string).toBe("1,948");

    string = MeasureUnits.numberToString(7.8421, "en", formatter);
    expect(string).toBe("7.84");
  });

  it("formats distance in meters", function () {
    var string = MeasureUnits.distanceToString(
      1947.8475,
      DistanceUnits.METERS,
      "en"
    );
    expect(string).toBe("1,947.85 m");
  });

  it("formats distance in centimeters", function () {
    var string = MeasureUnits.distanceToString(
      19.478475,
      DistanceUnits.CENTIMETERS,
      "en"
    );
    expect(string).toBe("1,947.85 cm");
  });

  it("formats distance in kilometers", function () {
    var string = MeasureUnits.distanceToString(
      1947.8475,
      DistanceUnits.KILOMETERS,
      "en"
    );
    expect(string).toBe("1.95 km");
  });

  it("formats distance in feet", function () {
    var string = MeasureUnits.distanceToString(
      1947.8475,
      DistanceUnits.FEET,
      "en"
    );
    expect(string).toBe("6,390.58 ft");
  });

  it("formats distance in US Survey Feet", function () {
    var string = MeasureUnits.distanceToString(
      1947.8475,
      DistanceUnits.US_SURVEY_FEET,
      "en"
    );
    expect(string).toBe("6,390.56 ft");
  });

  it("formats distance in inches", function () {
    var string = MeasureUnits.distanceToString(1.0, DistanceUnits.INCHES, "en");
    expect(string).toBe("39.37 in");
  });

  it("formats distance in yards", function () {
    var string = MeasureUnits.distanceToString(
      1947.8475,
      DistanceUnits.YARDS,
      "en"
    );
    expect(string).toBe("2,130.19 yd");
  });

  it("formats distance in miles", function () {
    var string = MeasureUnits.distanceToString(
      1947.8475,
      DistanceUnits.MILES,
      "en"
    );
    expect(string).toBe("1.21 mi");
  });

  it("formats distance with locale", function () {
    var string = MeasureUnits.distanceToString(
      1947.8475,
      DistanceUnits.FEET,
      "de-DE"
    );
    expect(string).toBe("6.390,58 ft");
  });

  it("formats distance with fixed digits", function () {
    var string = MeasureUnits.distanceToString(
      1947.8475,
      DistanceUnits.FEET,
      "en",
      5
    );
    expect(string).toBe("6,390.57579 ft");
  });

  it("formats area in square meters", function () {
    var string = MeasureUnits.areaToString(
      1947.8475,
      AreaUnits.SQUARE_METERS,
      "en"
    );
    expect(string).toBe("1,947.85 m²");
  });

  it("formats area in square centimeters", function () {
    var string = MeasureUnits.areaToString(
      0.19478475,
      AreaUnits.SQUARE_CENTIMETERS,
      "en"
    );
    expect(string).toBe("1,947.85 cm²");
  });

  it("formats area in square kilometers", function () {
    var string = MeasureUnits.areaToString(
      19478475,
      AreaUnits.SQUARE_KILOMETERS,
      "en"
    );
    expect(string).toBe("19.48 km²");
  });

  it("formats area in square feet", function () {
    var string = MeasureUnits.areaToString(
      1947.8475,
      AreaUnits.SQUARE_FEET,
      "en"
    );
    expect(string).toBe("20,966.46 sq ft");
  });

  it("formats area in square inches", function () {
    var string = MeasureUnits.areaToString(1.0, AreaUnits.SQUARE_INCHES, "en");
    expect(string).toBe("1,550.00 sq in");
  });

  it("formats area in square yards", function () {
    var string = MeasureUnits.areaToString(100.0, AreaUnits.SQUARE_YARDS, "en");
    expect(string).toBe("119.60 sq yd");
  });

  it("formats area in square miles", function () {
    var string = MeasureUnits.areaToString(
      52841927.3205308,
      AreaUnits.SQUARE_MILES,
      "en"
    );
    expect(string).toBe("20.40 sq mi");
  });

  it("formats area in acres", function () {
    var string = MeasureUnits.areaToString(
      23416.32531647,
      AreaUnits.ACRES,
      "en"
    );
    expect(string).toBe("5.79 ac");
  });

  it("formats area in hectares", function () {
    var string = MeasureUnits.areaToString(10000.0, AreaUnits.HECTARES, "en");
    expect(string).toBe("1.00 ha");
  });

  it("formats area with locale", function () {
    var string = MeasureUnits.areaToString(
      1947.8475,
      AreaUnits.SQUARE_FEET,
      "de-DE"
    );
    expect(string).toBe("20.966,46 sq ft");
  });

  it("formats area with fixed digits", function () {
    var string = MeasureUnits.areaToString(
      1947.8475,
      AreaUnits.SQUARE_FEET,
      "en",
      5
    );
    expect(string).toBe("20,966.45600 sq ft");
  });

  it("formats volume in cubic meters", function () {
    var string = MeasureUnits.volumeToString(
      1947.8475,
      VolumeUnits.CUBIC_METERS,
      "en"
    );
    expect(string).toBe("1,947.85 m³");
  });

  it("formats volume in cubic centimeters", function () {
    var string = MeasureUnits.volumeToString(
      0.0019478475,
      VolumeUnits.CUBIC_CENTIMETERS,
      "en"
    );
    expect(string).toBe("1,947.85 cm³");
  });

  it("formats volume in cubic kilometers", function () {
    var string = MeasureUnits.volumeToString(
      19478475000,
      VolumeUnits.CUBIC_KILOMETERS,
      "en"
    );
    expect(string).toBe("19.48 km³");
  });

  it("formats volume in cubic feet", function () {
    var string = MeasureUnits.volumeToString(
      1947.8475,
      VolumeUnits.CUBIC_FEET,
      "en"
    );
    expect(string).toBe("68,787.59 cu ft");
  });

  it("formats volume in cubic inches", function () {
    var string = MeasureUnits.volumeToString(
      1.0,
      VolumeUnits.CUBIC_INCHES,
      "en"
    );
    expect(string).toBe("61,023.74 cu in");
  });

  it("formats volume in cubic yards", function () {
    var string = MeasureUnits.volumeToString(
      23.524015,
      VolumeUnits.CUBIC_YARDS,
      "en"
    );
    expect(string).toBe("30.77 cu yd");
  });

  it("formats volume in cubic miles", function () {
    var string = MeasureUnits.volumeToString(
      4168181825.44058,
      VolumeUnits.CUBIC_MILES,
      "en"
    );
    expect(string).toBe("1.00 cu mi");
  });

  it("formats volume with locale", function () {
    var string = MeasureUnits.volumeToString(
      1947.8475,
      VolumeUnits.CUBIC_FEET,
      "de-DE"
    );
    expect(string).toBe("68.787,59 cu ft");
  });

  it("formats volume with fixed digits", function () {
    var string = MeasureUnits.volumeToString(
      1947.8475,
      VolumeUnits.CUBIC_FEET,
      "en",
      5
    );
    expect(string).toBe("68,787.58529 cu ft");
  });

  it("formats angle as degrees", function () {
    var string = MeasureUnits.angleToString(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.DEGREES,
      "en"
    );
    expect(string).toBe("45.00°");
  });

  it("formats angle as degrees minutes seconds", function () {
    var string = MeasureUnits.angleToString(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.DEGREES_MINUTES_SECONDS,
      "en"
    );
    expect(string).toBe("45° 0' 0.00\"");
  });

  it("formats angle as radians", function () {
    var string = MeasureUnits.angleToString(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.RADIANS,
      "en"
    );
    expect(string).toBe("0.79 rad");
  });

  it("formats angle as grade", function () {
    var string = MeasureUnits.angleToString(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.GRADE,
      "en"
    );
    expect(string).toBe("100.00%");
  });

  it("formats grade as Infinity if angle is near 90 degrees", function () {
    var string = MeasureUnits.angleToString(
      CesiumMath.PI_OVER_TWO,
      AngleUnits.GRADE,
      "en"
    );
    expect(string).toBe("∞%");
  });

  it("formats angle as ratio", function () {
    var string = MeasureUnits.angleToString(
      CesiumMath.PI_OVER_SIX,
      AngleUnits.RATIO,
      "en"
    );
    expect(string).toBe("1:1.73");
  });

  it("formats angle with locale", function () {
    var string = MeasureUnits.angleToString(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.DEGREES,
      "de-DE"
    );
    expect(string).toBe("45,00°");
  });

  it("formats angle with fixed digits", function () {
    var string = MeasureUnits.angleToString(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.DEGREES,
      "en",
      5
    );
    expect(string).toBe("45.00000°");
  });

  it("formats longitude as degrees minutes seconds (East)", function () {
    var string = MeasureUnits.longitudeToString(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.DEGREES_MINUTES_SECONDS,
      "en",
      5
    );
    expect(string).toBe("45° 0' 0.00000\" E");
  });

  it("formats longitude as degrees minutes seconds (West)", function () {
    var string = MeasureUnits.longitudeToString(
      -CesiumMath.PI_OVER_FOUR,
      AngleUnits.DEGREES_MINUTES_SECONDS,
      "en",
      5
    );
    expect(string).toBe("45° 0' 0.00000\" W");
  });

  it("formats latitude as degrees minutes seconds (North)", function () {
    var string = MeasureUnits.latitudeToString(
      CesiumMath.PI_OVER_FOUR,
      AngleUnits.DEGREES_MINUTES_SECONDS,
      "en",
      5
    );
    expect(string).toBe("45° 0' 0.00000\" N");
  });

  it("formats latitude as degrees minutes seconds (South)", function () {
    var string = MeasureUnits.latitudeToString(
      -CesiumMath.PI_OVER_FOUR,
      AngleUnits.DEGREES_MINUTES_SECONDS,
      "en",
      5
    );
    expect(string).toBe("45° 0' 0.00000\" S");
  });

  it("gets meters distance symbol", function () {
    var string = MeasureUnits.getDistanceUnitSymbol(DistanceUnits.METERS);
    expect(string).toBe("m");
  });

  it("gets centimeters distance symbol", function () {
    var string = MeasureUnits.getDistanceUnitSymbol(DistanceUnits.CENTIMETERS);
    expect(string).toBe("cm");
  });

  it("gets kilometers distance symbol", function () {
    var string = MeasureUnits.getDistanceUnitSymbol(DistanceUnits.KILOMETERS);
    expect(string).toBe("km");
  });

  it("gets feet distance symbol", function () {
    var string = MeasureUnits.getDistanceUnitSymbol(DistanceUnits.FEET);
    expect(string).toBe("ft");
  });

  it("gets US Survey Feet distance symbol", function () {
    var string = MeasureUnits.getDistanceUnitSymbol(
      DistanceUnits.US_SURVEY_FEET
    );
    expect(string).toBe("ft");
  });

  it("gets inches distance symbol", function () {
    var string = MeasureUnits.getDistanceUnitSymbol(DistanceUnits.INCHES);
    expect(string).toBe("in");
  });

  it("gets yards distance symbol", function () {
    var string = MeasureUnits.getDistanceUnitSymbol(DistanceUnits.YARDS);
    expect(string).toBe("yd");
  });

  it("gets miles distance symbol", function () {
    var string = MeasureUnits.getDistanceUnitSymbol(DistanceUnits.MILES);
    expect(string).toBe("mi");
  });

  it("gets square meters area symbol", function () {
    var string = MeasureUnits.getAreaUnitSymbol(AreaUnits.SQUARE_METERS);
    expect(string).toBe("m²");
  });

  it("gets square centimeters area symbol", function () {
    var string = MeasureUnits.getAreaUnitSymbol(AreaUnits.SQUARE_CENTIMETERS);
    expect(string).toBe("cm²");
  });

  it("gets square kilometers area symbol", function () {
    var string = MeasureUnits.getAreaUnitSymbol(AreaUnits.SQUARE_KILOMETERS);
    expect(string).toBe("km²");
  });

  it("gets square feet area symbol", function () {
    var string = MeasureUnits.getAreaUnitSymbol(AreaUnits.SQUARE_FEET);
    expect(string).toBe("sq ft");
  });

  it("gets square inches area symbol", function () {
    var string = MeasureUnits.getAreaUnitSymbol(AreaUnits.SQUARE_INCHES);
    expect(string).toBe("sq in");
  });

  it("gets square yards area symbol", function () {
    var string = MeasureUnits.getAreaUnitSymbol(AreaUnits.SQUARE_YARDS);
    expect(string).toBe("sq yd");
  });

  it("gets square miles area symbol", function () {
    var string = MeasureUnits.getAreaUnitSymbol(AreaUnits.SQUARE_MILES);
    expect(string).toBe("sq mi");
  });

  it("gets acres area symbol", function () {
    var string = MeasureUnits.getAreaUnitSymbol(AreaUnits.ACRES);
    expect(string).toBe("ac");
  });

  it("gets hectares area symbol", function () {
    var string = MeasureUnits.getAreaUnitSymbol(AreaUnits.HECTARES);
    expect(string).toBe("ha");
  });

  it("gets cubic meters volume symbol", function () {
    var string = MeasureUnits.getVolumeUnitSymbol(VolumeUnits.CUBIC_METERS);
    expect(string).toBe("m³");
  });

  it("gets cubic centimeters volume symbol", function () {
    var string = MeasureUnits.getVolumeUnitSymbol(
      VolumeUnits.CUBIC_CENTIMETERS
    );
    expect(string).toBe("cm³");
  });

  it("gets cubic kilometers volume symbol", function () {
    var string = MeasureUnits.getVolumeUnitSymbol(VolumeUnits.CUBIC_KILOMETERS);
    expect(string).toBe("km³");
  });

  it("gets cubic feet volume symbol", function () {
    var string = MeasureUnits.getVolumeUnitSymbol(VolumeUnits.CUBIC_FEET);
    expect(string).toBe("cu ft");
  });

  it("gets cubic inches volume symbol", function () {
    var string = MeasureUnits.getVolumeUnitSymbol(VolumeUnits.CUBIC_INCHES);
    expect(string).toBe("cu in");
  });

  it("gets cubic yards volume symbol", function () {
    var string = MeasureUnits.getVolumeUnitSymbol(VolumeUnits.CUBIC_YARDS);
    expect(string).toBe("cu yd");
  });

  it("gets cubic miles volume symbol", function () {
    var string = MeasureUnits.getVolumeUnitSymbol(VolumeUnits.CUBIC_MILES);
    expect(string).toBe("cu mi");
  });

  it("throws if distanceUnits is undefined", function () {
    expect(function () {
      return MeasureUnits.getDistanceUnitSymbol(undefined);
    }).toThrowDeveloperError();
  });

  it("throws if distanceUnits is invalid", function () {
    expect(function () {
      return MeasureUnits.getDistanceUnitSymbol("invalid");
    }).toThrowDeveloperError();
  });

  it("throws if areaUnits is undefined", function () {
    expect(function () {
      return MeasureUnits.getAreaUnitSymbol(undefined);
    }).toThrowDeveloperError();
  });

  it("throws if areaUnits is invalid", function () {
    expect(function () {
      return MeasureUnits.getAreaUnitSymbol("invalid");
    }).toThrowDeveloperError();
  });

  it("throws if volumeUnits is undefined", function () {
    expect(function () {
      return MeasureUnits.getVolumeUnitSymbol(undefined);
    }).toThrowDeveloperError();
  });

  it("throws if volumeUnits is invalid", function () {
    expect(function () {
      return MeasureUnits.getVolumeUnitSymbol("invalid");
    }).toThrowDeveloperError();
  });

  it("gets degrees symbol", function () {
    var string = MeasureUnits.getAngleUnitSymbol(AngleUnits.DEGREES);
    expect(string).toBe("°");
  });

  it("gets radians symbol", function () {
    var string = MeasureUnits.getAngleUnitSymbol(AngleUnits.RADIANS, 2);
    expect(string).toBe("rad");
  });

  it("gets grade symbol", function () {
    var string = MeasureUnits.getAngleUnitSymbol(AngleUnits.GRADE, 3);
    expect(string).toBe("%");
  });

  it("throws if angleUnits is undefined", function () {
    expect(function () {
      return MeasureUnits.getAngleUnitSymbol(undefined);
    }).toThrowDeveloperError();
  });

  it("throws if distanceUnits is invalid", function () {
    expect(function () {
      return MeasureUnits.getAngleUnitSymbol(AngleUnits.RATIO);
    }).toThrowDeveloperError();
  });
});
