import { DistanceUnits } from "../../../Source/Cesium.js";
import { DistanceMeasurement } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { HorizontalOrigin } from "../../../Source/Cesium.js";
import { LabelCollection } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import { SceneTransforms } from "../../../Source/Cesium.js";
import { MeasureUnits } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe("Widgets/Measure/DistanceMeasurement", function () {
  var labels;
  var points;
  var primitives;
  var scene;

  // corner points of half olympic sized swimming pool
  var positions = [
    new Cartesian3(1222099.0051609897, -4740070.1605999395, 4075158.6725652968),
    new Cartesian3(1222095.6646850954, -4740086.906038893, 4075140.3203022922),
  ];

  var metersUnits = new MeasureUnits({
    distanceUnits: DistanceUnits.METERS,
  });

  beforeAll(function () {
    scene = createScene();
    scene.globe = createGlobe();
    primitives = new PrimitiveCollection();
    points = new PointPrimitiveCollection();
    labels = new LabelCollection();
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("can create and destroy", function () {
    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });
    expect(measurement.icon).toBeDefined();
    expect(measurement.type).toBe("Distance");
    expect(measurement.instructions).toBeDefined();
    expect(measurement.id).toBe("distanceMeasurement");
    expect(measurement.showComponentLines).toBe(false);
    expect(measurement.isDestroyed()).toBe(false);

    measurement.destroy();

    expect(measurement.isDestroyed()).toBe(true);
  });

  it("throws with no scene", function () {
    expect(function () {
      return new DistanceMeasurement({
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("works with no globe", function () {
    var globe = scene.globe;
    scene.globe = undefined;
    expect(function () {
      return new DistanceMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).not.toThrowDeveloperError();
    scene.globe = globe;
  });

  it("throws with no units", function () {
    expect(function () {
      return new DistanceMeasurement({
        scene: scene,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no points", function () {
    expect(function () {
      return new DistanceMeasurement({
        scene: scene,
        units: metersUnits,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no labels", function () {
    expect(function () {
      return new DistanceMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no primitives", function () {
    expect(function () {
      return new DistanceMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
      });
    }).toThrowDeveloperError();
  });

  it("sets showComponentLines from options", function () {
    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
      showComponentLines: true,
    });
    expect(measurement.showComponentLines).toBe(true);

    measurement.destroy();
  });

  it("sets showComponentLines", function () {
    var positions = Cartesian3.fromDegreesArrayHeights([
      -120,
      50,
      0,
      -120.2,
      50,
      10,
    ]);
    spyOn(DistanceMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      primitives,
      result
    ) {
      return positions[mouse.x].clone(result);
    });

    spyOn(SceneTransforms, "wgs84ToWindowCoordinates").and.callFake(function (
      scene,
      position,
      result
    ) {
      if (Cartesian3.equals(position, positions[0])) {
        result.y = 30;
      } else {
        result.y = 50;
      }
      return result;
    });

    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));
    measurement.handleMouseMove(new Cartesian2(1, 100));
    measurement.handleClick(new Cartesian2(1, 100));

    expect(measurement._xyBox.show).toBe(false);
    measurement.showComponentLines = true;
    expect(measurement._xyBox.show).toBe(true);

    measurement.destroy();
  });

  it("updates label text", function () {
    spyOn(DistanceMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      primitives,
      result
    ) {
      return positions[mouse.x].clone(result);
    });
    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));
    measurement.handleMouseMove(new Cartesian2(1, 100));
    measurement.handleClick(new Cartesian2(1, 100));

    expect(measurement._distance).toEqualEpsilon(
      25.06739041,
      CesiumMath.EPSILON6
    );
    expect(measurement._label.text).toEqual("25.07 m");

    measurement.destroy();
  });

  it("updates label text feet", function () {
    spyOn(DistanceMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      primitives,
      result
    ) {
      return positions[mouse.x].clone(result);
    });
    var measurement = new DistanceMeasurement({
      scene: scene,
      units: new MeasureUnits({
        distanceUnits: DistanceUnits.FEET,
      }),
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));
    measurement.handleMouseMove(new Cartesian2(1, 100));
    measurement.handleClick(new Cartesian2(1, 100));

    expect(measurement._distance).toEqualEpsilon(
      25.06739041,
      CesiumMath.EPSILON6
    );
    expect(measurement._label.text).toEqual("82.24 ft");

    measurement.destroy();
  });

  it("updates label text", function () {
    spyOn(DistanceMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      primitives,
      result
    ) {
      return positions[mouse.x].clone(result);
    });
    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));
    measurement.handleMouseMove(new Cartesian2(1, 100));
    measurement.handleClick(new Cartesian2(1, 100));

    expect(measurement._distance).toEqualEpsilon(
      25.06739041,
      CesiumMath.EPSILON6
    );
    expect(measurement._label.text).toEqual("25.07 m");

    measurement.destroy();
  });

  it("refreshes label text on unit change", function () {
    spyOn(DistanceMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      primitives,
      result
    ) {
      return positions[mouse.x].clone(result);
    });
    var measurement = new DistanceMeasurement({
      scene: scene,
      units: new MeasureUnits({
        distanceUnits: DistanceUnits.FEET,
      }),
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));
    measurement.handleMouseMove(new Cartesian2(1, 100));
    measurement.handleClick(new Cartesian2(1, 100));

    expect(measurement._distance).toEqualEpsilon(
      25.06739041,
      CesiumMath.EPSILON6
    );
    expect(measurement._label.text).toEqual("82.24 ft");

    measurement._selectedUnits.distanceUnits = DistanceUnits.METERS;
    measurement._refreshLabels();

    expect(measurement._label.text).toEqual("25.07 m");

    measurement.destroy();
  });

  it("updateLabelPosition sets the label origin based on the line angle", function () {
    spyOn(DistanceMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      primitives,
      result
    ) {
      return positions[mouse.x].clone(result);
    });

    spyOn(SceneTransforms, "wgs84ToWindowCoordinates").and.callFake(function (
      scene,
      position,
      result
    ) {
      if (Cartesian3.equals(position, positions[0])) {
        result.y = 30;
      } else {
        result.y = 50;
      }
      return result;
    });

    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));
    measurement.handleMouseMove(new Cartesian2(1, 100));
    measurement.handleClick(new Cartesian2(1, 100));

    measurement._updateLabelPosition();

    expect(measurement._label.horizontalOrigin).toEqual(HorizontalOrigin.RIGHT);

    measurement.destroy();
  });

  it("updateLabelPosition sets the label origin based on the line angle", function () {
    spyOn(DistanceMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      primitives,
      result
    ) {
      return positions[mouse.x].clone(result);
    });

    spyOn(SceneTransforms, "wgs84ToWindowCoordinates").and.callFake(function (
      scene,
      position,
      result
    ) {
      if (Cartesian3.equals(position, positions[1])) {
        result.y = 30;
      } else {
        result.y = 50;
      }
      return result;
    });

    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));
    measurement.handleMouseMove(new Cartesian2(1, 100));
    measurement.handleClick(new Cartesian2(1, 100));

    measurement._updateLabelPosition();

    expect(measurement._label.horizontalOrigin).toEqual(HorizontalOrigin.LEFT);

    measurement.destroy();
  });

  it("handle click does nothing if click does not return a position", function () {
    spyOn(DistanceMeasurement, "_getWorldPosition");
    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2());

    expect(measurement._startPoint.show).toBe(false);

    measurement.destroy();
  });

  it("handleMouseMove does nothing if drawing is not active", function () {
    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    expect(measurement._positions[1]).toEqual(Cartesian3.ZERO);
    measurement.handleMouseMove(new Cartesian2());
    expect(measurement._positions[1]).toEqual(Cartesian3.ZERO);

    measurement.destroy();
  });

  it("handleMouseMove draws measurement at current mouse position", function () {
    spyOn(DistanceMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      cart2,
      primitives,
      result
    ) {
      return Cartesian3.fromDegrees(
        cart2.x,
        cart2.y,
        undefined,
        undefined,
        result
      );
    });
    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(10, 10));

    expect(measurement._positions[1]).toEqual(measurement._positions[0]);
    measurement.handleMouseMove(new Cartesian2(12, 10));
    expect(measurement._positions[1]).not.toEqual(measurement._positions[0]);

    measurement.destroy();
  });

  it("reset hides the drawing", function () {
    spyOn(DistanceMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      cart2,
      primitives,
      result
    ) {
      return Cartesian3.fromDegrees(
        cart2.x,
        cart2.y,
        undefined,
        undefined,
        result
      );
    });

    var measurement = new DistanceMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2());

    expect(measurement._polyline.show).toBe(true);

    measurement.reset();

    expect(measurement._polyline.show).toBe(false);

    measurement.destroy();
  });
});
