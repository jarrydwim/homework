import { MeasurementMouseHandler } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { KeyboardEventModifier } from "../../../Source/Cesium.js";
import { ScreenSpaceEventType } from "../../../Source/Cesium.js";
import createScene from "../../createScene.js";

describe("Widgets/Measure/MeasurementMouseHandler", function () {
  var scene;
  beforeAll(function () {
    scene = createScene();
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("throws without scene", function () {
    expect(function () {
      return new MeasurementMouseHandler();
    }).toThrowDeveloperError();
  });

  it("constructs and destroys", function () {
    var handler = new MeasurementMouseHandler(scene);
    expect(handler.selectedMeasurement).toBeUndefined();
    expect(handler.scene).toBe(scene);
    expect(handler.isDestroyed()).toBe(false);

    handler.destroy();

    expect(handler.isDestroyed()).toBe(true);
  });

  it("activates and deactivates", function () {
    var handler = new MeasurementMouseHandler(scene);
    var sseh = handler._sseh;

    expect(
      sseh.getInputAction(ScreenSpaceEventType.LEFT_CLICK)
    ).toBeUndefined();
    expect(
      sseh.getInputAction(
        ScreenSpaceEventType.LEFT_CLICK,
        KeyboardEventModifier.SHIFT
      )
    ).toBeUndefined();
    expect(
      sseh.getInputAction(ScreenSpaceEventType.MOUSE_MOVE)
    ).toBeUndefined();
    expect(
      sseh.getInputAction(
        ScreenSpaceEventType.MOUSE_MOVE,
        KeyboardEventModifier.SHIFT
      )
    ).toBeUndefined();
    expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_DOWN)).toBeUndefined();
    expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_UP)).toBeUndefined();
    expect(
      sseh.getInputAction(ScreenSpaceEventType.LEFT_DOUBLE_CLICK)
    ).toBeUndefined();

    handler.activate();

    expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_CLICK)).toBeDefined();
    expect(
      sseh.getInputAction(
        ScreenSpaceEventType.LEFT_CLICK,
        KeyboardEventModifier.SHIFT
      )
    ).toBeDefined();
    expect(sseh.getInputAction(ScreenSpaceEventType.MOUSE_MOVE)).toBeDefined();
    expect(
      sseh.getInputAction(
        ScreenSpaceEventType.MOUSE_MOVE,
        KeyboardEventModifier.SHIFT
      )
    ).toBeDefined();
    expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_DOWN)).toBeDefined();
    expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_UP)).toBeDefined();
    expect(
      sseh.getInputAction(ScreenSpaceEventType.LEFT_DOUBLE_CLICK)
    ).toBeDefined();

    handler.deactivate();

    expect(
      sseh.getInputAction(ScreenSpaceEventType.LEFT_CLICK)
    ).toBeUndefined();
    expect(
      sseh.getInputAction(
        ScreenSpaceEventType.LEFT_CLICK,
        KeyboardEventModifier.SHIFT
      )
    ).toBeUndefined();
    expect(
      sseh.getInputAction(ScreenSpaceEventType.MOUSE_MOVE)
    ).toBeUndefined();
    expect(
      sseh.getInputAction(
        ScreenSpaceEventType.MOUSE_MOVE,
        KeyboardEventModifier.SHIFT
      )
    ).toBeUndefined();
    expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_DOWN)).toBeUndefined();
    expect(sseh.getInputAction(ScreenSpaceEventType.LEFT_UP)).toBeUndefined();
    expect(
      sseh.getInputAction(ScreenSpaceEventType.LEFT_DOUBLE_CLICK)
    ).toBeUndefined();
  });

  it("calls measurement double click", function () {
    var selectedMeasurement = {
      handleDoubleClick: jasmine.createSpy(),
    };
    var clickPosition = new Cartesian2();
    var handler = new MeasurementMouseHandler(scene);
    handler.selectedMeasurement = selectedMeasurement;
    handler.activate();
    handler._sseh.getInputAction(ScreenSpaceEventType.LEFT_DOUBLE_CLICK)({
      position: clickPosition,
    });
    expect(selectedMeasurement.handleDoubleClick).toHaveBeenCalledWith(
      clickPosition
    );
  });

  it("calls measurement left click", function () {
    var selectedMeasurement = {
      handleClick: jasmine.createSpy(),
    };
    var clickPosition = new Cartesian2();
    var handler = new MeasurementMouseHandler(scene);
    handler.selectedMeasurement = selectedMeasurement;
    handler.activate();
    handler._sseh.getInputAction(ScreenSpaceEventType.LEFT_CLICK)({
      position: clickPosition,
    });
    expect(selectedMeasurement.handleClick).toHaveBeenCalledWith(
      clickPosition,
      false
    );
  });

  it("calls measurement left click shift", function () {
    var selectedMeasurement = {
      handleClick: jasmine.createSpy(),
    };
    var clickPosition = new Cartesian2();
    var handler = new MeasurementMouseHandler(scene);
    handler.selectedMeasurement = selectedMeasurement;
    handler.activate();
    handler._sseh.getInputAction(
      ScreenSpaceEventType.LEFT_CLICK,
      KeyboardEventModifier.SHIFT
    )({
      position: clickPosition,
    });
    expect(selectedMeasurement.handleClick).toHaveBeenCalledWith(
      clickPosition,
      true
    );
  });

  it("calls measurement mouse move", function () {
    var selectedMeasurement = {
      handleMouseMove: jasmine.createSpy(),
    };
    var clickPosition = new Cartesian2();
    var handler = new MeasurementMouseHandler(scene);
    handler.selectedMeasurement = selectedMeasurement;
    handler.activate();
    handler._sseh.getInputAction(ScreenSpaceEventType.MOUSE_MOVE)({
      endPosition: clickPosition,
    });
    expect(selectedMeasurement.handleMouseMove).toHaveBeenCalledWith(
      clickPosition,
      false
    );
  });

  it("calls measurement left down", function () {
    var selectedMeasurement = {
      handleLeftDown: jasmine.createSpy(),
    };
    var clickPosition = new Cartesian2();
    var handler = new MeasurementMouseHandler(scene);
    handler.selectedMeasurement = selectedMeasurement;
    handler.activate();
    handler._sseh.getInputAction(ScreenSpaceEventType.LEFT_DOWN)({
      position: clickPosition,
    });
    expect(selectedMeasurement.handleLeftDown).toHaveBeenCalledWith(
      clickPosition
    );
  });

  it("calls measurement left up", function () {
    var selectedMeasurement = {
      handleLeftUp: jasmine.createSpy(),
    };
    var clickPosition = new Cartesian2();
    var handler = new MeasurementMouseHandler(scene);
    handler.selectedMeasurement = selectedMeasurement;
    handler.activate();
    handler._sseh.getInputAction(ScreenSpaceEventType.LEFT_UP)({
      position: clickPosition,
    });
    expect(selectedMeasurement.handleLeftUp).toHaveBeenCalledWith(
      clickPosition
    );
  });

  it("calls measurement mouse move shift", function () {
    var selectedMeasurement = {
      handleMouseMove: jasmine.createSpy(),
    };
    var clickPosition = new Cartesian2();
    var handler = new MeasurementMouseHandler(scene);
    handler.selectedMeasurement = selectedMeasurement;
    handler.activate();
    handler._sseh.getInputAction(
      ScreenSpaceEventType.MOUSE_MOVE,
      KeyboardEventModifier.SHIFT
    )({
      endPosition: clickPosition,
    });
    expect(selectedMeasurement.handleMouseMove).toHaveBeenCalledWith(
      clickPosition,
      true
    );
  });
});
