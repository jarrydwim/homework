import { Measure } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe(
  "Widgets/Measure/Measure",
  function () {
    var scene;
    beforeAll(function () {
      scene = createScene();
      scene.globe = createGlobe();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("constructor sets values", function () {
      var container = document.createElement("div");
      var measure = new Measure({
        container: container,
        scene: scene,
      });
      expect(measure.container).toBe(container);
      expect(measure.viewModel.scene).toBe(scene);
      expect(measure.isDestroyed()).toEqual(false);
      measure.destroy();
      expect(measure.isDestroyed()).toEqual(true);
    });

    it("throws if container is undefined", function () {
      expect(function () {
        return new Measure({
          scene: scene,
        });
      }).toThrowDeveloperError();
    });

    it("throws if container is undefined", function () {
      expect(function () {
        return new Measure({
          scene: scene,
        });
      }).toThrowDeveloperError();
    });
  },
  "WebGL"
);
