import { getSlope } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe("Widgets/Measure/getSlope", function () {
  var scene;
  var cameraPositionScratch;

  beforeAll(function () {
    scene = createScene();
    scene.globe = createGlobe();
  });

  beforeEach(function () {
    cameraPositionScratch = scene.camera.position;
  });

  afterEach(function () {
    scene.camera.position = cameraPositionScratch;
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("throws without scene", function () {
    expect(function () {
      return getSlope(undefined, new Cartesian2());
    }).toThrowDeveloperError();
  });

  it("throws without window coordinates", function () {
    expect(function () {
      return getSlope(scene, undefined);
    }).toThrowDeveloperError();
  });

  it("computes slope", function () {
    scene.camera.position = new Cartesian3(0, 0, -1000);

    spyOn(getSlope, "_getWorldPosition").and.returnValues(
      new Cartesian3(0, 0, 1),
      new Cartesian3(-1, -1, 0),
      new Cartesian3(-1, 1, 0),
      new Cartesian3(1, 1, 2),
      new Cartesian3(1, -1, 2)
    );

    var slope = getSlope(scene, new Cartesian2(0, 10));
    expect(
      CesiumMath.equalsEpsilon(slope, CesiumMath.PI / 4, CesiumMath.EPSILON9)
    ).toBe(true);
  });

  it("does not compute slope if camera is more than 10km away", function () {
    scene.camera.position = new Cartesian3(0, 0, -10000);

    spyOn(getSlope, "_getWorldPosition").and.returnValues(
      new Cartesian3(0, 0, 1),
      new Cartesian3(-1, -1, 0),
      new Cartesian3(-1, 1, 0),
      new Cartesian3(1, 1, 2),
      new Cartesian3(1, -1, 2)
    );

    var slope = getSlope(scene, new Cartesian2(0, 10));
    expect(slope).toBeUndefined();
  });

  it("does not compute slope if all sampled points are far away", function () {
    scene.camera.position = new Cartesian3(0, 0, -150);

    spyOn(getSlope, "_getWorldPosition").and.returnValues(
      new Cartesian3(0, 0, 1),
      new Cartesian3(-10, -10, 0),
      new Cartesian3(-10, 10, 0),
      new Cartesian3(10, 10, 20),
      new Cartesian3(10, -10, 20)
    );

    var slope = getSlope(scene, new Cartesian2(0, 0));
    expect(slope).toBeUndefined();
  });

  it("does compute slope if some sampled points are far away", function () {
    scene.camera.position = new Cartesian3(0, 0, -2000);

    spyOn(getSlope, "_getWorldPosition").and.returnValues(
      new Cartesian3(0, 0, 1),
      new Cartesian3(-1, -1, 0),
      new Cartesian3(-10, 10, 0),
      new Cartesian3(1, 1, 2),
      new Cartesian3(10, -10, 20)
    );

    var slope = getSlope(scene, new Cartesian2(0, 5));
    expect(slope).toBeDefined();
  });
});
