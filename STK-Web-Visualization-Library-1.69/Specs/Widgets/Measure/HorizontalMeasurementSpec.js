import { DistanceUnits } from "../../../Source/Cesium.js";
import { HorizontalMeasurement } from "../../../Source/Cesium.js";
import { LabelCollection } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import { MeasureUnits } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe("Widgets/Measure/HorizontalMeasurement", function () {
  var labels;
  var points;
  var primitives;
  var scene;

  var metersUnits = new MeasureUnits({
    distanceUnits: DistanceUnits.METERS,
  });

  beforeAll(function () {
    scene = createScene();
    primitives = new PrimitiveCollection();
    points = new PointPrimitiveCollection();
    labels = new LabelCollection();
    scene.globe = createGlobe();
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("can create and destroy", function () {
    var measurement = new HorizontalMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });
    expect(measurement.icon).toBeDefined();
    expect(measurement.type).toBe("Horizontal distance");
    expect(measurement.instructions).toBeDefined();
    expect(measurement.id).toBe("horizontalMeasurement");
    expect(measurement.isDestroyed()).toBe(false);

    measurement.destroy();

    expect(measurement.isDestroyed()).toBe(true);
  });

  it("throws with no scene", function () {
    expect(function () {
      return new HorizontalMeasurement({
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("works with no globe", function () {
    var globe = scene.globe;
    scene.globe = undefined;
    expect(function () {
      return new HorizontalMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).not.toThrowDeveloperError();
    scene.globe = globe;
  });

  it("throws with no units", function () {
    expect(function () {
      return new HorizontalMeasurement({
        scene: scene,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no points", function () {
    expect(function () {
      return new HorizontalMeasurement({
        scene: scene,
        units: metersUnits,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no labels", function () {
    expect(function () {
      return new HorizontalMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no primitives", function () {
    expect(function () {
      return new HorizontalMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
      });
    }).toThrowDeveloperError();
  });
});
