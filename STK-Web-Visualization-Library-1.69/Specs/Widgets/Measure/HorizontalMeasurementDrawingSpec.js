import { DistanceUnits } from "../../../Source/Cesium.js";
import { HorizontalMeasurementDrawing } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { IntersectionTests } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { LabelCollection } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import { SceneTransforms } from "../../../Source/Cesium.js";
import { PolylineDrawing } from "../../../Source/Cesium.js";
import { MeasureUnits } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe("Widgets/Measure/HorizontalMeasurementDrawing", function () {
  var labels;
  var points;
  var primitives;
  var scene;

  // corner points of half olympic sized swimming pool
  var positions = [
    new Cartesian3(1222099.0051609897, -4740070.1605999395, 4075158.6725652968),
    new Cartesian3(1222095.6646850954, -4740086.906038893, 4075140.3203022922),
    new Cartesian3(1222108.6632761091, -4740085.962928512, 4075137.5378793897),
    new Cartesian3(1222111.3487180157, -4740069.32747863, 4075155.9588378966),
  ];

  var metersUnits = new MeasureUnits({
    distanceUnits: DistanceUnits.METERS,
  });

  var feetUnits = new MeasureUnits({
    distanceUnits: DistanceUnits.FEET,
  });

  beforeAll(function () {
    scene = createScene();
    primitives = new PrimitiveCollection();
    points = new PointPrimitiveCollection();
    labels = new LabelCollection();
    scene.globe = createGlobe();
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("can create and destroy", function () {
    var measurement = new HorizontalMeasurementDrawing({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });
    expect(measurement.isDestroyed()).toBe(false);

    measurement.destroy();

    expect(measurement.isDestroyed()).toBe(true);
  });

  it("throws with no scene", function () {
    expect(function () {
      return new HorizontalMeasurementDrawing({
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("works with no globe", function () {
    var globe = scene.globe;
    scene.globe = undefined;
    expect(function () {
      return new HorizontalMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).not.toThrowDeveloperError();
    scene.globe = globe;
  });

  it("throws with no units", function () {
    expect(function () {
      return new HorizontalMeasurementDrawing({
        scene: scene,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no points", function () {
    expect(function () {
      return new HorizontalMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no labels", function () {
    expect(function () {
      return new HorizontalMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no primitives", function () {
    expect(function () {
      return new HorizontalMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
      });
    }).toThrowDeveloperError();
  });

  it("updates label text", function () {
    spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return positions[mouse.x].clone(result);
    });
    spyOn(IntersectionTests, "rayPlane").and.callFake(function (
      ray,
      plane,
      result
    ) {
      return positions[1].clone(result);
    });
    var measurement = new HorizontalMeasurementDrawing({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10), false);
    measurement.handleMouseMove(new Cartesian2(1, 100), false);
    measurement.handleClick(new Cartesian2(1, 100), false);

    expect(measurement._distance).toEqualEpsilon(
      25.06739041,
      CesiumMath.EPSILON6
    );
    expect(measurement._label.text).toEqual("25.07 m");

    measurement.destroy();
  });

  it("updates label text feet", function () {
    spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return positions[mouse.x].clone(result);
    });
    spyOn(IntersectionTests, "rayPlane").and.callFake(function (
      ray,
      plane,
      result
    ) {
      return positions[1].clone(result);
    });
    var measurement = new HorizontalMeasurementDrawing({
      scene: scene,
      units: feetUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10), false);
    measurement.handleMouseMove(new Cartesian2(1, 100), false);
    measurement.handleClick(new Cartesian2(1, 100), false);

    expect(measurement._distance).toEqualEpsilon(
      25.06739041,
      CesiumMath.EPSILON6
    );
    expect(measurement._label.text).toEqual("82.24 ft");

    measurement.destroy();
  });

  it("refreshes label text on unit change", function () {
    spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return positions[mouse.x].clone(result);
    });
    spyOn(IntersectionTests, "rayPlane").and.callFake(function (
      ray,
      plane,
      result
    ) {
      return positions[1].clone(result);
    });
    var measurement = new HorizontalMeasurementDrawing({
      scene: scene,
      units: feetUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10), false);
    measurement.handleMouseMove(new Cartesian2(1, 100), false);
    measurement.handleClick(new Cartesian2(1, 100), false);

    expect(measurement._distance).toEqualEpsilon(
      25.06739041,
      CesiumMath.EPSILON6
    );
    expect(measurement._label.text).toEqual("82.24 ft");

    measurement._selectedUnits.distanceUnits = DistanceUnits.METERS;
    measurement._refreshLabels();

    expect(measurement._label.text).toEqual("25.07 m");

    measurement.destroy();
  });

  it("updateLabel sets the label position to the top position in screen coordinates", function () {
    var expectedPosition = positions[2];
    spyOn(SceneTransforms, "wgs84ToWindowCoordinates").and.callFake(function (
      scene,
      position,
      result
    ) {
      if (position === expectedPosition) {
        result.y = 30;
      } else {
        result.y = 50;
      }
      return result;
    });

    var drawing = new HorizontalMeasurementDrawing({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    for (var i = 0; i < positions.length; i++) {
      drawing.addPoint(positions[i]);
    }
    drawing._mode = 2;

    drawing.updateLabels();

    expect(drawing._label.position).toEqual(expectedPosition);

    drawing.destroy();
  });

  it("reset hides the drawing", function () {
    spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
      scene,
      cart2,
      result
    ) {
      return Cartesian3.fromDegrees(
        cart2.x,
        cart2.y,
        undefined,
        undefined,
        result
      );
    });

    var measurement = new HorizontalMeasurementDrawing({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(), false);

    expect(measurement._polyline.show).toBe(true);

    measurement.reset();

    expect(measurement._polyline.show).toBe(false);

    measurement.destroy();
  });
});
