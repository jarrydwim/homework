import { DistanceUnits } from "../../../Source/Cesium.js";
import { HeightMeasurement } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { LabelCollection } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import { MeasureUnits } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe("Widgets/Measure/HeightMeasurement", function () {
  var labels;
  var points;
  var primitives;
  var scene;

  var metersUnits = new MeasureUnits({
    distanceUnits: DistanceUnits.METERS,
  });

  var feetUnits = new MeasureUnits({
    distanceUnits: DistanceUnits.FEET,
  });

  beforeAll(function () {
    scene = createScene();
    primitives = new PrimitiveCollection();
    points = new PointPrimitiveCollection();
    labels = new LabelCollection();
    scene.globe = createGlobe();
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("can create and destroy", function () {
    var measurement = new HeightMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });
    expect(measurement.icon).toBeDefined();
    expect(measurement.type).toBe("Height from terrain");
    expect(measurement.instructions).toBeDefined();
    expect(measurement.id).toBe("heightMeasurement");
    expect(measurement.isDestroyed()).toBe(false);

    measurement.destroy();

    expect(measurement.isDestroyed()).toBe(true);
  });

  it("throws with no scene", function () {
    expect(function () {
      return new HeightMeasurement({
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("works with no globe", function () {
    var globe = scene.globe;
    scene.globe = undefined;
    expect(function () {
      return new HeightMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).not.toThrowDeveloperError();
    scene.globe = globe;
  });

  it("throws with no units", function () {
    expect(function () {
      return new HeightMeasurement({
        scene: scene,
        points: points,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no points", function () {
    expect(function () {
      return new HeightMeasurement({
        scene: scene,
        units: metersUnits,
        labels: labels,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no labels", function () {
    expect(function () {
      return new HeightMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        primitives: primitives,
      });
    }).toThrowDeveloperError();
  });

  it("throws with no primitives", function () {
    expect(function () {
      return new HeightMeasurement({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
      });
    }).toThrowDeveloperError();
  });

  it("updates label text", function () {
    spyOn(HeightMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return Cartesian3.fromDegrees(12, 12, 100, undefined, result);
    });
    spyOn(scene.globe, "pick").and.callFake(function (ray, scene, result) {
      return Cartesian3.fromDegrees(12, 12, 0, undefined, result);
    });
    var measurement = new HeightMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));

    expect(measurement._distance).toEqualEpsilon(100.0, CesiumMath.EPSILON6);
    expect(measurement._label.text).toEqual("100.00 m");

    measurement.destroy();
  });

  it("updates label text feet", function () {
    spyOn(HeightMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return Cartesian3.fromDegrees(12, 12, 100, undefined, result);
    });
    spyOn(scene.globe, "pick").and.callFake(function (ray, scene, result) {
      return Cartesian3.fromDegrees(12, 12, 0, undefined, result);
    });
    var measurement = new HeightMeasurement({
      scene: scene,
      units: feetUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));

    expect(measurement._distance).toEqualEpsilon(100.0, CesiumMath.EPSILON6);
    expect(measurement._label.text).toEqual("328.08 ft");

    measurement.destroy();
  });

  it("refreshes label text on unit change", function () {
    spyOn(HeightMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      mouse,
      result
    ) {
      return Cartesian3.fromDegrees(12, 12, 100, undefined, result);
    });
    spyOn(scene.globe, "pick").and.callFake(function (ray, scene, result) {
      return Cartesian3.fromDegrees(12, 12, 0, undefined, result);
    });
    var measurement = new HeightMeasurement({
      scene: scene,
      units: feetUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2(0, 10));

    expect(measurement._distance).toEqualEpsilon(100.0, CesiumMath.EPSILON6);
    expect(measurement._label.text).toEqual("328.08 ft");

    measurement._selectedUnits.distanceUnits = DistanceUnits.METERS;
    measurement._refreshLabels();

    expect(measurement._label.text).toEqual("100.00 m");

    measurement.destroy();
  });

  it("handle click does nothing if click does not return a position", function () {
    spyOn(HeightMeasurement, "_getWorldPosition");
    var measurement = new HeightMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2());

    expect(measurement._startPoint.show).toBe(false);

    measurement.destroy();
  });

  it("reset hides the drawing", function () {
    spyOn(HeightMeasurement, "_getWorldPosition").and.callFake(function (
      scene,
      cart2,
      result
    ) {
      return Cartesian3.fromDegrees(
        cart2.x,
        cart2.y,
        undefined,
        undefined,
        result
      );
    });

    var measurement = new HeightMeasurement({
      scene: scene,
      units: metersUnits,
      points: points,
      labels: labels,
      primitives: primitives,
    });

    measurement.handleClick(new Cartesian2());

    expect(measurement._polyline.show).toBe(true);

    measurement.reset();

    expect(measurement._polyline.show).toBe(false);

    measurement.destroy();
  });
});
