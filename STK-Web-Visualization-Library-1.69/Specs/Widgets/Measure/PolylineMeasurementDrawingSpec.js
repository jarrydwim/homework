import { DistanceUnits } from "../../../Source/Cesium.js";
import { PolylineMeasurementDrawing } from "../../../Source/Cesium.js";
import { Cartesian2 } from "../../../Source/Cesium.js";
import { Cartesian3 } from "../../../Source/Cesium.js";
import { Math as CesiumMath } from "../../../Source/Cesium.js";
import { LabelCollection } from "../../../Source/Cesium.js";
import { PointPrimitiveCollection } from "../../../Source/Cesium.js";
import { PrimitiveCollection } from "../../../Source/Cesium.js";
import { SceneTransforms } from "../../../Source/Cesium.js";
import { PolylineDrawing } from "../../../Source/Cesium.js";
import { MeasureUnits } from "../../../Source/Cesium.js";
import createGlobe from "../../createGlobe.js";
import createScene from "../../createScene.js";

describe(
  "Widgets/Measure/PolylineMeasurementDrawing",
  function () {
    var labels;
    var points;
    var primitives;
    var scene;

    // corner points of half olympic sized swimming pool
    var positions = [
      new Cartesian3(
        1222099.0051609897,
        -4740070.1605999395,
        4075158.6725652968
      ),
      new Cartesian3(
        1222095.6646850954,
        -4740086.906038893,
        4075140.3203022922
      ),
      new Cartesian3(
        1222108.6632761091,
        -4740085.962928512,
        4075137.5378793897
      ),
      new Cartesian3(1222111.3487180157, -4740069.32747863, 4075155.9588378966),
    ];

    var metersUnits = new MeasureUnits({
      distanceUnits: DistanceUnits.METERS,
    });

    var feetUnits = new MeasureUnits({
      distanceUnits: DistanceUnits.FEET,
    });

    beforeAll(function () {
      scene = createScene();
      scene.globe = createGlobe();
      primitives = new PrimitiveCollection();
      points = new PointPrimitiveCollection();
      labels = new LabelCollection();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("can create and destroy", function () {
      var drawing = new PolylineMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });
      expect(drawing.isDestroyed()).toBe(false);

      drawing.destroy();

      expect(drawing.isDestroyed()).toBe(true);
    });

    it("throws with no scene", function () {
      expect(function () {
        return new PolylineMeasurementDrawing({
          units: metersUnits,
          points: points,
          labels: labels,
          primitives: primitives,
        });
      }).toThrowDeveloperError();
    });

    it("throws with no units", function () {
      expect(function () {
        return new PolylineMeasurementDrawing({
          scene: scene,
          points: points,
          labels: labels,
          primitives: primitives,
        });
      }).toThrowDeveloperError();
    });

    it("throws with no points", function () {
      expect(function () {
        return new PolylineMeasurementDrawing({
          scene: scene,
          units: metersUnits,
          labels: labels,
          primitives: primitives,
        });
      }).toThrowDeveloperError();
    });

    it("throws with no labels", function () {
      expect(function () {
        return new PolylineMeasurementDrawing({
          scene: scene,
          units: metersUnits,
          points: points,
          primitives: primitives,
        });
      }).toThrowDeveloperError();
    });

    it("throws with no primitives", function () {
      expect(function () {
        return new PolylineMeasurementDrawing({
          scene: scene,
          units: metersUnits,
          points: points,
          labels: labels,
        });
      }).toThrowDeveloperError();
    });

    it("works with no globe", function () {
      var globe = scene.globe;
      scene.globe = undefined;
      expect(function () {
        var drawing = new PolylineMeasurementDrawing({
          scene: scene,
          units: metersUnits,
          points: points,
          labels: labels,
          primitives: primitives,
        });

        drawing.destroy();
      }).not.toThrowDeveloperError();
      scene.globe = globe;
    });

    it("updates label text", function () {
      spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
        scene,
        mouse,
        result
      ) {
        return positions[mouse.x].clone(result);
      });

      var drawing = new PolylineMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });

      drawing.handleClick(new Cartesian2(0, 10));
      drawing.handleMouseMove(new Cartesian2(1, 100));
      drawing.handleClick(new Cartesian2(1, 100));
      drawing.handleMouseMove(new Cartesian2(2, 50));
      drawing.handleClick(new Cartesian2(2, 50));

      expect(drawing._distance).toEqualEpsilon(
        38.39385666,
        CesiumMath.EPSILON6
      );
      expect(drawing._label.text).toEqual("38.39 m");

      drawing.destroy();
    });

    it("updates label text feet", function () {
      spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
        scene,
        mouse,
        result
      ) {
        return positions[mouse.x].clone(result);
      });

      var drawing = new PolylineMeasurementDrawing({
        scene: scene,
        units: feetUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });

      drawing.handleClick(new Cartesian2(0, 10));
      drawing.handleMouseMove(new Cartesian2(1, 100));
      drawing.handleClick(new Cartesian2(1, 100));
      drawing.handleMouseMove(new Cartesian2(2, 50));
      drawing.handleClick(new Cartesian2(2, 50));

      expect(drawing._distance).toEqualEpsilon(
        38.39385666,
        CesiumMath.EPSILON6
      );
      expect(drawing._label.text).toEqual("125.96 ft");

      drawing.destroy();
    });

    it("updateLabel sets the label position to the top position in screen coordinates", function () {
      var expectedPosition = positions[2];
      spyOn(SceneTransforms, "wgs84ToWindowCoordinates").and.callFake(function (
        scene,
        position,
        result
      ) {
        if (position === expectedPosition) {
          result.y = 30;
        } else {
          result.y = 50;
        }
        return result;
      });

      var drawing = new PolylineMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });

      for (var i = 0; i < positions.length; i++) {
        drawing.addPoint(positions[i]);
      }
      drawing._mode = 2;

      drawing.updateLabel();

      expect(drawing._label.position).toEqual(expectedPosition);

      drawing.destroy();
    });

    it("refreshes all label text on unit change", function () {
      spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
        scene,
        mouse,
        result
      ) {
        return positions[mouse.x].clone(result);
      });

      var drawing = new PolylineMeasurementDrawing({
        scene: scene,
        units: feetUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });

      drawing.handleClick(new Cartesian2(0, 10));
      drawing.handleMouseMove(new Cartesian2(1, 100));
      drawing.handleClick(new Cartesian2(1, 100));
      drawing.handleMouseMove(new Cartesian2(2, 50));
      drawing.handleClick(new Cartesian2(2, 50));

      expect(drawing._distance).toEqualEpsilon(
        38.39385666,
        CesiumMath.EPSILON6
      );
      expect(drawing._label.text).toEqual("125.96 ft");
      expect(drawing._segmentLabels[0].text).toEqual("82.24 ft");
      expect(drawing._segmentLabels[1].text).toEqual("43.72 ft");

      drawing._selectedUnits.distanceUnits = DistanceUnits.METERS;
      drawing._refreshLabels();

      expect(drawing._label.text).toEqual("38.39 m");
      expect(drawing._segmentLabels[0].text).toEqual("25.07 m");
      expect(drawing._segmentLabels[1].text).toEqual("13.33 m");

      drawing.destroy();
    });

    it("reset hides the drawing", function () {
      spyOn(PolylineDrawing, "_getWorldPosition").and.callFake(function (
        scene,
        mouse,
        result
      ) {
        return positions[mouse.x].clone(result);
      });

      var drawing = new PolylineMeasurementDrawing({
        scene: scene,
        units: metersUnits,
        points: points,
        labels: labels,
        primitives: primitives,
      });

      drawing.handleClick(new Cartesian2(0, 10));
      drawing.handleMouseMove(new Cartesian2(1, 100));
      drawing.handleClick(new Cartesian2(1, 100));
      drawing.handleMouseMove(new Cartesian2(2, 50));
      drawing.handleClick(new Cartesian2(2, 50));

      expect(drawing._polyline.show).toBe(true);
      expect(drawing._label.show).toBe(true);
      expect(drawing._pointCollection.length).toBe(3);
      expect(drawing._points.length).toBe(3);
      expect(drawing._labelCollection.length).toBe(3);
      expect(drawing._segmentLabels.length).toBe(2);

      drawing.reset();

      expect(drawing._polyline.show).toBe(false);
      expect(drawing._label.show).toBe(false);
      expect(drawing._pointCollection.length).toBe(0);
      expect(drawing._points.length).toBe(0);
      expect(drawing._labelCollection.length).toBe(1);
      expect(drawing._segmentLabels.length).toBe(0);

      drawing.destroy();
    });
  },
  "WebGL"
);
