import { ConicSensor } from "../../Source/Cesium.js";
import { BoxGeometry } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Cartographic } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { ColorGeometryInstanceAttribute } from "../../Source/Cesium.js";
import { defaultValue } from "../../Source/Cesium.js";
import { defined } from "../../Source/Cesium.js";
import { Ellipsoid } from "../../Source/Cesium.js";
import { GeometryInstance } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";
import { Matrix3 } from "../../Source/Cesium.js";
import { Matrix4 } from "../../Source/Cesium.js";
import { Transforms } from "../../Source/Cesium.js";
import { Material } from "../../Source/Cesium.js";
import { PerInstanceColorAppearance } from "../../Source/Cesium.js";
import { Primitive } from "../../Source/Cesium.js";
import { SceneMode } from "../../Source/Cesium.js";
import { SensorVolumePortionToDisplay } from "../../Source/Cesium.js";
import { ShadowMode } from "../../Source/Cesium.js";
import createFrameState from "../createFrameState.js";
import createScene from "../createScene.js";

describe(
  "Scene/ConicSensor",
  function () {
    var scene;
    var context;
    var ellipsoid = Ellipsoid.WGS84;

    function getModelMatrix(ellipsoid, cartographic, clock, cone, twist) {
      var location = ellipsoid.cartographicToCartesian(cartographic);
      var modelMatrix = Transforms.northEastDownToFixedFrame(location);
      var orientation = Matrix3.multiply(
        Matrix3.multiply(
          Matrix3.fromRotationZ(clock),
          Matrix3.fromRotationY(cone),
          new Matrix3()
        ),
        Matrix3.fromRotationX(twist),
        new Matrix3()
      );
      return Matrix4.multiply(
        modelMatrix,
        Matrix4.fromRotationTranslation(orientation, Cartesian3.ZERO),
        new Matrix4()
      );
    }

    function addSensor(options) {
      options = defaultValue(options, defaultValue.EMPTY_OBJECT);

      var longitude = defaultValue(options.longitude, 0.0);
      var latitude = defaultValue(options.latitude, 0.0);
      var altitude = defaultValue(options.altitude, 0.0);
      var clock = defaultValue(options.clock, 0.0);
      var cone = defaultValue(options.cone, 0.0);
      var twist = defaultValue(options.twist, 0.0);
      var innerHalfAngle = defaultValue(options.innerHalfAngle, 0.0);
      var outerHalfAngle = defaultValue(options.outerHalfAngle, 45.0);
      var minimumClockAngle = defaultValue(options.minimumClockAngle, 0.0);
      var maximumClockAngle = defaultValue(options.maximumClockAngle, 360.0);
      var radius = defaultValue(options.radius, Number.POSITIVE_INFINITY);

      return scene.primitives.add(
        new ConicSensor({
          innerHalfAngle: CesiumMath.toRadians(innerHalfAngle),
          outerHalfAngle: CesiumMath.toRadians(outerHalfAngle),
          minimumClockAngle: CesiumMath.toRadians(minimumClockAngle),
          maximumClockAngle: CesiumMath.toRadians(maximumClockAngle),
          radius: radius,
          modelMatrix: getModelMatrix(
            Ellipsoid.WGS84,
            Cartographic.fromDegrees(longitude, latitude, altitude),
            CesiumMath.toRadians(clock),
            CesiumMath.toRadians(cone),
            CesiumMath.toRadians(twist)
          ),
        })
      );
    }

    function addSensorForEnvironmentConstraint() {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 2500000.0,
      });
      sensor.portionToDisplay = SensorVolumePortionToDisplay.COMPLETE;
      sensor.radius = 1000.0;

      var translation = Matrix4.fromTranslation(
        new Cartesian3(0.0, 0.0, 300.0)
      );
      var modelMatrix = Matrix4.multiply(
        sensor.modelMatrix,
        translation,
        new Matrix4()
      );

      scene.primitives.add(
        new Primitive({
          geometryInstances: new GeometryInstance({
            geometry: BoxGeometry.fromDimensions({
              vertexFormat: PerInstanceColorAppearance.VERTEX_FORMAT,
              dimensions: new Cartesian3(100.0, 100.0, 100.0),
            }),
            modelMatrix: modelMatrix,
            attributes: {
              color: ColorGeometryInstanceAttribute.fromColor(
                new Color(1.0, 0.0, 1.0, 0.5)
              ),
            },
          }),
          appearance: new PerInstanceColorAppearance({
            closed: true,
          }),
          asynchronous: false,
          shadows: ShadowMode.CAST_ONLY,
        })
      );

      var camera = scene.camera;
      camera.lookAtTransform(
        sensor.modelMatrix,
        new Cartesian3(0.0, 0.0, sensor.radius - 2.0)
      );

      Cartesian3.negate(camera.direction, camera.direction);
      Cartesian3.cross(camera.direction, camera.up, camera.right);

      return sensor;
    }

    beforeAll(function () {
      scene = createScene();
      context = scene._context;
    });

    afterEach(function () {
      scene.primitives.removeAll();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    function check2D(context, offset) {
      scene.morphTo2D(0);

      var sensor = scene.primitives.get(0);
      var modelMatrix = sensor.modelMatrix;

      var camera = scene.camera;
      if (!defined(offset)) {
        var offsetZ = 6500000.0;
        offset = new Cartesian3(0.0, 0.0, offsetZ);
      }
      camera.lookAtTransform(modelMatrix, offset);

      expect(scene).notToRender([0, 0, 0, 255]);
    }

    function check3D(context, offset) {
      scene.morphTo3D(0);

      var sensor = scene.primitives.get(0);
      var modelMatrix = sensor.modelMatrix;

      if (!defined(offset)) {
        var offsetZ = 6500000.0;
        offset = new Cartesian3(0.0, 0.0, -offsetZ);
      }

      var camera = scene.camera;
      camera.lookAtTransform(modelMatrix, offset);

      expect(scene).notToRender([0, 0, 0, 255]);
    }

    it("gets the default properties", function () {
      var p = new ConicSensor();
      expect(p.ellipsoid).toEqual(Ellipsoid.WGS84);
      expect(p.show).toEqual(true);
      expect(p.showIntersection).toEqual(true);
      expect(p.showThroughEllipsoid).toEqual(false);
      expect(p.portionToDisplay).toEqual(SensorVolumePortionToDisplay.COMPLETE);
      expect(p.modelMatrix).toEqual(Matrix4.IDENTITY);
      expect(p.innerHalfAngle).toEqual(0.0);
      expect(p.outerHalfAngle).toEqual(CesiumMath.PI_OVER_TWO);
      expect(p.minimumClockAngle).toEqual(0.0);
      expect(p.maximumClockAngle).toEqual(CesiumMath.TWO_PI);
      expect(p.radius).toEqual(Number.POSITIVE_INFINITY);
      expect(p.lateralSurfaceMaterial.type).toEqual(Material.ColorType);
      expect(p.showLateralSurfaces).toEqual(true);
      expect(p.ellipsoidHorizonSurfaceMaterial).toBeUndefined();
      expect(p.showEllipsoidHorizonSurfaces).toEqual(true);
      expect(p.domeSurfaceMaterial).toBeUndefined();
      expect(p.showDomeSurfaces).toEqual(true);
      expect(p.ellipsoidSurfaceMaterial).toBeUndefined();
      expect(p.showEllipsoidSurfaces).toEqual(true);
      expect(p.intersectionColor).toEqual(Color.WHITE);
      expect(p.intersectionWidth).toEqual(5.0);
      expect(p.id).not.toBeDefined();
      expect(p.environmentConstraint).toEqual(false);
      expect(p.showEnvironmentOcclusion).toEqual(false);
      expect(p.environmentOcclusionMaterial.type).toEqual(Material.ColorType);
      expect(p.showEnvironmentIntersection).toEqual(false);
      expect(p.environmentIntersectionColor).toEqual(Color.WHITE);
      expect(p.environmentIntersectionWidth).toEqual(5.0);
      expect(p.debugShowCrossingPoints).toEqual(false);
      expect(p.debugShowProxyGeometry).toEqual(false);
      expect(p.debugShowBoundingVolume).toEqual(false);
      p.destroy();
    });

    it("constructs with options", function () {
      var stripe = Material.fromType(Material.StripeType);
      var dot = Material.fromType(Material.DotType);
      var grid = Material.fromType(Material.GridType);
      var checkerboard = Material.fromType(Material.CheckerboardType);

      var p = new ConicSensor({
        ellipsoid: Ellipsoid.MOON,
        show: false,
        showIntersection: false,
        showThroughEllipsoid: true,
        portionToDisplay: SensorVolumePortionToDisplay.ABOVE_ELLIPSOID_HORIZON,
        modelMatrix: Matrix4.fromUniformScale(2.0),
        innerHalfAngle: CesiumMath.PI_OVER_TWO,
        outerHalfAngle: CesiumMath.TWO_PI,
        minimumClockAngle: -CesiumMath.PI_OVER_TWO,
        maximumClockAngle: CesiumMath.PI_OVER_TWO,
        radius: 5000000.0,
        lateralSurfaceMaterial: stripe,
        showLateralSurfaces: false,
        ellipsoidHorizonSurfaceMaterial: dot,
        showEllipsoidHorizonSurfaces: false,
        domeSurfaceMaterial: grid,
        showDomeSurfaces: false,
        ellipsoidSurfaceMaterial: checkerboard,
        showEllipsoidSurfaces: false,
        intersectionColor: Color.GREY,
        intersectionWidth: 10.0,
        id: "id",
        environmentConstraint: true,
        showEnvironmentOcclusion: true,
        environmentOcclusionMaterial: grid,
        showEnvironmentIntersection: true,
        environmentIntersectionColor: Color.BLUE,
        environmentIntersectionWidth: 10.0,
        debugShowCrossingPoints: true,
        debugShowProxyGeometry: true,
        debugShowBoundingVolume: true,
      });
      expect(p.ellipsoid).toEqual(Ellipsoid.MOON);
      expect(p.show).toEqual(false);
      expect(p.showIntersection).toEqual(false);
      expect(p.showThroughEllipsoid).toEqual(true);
      expect(p.portionToDisplay).toEqual(
        SensorVolumePortionToDisplay.ABOVE_ELLIPSOID_HORIZON
      );
      expect(p.modelMatrix).toEqual(Matrix4.fromUniformScale(2.0));
      expect(p.innerHalfAngle).toEqual(CesiumMath.PI_OVER_TWO);
      expect(p.outerHalfAngle).toEqual(CesiumMath.TWO_PI);
      expect(p.minimumClockAngle).toEqual(-CesiumMath.PI_OVER_TWO);
      expect(p.maximumClockAngle).toEqual(CesiumMath.PI_OVER_TWO);
      expect(p.radius).toEqual(5000000.0);
      expect(p.lateralSurfaceMaterial).toBe(stripe);
      expect(p.showLateralSurfaces).toEqual(false);
      expect(p.ellipsoidHorizonSurfaceMaterial).toBe(dot);
      expect(p.showEllipsoidHorizonSurfaces).toEqual(false);
      expect(p.domeSurfaceMaterial).toBe(grid);
      expect(p.showDomeSurfaces).toEqual(false);
      expect(p.ellipsoidSurfaceMaterial).toBe(checkerboard);
      expect(p.showEllipsoidSurfaces).toEqual(false);
      expect(p.intersectionColor).toEqual(Color.GREY);
      expect(p.intersectionWidth).toEqual(10.0);
      expect(p.id).toEqual("id");
      expect(p.environmentConstraint).toEqual(true);
      expect(p.showEnvironmentOcclusion).toEqual(true);
      expect(p.environmentOcclusionMaterial).toBe(grid);
      expect(p.showEnvironmentIntersection).toEqual(true);
      expect(p.environmentIntersectionColor).toEqual(Color.BLUE);
      expect(p.environmentIntersectionWidth).toEqual(10.0);
      expect(p.debugShowCrossingPoints).toEqual(true);
      expect(p.debugShowProxyGeometry).toEqual(true);
      expect(p.debugShowBoundingVolume).toEqual(true);
      p.destroy();
    });

    it("renders when spanning date line", function () {
      var frameState = createFrameState(context);
      frameState.mode = SceneMode.SCENE2D;
      var commands = frameState.commandList;

      var sensor = addSensor({
        longitude: 0.0,
        latitude: 0.0,
        altitude: 9000000.0,
      });

      sensor.update(frameState);
      expect(commands.length).toEqual(1);

      commands.length = 0;

      sensor = addSensor({
        longitude: 180.0,
        latitude: 0.0,
        altitude: 9000000.0,
      });

      sensor.update(frameState);
      expect(commands.length).toEqual(2);
    });

    it("renders using complete (default) option", function () {
      addSensor({
        latitude: -90.0,
        altitude: 9000000.0,
        radius: 10000000.0,
      });

      check3D(context);
      check2D(context);
    });

    it("renders bounding volume", function () {
      var frameState = createFrameState(context);
      frameState.mode = SceneMode.SCENE2D;
      var commands = frameState.commandList;

      var sensor = addSensor({
        longitude: 0.0,
        latitude: 0.0,
        altitude: 9000000.0,
      });
      sensor.debugShowBoundingVolume = true;

      sensor.update(frameState);
      expect(commands.length).toEqual(1);
      expect(commands[0].debugShowBoundingVolume).toEqual(true);
    });

    it("renders when definition is changed", function () {
      var sensor = addSensor({
        altitude: 9000000.0,
        radius: 10000000.0,
      });
      sensor.portionToDisplay = SensorVolumePortionToDisplay.COMPLETE;

      check3D(context);
      check2D(context);

      // change inner half angle
      sensor.innerHalfAngle = CesiumMath.toRadians(0.5);

      check3D(context);
      check2D(context);

      // change outer half angle
      sensor.outerHalfAngle = CesiumMath.toRadians(89.5);

      check3D(context);
      check2D(context);

      // change minimum clock angle
      sensor.minimumClockAngle = CesiumMath.toRadians(0.5);

      check3D(context);
      check2D(context);

      // change maximum clock angle
      sensor.maximumClockAngle = CesiumMath.toRadians(359.5);

      check3D(context);
      check2D(context);
    });

    it("renders using below ellipsoid horizon option", function () {
      var sensor = addSensor({
        altitude: 9000000.0,
        radius: 10000000.0,
      });
      sensor.portionToDisplay =
        SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON;

      check3D(context);
      check2D(context);
    });

    it("renders using above ellipsoid horizon option", function () {
      var sensor = addSensor({
        altitude: 9000000.0,
        radius: 10000000.0,
      });
      sensor.portionToDisplay =
        SensorVolumePortionToDisplay.ABOVE_ELLIPSOID_HORIZON;

      check3D(context);
      check2D(context);
    });

    it("renders using outside ellipsoid horizon fragment shader", function () {
      addSensor({
        latitude: -90.0,
        altitude: 8000000.0,
        outerHalfAngle: 85.0,
        radius: 5000000.0,
      });

      check3D(context);
    });

    it("renders using inside ellipsoid horizon fragment shader", function () {
      addSensor({
        latitude: -90.0,
        altitude: 12000000.0,
        outerHalfAngle: 85.0,
        radius: 5000000.0,
      });

      check3D(context);
    });

    it("renders using outside dome fragment shader", function () {
      addSensor({
        latitude: -90.0,
        altitude: 8000000.0,
        radius: 1000000.0,
      });

      check3D(context);
    });

    it("renders using inside dome fragment shader", function () {
      addSensor({
        latitude: -90.0,
        altitude: 8000000.0,
        radius: 3000000.0,
      });

      check3D(context);
    });

    function iterateOverOuterConeAngles(cases, sensor, outerHalfAngles) {
      var inner = sensor.innerHalfAngle;
      for (var i = 0; i < outerHalfAngles.length; ++i) {
        var outer = CesiumMath.toRadians(outerHalfAngles[i]);
        if (outer > inner) {
          sensor.outerHalfAngle = outer;
          check3D(context);
          ++cases;
        } else {
          break;
        }
      }
      return cases;
    }

    it("renders simple conic", function () {
      var sensor = addSensor({
        longitude: 0.0,
        latitude: -90.0,
        altitude: 5000000.0,
        radius: 2500000.0,
      });

      var outerHalfAngles = [180.0, 135.0, 90.0, 45.0];

      var cases = 0;
      cases = iterateOverOuterConeAngles(cases, sensor, outerHalfAngles);
      expect(cases).toEqual(4);
    });

    it("renders simple conic with hole", function () {
      var sensor = addSensor({
        longitude: 0.0,
        latitude: -85.0,
        altitude: 5000000.0,
        radius: 2500000.0,
      });

      var innerHalfAngles = [135.0, 90.0, 45.0];
      var outerHalfAngles = [180.0, 135.0, 90.0];

      var cases = 0;
      for (var i = 0; i < innerHalfAngles.length; ++i) {
        sensor.innerHalfAngle = CesiumMath.toRadians(innerHalfAngles[i]);
        cases = iterateOverOuterConeAngles(cases, sensor, outerHalfAngles);
      }
      expect(cases).toEqual(6);
    });

    it("renders partial or simple conic with various clock angle ranges", function () {
      var longitude = 0.0;
      var latitude = -90.0;
      var altitude = 5000000.0;

      var sensor = addSensor({
        longitude: longitude,
        latitude: latitude,
        altitude: altitude,
        radius: 2500000.0,
      });

      var position = ellipsoid.cartographicToCartesian(
        Cartographic.fromDegrees(longitude, latitude, altitude)
      );

      var outerHalfAngles = [180.0, 135.0, 90.0, 45.0];

      var minimumClockAngles = [
        -360.0,
        -315.0,
        -270.0,
        -225.0,
        -180.0,
        -135.0,
        -90.0,
        -45.0,
        0.0,
      ];
      var span = [45.0, 90.0, 135.0, 180.0, 225.0, 270.0, 315.0, 360.0];

      var camera = scene.camera;
      camera.lookAtTransform(Matrix4.IDENTITY);

      var cases = 0;
      for (var i = 0; i < minimumClockAngles.length; ++i) {
        var minimum = CesiumMath.toRadians(minimumClockAngles[i]);
        for (var j = 0; j < span.length; ++j) {
          var maximum = minimum + CesiumMath.toRadians(span[j]);
          sensor.minimumClockAngle = minimum;
          sensor.maximumClockAngle = maximum;

          var inner = sensor.innerHalfAngle;
          for (var k = 0; k < outerHalfAngles.length; ++k) {
            var outer = CesiumMath.toRadians(outerHalfAngles[k]);
            if (outer > inner) {
              var middleConeAngle = (outer + inner) / 2.0;
              var middleClockAngle = (maximum + minimum) / 2.0;
              var sineCone = Math.sin(middleConeAngle);

              var direction = Cartesian3.fromElements(
                Math.cos(middleClockAngle) * sineCone,
                Math.sin(middleClockAngle) * sineCone,
                Math.cos(middleConeAngle)
              );
              camera.direction = Cartesian3.negate(direction, camera.direction);
              camera.up = Cartesian3.mostOrthogonalAxis(direction, camera.up);
              Cartesian3.cross(camera.direction, camera.up, camera.right);
              Cartesian3.cross(camera.right, camera.direction, camera.up);
              Cartesian3.normalize(camera.right, camera.right);
              Cartesian3.normalize(camera.up, camera.up);

              camera.position = Cartesian3.add(
                position,
                Cartesian3.multiplyByScalar(
                  direction,
                  2.0 * sensor.radius,
                  camera.position
                ),
                camera.position
              );

              sensor.outerHalfAngle = outer;

              expect(scene).notToRender([0, 0, 0, 255]);

              ++cases;
            } else {
              break;
            }
          }
        }
      }
      expect(cases).toEqual(288);
    });

    it("renders sensor on surface looking up", function () {
      var frameState = createFrameState(context);
      var commands = frameState.commandList;

      var sensor = addSensor({
        longitude: 0.0,
        latitude: -90.0,
        altitude: 0.0,
        cone: 180.0,
        outerHalfAngle: 85,
        radius: 5000000.0,
      });

      sensor.update(frameState);
      expect(commands.length).not.toEqual(0);
      check3D(context);
    });

    it("2D definition change affects 3D", function () {
      var frameState = createFrameState(context);

      var sensor = addSensor({
        altitude: 4000000.0,
        cone: -45.0,
        radius: 20000000.0,
        outerHalfAngle: 75.0,
        minimumClockAngle: -45.0,
        maximumClockAngle: 45.0,
      });

      // Update first in 3D and check.
      frameState.mode = SceneMode.SCENE3D;
      sensor.update(frameState);
      expect(sensor._ellipsoidHorizonSurfaceColorCommandList.length).toEqual(3);

      // Switch to 2D.
      frameState.mode = SceneMode.SCENE2D;
      sensor.update(frameState);

      // Change definition in 2D.
      sensor.innerHalfAngle = CesiumMath.toRadians(15.0);

      // Switch to 3D.
      frameState.mode = SceneMode.SCENE3D;
      sensor.update(frameState);
      expect(sensor._ellipsoidHorizonSurfaceColorCommandList.length).toEqual(2);
    });

    it("does not render when show is false", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
        radius: 10000000.0,
      });

      scene.mode = SceneMode.SCENE3D;
      check3D(context);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);
    });

    it("renders opaque lateral surfaces", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 7500000.0,
        outerHalfAngle: 75.0,
        radius: 2000000.0,
      });
      sensor.radius = 2000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = true;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;

      sensor.lateralSurfaceMaterial = Material.fromType("Color");
      sensor.lateralSurfaceMaterial.uniforms.color = new Color(
        1.0,
        1.0,
        1.0,
        1.0
      );

      check3D(context);
    });

    it("showLateralSurfaces functions properly", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 7500000.0,
        outerHalfAngle: 75.0,
        radius: 2000000.0,
      });
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;

      scene.mode = SceneMode.SCENE3D;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.showLateralSurfaces = true;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("renders opaque ellipsoid horizon surfaces", function () {
      var sensor = addSensor({
        altitude: 3000000.0,
      });
      sensor.radius = 20000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = true;
      sensor.showDomeSurfaces = false;

      sensor.ellipsoidHorizonSurfaceMaterial = Material.fromType("Color");
      sensor.ellipsoidHorizonSurfaceMaterial.uniforms.color = new Color(
        1.0,
        1.0,
        1.0,
        1.0
      );

      // check3D doesn't work here because there is a huge hole where the Earth is.
      // The position was found in Viewer by panning and zooming until the camera
      // was close enough to the ellipsoid horizon surfaces. Then align up to north
      // and create the orthonormal basis.
      var camera = scene.camera;
      camera.lookAtTransform(Matrix4.IDENTITY);
      camera.position = new Cartesian3(
        -132408.72122866797,
        -10631460.24052539,
        2091551.1791969084
      );
      Cartesian3.negate(camera.position, camera.direction);
      Cartesian3.normalize(camera.direction, camera.direction);
      Cartesian3.cross(camera.direction, Cartesian3.UNIT_Z, camera.right);
      Cartesian3.cross(camera.right, camera.direction, camera.up);
      Cartesian3.normalize(camera.up, camera.up);
      Cartesian3.cross(camera.direction, camera.up, camera.right);
      Cartesian3.normalize(camera.right, camera.right);

      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("showEllipsoidHorizonSurfaces functions properly", function () {
      var sensor = addSensor({
        latitude: 0.0,
        altitude: 3000000.0,
      });
      sensor.radius = 20000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;

      scene.mode = SceneMode.SCENE3D;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.showEllipsoidHorizonSurfaces = true;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("renders opaque dome surfaces", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });
      sensor.radius = 2000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = true;

      sensor.domeSurfaceMaterial = Material.fromType("Color");
      sensor.domeSurfaceMaterial.uniforms.color = new Color(1.0, 1.0, 1.0, 1.0);

      check3D(context);
    });

    it("showDomeSurfaces functions properly", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });
      sensor.radius = 2000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;

      scene.mode = SceneMode.SCENE3D;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.showDomeSurfaces = true;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("is picked (with inner cone)", function () {
      var camera = scene.camera;
      camera.direction = Cartesian3.clone(Cartesian3.UNIT_Z, camera.direction);
      camera.up = Cartesian3.clone(Cartesian3.UNIT_Y, camera.up);
      var x = 1000000.0;
      var y = 1000000.0;
      var z = -(Ellipsoid.WGS84.radii.z + 10000000.0);
      camera.position = new Cartesian3(x, y, z);

      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
        minimumClockAngle: -165.0,
        maximumClockAngle: 165.0,
        innerHalfAngle: 5.0,
        outerHalfAngle: 85.0,
      });
      sensor.id = "id";
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });
    });

    it("is picked (without inner cone)", function () {
      var camera = scene.camera;
      camera.direction = Cartesian3.clone(Cartesian3.UNIT_Z, camera.direction);
      camera.up = Cartesian3.clone(Cartesian3.UNIT_Y, camera.up);
      var x = 1000000.0;
      var y = 1000000.0;
      var z = -(Ellipsoid.WGS84.radii.z + 10000000.0);
      camera.position = new Cartesian3(x, y, z);

      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
        minimumClockAngle: -165.0,
        maximumClockAngle: 165.0,
        outerHalfAngle: 85.0,
      });
      sensor.id = "id";
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });
    });

    it("renders with environment constraint", function () {
      var sensor = addSensorForEnvironmentConstraint();

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("renders showing environment constraint occlusion", function () {
      var sensor = addSensorForEnvironmentConstraint();

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      expect(scene).toRender(backgroundColor);

      sensor.showEnvironmentOcclusion = true;
      sensor.environmentOcclusionMaterial.uniforms.color = Color.CYAN;
      expect(scene).toRenderAndCall(function (rgba) {
        expect(rgba).not.toEqual(sensorColor);
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.showEnvironmentOcclusion = false;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("renders with environment intersection", function () {
      var sensor = addSensorForEnvironmentConstraint();

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      expect(scene).toRender(backgroundColor);

      sensor.showEnvironmentIntersection = true;
      sensor.environmentIntersectionColor = Color.CYAN;
      sensor.environmentIntersectionWidth = 100.0;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("renders with environment constraint and inner cone", function () {
      var sensor = addSensorForEnvironmentConstraint();
      sensor.innerHalfAngle = CesiumMath.toRadians(1.0);

      var camera = scene.camera;
      camera.direction = new Cartesian3(0.0, 0.15, 1.0);
      Cartesian3.normalize(camera.direction, camera.direction);
      Cartesian3.multiplyByScalar(
        camera.direction,
        sensor.radius - 2.0,
        camera.position
      );
      Cartesian3.cross(camera.direction, camera.up, camera.right);

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      sensor.showEnvironmentIntersection = true;
      sensor.environmentIntersectionColor = Color.CYAN;
      sensor.environmentIntersectionWidth = 100.0;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("isDestroyed", function () {
      var p = new ConicSensor();
      expect(p.isDestroyed()).toEqual(false);
      p.destroy();
      expect(p.isDestroyed()).toEqual(true);
    });

    function checkInfiniteRadiusHasFewerCommands(sensor, frameState) {
      var commands = frameState.commandList;
      commands.length = 0;
      sensor.update(frameState);
      var withFiniteRadius = commands.length;
      sensor.radius = Number.POSITIVE_INFINITY;
      commands.length = 0;
      sensor.update(frameState);
      var withInfiniteRadius = commands.length;
      expect(withInfiniteRadius).toBeLessThan(withFiniteRadius);
    }

    it("infinite radius results in no dome command", function () {
      var frameState = createFrameState(context);

      // Above ellipsoid with no possible intersections.
      var sensor = addSensor({
        altitude: 100.0,
        cone: 180.0,
        radius: 10.0,
      });

      checkInfiniteRadiusHasFewerCommands(sensor, frameState);

      // Below ellipsoid.
      sensor = addSensor({
        altitude: -100.0,
        cone: 180.0,
        radius: 10.0,
      });

      checkInfiniteRadiusHasFewerCommands(sensor, frameState);

      // Above ellipsoid with possible intersections.
      sensor = addSensor({
        altitude: 100.0,
        cone: 180.0,
        radius: 1000.0,
      });

      checkInfiniteRadiusHasFewerCommands(sensor, frameState);
    });

    it("throws if inner half angle is negative", function () {
      var sensor = addSensor();
      sensor.innerHalfAngle = CesiumMath.toRadians(-1.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if inner half angle is greater than pi", function () {
      var sensor = addSensor();
      sensor.innerHalfAngle = CesiumMath.toRadians(181.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if outer half angle is negative", function () {
      var sensor = addSensor();
      sensor.outerHalfAngle = CesiumMath.toRadians(-1.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if outer half angle is greater than pi", function () {
      var sensor = addSensor();
      sensor.outerHalfAngle = CesiumMath.toRadians(181.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if inner half angle is greater than outer half angle", function () {
      var sensor = addSensor();
      sensor.innerHalfAngle = CesiumMath.toRadians(60.0);
      sensor.outerHalfAngle = CesiumMath.toRadians(30.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if inner half angle is equal to outer half angle", function () {
      var sensor = addSensor();
      sensor.innerHalfAngle = CesiumMath.toRadians(30.0);
      sensor.outerHalfAngle = CesiumMath.toRadians(30.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if minimum clock angle is less than 2 * pi", function () {
      var sensor = addSensor();
      sensor.minimumClockAngle = CesiumMath.toRadians(-361.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if minimum clock angle is greater than 2 * pi", function () {
      var sensor = addSensor();
      sensor.minimumClockAngle = CesiumMath.toRadians(360.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if maximum clock angle is less than 2 * pi", function () {
      var sensor = addSensor();
      sensor.maximumClockAngle = CesiumMath.toRadians(-361.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if maximum clock angle is greater than 2 * pi", function () {
      var sensor = addSensor();
      sensor.maximumClockAngle = CesiumMath.toRadians(361.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if minimum clock angle is greater than maximum clock angle", function () {
      var sensor = addSensor();
      sensor.minimumClockAngle = CesiumMath.toRadians(30.0);
      sensor.maximumClockAngle = CesiumMath.toRadians(-30.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if minimum clock angle is equal to maximum clock angle", function () {
      var sensor = addSensor();
      sensor.minimumClockAngle = CesiumMath.toRadians(0.0);
      sensor.maximumClockAngle = CesiumMath.toRadians(0.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if radius is negative", function () {
      var sensor = addSensor();
      sensor.radius = -1.0;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if lateralSurfaceMaterial is not defined", function () {
      var sensor = addSensor();
      sensor.lateralSurfaceMaterial = undefined;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if portionToDisplay is not defined", function () {
      var sensor = addSensor();
      sensor.portionToDisplay = undefined;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if portionToDisplay is invalid", function () {
      var sensor = addSensor();
      sensor.portionToDisplay = -1;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });
  },
  "WebGL"
);
