import { Vector } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { Ellipsoid } from "../../Source/Cesium.js";
import { HeadingPitchRange } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";
import { Matrix4 } from "../../Source/Cesium.js";
import { PrimitiveType } from "../../Source/Cesium.js";
import { Transforms } from "../../Source/Cesium.js";
import createScene from "../createScene.js";
import pollToPromise from "../pollToPromise.js";

describe(
  "Scene/Vector",
  function () {
    var scene;
    var primitives;
    var vector;

    beforeAll(function () {
      scene = createScene({
        scene3DOnly: true,
      });
      primitives = scene.primitives;
      return loadVector({
        show: false,
        length: 20.0,
        id: "id",
      }).then(function (v) {
        vector = v;
      });
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    function loadVector(options) {
      var vector = primitives.add(new Vector(options));
      var model = vector._model;

      return pollToPromise(
        function () {
          // Render scene to progressively load the model
          scene.renderForSpecs();
          return model.ready;
        },
        "ready",
        10000
      ).then(function () {
        vector.zoomTo = function () {
          var center = Matrix4.multiplyByPoint(
            model.modelMatrix,
            model.boundingSphere.center,
            new Matrix4()
          );
          var transform = Transforms.eastNorthUpToFixedFrame(center);

          // View in east-north-up frame
          var camera = scene.camera;
          camera.constrainedAxis = Cartesian3.UNIT_Z;

          var controller = scene.screenSpaceCameraController;
          controller.ellipsoid = Ellipsoid.UNIT_SPHERE;
          controller.enableTilt = false;

          // Zoom in
          var r = Math.max(model.boundingSphere.radius, camera.frustum.near);
          camera.lookAtTransform(
            transform,
            new HeadingPitchRange(0.0, CesiumMath.toRadians(-45.0), r + 1.0)
          );
        };
        return vector;
      });
    }

    it("gets default properties", function () {
      var v = new Vector();

      expect(v.show).toEqual(true);
      expect(v.position).toEqual(Cartesian3.ZERO);
      expect(v.direction).toEqual(Cartesian3.UNIT_Y);
      expect(v.length).toEqual(1.0);
      expect(v.minimumLengthInPixels).toEqual(0.0);
      expect(v.color).toEqual(Color.WHITE);
      expect(v.id).not.toBeDefined();
      expect(v.debugShowBoundingVolume).toEqual(false);
      expect(v.debugWireframe).toEqual(false);
      expect(v.allowPicking).toEqual(true);

      v.destroy();
    });

    it("constructor function sets properties", function () {
      var position = new Cartesian3(1.0, 2.0, 3.0);
      var v = new Vector({
        show: false,
        position: position,
        direction: Cartesian3.UNIT_X,
        length: 2.0,
        minimumLengthInPixels: 1.0,
        color: Color.BLUE,
        id: "id",
        allowPicking: false,
        debugShowBoundingVolume: true,
        debugWireframe: true,
      });

      expect(v.show).toEqual(false);
      expect(v.position).toEqual(position);
      expect(v.direction).toEqual(Cartesian3.UNIT_X);
      expect(v.length).toEqual(2.0);
      expect(v.minimumLengthInPixels).toEqual(1.0);
      expect(v.color).toEqual(Color.BLUE);
      expect(v.id).toEqual("id");
      expect(v.allowPicking).toEqual(false);
      expect(v.debugShowBoundingVolume).toEqual(true);
      expect(v.debugWireframe).toEqual(true);

      v.destroy();
    });

    it("sets properties", function () {
      var position = new Cartesian3(1.0, 2.0, 3.0);
      var v = new Vector();

      v.show = false;
      v.position = position;
      v.direction = Cartesian3.UNIT_X;
      v.length = 2.0;
      v.minimumLengthInPixels = 1.0;
      v.color = Color.BLUE;
      v.id = "id";
      v.debugShowBoundingVolume = true;
      v.debugWireframe = true;

      expect(v.show).toEqual(false);
      expect(v.position).toEqual(position);
      expect(v.direction).toEqual(Cartesian3.UNIT_X);
      expect(v.length).toEqual(2.0);
      expect(v.minimumLengthInPixels).toEqual(1.0);
      expect(v.color).toEqual(Color.BLUE);
      expect(v.id).toEqual("id");
      expect(v.debugShowBoundingVolume).toEqual(true);
      expect(v.debugWireframe).toEqual(true);

      v.destroy();
    });

    it("setting position to undefined to throw", function () {
      expect(function () {
        vector.position = undefined;
      }).toThrowDeveloperError();
    });

    it("setting direction to undefined to throw", function () {
      expect(function () {
        vector.direction = undefined;
      }).toThrowDeveloperError();
    });

    it("renders", function () {
      expect(scene).toRender([0, 0, 0, 255]);

      vector.show = true;
      vector.zoomTo();
      expect(scene).notToRender([0, 0, 0, 255]);
      vector.show = false;
    });

    it("renders along UNIT_Z", function () {
      expect(scene).toRender([0, 0, 0, 255]);

      vector.show = true;
      vector.direction = Cartesian3.UNIT_Z;
      vector.zoomTo();
      expect(scene).notToRender([0, 0, 0, 255]);
      vector.show = false;
      vector.direction = Cartesian3.UNIT_Y;
    });

    it("renders bounding volume", function () {
      expect(scene).toRender([0, 0, 0, 255]);

      vector.show = true;
      vector.debugShowBoundingVolume = true;
      vector.zoomTo();
      expect(scene).notToRender([0, 0, 0, 255]);
      vector.show = false;
      vector.debugShowBoundingVolume = false;
    });

    it("renders in wireframe", function () {
      expect(scene).toRender([0, 0, 0, 255]);

      vector.show = true;
      vector.debugWireframe = true;
      vector.zoomTo();
      scene.renderForSpecs();

      var commands = vector._model._nodeCommands;
      var length = commands.length;
      for (var i = 0; i < length; ++i) {
        expect(commands[i].command.primitiveType).toEqual(PrimitiveType.LINES);
      }

      vector.show = false;
      vector.debugWireframe = false;
    });

    it("is picked", function () {
      vector.show = true;
      vector.zoomTo();

      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(vector);
        expect(result.id).toEqual(vector.id);
      });

      vector.show = false;
    });

    it("is not picked (show === false)", function () {
      vector.zoomTo();

      expect(scene).notToPick();
    });

    it("destroys", function () {
      var v = new Vector();

      expect(v.isDestroyed()).toEqual(false);
      v.destroy();
      expect(v.isDestroyed()).toEqual(true);
    });
  },
  "WebGL"
);
