import { PolygonPrimitive } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { ColorGeometryInstanceAttribute } from "../../Source/Cesium.js";
import { PerInstanceColorAppearance } from "../../Source/Cesium.js";
import { Primitive } from "../../Source/Cesium.js";
import createScene from "../createScene.js";

describe(
  "Scene/PolygonPrimitive",
  function () {
    var scene;

    beforeAll(function () {
      scene = createScene();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("creates and destroys", function () {
      var polygon = new PolygonPrimitive({
        color: Color.RED,
      });
      expect(polygon.color).toEqual(Color.RED);
      expect(polygon.positions).toEqual([]);
      expect(polygon.boundingVolume).toBeDefined();
      expect(polygon.show).toBe(true);
      expect(polygon.isDestroyed()).toBe(false);
      polygon.destroy();
      expect(polygon.isDestroyed()).toBe(true);
    });

    it("creates with options", function () {
      var polygon = new PolygonPrimitive({
        color: Color.RED,
        show: false,
      });

      expect(polygon.show).toBe(false);
    });

    it("creates geometry and updates", function () {
      spyOn(Primitive.prototype, "update");
      var mockFrameState = {};
      var polygon = new PolygonPrimitive({
        color: Color.RED,
      });
      polygon.positions = Cartesian3.fromDegreesArray([
        -123,
        40,
        -122,
        40,
        -122,
        41,
      ]);
      polygon.update(mockFrameState);
      var primitive = polygon._primitive;
      expect(primitive).toBeDefined();
      expect(Primitive.prototype.update).toHaveBeenCalledWith(mockFrameState);

      var instance = primitive.geometryInstances;
      expect(instance).toBeDefined();
      expect(instance.attributes.color).toEqual(
        ColorGeometryInstanceAttribute.fromColor(Color.RED)
      );

      expect(primitive.appearance).toBeInstanceOf(PerInstanceColorAppearance);
    });

    it("hides", function () {
      var polygon = new PolygonPrimitive({
        color: Color.RED,
        show: false,
      });
      polygon.positions = Cartesian3.fromDegreesArray([
        -123,
        40,
        -122,
        40,
        -122,
        41,
      ]);
      polygon.update();
      var primitive = polygon._primitive;
      expect(primitive).toBeUndefined();
    });

    it("does not create primitive with less than 3 positions", function () {
      var polygon = new PolygonPrimitive({
        color: Color.RED,
        show: false,
      });
      polygon.positions = Cartesian3.fromDegreesArray([-123, 40, -122, 40]);
      polygon.update();
      var primitive = polygon._primitive;
      expect(primitive).toBeUndefined();
    });

    it("updates when positions change", function () {
      var polygon = new PolygonPrimitive({
        color: Color.RED,
      });
      polygon.positions = Cartesian3.fromDegreesArray([
        -123,
        40,
        -122,
        40,
        -122,
        41,
      ]);
      scene.primitives.add(polygon);
      scene.renderForSpecs();

      var primitive1 = polygon._primitive;
      expect(primitive1).toBeDefined();

      polygon.positions = Cartesian3.fromDegreesArray([
        -123,
        42,
        -122,
        42,
        -122,
        41,
      ]);
      scene.renderForSpecs();

      var primitive2 = polygon._primitive;
      expect(primitive2).toBeDefined();
      expect(primitive2).not.toEqual(primitive1);
      expect(primitive1.isDestroyed()).toBe(true);
    });

    it("does not make a new primitive when show changes", function () {
      var polygon = new PolygonPrimitive({
        color: Color.RED,
      });
      polygon.positions = Cartesian3.fromDegreesArray([
        -123,
        40,
        -122,
        40,
        -122,
        41,
      ]);
      scene.primitives.add(polygon);
      scene.renderForSpecs();

      var primitive1 = polygon._primitive;
      expect(primitive1).toBeDefined();

      polygon.show = false;
      scene.renderForSpecs();

      var primitive2 = polygon._primitive;
      expect(primitive2).toBeDefined();
      expect(primitive2).toBe(primitive1);
    });
  },
  "WebGL"
);
