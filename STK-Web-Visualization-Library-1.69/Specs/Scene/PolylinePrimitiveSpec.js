import { PolylinePrimitive } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { ColorGeometryInstanceAttribute } from "../../Source/Cesium.js";
import { PolylineColorAppearance } from "../../Source/Cesium.js";
import { PolylineMaterialAppearance } from "../../Source/Cesium.js";
import { Primitive } from "../../Source/Cesium.js";
import createScene from "../createScene.js";

describe(
  "Scene/PolylinePrimitive",
  function () {
    var scene;
    beforeAll(function () {
      scene = createScene();
    });
    afterAll(function () {
      scene.destroyForSpecs();
    });

    it("creates and destroys", function () {
      var polyline = new PolylinePrimitive({
        color: Color.RED,
      });
      expect(polyline.color).toEqual(Color.RED);
      expect(polyline.positions).toEqual([]);
      expect(polyline.width).toEqual(3);
      expect(polyline.loop).toBe(false);
      expect(polyline.dashed).toBe(false);
      expect(polyline.boundingVolume).toBeDefined();
      expect(polyline.show).toBe(true);

      expect(polyline.isDestroyed()).toBe(false);
      polyline.destroy();
      expect(polyline.isDestroyed()).toBe(true);
    });

    it("creates with options", function () {
      var positions = Cartesian3.fromDegreesArray([-120, 50, -121, 20]);
      var polyline = new PolylinePrimitive({
        color: Color.RED,
        show: false,
        width: 5,
        loop: true,
        dashed: true,
        positions: positions,
      });

      expect(polyline.color).toEqual(Color.RED);
      expect(polyline.show).toBe(false);
      expect(polyline.width).toBe(5);
      expect(polyline.loop).toBe(true);
      expect(polyline.dashed).toBe(true);
      expect(polyline.positions).toEqual(positions);
    });

    it("creates polyline and updates", function () {
      spyOn(Primitive.prototype, "update");
      var mockFrameState = {};
      var polyline = new PolylinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -122, 40, -122, 41]),
      });
      polyline.update(mockFrameState);
      var primitive = polyline._primitive;
      expect(primitive).toBeDefined();
      expect(Primitive.prototype.update).toHaveBeenCalledWith(mockFrameState);

      var instance = primitive.geometryInstances;
      expect(instance).toBeDefined();
      expect(instance.attributes.color).toEqual(
        ColorGeometryInstanceAttribute.fromColor(Color.RED)
      );
      expect(instance.attributes.depthFailColor).toEqual(
        ColorGeometryInstanceAttribute.fromColor(Color.RED)
      );

      expect(primitive.appearance).toBeInstanceOf(PolylineColorAppearance);
      expect(primitive.depthFailAppearance).toBeInstanceOf(
        PolylineColorAppearance
      );
    });

    it("creates dashed polyline and updates", function () {
      spyOn(Primitive.prototype, "update");
      var mockFrameState = {};
      var polyline = new PolylinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -122, 40, -122, 41]),
        dashed: true,
      });
      polyline.update(mockFrameState);
      var primitive = polyline._primitive;
      expect(primitive).toBeDefined();
      expect(Primitive.prototype.update).toHaveBeenCalledWith(mockFrameState);

      var instance = primitive.geometryInstances;
      expect(instance).toBeDefined();
      expect(instance.attributes.color).toEqual(
        ColorGeometryInstanceAttribute.fromColor(Color.RED)
      );
      expect(instance.attributes.depthFailColor).toEqual(
        ColorGeometryInstanceAttribute.fromColor(Color.RED)
      );

      expect(primitive.appearance).toBeInstanceOf(PolylineMaterialAppearance);
      expect(primitive.depthFailAppearance).toBeInstanceOf(
        PolylineMaterialAppearance
      );
    });

    it("hides", function () {
      var polyline = new PolylinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40, -122, 40, -122, 41]),
        show: false,
      });
      polyline.update();
      var primitive = polyline._primitive;
      expect(primitive).toBeUndefined();
    });

    it("does not create primitive with less than 2 positions", function () {
      var polyline = new PolylinePrimitive({
        color: Color.RED,
        positions: Cartesian3.fromDegreesArray([-123, 40]),
        show: false,
      });
      polyline.update();
      var primitive = polyline._primitive;
      expect(primitive).toBeUndefined();
    });

    it("updates when positions change", function () {
      var polyline = new PolylinePrimitive({
        color: Color.RED,
      });
      polyline.positions = Cartesian3.fromDegreesArray([
        -123,
        40,
        -122,
        40,
        -122,
        41,
      ]);
      scene.primitives.add(polyline);
      scene.renderForSpecs();

      var primitive1 = polyline._primitive;
      expect(primitive1).toBeDefined();

      polyline.positions = Cartesian3.fromDegreesArray([
        -123,
        42,
        -122,
        42,
        -122,
        41,
      ]);
      scene.renderForSpecs();

      var primitive2 = polyline._primitive;
      expect(primitive2).toBeDefined();
      expect(primitive2).not.toEqual(primitive1);
      expect(primitive1.isDestroyed()).toBe(true);
    });

    it("does not make a new primitive when show changes", function () {
      var polyline = new PolylinePrimitive({
        color: Color.RED,
      });
      polyline.positions = Cartesian3.fromDegreesArray([
        -123,
        40,
        -122,
        40,
        -122,
        41,
      ]);
      scene.primitives.add(polyline);
      scene.renderForSpecs();

      var primitive1 = polyline._primitive;
      expect(primitive1).toBeDefined();

      polyline.show = false;
      scene.renderForSpecs();

      var primitive2 = polyline._primitive;
      expect(primitive2).toBeDefined();
      expect(primitive2).toBe(primitive1);
    });
  },
  "WebGL"
);
