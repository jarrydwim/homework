import { Cartesian2 } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";
import createScene from "../createScene.js";
import { getScreenSpaceScalingMatrix } from "../../Source/Cesium.js";
import { HeadingPitchRoll } from "../../Source/Cesium.js";
import { Matrix3 } from "../../Source/Cesium.js";
import { Matrix4 } from "../../Source/Cesium.js";
import { Quaternion } from "../../Source/Cesium.js";
import { TranslationRotationScale } from "../../Source/Cesium.js";

describe("Scene/getScreenSpaceScalingMatrix", function () {
  var scene;

  beforeAll(function () {
    scene = createScene();
    scene.frameState.passes.render = true;
  });

  afterAll(function () {
    scene.destroyForSpecs();
  });

  it("translation of provided matrix is unaffected", function () {
    var pixelSize = new Cartesian2(0.1, 0.1);
    var maximumSizeInMeters = new Cartesian2(1, 1);

    var hpr = new HeadingPitchRoll(
      CesiumMath.PI_OVER_TWO,
      CesiumMath.PI_OVER_TWO,
      CesiumMath.PI_OVER_TWO
    );

    // create an arbitrary transformation matrix so we can verify only the scale / rotation portion of
    // the matrix is actually affected
    var transRotateScale = Matrix4.fromTranslationRotationScale(
      new TranslationRotationScale(
        new Cartesian3(1, 2, 3),
        Quaternion.fromHeadingPitchRoll(hpr),
        new Cartesian3(0.1, 0.1, 0.1)
      )
    );

    var prevScale = Matrix4.getScale(transRotateScale, new Cartesian3());
    var prevTranslate = Matrix4.getTranslation(
      transRotateScale,
      new Cartesian3()
    );
    var prevRotate = Quaternion.fromRotationMatrix(
      Matrix4.getMatrix3(transRotateScale, new Matrix3())
    );
    var actualRotate = Quaternion.fromRotationMatrix(
      Matrix4.getMatrix3(transRotateScale, new Matrix3())
    );

    transRotateScale = getScreenSpaceScalingMatrix(
      pixelSize,
      maximumSizeInMeters,
      scene.frameState,
      transRotateScale,
      transRotateScale
    );

    var actualScale = Matrix4.getScale(transRotateScale, new Cartesian3());
    var actualTranslate = Matrix4.getTranslation(
      transRotateScale,
      new Cartesian3()
    );
    actualRotate = Quaternion.fromRotationMatrix(
      Matrix4.getMatrix3(transRotateScale, new Matrix3())
    );

    expect(prevTranslate.x).toEqual(actualTranslate.x);
    expect(prevTranslate.y).toEqual(actualTranslate.y);
    expect(prevTranslate.z).toEqual(actualTranslate.z);

    expect(prevRotate.x).not.toEqual(actualRotate.x);
    expect(prevRotate.y).not.toEqual(actualRotate.y);
    expect(prevRotate.z).not.toEqual(actualRotate.z);
    expect(prevScale.x).not.toEqual(actualScale.x);
    expect(prevScale.y).not.toEqual(actualScale.y);
    expect(prevScale.z).not.toEqual(actualScale.z);
  });

  it("maximumSizeInMeters is used / applied non-uniformly", function () {
    var modelMatrix = Matrix4.fromScale(
      new Cartesian3(1, 1, 1),
      Matrix4.clone(Matrix4.IDENTITY)
    );
    var pixelSize = new Cartesian2(100, 100);
    var maximumSizeInMeters = new Cartesian2(50, 75);
    var averagePixelSize =
      (maximumSizeInMeters.x + maximumSizeInMeters.y) / 2.0;

    modelMatrix = getScreenSpaceScalingMatrix(
      pixelSize,
      maximumSizeInMeters,
      scene.frameState,
      modelMatrix,
      modelMatrix
    );
    var scaleFactor = Matrix4.getScale(modelMatrix, new Cartesian3());
    expect(scaleFactor.x).toEqual(maximumSizeInMeters.x);
    expect(scaleFactor.y).toEqual(maximumSizeInMeters.y);
    expect(scaleFactor.z).toEqual(averagePixelSize);
  });

  it("DeveloperError is thrown if negative pixel scaling or negative maximumSizeInMeters provided", function () {
    var pixelSize = new Cartesian2(-128, 0);
    var maximumSizeInMeters = new Cartesian2(1, 1);
    var unused = new Matrix4();
    expect(function () {
      return getScreenSpaceScalingMatrix(
        pixelSize,
        maximumSizeInMeters,
        scene.frameState,
        unused,
        unused
      );
    }).toThrowDeveloperError();

    pixelSize = new Cartesian2(2, 2);
    maximumSizeInMeters = new Cartesian2(-Infinity, 1);
    expect(function () {
      return getScreenSpaceScalingMatrix(
        pixelSize,
        maximumSizeInMeters,
        scene.frameState,
        unused,
        unused
      );
    }).toThrowDeveloperError();
  });

  it("no scaling is performed if pixelSize is 0", function () {
    var pixelSize = new Cartesian2(0, 0);
    var maximumSizeInMeters = new Cartesian2(Infinity, Infinity);
    var scaleFactor = new Cartesian3(1, 1, 1);

    var noScaling = Matrix4.clone(Matrix4.IDENTITY);
    noScaling = getScreenSpaceScalingMatrix(
      pixelSize,
      maximumSizeInMeters,
      scene.frameState,
      noScaling,
      noScaling
    );

    var currentScaling = Matrix4.getScale(noScaling, new Cartesian3());
    expect(currentScaling.x).toEqual(scaleFactor.x);
    expect(currentScaling.y).toEqual(scaleFactor.y);
    expect(currentScaling.z).toEqual(scaleFactor.z);
  });
});
