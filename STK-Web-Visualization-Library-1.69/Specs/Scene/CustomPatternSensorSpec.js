import { CustomPatternSensor } from "../../Source/Cesium.js";
import { BoxGeometry } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Cartographic } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { ColorGeometryInstanceAttribute } from "../../Source/Cesium.js";
import { defaultValue } from "../../Source/Cesium.js";
import { defined } from "../../Source/Cesium.js";
import { Ellipsoid } from "../../Source/Cesium.js";
import { GeometryInstance } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";
import { Matrix3 } from "../../Source/Cesium.js";
import { Matrix4 } from "../../Source/Cesium.js";
import { Transforms } from "../../Source/Cesium.js";
import { ShaderProgram } from "../../Source/Cesium.js";
import { Material } from "../../Source/Cesium.js";
import { PerInstanceColorAppearance } from "../../Source/Cesium.js";
import { Primitive } from "../../Source/Cesium.js";
import { SceneMode } from "../../Source/Cesium.js";
import { SensorVolumePortionToDisplay } from "../../Source/Cesium.js";
import { ShadowMode } from "../../Source/Cesium.js";
import createFrameState from "../createFrameState.js";
import createScene from "../createScene.js";

describe(
  "Scene/CustomPatternSensor",
  function () {
    var scene;
    var context;

    function getSphericals(numberOfVertices) {
      var increment = 360.0 / numberOfVertices;
      var directions = [];
      for (var i = 0; i < numberOfVertices; ++i) {
        directions.push({
          clock: CesiumMath.toRadians(increment * i),
          cone: CesiumMath.toRadians(45.0),
        });
      }
      return directions;
    }

    function getModelMatrix(ellipsoid, cartographic, clock, cone, twist) {
      var location = ellipsoid.cartographicToCartesian(cartographic);
      var modelMatrix = Transforms.northEastDownToFixedFrame(location);
      var orientation = Matrix3.multiply(
        Matrix3.multiply(
          Matrix3.fromRotationZ(clock),
          Matrix3.fromRotationY(cone),
          new Matrix3()
        ),
        Matrix3.fromRotationX(twist),
        new Matrix3()
      );
      return Matrix4.multiply(
        modelMatrix,
        Matrix4.fromRotationTranslation(orientation, Cartesian3.ZERO),
        new Matrix4()
      );
    }

    function addSensor(options) {
      options = defaultValue(options, defaultValue.EMPTY_OBJECT);

      var longitude = defaultValue(options.longitude, 0.0);
      var latitude = defaultValue(options.latitude, 0.0);
      var altitude = defaultValue(options.altitude, 0.0);
      var clock = defaultValue(options.clock, 0.0);
      var cone = defaultValue(options.cone, 0.0);
      var twist = defaultValue(options.twist, 0.0);
      var radius = defaultValue(options.radius, Number.POSITIVE_INFINITY);
      var numberOfVertices = defaultValue(options.numberOfVertices, 8);

      return scene.primitives.add(
        new CustomPatternSensor({
          directions: getSphericals(numberOfVertices),
          radius: radius,
          modelMatrix: getModelMatrix(
            Ellipsoid.WGS84,
            Cartographic.fromDegrees(longitude, latitude, altitude),
            CesiumMath.toRadians(clock),
            CesiumMath.toRadians(cone),
            CesiumMath.toRadians(twist)
          ),
        })
      );
    }

    function addSensorForEnvironmentConstraint() {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 2500000.0,
      });
      sensor.portionToDisplay = SensorVolumePortionToDisplay.COMPLETE;
      sensor.radius = 1000.0;

      var translation = Matrix4.fromTranslation(
        new Cartesian3(0.0, 0.0, 300.0)
      );
      var modelMatrix = Matrix4.multiply(
        sensor.modelMatrix,
        translation,
        new Matrix4()
      );

      scene.primitives.add(
        new Primitive({
          geometryInstances: new GeometryInstance({
            geometry: BoxGeometry.fromDimensions({
              vertexFormat: PerInstanceColorAppearance.VERTEX_FORMAT,
              dimensions: new Cartesian3(100.0, 100.0, 100.0),
            }),
            modelMatrix: modelMatrix,
            attributes: {
              color: ColorGeometryInstanceAttribute.fromColor(
                new Color(1.0, 0.0, 1.0, 0.5)
              ),
            },
          }),
          appearance: new PerInstanceColorAppearance({
            closed: true,
          }),
          asynchronous: false,
          shadows: ShadowMode.CAST_ONLY,
        })
      );

      var camera = scene.camera;
      camera.lookAtTransform(
        sensor.modelMatrix,
        new Cartesian3(0.0, 0.0, sensor.radius - 2.0)
      );

      Cartesian3.negate(camera.direction, camera.direction);
      Cartesian3.cross(camera.direction, camera.up, camera.right);

      return sensor;
    }

    beforeAll(function () {
      scene = createScene();
      context = scene._context;
    });

    afterEach(function () {
      scene.primitives.removeAll();
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    function check2D(context, offset) {
      scene.morphTo2D(0);

      var sensor = scene.primitives.get(0);
      var modelMatrix = sensor.modelMatrix;

      var camera = scene.camera;
      if (!defined(offset)) {
        var offsetZ = 6500000.0;
        offset = new Cartesian3(0.0, 0.0, offsetZ);
      }
      camera.lookAtTransform(modelMatrix, offset);

      expect(scene).notToRender([0, 0, 0, 255]);
    }

    function check3D(context, offset) {
      scene.morphTo3D(0);

      var sensor = scene.primitives.get(0);
      var modelMatrix = sensor.modelMatrix;

      if (!defined(offset)) {
        var offsetZ = 6500000.0;
        offset = new Cartesian3(0.0, 0.0, -offsetZ);
      }

      var camera = scene.camera;
      camera.lookAtTransform(modelMatrix, offset);

      expect(scene).notToRender([0, 0, 0, 255]);
    }

    it("gets the default properties", function () {
      var p = new CustomPatternSensor();
      expect(p.directions).toEqual([]);
      expect(p.ellipsoid).toEqual(Ellipsoid.WGS84);
      expect(p.show).toEqual(true);
      expect(p.showIntersection).toEqual(true);
      expect(p.showThroughEllipsoid).toEqual(false);
      expect(p.portionToDisplay).toEqual(SensorVolumePortionToDisplay.COMPLETE);
      expect(p.modelMatrix).toEqual(Matrix4.IDENTITY);
      expect(p.radius).toEqual(Number.POSITIVE_INFINITY);
      expect(p.lateralSurfaceMaterial.type).toEqual(Material.ColorType);
      expect(p.showLateralSurfaces).toEqual(true);
      expect(p.ellipsoidHorizonSurfaceMaterial).toBeUndefined();
      expect(p.showEllipsoidHorizonSurfaces).toEqual(true);
      expect(p.domeSurfaceMaterial).toBeUndefined();
      expect(p.showDomeSurfaces).toEqual(true);
      expect(p.ellipsoidSurfaceMaterial).toBeUndefined();
      expect(p.showEllipsoidSurfaces).toEqual(true);
      expect(p.intersectionColor).toEqual(Color.WHITE);
      expect(p.intersectionWidth).toEqual(5.0);
      expect(p.id).not.toBeDefined();
      expect(p.environmentConstraint).toEqual(false);
      expect(p.showEnvironmentOcclusion).toEqual(false);
      expect(p.environmentOcclusionMaterial.type).toEqual(Material.ColorType);
      expect(p.showEnvironmentIntersection).toEqual(false);
      expect(p.environmentIntersectionColor).toEqual(Color.WHITE);
      expect(p.environmentIntersectionWidth).toEqual(5.0);
      expect(p.debugShowCrossingPoints).toEqual(false);
      expect(p.debugShowProxyGeometry).toEqual(false);
      expect(p.debugShowBoundingVolume).toEqual(false);
      p.destroy();
    });

    it("constructs with options", function () {
      var stripe = Material.fromType(Material.StripeType);
      var dot = Material.fromType(Material.DotType);
      var grid = Material.fromType(Material.GridType);
      var checkerboard = Material.fromType(Material.CheckerboardType);

      var sphericals = getSphericals(4);

      var p = new CustomPatternSensor({
        directions: sphericals,
        ellipsoid: Ellipsoid.MOON,
        show: false,
        showIntersection: false,
        showThroughEllipsoid: true,
        portionToDisplay: SensorVolumePortionToDisplay.ABOVE_ELLIPSOID_HORIZON,
        modelMatrix: Matrix4.fromUniformScale(2.0),
        radius: 5000000.0,
        lateralSurfaceMaterial: stripe,
        showLateralSurfaces: false,
        ellipsoidHorizonSurfaceMaterial: dot,
        showEllipsoidHorizonSurfaces: false,
        domeSurfaceMaterial: grid,
        showDomeSurfaces: false,
        ellipsoidSurfaceMaterial: checkerboard,
        showEllipsoidSurfaces: false,
        intersectionColor: Color.GREY,
        intersectionWidth: 10.0,
        id: "id",
        environmentConstraint: true,
        showEnvironmentOcclusion: true,
        environmentOcclusionMaterial: grid,
        showEnvironmentIntersection: true,
        environmentIntersectionColor: Color.BLUE,
        environmentIntersectionWidth: 10.0,
        debugShowCrossingPoints: true,
        debugShowProxyGeometry: true,
        debugShowBoundingVolume: true,
      });
      expect(p.directions).toEqual(sphericals);
      expect(p.ellipsoid).toEqual(Ellipsoid.MOON);
      expect(p.show).toEqual(false);
      expect(p.showIntersection).toEqual(false);
      expect(p.showThroughEllipsoid).toEqual(true);
      expect(p.portionToDisplay).toEqual(
        SensorVolumePortionToDisplay.ABOVE_ELLIPSOID_HORIZON
      );
      expect(p.modelMatrix).toEqual(Matrix4.fromUniformScale(2.0));
      expect(p.radius).toEqual(5000000.0);
      expect(p.lateralSurfaceMaterial).toBe(stripe);
      expect(p.showLateralSurfaces).toEqual(false);
      expect(p.ellipsoidHorizonSurfaceMaterial).toBe(dot);
      expect(p.showEllipsoidHorizonSurfaces).toEqual(false);
      expect(p.domeSurfaceMaterial).toBe(grid);
      expect(p.showDomeSurfaces).toEqual(false);
      expect(p.ellipsoidSurfaceMaterial).toBe(checkerboard);
      expect(p.showEllipsoidSurfaces).toEqual(false);
      expect(p.intersectionColor).toEqual(Color.GREY);
      expect(p.intersectionWidth).toEqual(10.0);
      expect(p.id).toEqual("id");
      expect(p.environmentConstraint).toEqual(true);
      expect(p.showEnvironmentOcclusion).toEqual(true);
      expect(p.environmentOcclusionMaterial).toBe(grid);
      expect(p.showEnvironmentIntersection).toEqual(true);
      expect(p.environmentIntersectionColor).toEqual(Color.BLUE);
      expect(p.environmentIntersectionWidth).toEqual(10.0);
      expect(p.debugShowCrossingPoints).toEqual(true);
      expect(p.debugShowProxyGeometry).toEqual(true);
      expect(p.debugShowBoundingVolume).toEqual(true);
      p.destroy();
    });

    it("renders when spanning date line", function () {
      var frameState = createFrameState(context);
      frameState.mode = SceneMode.SCENE2D;
      var commands = frameState.commandList;

      var sensor = addSensor({
        longitude: 0.0,
        latitude: 0.0,
        altitude: 9000000.0,
      });

      sensor.update(frameState);
      expect(commands.length).toEqual(1);

      commands.length = 0;

      sensor = addSensor({
        longitude: 180.0,
        latitude: 0.0,
        altitude: 9000000.0,
      });

      sensor.update(frameState);
      expect(commands.length).toEqual(2);
    });

    it("renders using complete (default) option", function () {
      addSensor({
        latitude: -90.0,
        altitude: 9000000.0,
        radius: 10000000.0,
      });

      check3D(context);
      check2D(context);
    });

    it("renders even if duplicate directions", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 9000000.0,
        radius: 10000000.0,
      });

      // add duplicate points including loop back to first point
      var directions = sensor.directions.slice(0, 2);
      directions.push(directions[1]);
      directions = directions.concat(sensor.directions.slice(2));
      directions.push(directions[0]);
      sensor.directions = directions;

      check3D(context);
      check2D(context);
    });

    it("renders using compiled normals instead of uniforms", function () {
      spyOn(ShaderProgram, "replaceCache").and.callThrough();

      var frameState = createFrameState(context);
      frameState.mode = SceneMode.SCENE3D;

      var sensor = addSensor();
      sensor.update(frameState);

      expect(
        ShaderProgram.replaceCache.calls
          .mostRecent()
          .args[0].fragmentShaderSource.sources.join()
      ).not.toMatch(/u_kDopFacetNormal_/);
    });

    it("renders bounding volume", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 9000000.0,
        radius: 10000000.0,
      });
      sensor.debugShowBoundingVolume = true;

      check3D(context);
      check2D(context);
    });

    it("renders when definition is changed", function () {
      var sensor = addSensor({
        numberOfVertices: 4,
        latitude: -90.0,
        altitude: 8000000.0,
        radius: 10000000.0,
      });
      sensor.portionToDisplay = SensorVolumePortionToDisplay.COMPLETE;

      check3D(context);
      check2D(context);

      // increase number of vertices
      sensor.directions = getSphericals(8);

      check3D(context);
      check2D(context);

      // decrease number of vertices
      sensor.directions = getSphericals(6);

      check3D(context);
      check2D(context);

      // increase number of vertices
      sensor.directions = getSphericals(10);

      check3D(context);
      check2D(context);

      // repeat number of vertices
      sensor.directions = getSphericals(10);

      check3D(context);
      check2D(context);
    });

    it("renders using below ellipsoid horizon option", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 9000000.0,
        radius: 10000000.0,
      });
      sensor.portionToDisplay =
        SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON;

      check3D(context);
      check2D(context);
    });

    it("renders using above ellipsoid horizon option", function () {
      var sensor = addSensor({
        altitude: 9000000.0,
        radius: 10000000.0,
      });
      sensor.portionToDisplay =
        SensorVolumePortionToDisplay.ABOVE_ELLIPSOID_HORIZON;

      check3D(context);
      check2D(context);
    });

    it("renders using outside ellipsoid horizon fragment shader", function () {
      addSensor({
        latitude: -90.0,
        altitude: 8000000.0,
        radius: 5000000.0,
      });

      check3D(context);
    });

    it("renders using inside ellipsoid horizon fragment shader", function () {
      addSensor({
        latitude: -90.0,
        altitude: 12000000.0,
        radius: 5000000.0,
      });

      check3D(context);
    });

    it("renders using outside dome fragment shader", function () {
      addSensor({
        latitude: -90.0,
        altitude: 8000000.0,
        radius: 1000000.0,
      });

      check3D(context);
    });

    it("renders using inside dome fragment shader", function () {
      addSensor({
        latitude: -90.0,
        altitude: 8000000.0,
        radius: 3000000.0,
      });

      check3D(context);
    });

    it("renders nonconvex volume", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 9000000.0,
      });

      var directions = [];
      directions.push({
        clock: CesiumMath.toRadians(0.0),
        cone: CesiumMath.toRadians(14.03624347),
      });
      directions.push({
        clock: CesiumMath.toRadians(74.3),
        cone: CesiumMath.toRadians(32.98357092),
      });
      directions.push({
        clock: CesiumMath.toRadians(80.54),
        cone: CesiumMath.toRadians(37.23483398),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(37.77568431),
      });
      directions.push({
        clock: CesiumMath.toRadians(101.31),
        cone: CesiumMath.toRadians(37.41598862),
      });
      directions.push({
        clock: CesiumMath.toRadians(116.56),
        cone: CesiumMath.toRadians(29.20519037),
      });
      directions.push({
        clock: CesiumMath.toRadians(180.0),
        cone: CesiumMath.toRadians(14.03624347),
      });
      directions.push({
        clock: CesiumMath.toRadians(247.2),
        cone: CesiumMath.toRadians(34.11764003),
      });
      directions.push({
        clock: CesiumMath.toRadians(251.56),
        cone: CesiumMath.toRadians(44.67589157),
      });
      directions.push({
        clock: CesiumMath.toRadians(253.96),
        cone: CesiumMath.toRadians(46.12330271),
      });
      directions.push({
        clock: CesiumMath.toRadians(259.63),
        cone: CesiumMath.toRadians(46.19202903),
      });
      directions.push({
        clock: CesiumMath.toRadians(262.87),
        cone: CesiumMath.toRadians(45.21405547),
      });
      directions.push({
        clock: CesiumMath.toRadians(262.4),
        cone: CesiumMath.toRadians(37.09839406),
      });
      directions.push({
        clock: CesiumMath.toRadians(266.47),
        cone: CesiumMath.toRadians(45.42651157),
      });
      directions.push({
        clock: CesiumMath.toRadians(270.0),
        cone: CesiumMath.toRadians(45.42651157),
      });
      directions.push({
        clock: CesiumMath.toRadians(270.97),
        cone: CesiumMath.toRadians(36.40877457),
      });
      directions.push({
        clock: CesiumMath.toRadians(272.79),
        cone: CesiumMath.toRadians(45.70731937),
      });
      directions.push({
        clock: CesiumMath.toRadians(278.33),
        cone: CesiumMath.toRadians(45.98533395),
      });
      directions.push({
        clock: CesiumMath.toRadians(281.89),
        cone: CesiumMath.toRadians(36.0358894),
      });
      directions.push({
        clock: CesiumMath.toRadians(282.99),
        cone: CesiumMath.toRadians(45.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(286.99),
        cone: CesiumMath.toRadians(43.26652934),
      });
      directions.push({
        clock: CesiumMath.toRadians(291.8),
        cone: CesiumMath.toRadians(33.97011942),
      });
      directions.push({
        clock: CesiumMath.toRadians(293.3),
        cone: CesiumMath.toRadians(41.50882857),
      });
      directions.push({
        clock: CesiumMath.toRadians(300.96),
        cone: CesiumMath.toRadians(36.0826946),
      });
      directions.push({
        clock: CesiumMath.toRadians(319.18),
        cone: CesiumMath.toRadians(19.9888568),
      });

      sensor.radius = 10000000.0;
      sensor.directions = directions;

      check3D(context);
      check2D(context);
    });

    //Ignored until https://github.com/CesiumGS/cesium-analytics/issues/384 is fixed.
    xit("renders coplanar span of directions", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 9000000.0,
        cone: 15.0,
      });

      var directions = [];
      directions.push({
        clock: CesiumMath.toRadians(-135.0),
        cone: CesiumMath.toRadians(45.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(-90.0),
        cone: CesiumMath.toRadians(45.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(-90.0),
        cone: CesiumMath.toRadians(40.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(-90.0),
        cone: CesiumMath.toRadians(35.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(-90.0),
        cone: CesiumMath.toRadians(30.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(-90.0),
        cone: CesiumMath.toRadians(25.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(-90.0),
        cone: CesiumMath.toRadians(20.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(-90.0),
        cone: CesiumMath.toRadians(15.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(-90.0),
        cone: CesiumMath.toRadians(10.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(-90.0),
        cone: CesiumMath.toRadians(5.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(0.0),
        cone: CesiumMath.toRadians(0.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(5.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(10.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(15.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(20.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(25.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(30.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(35.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(40.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(90.0),
        cone: CesiumMath.toRadians(45.0),
      });
      directions.push({
        clock: CesiumMath.toRadians(135.0),
        cone: CesiumMath.toRadians(45.0),
      });

      sensor.radius = 10000000.0;
      sensor.directions = directions;

      check3D(
        context,
        new Cartesian3(-sensor.radius * 0.5, 0.0, sensor.radius + 5000000.0)
      );
      check2D(context);
    });

    it("renders sensor on surface looking up", function () {
      addSensor({
        longitude: 0.0,
        latitude: -90.0,
        altitude: 0.0,
        cone: 180.0,
        radius: 5000000.0,
      });

      check3D(context);
    });

    it("2D definition change affects 3D", function () {
      var frameState = createFrameState(context);

      var sensor = addSensor({
        numberOfVertices: 4,
      });

      // Update first in 3D and check.
      frameState.mode = SceneMode.SCENE3D;
      sensor.update(frameState);
      expect(sensor._ellipsoidHorizonSurfaceColorCommands.length).toEqual(5);

      // Switch to 2D.
      frameState.mode = SceneMode.SCENE2D;
      sensor.update(frameState);

      // Change definition in 2D.
      sensor.directions = getSphericals(8);
      sensor.update(frameState);
      expect(sensor._ellipsoidHorizonSurfaceColorCommands.length).toEqual(9);
    });

    it("does not render when show is false", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
        radius: 10000000.0,
      });
      sensor.show = true;

      var frameState = createFrameState(context);
      frameState.mode = SceneMode.SCENE3D;
      sensor.update(frameState);
      expect(frameState.commandList.length).toEqual(11);

      sensor.show = false;

      frameState.commandList.length = 0;
      sensor.update(frameState);
      expect(frameState.commandList.length).toEqual(0);
    });

    it("renders opaque lateral surfaces", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 9000000.0,
        radius: 2000000.0,
      });
      sensor.radius = 2000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = true;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;

      sensor.lateralSurfaceMaterial = Material.fromType("Color");
      sensor.lateralSurfaceMaterial.uniforms.color = new Color(
        1.0,
        1.0,
        1.0,
        1.0
      );

      check3D(context);
    });

    it("showLateralSurfaces functions properly", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 9000000.0,
        radius: 2000000.0,
      });
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;

      scene.mode = SceneMode.SCENE3D;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.showLateralSurfaces = true;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("renders opaque ellipsoid horizon surfaces", function () {
      var sensor = addSensor({
        altitude: 3000000.0,
      });
      sensor.radius = 20000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = true;
      sensor.showDomeSurfaces = false;

      sensor.ellipsoidHorizonSurfaceMaterial = Material.fromType("Color");
      sensor.ellipsoidHorizonSurfaceMaterial.uniforms.color = new Color(
        1.0,
        1.0,
        1.0,
        1.0
      );

      // check3D doesn't work here because there is a huge hole where the Earth is.
      // The position was found in Viewer by panning and zooming until the camera
      // was close enough to the ellipsoid horizon surfaces. Then align up to north
      // and create the orthonormal basis.
      var camera = scene.camera;
      camera.lookAtTransform(Matrix4.IDENTITY);
      camera.position = new Cartesian3(
        -132408.72122866797,
        -10631460.24052539,
        2091551.1791969084
      );
      Cartesian3.negate(camera.position, camera.direction);
      Cartesian3.normalize(camera.direction, camera.direction);
      Cartesian3.cross(camera.direction, Cartesian3.UNIT_Z, camera.right);
      Cartesian3.cross(camera.right, camera.direction, camera.up);
      Cartesian3.normalize(camera.up, camera.up);
      Cartesian3.cross(camera.direction, camera.up, camera.right);
      Cartesian3.normalize(camera.right, camera.right);

      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("showEllipsoidHorizonSurfaces functions properly", function () {
      var sensor = addSensor({
        latitude: 0.0,
        altitude: 3000000.0,
      });
      sensor.radius = 20000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;

      scene.mode = SceneMode.SCENE3D;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.showEllipsoidHorizonSurfaces = true;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("renders opaque dome surfaces", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });
      sensor.radius = 2000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = true;

      sensor.domeSurfaceMaterial = Material.fromType("Color");
      sensor.domeSurfaceMaterial.uniforms.color = new Color(1.0, 1.0, 1.0, 1.0);

      check3D(context);
    });

    it("showDomeSurfaces functions properly", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });
      sensor.radius = 2000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;

      scene.mode = SceneMode.SCENE3D;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.showDomeSurfaces = true;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("is picked", function () {
      var camera = scene.camera;
      camera.direction = Cartesian3.clone(Cartesian3.UNIT_Z, camera.direction);
      camera.up = Cartesian3.clone(Cartesian3.UNIT_Y, camera.up);
      var x = 1000000.0;
      var y = 1000000.0;
      var z = -(Ellipsoid.WGS84.radii.z + 10000000.0);
      camera.position = new Cartesian3(x, y, z);

      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });
      sensor.id = "id";
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });
    });

    it("renders with environment constraint", function () {
      var sensor = addSensorForEnvironmentConstraint();

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("renders showing environment constraint occlusion", function () {
      var sensor = addSensorForEnvironmentConstraint();

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      expect(scene).toRender(backgroundColor);

      sensor.showEnvironmentOcclusion = true;
      sensor.environmentOcclusionMaterial.uniforms.color = Color.CYAN;
      expect(scene).toRenderAndCall(function (rgba) {
        expect(rgba).not.toEqual(sensorColor);
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.showEnvironmentOcclusion = false;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("renders with environment intersection", function () {
      var sensor = addSensorForEnvironmentConstraint();

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      expect(scene).toRender(backgroundColor);

      sensor.showEnvironmentIntersection = true;
      sensor.environmentIntersectionColor = Color.CYAN;
      sensor.environmentIntersectionWidth = 100.0;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("isDestroyed", function () {
      var p = new CustomPatternSensor();
      expect(p.isDestroyed()).toEqual(false);
      p.destroy();
      expect(p.isDestroyed()).toEqual(true);
    });

    function checkInfiniteRadiusHasFewerCommands(sensor, frameState) {
      var commands = frameState.commandList;
      commands.length = 0;
      sensor.update(frameState);
      var withFiniteRadius = commands.length;
      sensor.radius = Number.POSITIVE_INFINITY;
      commands.length = 0;
      sensor.update(frameState);
      var withInfiniteRadius = commands.length;
      expect(withInfiniteRadius).toBeLessThan(withFiniteRadius);
    }

    it("infinite radius results in no dome command", function () {
      var frameState = createFrameState(context);

      // Above ellipsoid with no possible intersections.
      var sensor = addSensor({
        altitude: 100.0,
        cone: 180.0,
        radius: 10.0,
      });

      checkInfiniteRadiusHasFewerCommands(sensor, frameState);

      // Below ellipsoid.
      sensor = addSensor({
        altitude: -100.0,
        cone: 180.0,
        radius: 10.0,
      });

      checkInfiniteRadiusHasFewerCommands(sensor, frameState);

      // Above ellipsoid with possible intersections.
      sensor = addSensor({
        altitude: 100.0,
        cone: 180.0,
        radius: 1000.0,
      });

      checkInfiniteRadiusHasFewerCommands(sensor, frameState);
    });

    it("throws if radius is negative", function () {
      var sensor = addSensor();
      sensor.radius = -1.0;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if lateralSurfaceMaterial is not defined", function () {
      var sensor = addSensor();
      sensor.lateralSurfaceMaterial = undefined;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if portionToDisplay is not defined", function () {
      var sensor = addSensor();
      sensor.portionToDisplay = undefined;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if portionToDisplay is invalid", function () {
      var sensor = addSensor();
      sensor.portionToDisplay = -1;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });
  },
  "WebGL"
);
