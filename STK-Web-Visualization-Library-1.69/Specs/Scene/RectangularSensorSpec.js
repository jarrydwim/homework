import { RectangularSensor } from "../../Source/Cesium.js";
import { BoxGeometry } from "../../Source/Cesium.js";
import { Cartesian3 } from "../../Source/Cesium.js";
import { Cartographic } from "../../Source/Cesium.js";
import { Color } from "../../Source/Cesium.js";
import { ColorGeometryInstanceAttribute } from "../../Source/Cesium.js";
import { defaultValue } from "../../Source/Cesium.js";
import { Ellipsoid } from "../../Source/Cesium.js";
import { GeometryInstance } from "../../Source/Cesium.js";
import { Math as CesiumMath } from "../../Source/Cesium.js";
import { Matrix3 } from "../../Source/Cesium.js";
import { Matrix4 } from "../../Source/Cesium.js";
import { Transforms } from "../../Source/Cesium.js";
import { ShaderProgram } from "../../Source/Cesium.js";
import { Material } from "../../Source/Cesium.js";
import { PerInstanceColorAppearance } from "../../Source/Cesium.js";
import { Primitive } from "../../Source/Cesium.js";
import { SceneMode } from "../../Source/Cesium.js";
import { SensorVolumePortionToDisplay } from "../../Source/Cesium.js";
import { ShadowMode } from "../../Source/Cesium.js";
import createFrameState from "../createFrameState.js";
import createScene from "../createScene.js";

describe(
  "Scene/RectangularSensor",
  function () {
    var scene;
    var context;

    beforeAll(function () {
      scene = createScene();
      context = scene._context;
    });

    afterAll(function () {
      scene.destroyForSpecs();
    });

    function getModelMatrix(ellipsoid, cartographic, clock, cone, twist) {
      var location = ellipsoid.cartographicToCartesian(cartographic);
      var modelMatrix = Transforms.northEastDownToFixedFrame(location);
      var orientation = Matrix3.multiply(
        Matrix3.multiply(
          Matrix3.fromRotationZ(clock),
          Matrix3.fromRotationY(cone),
          new Matrix3()
        ),
        Matrix3.fromRotationX(twist),
        new Matrix3()
      );
      return Matrix4.multiply(
        modelMatrix,
        Matrix4.fromRotationTranslation(orientation, Cartesian3.ZERO),
        new Matrix4()
      );
    }

    function addSensor(options) {
      options = defaultValue(options, defaultValue.EMPTY_OBJECT);

      var longitude = defaultValue(options.longitude, 0.0);
      var latitude = defaultValue(options.latitude, 0.0);
      var altitude = defaultValue(options.altitude, 0.0);
      var clock = defaultValue(options.clock, 0.0);
      var cone = defaultValue(options.cone, 0.0);
      var twist = defaultValue(options.twist, 0.0);

      return scene.primitives.add(
        new RectangularSensor({
          xHalfAngle: CesiumMath.toRadians(45.0),
          yHalfAngle: CesiumMath.toRadians(45.0),
          modelMatrix: getModelMatrix(
            Ellipsoid.WGS84,
            new Cartographic(
              CesiumMath.toRadians(longitude),
              CesiumMath.toRadians(latitude),
              altitude
            ),
            CesiumMath.toRadians(clock),
            CesiumMath.toRadians(cone),
            CesiumMath.toRadians(twist)
          ),
        })
      );
    }

    function addSensorForEnvironmentConstraint() {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 2500000.0,
      });
      sensor.portionToDisplay = SensorVolumePortionToDisplay.COMPLETE;
      sensor.radius = 1000.0;

      var translation = Matrix4.fromTranslation(
        new Cartesian3(0.0, 0.0, 300.0)
      );
      var modelMatrix = Matrix4.multiply(
        sensor.modelMatrix,
        translation,
        new Matrix4()
      );

      scene.primitives.add(
        new Primitive({
          geometryInstances: new GeometryInstance({
            geometry: BoxGeometry.fromDimensions({
              vertexFormat: PerInstanceColorAppearance.VERTEX_FORMAT,
              dimensions: new Cartesian3(100.0, 100.0, 100.0),
            }),
            modelMatrix: modelMatrix,
            attributes: {
              color: ColorGeometryInstanceAttribute.fromColor(
                new Color(1.0, 0.0, 1.0, 0.5)
              ),
            },
          }),
          appearance: new PerInstanceColorAppearance({
            closed: true,
          }),
          asynchronous: false,
          shadows: ShadowMode.CAST_ONLY,
        })
      );

      var camera = scene.camera;
      camera.lookAtTransform(
        sensor.modelMatrix,
        new Cartesian3(0.0, 0.0, sensor.radius - 2.0)
      );

      Cartesian3.negate(camera.direction, camera.direction);
      Cartesian3.cross(camera.direction, camera.up, camera.right);

      return sensor;
    }

    beforeEach(function () {
      var camera = scene.camera;
      camera.direction = Cartesian3.clone(Cartesian3.UNIT_Z, camera.direction);
      camera.up = Cartesian3.clone(Cartesian3.UNIT_Y, camera.up);
      var z = -(Ellipsoid.WGS84.radii.z + 10000000.0);
      camera.position = new Cartesian3(0.0, 0.0, z);
    });

    afterEach(function () {
      scene.primitives.removeAll();
    });

    it("gets the default properties", function () {
      var p = new RectangularSensor();
      expect(p.ellipsoid).toEqual(Ellipsoid.WGS84);
      expect(p.show).toEqual(true);
      expect(p.showIntersection).toEqual(true);
      expect(p.showThroughEllipsoid).toEqual(false);
      expect(p.portionToDisplay).toEqual(SensorVolumePortionToDisplay.COMPLETE);
      expect(p.modelMatrix).toEqual(Matrix4.IDENTITY);
      expect(p.xHalfAngle).toEqual(CesiumMath.PI_OVER_TWO);
      expect(p.yHalfAngle).toEqual(CesiumMath.PI_OVER_TWO);
      expect(p.radius).toEqual(Number.POSITIVE_INFINITY);
      expect(p.lateralSurfaceMaterial.type).toEqual(Material.ColorType);
      expect(p.showLateralSurfaces).toEqual(true);
      expect(p.ellipsoidHorizonSurfaceMaterial).toBeUndefined();
      expect(p.showEllipsoidHorizonSurfaces).toEqual(true);
      expect(p.domeSurfaceMaterial).toBeUndefined();
      expect(p.showDomeSurfaces).toEqual(true);
      expect(p.intersectionColor).toEqual(Color.WHITE);
      expect(p.intersectionWidth).toEqual(5.0);
      expect(p.id).not.toBeDefined();
      expect(p.environmentConstraint).toEqual(false);
      expect(p.showEnvironmentOcclusion).toEqual(false);
      expect(p.environmentOcclusionMaterial.type).toEqual(Material.ColorType);
      expect(p.showEnvironmentIntersection).toEqual(false);
      expect(p.environmentIntersectionColor).toEqual(Color.WHITE);
      expect(p.environmentIntersectionWidth).toEqual(5.0);
      expect(p.debugShowCrossingPoints).toEqual(false);
      expect(p.debugShowProxyGeometry).toEqual(false);
      expect(p.debugShowBoundingVolume).toEqual(false);
      p.destroy();
    });

    it("constructs with options", function () {
      var stripe = Material.fromType(Material.StripeType);
      var dot = Material.fromType(Material.StripeType);
      var grid = Material.fromType(Material.GridType);

      var p = new RectangularSensor({
        ellipsoid: Ellipsoid.MOON,
        show: false,
        showIntersection: false,
        showThroughEllipsoid: true,
        portionToDisplay: SensorVolumePortionToDisplay.ABOVE_ELLIPSOID_HORIZON,
        modelMatrix: Matrix4.fromUniformScale(2.0),
        xHalfAngle: CesiumMath.PI_OVER_FOUR,
        yHalfAngle: CesiumMath.PI_OVER_FOUR,
        radius: 5000000.0,
        lateralSurfaceMaterial: stripe,
        showLateralSurfaces: false,
        ellipsoidHorizonSurfaceMaterial: dot,
        showEllipsoidHorizonSurfaces: false,
        domeSurfaceMaterial: grid,
        showDomeSurfaces: false,
        intersectionColor: Color.GREY,
        intersectionWidth: 10.0,
        id: "id",
        environmentConstraint: true,
        showEnvironmentOcclusion: true,
        environmentOcclusionMaterial: grid,
        showEnvironmentIntersection: true,
        environmentIntersectionColor: Color.BLUE,
        environmentIntersectionWidth: 10.0,
        debugShowCrossingPoints: true,
        debugShowProxyGeometry: true,
        debugShowBoundingVolume: true,
      });
      expect(p.ellipsoid).toEqual(Ellipsoid.MOON);
      expect(p.show).toEqual(false);
      expect(p.showIntersection).toEqual(false);
      expect(p.showThroughEllipsoid).toEqual(true);
      expect(p.portionToDisplay).toEqual(
        SensorVolumePortionToDisplay.ABOVE_ELLIPSOID_HORIZON
      );
      expect(p.modelMatrix).toEqual(Matrix4.fromUniformScale(2.0));
      expect(p.xHalfAngle).toEqual(CesiumMath.PI_OVER_FOUR);
      expect(p.yHalfAngle).toEqual(CesiumMath.PI_OVER_FOUR);
      expect(p.radius).toEqual(5000000.0);
      expect(p.lateralSurfaceMaterial).toBe(stripe);
      expect(p.showLateralSurfaces).toEqual(false);
      expect(p.ellipsoidHorizonSurfaceMaterial).toBe(dot);
      expect(p.showEllipsoidHorizonSurfaces).toEqual(false);
      expect(p.domeSurfaceMaterial).toBe(grid);
      expect(p.showDomeSurfaces).toEqual(false);
      expect(p.intersectionColor).toEqual(Color.GREY);
      expect(p.intersectionWidth).toEqual(10.0);
      expect(p.id).toEqual("id");
      expect(p.environmentConstraint).toEqual(true);
      expect(p.showEnvironmentOcclusion).toEqual(true);
      expect(p.environmentOcclusionMaterial).toBe(grid);
      expect(p.showEnvironmentIntersection).toEqual(true);
      expect(p.environmentIntersectionColor).toEqual(Color.BLUE);
      expect(p.environmentIntersectionWidth).toEqual(10.0);
      expect(p.debugShowCrossingPoints).toEqual(true);
      expect(p.debugShowProxyGeometry).toEqual(true);
      expect(p.debugShowBoundingVolume).toEqual(true);
      p.destroy();
    });

    it("renders using complete (default) option", function () {
      var camera = scene.camera;
      camera.direction = Cartesian3.clone(Cartesian3.UNIT_Z, camera.direction);
      camera.up = Cartesian3.clone(Cartesian3.UNIT_Y, camera.up);
      var x = 1000000.0;
      var y = 1000000.0;
      var z = -(Ellipsoid.WGS84.radii.z + 10000000.0);
      camera.position = new Cartesian3(x, y, z);

      var sensor = addSensor({
        latitude: -90.0,
        altitude: 2500000.0,
      });
      sensor.portionToDisplay = SensorVolumePortionToDisplay.COMPLETE;

      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("renders using uniforms instead of compiled normals", function () {
      spyOn(ShaderProgram, "replaceCache").and.callThrough();

      var frameState = createFrameState(context);
      frameState.mode = SceneMode.SCENE3D;

      var sensor = addSensor();
      sensor.update(frameState);

      expect(
        ShaderProgram.replaceCache.calls
          .mostRecent()
          .args[0].fragmentShaderSource.sources.join()
      ).toMatch(/u_kDopFacetNormal_/);
    });

    it("renders using below ellipsoid horizon option", function () {
      var camera = scene.camera;
      camera.direction = Cartesian3.clone(Cartesian3.UNIT_Z, camera.direction);
      camera.up = Cartesian3.clone(Cartesian3.UNIT_Y, camera.up);
      var x = 1000000.0;
      var y = 1000000.0;
      var z = -(Ellipsoid.WGS84.radii.z + 10000000.0);
      camera.position = new Cartesian3(x, y, z);

      var sensor = addSensor({
        latitude: -90.0,
        altitude: 2500000.0,
      });
      sensor.portionToDisplay =
        SensorVolumePortionToDisplay.BELOW_ELLIPSOID_HORIZON;

      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("renders using above ellipsoid horizon option", function () {
      var camera = scene.camera;
      camera.direction = Cartesian3.clone(Cartesian3.UNIT_Z, camera.direction);
      camera.up = Cartesian3.clone(Cartesian3.UNIT_Y, camera.up);
      var x = 1000000.0;
      var y = 1000000.0;
      var z = -(Ellipsoid.WGS84.radii.z + 10000000.0);
      camera.position = new Cartesian3(x, y, z);

      var sensor = addSensor({
        latitude: -90.0,
        altitude: 2500000.0,
      });
      sensor.portionToDisplay =
        SensorVolumePortionToDisplay.ABOVE_ELLIPSOID_HORIZON;

      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("renders", function () {
      addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });

      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("renders bounding volumes", function () {
      addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
        debugShowBoundingVolume: true,
      });

      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("does not render when show is false", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });
      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);
    });

    it("showLateralSurfaces functions properly", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });
      sensor.radius = 2000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.showLateralSurfaces = true;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("showEllipsoidHorizonSurfaces functions properly", function () {
      var sensor = addSensor({
        latitude: 0.0,
        altitude: 3000000.0,
      });
      sensor.radius = 20000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.showEllipsoidHorizonSurfaces = true;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("showDomeSurfaces functions properly", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });
      sensor.radius = 2000000.0;
      sensor.show = true;
      sensor.showLateralSurfaces = false;
      sensor.showEllipsoidHorizonSurfaces = false;
      sensor.showDomeSurfaces = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = false;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.showDomeSurfaces = true;
      expect(scene).toRender([0, 0, 0, 255]);

      sensor.show = true;
      expect(scene).notToRender([0, 0, 0, 255]);
    });

    it("is picked", function () {
      var sensor = addSensor({
        latitude: -90.0,
        altitude: 3000000.0,
      });
      sensor.id = "id";
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });
    });

    it("is wider in x model direction", function () {
      // The camera is looking along the negative x-axis with z as the up direction and 2km from the surface.
      var camera = scene.camera;
      camera.direction = Cartesian3.negate(Cartesian3.UNIT_X, new Cartesian3());
      camera.up = Cartesian3.clone(Cartesian3.UNIT_Z, camera.up);
      var x = Ellipsoid.WGS84.radii.x + 2000.0;
      camera.position = new Cartesian3(x, 0.0, 0.0);

      // Verify that there is nothing to pick.
      expect(scene).notToPick();

      // The sensor is located on the x-axis at 1km above the surface and oriented along north-east-down (x is north, y is east, Z is down).
      var sensor = addSensor({
        longitude: 0.0,
        latitude: 0.0,
        altitude: 1000.0,
      });
      sensor.id = "id";

      // The half angle along the x-axis is much larger than the half angle along the y-axis.
      sensor.xHalfAngle = CesiumMath.toRadians(85.0);
      sensor.yHalfAngle = CesiumMath.toRadians(5.0);

      // Verify that we can pick the sensor.
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });

      // Move the camera 'up' along the z-direction which is north and corresponds to the sensor model x-axis.
      camera.position = new Cartesian3(x, 0.0, 1000.0);

      // Verify that we can pick the sensor.
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });

      // Move the camera 'down' along the -z-direction which is south and corresponds to the sensor model -x-axis.
      camera.position = new Cartesian3(x, 0.0, -1000.0);

      // Verify that we can pick the sensor.
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });

      // Move the camera 'right' along the y-direction which is east and corresponds to the sensor model y-axis.
      camera.position = new Cartesian3(x, 1000.0, 0.0);

      // Verify that we cannot pick the sensor.
      expect(scene).notToPick();

      // Move the camera 'left' along the -y-direction which is west and corresponds to the sensor model -y-axis.
      camera.position = new Cartesian3(x, -1000.0, 0.0);

      // Verify that we cannot pick the sensor.
      expect(scene).notToPick();
    });

    it("is wider in y model direction", function () {
      // The camera is looking along the negative x-axis with z as the up direction and 2km from the surface.
      var camera = scene.camera;
      camera.direction = Cartesian3.negate(Cartesian3.UNIT_X, new Cartesian3());
      camera.up = Cartesian3.clone(Cartesian3.UNIT_Z, camera.up);
      var x = Ellipsoid.WGS84.radii.x + 2000.0;
      camera.position = new Cartesian3(x, 0.0, 0.0);

      // Verify that there is nothing to pick.
      expect(scene).notToPick();

      // The sensor is located on the x-axis at 1km above the surface and oriented along north-east-down (x is north, y is east, Z is down).
      var sensor = addSensor({
        longitude: 0.0,
        latitude: 0.0,
        altitude: 1000.0,
      });
      sensor.id = "id";

      // The half angle along the x-axis is much larger than the half angle along the y-axis.
      sensor.xHalfAngle = CesiumMath.toRadians(5.0);
      sensor.yHalfAngle = CesiumMath.toRadians(85.0);

      // Verify that we can pick the sensor.
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });

      // Move the camera 'up' along the z-direction which is north and corresponds to the sensor model x-axis.
      camera.position = new Cartesian3(x, 0.0, 1000.0);

      // Verify that we cannot pick the sensor.
      expect(scene).notToPick();

      // Move the camera 'down' along the -z-direction which is south and corresponds to the sensor model -x-axis.
      camera.position = new Cartesian3(x, 0.0, -1000.0);

      // Verify that we cannot pick the sensor.
      expect(scene).notToPick();

      // Move the camera 'right' along the y-direction which is east and corresponds to the sensor model y-axis.
      camera.position = new Cartesian3(x, 1000.0, 0.0);

      // Verify that we can pick the sensor.
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });

      // Move the camera 'left' along the -y-direction which is west and corresponds to the sensor model -y-axis.
      camera.position = new Cartesian3(x, -1000.0, 0.0);

      // Verify that we can pick the sensor.
      expect(scene).toPickAndCall(function (result) {
        expect(result.primitive).toEqual(sensor);
        expect(result.id).toEqual("id");
      });
    });

    it("renders with environment constraint", function () {
      var sensor = addSensorForEnvironmentConstraint();

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("renders showing environment constraint occlusion", function () {
      var sensor = addSensorForEnvironmentConstraint();

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      expect(scene).toRender(backgroundColor);

      sensor.showEnvironmentOcclusion = true;
      sensor.environmentOcclusionMaterial.uniforms.color = Color.CYAN;
      expect(scene).toRenderAndCall(function (rgba) {
        expect(rgba).not.toEqual(sensorColor);
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.showEnvironmentOcclusion = false;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("renders with environment intersection", function () {
      var sensor = addSensorForEnvironmentConstraint();

      var backgroundColor = [0, 0, 0, 255];
      var sensorColor;
      expect(scene).toRenderAndCall(function (rgba) {
        sensorColor = rgba;
        expect(rgba).not.toEqual(backgroundColor);
      });

      sensor.environmentConstraint = true;
      expect(scene).toRender(backgroundColor);

      sensor.showEnvironmentIntersection = true;
      sensor.environmentIntersectionColor = Color.CYAN;
      sensor.environmentIntersectionWidth = 100.0;
      expect(scene).toRender(backgroundColor);

      sensor.environmentConstraint = false;
      expect(scene).toRender(sensorColor);
    });

    it("isDestroyed", function () {
      var p = new RectangularSensor();
      expect(p.isDestroyed()).toEqual(false);
      p.destroy();
      expect(p.isDestroyed()).toEqual(true);
    });

    it("throws if radius is negative", function () {
      var sensor = addSensor();
      sensor.radius = -1.0;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if lateralSurfaceMaterial is not defined", function () {
      var sensor = addSensor();
      sensor.lateralSurfaceMaterial = undefined;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if xHalfAngle is greater than 90 degrees", function () {
      var sensor = addSensor();
      sensor.xHalfAngle = CesiumMath.toRadians(91.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if yHalfAngle is greater than 90 degrees", function () {
      var sensor = addSensor();
      sensor.yHalfAngle = CesiumMath.toRadians(91.0);
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if portionToDisplay is not defined", function () {
      var sensor = addSensor();
      sensor.portionToDisplay = undefined;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });

    it("throws if portionToDisplay is invalid", function () {
      var sensor = addSensor();
      sensor.portionToDisplay = -1;
      var frameState = createFrameState(context);
      expect(function () {
        sensor.update(frameState);
      }).toThrowDeveloperError();
    });
  },
  "WebGL"
);
