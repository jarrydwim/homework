/* This file is automatically rebuilt by the Cesium build process. */
define(['./when-5d1c9db5', './Check-0081bf29', './Math-c5b6b5a3', './Cartesian2-965e01f0', './Transforms-81e56fb7', './RuntimeError-77179770', './WebGLConstants-21451dcd', './ComponentDatatype-6080fe8f', './GeometryAttribute-7c8a441d', './GeometryAttributes-281d90a3', './AttributeCompression-299f9164', './GeometryPipeline-f4aec327', './EncodedCartesian3-7b3f6b05', './IndexDatatype-748ddad3', './IntersectionTests-dc52115e', './Plane-a11f4cce', './PrimitivePipeline-4d953c83', './WebMercatorProjection-fab569d0', './createTaskProcessorWorker'], function (when, Check, _Math, Cartesian2, Transforms, RuntimeError, WebGLConstants, ComponentDatatype, GeometryAttribute, GeometryAttributes, AttributeCompression, GeometryPipeline, EncodedCartesian3, IndexDatatype, IntersectionTests, Plane, PrimitivePipeline, WebMercatorProjection, createTaskProcessorWorker) { 'use strict';

  function combineGeometry(packedParameters, transferableObjects) {
    var parameters = PrimitivePipeline.PrimitivePipeline.unpackCombineGeometryParameters(
      packedParameters
    );
    var results = PrimitivePipeline.PrimitivePipeline.combineGeometry(parameters);
    return PrimitivePipeline.PrimitivePipeline.packCombineGeometryResults(
      results,
      transferableObjects
    );
  }
  var combineGeometry$1 = createTaskProcessorWorker(combineGeometry);

  return combineGeometry$1;

});
