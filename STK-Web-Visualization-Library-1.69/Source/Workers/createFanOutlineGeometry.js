/* This file is automatically rebuilt by the Cesium build process. */
define(['./when-5d1c9db5', './Check-0081bf29', './Math-c5b6b5a3', './Cartesian2-965e01f0', './Transforms-81e56fb7', './RuntimeError-77179770', './WebGLConstants-21451dcd', './ComponentDatatype-6080fe8f', './GeometryAttribute-7c8a441d', './GeometryAttributes-281d90a3', './IndexDatatype-748ddad3', './VertexFormat-d0ae0e9e'], function (when, Check, _Math, Cartesian2, Transforms, RuntimeError, WebGLConstants, ComponentDatatype, GeometryAttribute, GeometryAttributes, IndexDatatype, VertexFormat) { 'use strict';

  var scratchCartesian = new Cartesian2.Cartesian3();

  /**
   * Describes the outline of a {@link FanGeometry}.
   *
   * @alias FanOutlineGeometry
   * @ionsdk
   * @constructor
   *
   * @param {Spherical[]} options.directions The directions, pointing outward from the origin, that defined the fan.
   * @param {Number} options.radius The radius at which to draw the fan.
   * @param {Boolean} options.perDirectionRadius When set to true, the magnitude of each direction is used in place of a constant radius.
   * @param {Number} [options.numberOfRings=6] The number of outline rings to draw, starting from the outer edge and equidistantly spaced towards the center.
   * @param {VertexFormat} [options.vertexFormat=VertexFormat.DEFAULT] The vertex attributes to be computed.
   */
  function FanOutlineGeometry(options) {
    options = when.defaultValue(options, when.defaultValue.EMPTY_OBJECT);

    //>>includeStart('debug', pragmas.debug);
    if (!when.defined(options.directions)) {
      throw new Check.DeveloperError("options.directions is required");
    }
    if (!options.perDirectionRadius && !when.defined(options.radius)) {
      throw new Check.DeveloperError(
        "options.radius is required when options.perDirectionRadius is undefined or false."
      );
    }
    //>>includeEnd('debug');

    this._radius = options.radius;
    this._directions = options.directions;
    this._perDirectionRadius = options.perDirectionRadius;
    this._numberOfRings = when.defaultValue(options.numberOfRings, 6);
    this._vertexFormat = when.defaultValue(options.vertexFormat, VertexFormat.VertexFormat.DEFAULT);
    this._workerName = "createFanOutlineGeometry";
  }

  /**
   * Computes the geometric representation of a fan outline, including its vertices, indices, and a bounding sphere.
   *
   * @param {FanOutlineGeometry} fanGeometry A description of the fan.
   * @returns {Geometry} The computed vertices and indices.
   */
  FanOutlineGeometry.createGeometry = function (fanGeometry) {
    //>>includeStart('debug', pragmas.debug);
    if (!when.defined(fanGeometry)) {
      throw new Check.DeveloperError("fanGeometry is required");
    }
    //>>includeEnd('debug');

    var radius = fanGeometry._radius;
    var perDirectionRadius =
      when.defined(fanGeometry._perDirectionRadius) && fanGeometry._perDirectionRadius;
    var directions = fanGeometry._directions;
    var vertexFormat = fanGeometry._vertexFormat;
    var numberOfRings = fanGeometry._numberOfRings;

    var i;
    var x;
    var ring;
    var length;
    var maxRadius = 0;
    var indices;
    var positions;
    var directionsLength = directions.length;
    var attributes = new GeometryAttributes.GeometryAttributes();

    if (vertexFormat.position) {
      x = 0;
      length = directionsLength * 3 * numberOfRings;
      positions = new Float64Array(length);

      for (ring = 0; ring < numberOfRings; ring++) {
        for (i = 0; i < directionsLength; i++) {
          scratchCartesian = Cartesian2.Cartesian3.fromSpherical(
            directions[i],
            scratchCartesian
          );
          var currentRadius = perDirectionRadius
            ? Cartesian2.Cartesian3.magnitude(scratchCartesian)
            : radius;
          var ringRadius = (currentRadius / numberOfRings) * (ring + 1);
          scratchCartesian = Cartesian2.Cartesian3.normalize(
            scratchCartesian,
            scratchCartesian
          );

          positions[x++] = scratchCartesian.x * ringRadius;
          positions[x++] = scratchCartesian.y * ringRadius;
          positions[x++] = scratchCartesian.z * ringRadius;
          maxRadius = Math.max(maxRadius, currentRadius);
        }
      }

      attributes.position = new GeometryAttribute.GeometryAttribute({
        componentDatatype: ComponentDatatype.ComponentDatatype.DOUBLE,
        componentsPerAttribute: 3,
        values: positions,
      });
    }

    x = 0;
    length = directionsLength * 2 * numberOfRings;
    indices = IndexDatatype.IndexDatatype.createTypedArray(length / 3, length);

    for (ring = 0; ring < numberOfRings; ring++) {
      var offset = ring * directionsLength;
      for (i = 0; i < directionsLength - 1; i++) {
        indices[x++] = i + offset;
        indices[x++] = i + 1 + offset;
      }
      indices[x++] = i + offset;
      indices[x++] = 0 + offset;
    }

    return new GeometryAttribute.Geometry({
      attributes: attributes,
      indices: indices,
      primitiveType: GeometryAttribute.PrimitiveType.LINES,
      boundingSphere: new Transforms.BoundingSphere(Cartesian2.Cartesian3.ZERO, maxRadius),
    });
  };

  var createFanOutlineGeometry = FanOutlineGeometry.createGeometry;

  return createFanOutlineGeometry;

});
