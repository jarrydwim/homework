/* This file is automatically rebuilt by the Cesium build process. */
define(['./when-5d1c9db5', './Check-0081bf29', './Math-c5b6b5a3', './Cartesian2-965e01f0', './Transforms-81e56fb7', './RuntimeError-77179770', './WebGLConstants-21451dcd', './ComponentDatatype-6080fe8f', './GeometryAttribute-7c8a441d', './GeometryAttributes-281d90a3', './IndexDatatype-748ddad3', './GeometryOffsetAttribute-5c864824', './VertexFormat-d0ae0e9e', './CylinderGeometryLibrary-d6024fe9', './CylinderGeometry-0ea50fce'], function (when, Check, _Math, Cartesian2, Transforms, RuntimeError, WebGLConstants, ComponentDatatype, GeometryAttribute, GeometryAttributes, IndexDatatype, GeometryOffsetAttribute, VertexFormat, CylinderGeometryLibrary, CylinderGeometry) { 'use strict';

  function createCylinderGeometry(cylinderGeometry, offset) {
    if (when.defined(offset)) {
      cylinderGeometry = CylinderGeometry.CylinderGeometry.unpack(cylinderGeometry, offset);
    }
    return CylinderGeometry.CylinderGeometry.createGeometry(cylinderGeometry);
  }

  return createCylinderGeometry;

});
