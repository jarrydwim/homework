/* This file is automatically rebuilt by the Cesium build process. */
define(['./when-5d1c9db5', './Check-0081bf29', './Math-c5b6b5a3', './Cartesian2-965e01f0', './Transforms-81e56fb7', './RuntimeError-77179770', './WebGLConstants-21451dcd', './ComponentDatatype-6080fe8f', './GeometryAttribute-7c8a441d', './GeometryAttributes-281d90a3', './IndexDatatype-748ddad3', './GeometryOffsetAttribute-5c864824', './EllipseGeometryLibrary-1b8db1ef', './EllipseOutlineGeometry-7f5ff91a'], function (when, Check, _Math, Cartesian2, Transforms, RuntimeError, WebGLConstants, ComponentDatatype, GeometryAttribute, GeometryAttributes, IndexDatatype, GeometryOffsetAttribute, EllipseGeometryLibrary, EllipseOutlineGeometry) { 'use strict';

  function createEllipseOutlineGeometry(ellipseGeometry, offset) {
    if (when.defined(offset)) {
      ellipseGeometry = EllipseOutlineGeometry.EllipseOutlineGeometry.unpack(ellipseGeometry, offset);
    }
    ellipseGeometry._center = Cartesian2.Cartesian3.clone(ellipseGeometry._center);
    ellipseGeometry._ellipsoid = Cartesian2.Ellipsoid.clone(ellipseGeometry._ellipsoid);
    return EllipseOutlineGeometry.EllipseOutlineGeometry.createGeometry(ellipseGeometry);
  }

  return createEllipseOutlineGeometry;

});
