import Cartesian2 from "../../Core/Cartesian2.js";
import Cartesian3 from "../../Core/Cartesian3.js";
import Check from "../../Core/Check.js";
import Color from "../../Core/Color.js";
import ColorGeometryInstanceAttribute from "../../Core/ColorGeometryInstanceAttribute.js";
import createGuid from "../../Core/createGuid.js";
import defaultValue from "../../Core/defaultValue.js";
import defined from "../../Core/defined.js";
import destroyObject from "../../Core/destroyObject.js";
import GeometryInstance from "../../Core/GeometryInstance.js";
import Matrix4 from "../../Core/Matrix4.js";
import Plane from "../../Core/Plane.js";
import PlaneGeometry from "../../Core/PlaneGeometry.js";
import PlaneOutlineGeometry from "../../Core/PlaneOutlineGeometry.js";
import WebGLConstants from "../../Core/WebGLConstants.js";
import PlaneGeometryUpdater from "../../DataSources/PlaneGeometryUpdater.js";
import getScreenSpaceScalingMatrix from "../../Scene/getScreenSpaceScalingMatrix.js";
import PerInstanceColorAppearance from "../../Scene/PerInstanceColorAppearance.js";
import Primitive from "../../Scene/Primitive.js";

var defaultOutlineColor = Color.WHITE;
var defaultFrontColor = Color.RED.withAlpha(0.2);
var defaultBackColor = Color.CYAN.withAlpha(0.2);
var defaultHighlightColor = Color.WHITE.withAlpha(0.2);
var defaultPixelSize = new Cartesian2(100, 100);
var defaultMaximumMeterSize = new Cartesian2(Infinity, Infinity);
var defaultSize = new Cartesian2(50, 50);

function updateModelMatrix(clippingPlane, frameState) {
  var transform = clippingPlane._transform;

  var modelMatrix;
  if (Matrix4.equals(transform, Matrix4.IDENTITY)) {
    modelMatrix = Matrix4.clone(Matrix4.IDENTITY, clippingPlane._modelMatrix);
  } else {
    modelMatrix = PlaneGeometryUpdater.createPrimitiveMatrix(
      clippingPlane._plane,
      clippingPlane._dimensions,
      transform,
      frameState.mapProjection.ellipsoid,
      clippingPlane._modelMatrix
    );
  }

  if (clippingPlane._pixelSize.x > 0 || clippingPlane._pixelSize.y > 0) {
    modelMatrix = getScreenSpaceScalingMatrix(
      clippingPlane._pixelSize,
      clippingPlane._maximumSizeInMeters,
      frameState,
      modelMatrix,
      modelMatrix
    );
  }
  return modelMatrix;
}

/**
 * @private
 * @ionsdk
 */
function ClippingPlanePrimitive(options) {
  options = defaultValue(options, defaultValue.EMPTY_OBJECT);
  var clippingPlane = options.clippingPlane;
  var transform = options.transform;
  var size = Cartesian2.clone(defaultValue(options.size, defaultSize));

  //>>includeStart('debug', pragmas.debug);
  Check.defined("options.clippingPlane", clippingPlane);
  Check.defined("options.transform", transform);
  //>>includeEnd('debug');

  var normalWC = Matrix4.multiplyByPointAsVector(
    transform,
    clippingPlane.normal,
    new Cartesian3()
  );
  normalWC = Cartesian3.normalize(normalWC, normalWC);

  this.show = defaultValue(options.show, true);

  this._disableDepthFail = defaultValue(options.disableDepthFail, false);
  this._outlineColor = defaultValue(options.outlineColor, defaultOutlineColor);
  this._frontColor = defaultValue(options.frontColor, defaultFrontColor);
  this._backColor = defaultValue(options.backColor, defaultBackColor);
  this._highlightColor = defaultValue(
    options.highlightColor,
    defaultHighlightColor
  );
  this._pixelSize = Cartesian2.clone(
    defaultValue(options.pixelSize, defaultPixelSize)
  );
  this._maximumSizeInMeters = Cartesian2.clone(
    defaultValue(options.maximumSizeInMeters, defaultMaximumMeterSize)
  );

  this._plane = new Plane(clippingPlane.normal, clippingPlane.distance);
  this._clippingPlane = clippingPlane;

  this._backPrimitiveId = {
    name: "back",
    plane: this,
  };
  this._frontPrimitiveId = {
    name: "front",
    plane: this,
  };
  this._primitiveOutlineId = createGuid();

  this._modelMatrix = Matrix4.IDENTITY.clone();

  this._originalDistance = clippingPlane.distance;
  this._transform = Matrix4.clone(transform);
  this._normalWC = normalWC;
  this._centerPositionScratch = new Cartesian3();
  this._highlighted = false;
  this._dimensions = size;

  this._backPrimitive = undefined;
  this._frontPrimitive = undefined;
  this._outlinePrimitive = undefined;

  this._update = true;
}

Object.defineProperties(ClippingPlanePrimitive.prototype, {
  size: {
    get: function () {
      return this._dimensions;
    },
  },
  clippingPlane: {
    get: function () {
      return this._clippingPlane;
    },
  },
  transform: {
    get: function () {
      return this._transform;
    },
  },
  centerPosition: {
    get: function () {
      var point = this._centerPositionScratch;
      Cartesian3.clone(Cartesian3.ZERO, point);
      return Matrix4.multiplyByPoint(this._modelMatrix, point, point);
    },
  },
  frontColor: {
    get: function () {
      return this._highlighted ? this._highlightColor : this._frontColor;
    },
  },
  backColor: {
    get: function () {
      return this._highlighted ? this._highlightColor : this._backColor;
    },
  },
  pixelSize: {
    get: function () {
      return this._pixelSize;
    },
  },
  maximumSizeInMeters: {
    get: function () {
      return this._maximumSizeInMeters;
    },
  },
});

ClippingPlanePrimitive.prototype.highlight = function (highlight) {
  this._highlighted = highlight;

  if (defined(this._backPrimitive)) {
    var depthFailColor;

    var newColor = this.backColor;
    var color = this._backPrimitive.getGeometryInstanceAttributes(
      this._backPrimitiveId
    ).color;
    color[0] = Color.floatToByte(newColor.red);
    color[1] = Color.floatToByte(newColor.green);
    color[2] = Color.floatToByte(newColor.blue);
    color[3] = Color.floatToByte(newColor.alpha);
    this._backPrimitive.getGeometryInstanceAttributes(
      this._backPrimitiveId
    ).color = color;

    if (!this._disableDepthFail) {
      depthFailColor = this._backPrimitive.getGeometryInstanceAttributes(
        this._backPrimitiveId
      ).depthFailColor;
      depthFailColor[0] = Color.floatToByte(newColor.red);
      depthFailColor[1] = Color.floatToByte(newColor.green);
      depthFailColor[2] = Color.floatToByte(newColor.blue);
      depthFailColor[3] = Color.floatToByte(newColor.alpha);
      this._backPrimitive.getGeometryInstanceAttributes(
        this._backPrimitiveId
      ).depthFailColor = depthFailColor;
    }

    newColor = this.frontColor;
    color = this._frontPrimitive.getGeometryInstanceAttributes(
      this._frontPrimitiveId
    ).color;
    color[0] = Color.floatToByte(newColor.red);
    color[1] = Color.floatToByte(newColor.green);
    color[2] = Color.floatToByte(newColor.blue);
    color[3] = Color.floatToByte(newColor.alpha);
    this._frontPrimitive.getGeometryInstanceAttributes(
      this._frontPrimitiveId
    ).color = color;

    if (!this._disableDepthFail) {
      depthFailColor = this._frontPrimitive.getGeometryInstanceAttributes(
        this._frontPrimitiveId
      ).depthFailColor;
      depthFailColor[0] = Color.floatToByte(newColor.red);
      depthFailColor[1] = Color.floatToByte(newColor.green);
      depthFailColor[2] = Color.floatToByte(newColor.blue);
      depthFailColor[3] = Color.floatToByte(newColor.alpha);
      this._frontPrimitive.getGeometryInstanceAttributes(
        this._frontPrimitiveId
      ).depthFailColor = depthFailColor;
    }
  }
};

ClippingPlanePrimitive.prototype.reset = function () {
  this._clippingPlane.distance = this._originalDistance;
  this._update = true;
};

ClippingPlanePrimitive.prototype.update = function (frameState) {
  if (!this.show) {
    return;
  }

  var plane = this._plane;
  var clippingPlane = this._clippingPlane;
  var modelMatrix = updateModelMatrix(this, frameState);

  if (this._update || !Plane.equals(clippingPlane, plane)) {
    this._update = false;

    this._plane = Plane.clone(clippingPlane, plane);
    this._normalWC = Matrix4.multiplyByPointAsVector(
      this._transform,
      clippingPlane.normal,
      this._normalWC
    );

    this._frontPrimitive =
      this._frontPrimitive && this._frontPrimitive.destroy();
    this._backPrimitive = this._backPrimitive && this._backPrimitive.destroy();
    this._outlinePrimitive =
      this._outlinePrimitive && this._outlinePrimitive.destroy();

    var backPrimitiveGeometryDepthFailColor;
    var backPrimitiveDepthFailAppearance;

    var frontPrimitiveGeometryDepthFailColor;
    var frontPrimitiveDepthFailAppearance;

    if (!this._disableDepthFail) {
      backPrimitiveGeometryDepthFailColor = ColorGeometryInstanceAttribute.fromColor(
        this.backColor
      );
      backPrimitiveDepthFailAppearance = new PerInstanceColorAppearance({
        flat: true,
        closed: false,
        translucent: true,
        renderState: {
          cull: {
            enabled: true,
          },
        },
      });

      frontPrimitiveGeometryDepthFailColor = ColorGeometryInstanceAttribute.fromColor(
        this.frontColor
      );
      frontPrimitiveDepthFailAppearance = new PerInstanceColorAppearance({
        flat: true,
        closed: false,
        translucent: true,
        renderState: {
          cull: {
            enabled: true,
            face: WebGLConstants.FRONT,
          },
        },
      });
    }

    this._backPrimitive = new Primitive({
      geometryInstances: new GeometryInstance({
        geometry: PlaneGeometry.createGeometry(new PlaneGeometry()),
        attributes: {
          color: ColorGeometryInstanceAttribute.fromColor(this.backColor),
          depthFailColor: backPrimitiveGeometryDepthFailColor,
        },
        id: this._backPrimitiveId,
      }),
      appearance: new PerInstanceColorAppearance({
        flat: true,
        closed: false,
        translucent: true,
        renderState: {
          cull: {
            enabled: true,
          },
        },
      }),
      depthFailAppearance: backPrimitiveDepthFailAppearance,
      asynchronous: false,
    });

    this._frontPrimitive = new Primitive({
      geometryInstances: new GeometryInstance({
        geometry: PlaneGeometry.createGeometry(new PlaneGeometry()),
        attributes: {
          color: ColorGeometryInstanceAttribute.fromColor(this.frontColor),
          depthFailColor: frontPrimitiveGeometryDepthFailColor,
        },
        id: this._frontPrimitiveId,
      }),
      appearance: new PerInstanceColorAppearance({
        flat: true,
        closed: false,
        translucent: true,
        renderState: {
          cull: {
            enabled: true,
            face: WebGLConstants.FRONT,
          },
        },
      }),
      depthFailAppearance: frontPrimitiveDepthFailAppearance,
      asynchronous: false,
    });

    this._outlinePrimitive = new Primitive({
      geometryInstances: new GeometryInstance({
        geometry: PlaneOutlineGeometry.createGeometry(
          new PlaneOutlineGeometry()
        ),
        attributes: {
          color: ColorGeometryInstanceAttribute.fromColor(this._outlineColor),
        },
        id: this._primitiveOutlineId,
      }),
      appearance: new PerInstanceColorAppearance({
        flat: true,
        translucent: false,
        renderState: {
          lineWidth: 1.0,
        },
      }),
      asynchronous: false,
    });
  }

  this._backPrimitive.modelMatrix = modelMatrix;
  this._frontPrimitive.modelMatrix = modelMatrix;
  this._outlinePrimitive.modelMatrix = modelMatrix;

  this._backPrimitive.update(frameState);
  this._frontPrimitive.update(frameState);
  this._outlinePrimitive.update(frameState);
};

ClippingPlanePrimitive.prototype.isDestroyed = function () {
  return false;
};

ClippingPlanePrimitive.prototype.destroy = function () {
  if (defined(this._backPrimitive)) {
    this._backPrimitive.destroy();
  }
  if (defined(this._frontPrimitive)) {
    this._frontPrimitive.destroy();
  }
  if (defined(this._outlinePrimitive)) {
    this._outlinePrimitive.destroy();
  }

  return destroyObject(this);
};
export default ClippingPlanePrimitive;
