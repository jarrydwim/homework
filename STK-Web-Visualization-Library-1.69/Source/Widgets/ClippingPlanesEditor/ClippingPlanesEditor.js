import Cartesian2 from "../../Core/Cartesian2.js";
import Cartesian3 from "../../Core/Cartesian3.js";
import Check from "../../Core/Check.js";
import defaultValue from "../../Core/defaultValue.js";
import defined from "../../Core/defined.js";
import destroyObject from "../../Core/destroyObject.js";
import DeveloperError from "../../Core/DeveloperError.js";
import IntersectionTests from "../../Core/IntersectionTests.js";
import Matrix3 from "../../Core/Matrix3.js";
import Matrix4 from "../../Core/Matrix4.js";
import Plane from "../../Core/Plane.js";
import Ray from "../../Core/Ray.js";
import ScreenSpaceEventHandler from "../../Core/ScreenSpaceEventHandler.js";
import ScreenSpaceEventType from "../../Core/ScreenSpaceEventType.js";
import Cesium3DTileset from "../../Scene/Cesium3DTileset.js";
import GlobeSurfaceTileProvider from "../../Scene/GlobeSurfaceTileProvider.js";
import Model from "../../Scene/Model.js";
import PrimitiveCollection from "../../Scene/PrimitiveCollection.js";
import SceneMode from "../../Scene/SceneMode.js";
import when from "../../ThirdParty/when.js";
import ClippingPlanePrimitive from "./ClippingPlanePrimitive.js";

var transformPlaneScratch = new Plane(Cartesian3.UNIT_X, 0);
var planeScratch = new Plane(Cartesian3.UNIT_X, 0);
var scalarScratch = new Cartesian3();
var positionScratch = new Cartesian3();
var normalScratch = new Cartesian3();
var rotationScratch = new Matrix3();
var transformScratch = new Matrix4();
var cart3Scratch1 = new Cartesian3();
var cart3Scratch2 = new Cartesian3();
var car3Scratch3 = new Cartesian3();
var rayScratch = new Ray();

function computeOrigin(transform, origin, plane, result) {
  var adjustedPlane = Plane.clone(plane, planeScratch);
  adjustedPlane.distance = 0;

  adjustedPlane = Plane.transform(adjustedPlane, transform, adjustedPlane);
  return Cartesian3.subtract(
    origin,
    Cartesian3.multiplyByScalar(
      adjustedPlane.normal,
      Plane.getPointDistance(adjustedPlane, origin),
      scalarScratch
    ),
    result
  );
}

function movePlane(transform, origin, plane) {
  var adjustedPlane = Plane.clone(plane, planeScratch);
  adjustedPlane.distance = 0;
  adjustedPlane = Plane.transform(adjustedPlane, transform, adjustedPlane);
  plane.distance = -Plane.getPointDistance(adjustedPlane, origin);
}

/**
 * Creates visual clipping planes and mouse handlers for adjusting the clipping planes added to a {@link Cesium3DTileset}, {@link Model} or {@link Globe}.
 *
 * @alias ClippingPlanesEditor
 * @ionsdk
 * @constructor
 *
 * @param {Object} options An object with the following properties:
 * @param {Scene} options.scene The scene.
 * @param {ClippingPlaneCollection} options.clippingPlanes A clipping plane collection that has been set for a Cesium3DTileset, Model or Globe.
 * @param {Cartesian3} [options.origin] The position the visual clipping planes are relative to.  If not provided, a position will be computed from the modelMatrix or the primitive bounding sphere.
 * @param {Cartesian2} [options.planeSizeInMeters] The width and height of the clipping planes in meters. If not provided, size will be computed based on the primitive bounding sphere radius, or 200.0 for terrain clipping.
 * @param {Boolean} [options.movePlanesToOrigin=true] If true, ClippingPlanesEditor recomputes the plane distance to move the clipping planes to the origin.  Otherwise the original plane distance is unaltered.
 * @param {Cartesian2} [options.pixelSize=Cesium.Cartesian2(100, 100)] The size of the clipping planes in pixels of screen space in each direction. This overrides options.planeSizeInMeters. To disable fixed screen-space size for either the x- or y-direction, set the corresponding component(s) to 0
 * @param {Cartesian2} [options.maximumSizeInMeters=Cesium.Cartesian2(Infinity, Infinity)] The maximum size of the clipping planes in meters when fixed screen-space size is enabled. To disable this size limit for either the x- or y-direction, set the corresponding component(s) to Infinity
 *
 * @demo <a href="/Apps/Sandcastle/index.html?src=Clipping%20Planes%20Editor.html">Cesium Sandcastle Clipping Planes Editor Demo</a>
 * @example
 * // Attach 4 clipping planes to a tileset
 * tileset.clippingPlanes = new Cesium.ClippingPlaneCollection({
 *  planes : [
 *    new Cesium.ClippingPlane(new Cesium.Cartesian3(1.0, 0.0, 0.0), 100),
 *    new Cesium.ClippingPlane(new Cesium.Cartesian3(-1.0, 0.0, 0.0), 100),
 *    new Cesium.ClippingPlane(new Cesium.Cartesian3(0.0, 1.0, 0.0), 100),
 *    new Cesium.ClippingPlane(new Cesium.Cartesian3(0.0, -1.0, 0.0), 100),
 *  ],
 *  unionClippingRegions: true,
 *  edgeWidth : 1.0
 * });
 * // Create and activate the editor to visualize and move the planes.
 * var clippingPlanesEditor = new Cesium.ClippingPlanesEditor({
 *  scene: viewer.scene,
 *  clippingPlanes: tileset.clippingPlanes,
 *  movePlanesToOrigin: false
 * });
 * clippingPlanesEditor.activate();
 */
function ClippingPlanesEditor(options) {
  var scene = options.scene;
  var clippingPlanes = options.clippingPlanes;

  //>>includeStart('debug', pragmas.debug);
  Check.defined("options.scene", scene);
  Check.defined("options.clippingPlanes", clippingPlanes);
  //>>includeEnd('debug');

  var primitives = scene.primitives.add(new PrimitiveCollection());

  this._removeAddEventListener = clippingPlanes.planeAdded.addEventListener(
    ClippingPlanesEditor.prototype._addPlane,
    this
  );
  this._removeRemoveEventListener = clippingPlanes.planeRemoved.addEventListener(
    ClippingPlanesEditor.prototype._removePlane,
    this
  );

  this._planeSizeInMeters = options.planeSizeInMeters;
  this._scene = scene;
  this._sseh = new ScreenSpaceEventHandler(scene.canvas);
  this._clippingPlanes = clippingPlanes;
  this._primitives = primitives;
  this._origin = options.origin;
  this._movePlanesToOrigin = defaultValue(options.movePlanesToOrigin, true);
  this._primitiveOptions = defaultValue(
    options.primitiveOptions,
    defaultValue.EMPTY_OBJECT
  );
  this._active = false;
  this._pickedPlane = undefined;
  this._hoverPlane = undefined;
  this._draggingPlane = new Plane(Cartesian3.UNIT_X, 0);
  this._pixelSize = options.pixelSize;
  this._maximumSizeInMeters = options.maximumSizeInMeters;

  this._target = undefined;
  this._transform = Matrix4.clone(Matrix4.IDENTITY);
  this._readyPromise = when.defer();

  this._createPlanes();
}

Object.defineProperties(ClippingPlanesEditor.prototype, {
  /**
   * Gets whether the clipping plane tool is active.
   * @type {Boolean}
   * @memberof ClippingPlanesEditor.prototype
   * @readonly
   */
  active: {
    get: function () {
      return this._active;
    },
  },
  /**
   * Gets the scene.
   * @type {Scene}
   * @memberof ClippingPlanesEditor.prototype
   * @readonly
   */
  scene: {
    get: function () {
      return this._scene;
    },
  },
  /**
   * Gets the clipping plane collection.
   * @type {ClippingPlaneCollection}
   * @memberof ClippingPlanesEditor.prototype
   * @readonly
   */
  clippingPlanes: {
    get: function () {
      return this._clippingPlanes;
    },
  },
});

/**
 * Activates mouse handlers.
 */
ClippingPlanesEditor.prototype.activate = function () {
  this._active = true;

  var primitives = this._primitives;
  for (var i = 0; i < primitives.length; i++) {
    primitives.get(i).show = true;
  }
  var sseh = this._sseh;

  sseh.setInputAction(
    this._handleLeftDown.bind(this),
    ScreenSpaceEventType.LEFT_DOWN
  );
  sseh.setInputAction(
    this._handleLeftUp.bind(this),
    ScreenSpaceEventType.LEFT_UP
  );
  sseh.setInputAction(
    this._handleMouseMove.bind(this),
    ScreenSpaceEventType.MOUSE_MOVE
  );
};

/**
 * Deactivates the mouse handlers.
 */
ClippingPlanesEditor.prototype.deactivate = function () {
  var primitives = this._primitives;
  for (var i = 0; i < primitives.length; i++) {
    primitives.get(i).show = false;
  }

  var sseh = this._sseh;
  sseh.removeInputAction(ScreenSpaceEventType.LEFT_DOWN);
  sseh.removeInputAction(ScreenSpaceEventType.LEFT_UP);
  sseh.removeInputAction(ScreenSpaceEventType.MOUSE_MOVE);

  this._active = false;
};

/**
 * @private
 */
ClippingPlanesEditor.prototype._createPlanes = function () {
  var clippingPlanes = this._clippingPlanes;

  var target = clippingPlanes.owner;
  var that = this;
  if (typeof target === "undefined") {
    var removeListener = this._scene.preRender.addEventListener(function () {
      // if plane is on a model entity, we have to wait for it to be assigned the actual Model
      removeListener();
      if (!that.isDestroyed()) {
        that._createPlanes();
      }
    });
    return;
  }
  this._target = target;

  var origin = this._origin;
  var promise;
  if (target instanceof Cesium3DTileset) {
    promise = target.readyPromise.then(function () {
      var transform = target.clippingPlanesOriginMatrix;
      if (!defined(origin)) {
        that._origin = Cartesian3.clone(target.boundingSphere.center);
      }
      that._transform = Matrix4.clone(transform, that._transform);
    });
  } else if (target instanceof Model) {
    promise = target.readyPromise.then(function () {
      var transform = Matrix4.clone(target.modelMatrix, that._transform);
      if (!defined(origin)) {
        that._origin = Matrix4.multiplyByPoint(
          transform,
          target.boundingSphere.center,
          new Cartesian3()
        );
      }
    });
  } else if (target instanceof GlobeSurfaceTileProvider) {
    this._transform = Matrix4.clone(
      clippingPlanes.modelMatrix,
      this._transform
    );
    if (!defined(origin)) {
      this._origin = Matrix4.getTranslation(
        clippingPlanes.modelMatrix,
        new Cartesian3()
      );
    }
  }
  //>>includeStart('debug', pragmas.debug);
  else {
    throw new DeveloperError(
      "ClippingPlanesEditor is incompatible with the object owning options.clippingPlanes"
    );
  }
  //>>includeEnd('debug');

  when(promise)
    .then(function () {
      that._readyPromise.resolve();

      for (var i = 0; i < clippingPlanes.length; i++) {
        that._addPlane(clippingPlanes.get(i));
      }
    })
    .otherwise(function () {
      that._readyPromise.reject();
    });
};

/**
 * @private
 */
ClippingPlanesEditor.prototype._addPlane = function (clippingPlane) {
  var that = this;
  var movePlanesToOrigin = this._movePlanesToOrigin;
  var primitives = this._primitives;
  var planePosition;
  var planeSizeInMeters = this._planeSizeInMeters;
  var pixelSize = this._pixelSize;
  var maximumSizeInMeters = this._maximumSizeInMeters;
  var primitiveOptions = this._primitiveOptions;
  when(this._readyPromise.promise).then(function () {
    var target = that._target;
    var origin = that._origin;
    var transform = that._transform;
    if (Matrix4.equals(transform, Matrix4.IDENTITY)) {
      planePosition = origin;
      if (movePlanesToOrigin) {
        clippingPlane.distance = 0;
      }
    } else {
      planePosition = computeOrigin(
        transform,
        origin,
        clippingPlane,
        positionScratch
      );
      if (movePlanesToOrigin) {
        movePlane(transform, origin, clippingPlane);
      }
    }

    if (!defined(planeSizeInMeters)) {
      planeSizeInMeters = that._computePlaneSize(target);
    }
    transform = Matrix4.setTranslation(
      transform,
      planePosition,
      transformScratch
    );

    primitives.add(
      new ClippingPlanePrimitive({
        transform: transform,
        clippingPlane: clippingPlane,
        show: that._active,
        size: planeSizeInMeters,
        disableDepthFail: primitiveOptions.disableDepthFail,
        outlineColor: primitiveOptions.outlineColor,
        frontColor: primitiveOptions.frontColor,
        backColor: primitiveOptions.backColor,
        highlightColor: primitiveOptions.highlightColor,
        pixelSize: pixelSize,
        maximumSizeInMeters: maximumSizeInMeters,
      })
    );
  });
};

/**
 * @private
 */
ClippingPlanesEditor.prototype._computePlaneSize = function (target) {
  if (target instanceof GlobeSurfaceTileProvider) {
    return new Cartesian2(200, 200);
  }

  var halfRadius = target.boundingSphere.radius * 0.5;
  return new Cartesian2(halfRadius, halfRadius);
};

/**
 * @private
 */
ClippingPlanesEditor.prototype._removePlane = function (plane) {
  var primitives = this._primitives;
  for (var i = 0; i < primitives.length; i++) {
    var planePrimitive = primitives.get(i);
    if (planePrimitive.clippingPlane === plane) {
      primitives.remove(planePrimitive);
      break;
    }
  }
};

/**
 * @private
 */
ClippingPlanesEditor.prototype._handleLeftDown = function (movement) {
  var scene = this._scene;
  if (scene.mode !== SceneMode.SCENE3D) {
    return;
  }
  var pickedObject = scene.pick(movement.position);
  if (
    defined(pickedObject) &&
    defined(pickedObject.id) &&
    pickedObject.id.plane instanceof ClippingPlanePrimitive
  ) {
    var pickedPlane = pickedObject.id.plane;
    pickedPlane.highlight(true);

    var clippingPlaneNormal = pickedPlane._normalWC;
    var planeNormal = Cartesian3.cross(
      clippingPlaneNormal,
      scene.camera.directionWC,
      normalScratch
    );
    planeNormal = Cartesian3.cross(
      clippingPlaneNormal,
      planeNormal,
      planeNormal
    );
    planeNormal = Cartesian3.normalize(planeNormal, planeNormal);

    this._draggingPlane = Plane.fromPointNormal(
      pickedPlane.centerPosition,
      planeNormal,
      this._draggingPlane
    );
    scene.screenSpaceCameraController.enableInputs = false;
    this._pickedPlane = pickedPlane;
    if (scene.requestRenderMode) {
      scene.requestRender();
    }
  }
};

/**
 * @private
 */
ClippingPlanesEditor.prototype._handleLeftUp = function () {
  var scene = this._scene;
  if (scene.mode !== SceneMode.SCENE3D) {
    return;
  }
  var pickedPlane = this._pickedPlane;
  if (defined(pickedPlane)) {
    pickedPlane.highlight(false);
    this._pickedPlane = undefined;
    if (scene.requestRenderMode) {
      scene.requestRender();
    }
  }
  scene.screenSpaceCameraController.enableInputs = true;
};

/**
 * @private
 */
ClippingPlanesEditor.prototype._handleMouseMove = function (movement) {
  var scene = this._scene;
  if (scene.mode !== SceneMode.SCENE3D) {
    return;
  }
  var pickedPlane = this._pickedPlane;
  if (defined(pickedPlane)) {
    var draggingPlane = this._draggingPlane;
    var pickRay = scene.camera.getPickRay(movement.startPosition, rayScratch);
    var startDragPos = IntersectionTests.rayPlane(
      pickRay,
      draggingPlane,
      cart3Scratch1
    );
    if (!defined(startDragPos)) {
      return;
    }

    pickRay = scene.camera.getPickRay(movement.endPosition, rayScratch);
    var endDragPos = IntersectionTests.rayPlane(
      pickRay,
      draggingPlane,
      cart3Scratch2
    );
    if (!defined(endDragPos)) {
      return;
    }

    var v1 = Cartesian3.subtract(
      startDragPos,
      pickedPlane.centerPosition,
      cart3Scratch1
    );
    v1 = Cartesian3.projectVector(v1, pickedPlane._normalWC, v1);

    var v2 = Cartesian3.subtract(
      endDragPos,
      pickedPlane.centerPosition,
      cart3Scratch2
    );
    v2 = Cartesian3.projectVector(v2, pickedPlane._normalWC, v2);

    var v = Cartesian3.subtract(v1, v2, car3Scratch3);
    var p = Cartesian3.add(pickedPlane.centerPosition, v, car3Scratch3);

    var translation = Matrix4.getTranslation(
      pickedPlane._modelMatrix,
      cart3Scratch1
    );
    var rotation = Matrix4.getMatrix3(pickedPlane.transform, rotationScratch);
    var transform = Matrix4.fromRotationTranslation(
      rotation,
      translation,
      transformScratch
    );
    var plane = Plane.transform(
      pickedPlane.clippingPlane,
      transform,
      transformPlaneScratch
    );

    pickedPlane.clippingPlane.distance = Plane.getPointDistance(plane, p);

    if (scene.requestRenderMode) {
      scene.requestRender();
    }
  } else {
    if (defined(this._hoverPlane)) {
      this._hoverPlane.highlight(false);
    }
    var pickedObject = scene.pick(movement.endPosition);
    if (
      defined(pickedObject) &&
      defined(pickedObject.id) &&
      pickedObject.id.plane instanceof ClippingPlanePrimitive
    ) {
      var hoverPlane = pickedObject.id.plane;
      hoverPlane.highlight(true);
      this._hoverPlane = hoverPlane;
    }
  }
};

/**
 * Resets the clipping planes to their start position.
 */
ClippingPlanesEditor.prototype.reset = function () {
  var primitives = this._primitives;
  for (var i = 0; i < primitives.length; i++) {
    primitives.get(i).reset();
  }
};

/**
 * @returns {Boolean} true if the object has been destroyed, false otherwise.
 */
ClippingPlanesEditor.prototype.isDestroyed = function () {
  return false;
};

/**
 * Destroys the clipping planes tool.
 */
ClippingPlanesEditor.prototype.destroy = function () {
  this.deactivate();
  this._sseh.destroy();
  this._scene.primitives.remove(this._primitives);
  this._removeAddEventListener();
  this._removeRemoveEventListener();

  return destroyObject(this);
};
export default ClippingPlanesEditor;
