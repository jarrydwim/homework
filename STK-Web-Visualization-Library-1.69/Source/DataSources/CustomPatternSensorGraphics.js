import defaultValue from "../Core/defaultValue.js";
import defined from "../Core/defined.js";
import DeveloperError from "../Core/DeveloperError.js";
import Event from "../Core/Event.js";
import createMaterialPropertyDescriptor from "./createMaterialPropertyDescriptor.js";
import createPropertyDescriptor from "./createPropertyDescriptor.js";

/**
 * An optionally time-dynamic custom patterned sensor.
 *
 * @alias CustomPatternSensorGraphics
 * @ionsdk
 * @constructor
 */
function CustomPatternSensorGraphics(options) {
  this._directions = undefined;
  this._directionsSubscription = undefined;

  this._lateralSurfaceMaterial = undefined;
  this._lateralSurfaceMaterialSubscription = undefined;
  this._showLateralSurfaces = undefined;
  this._showLateralSurfacesSubscription = undefined;
  this._ellipsoidHorizonSurfaceMaterial = undefined;
  this._ellipsoidHorizonSurfaceMaterialSubscription = undefined;
  this._showEllipsoidHorizonSurfaces = undefined;
  this._showEllipsoidHorizonSurfacesSubscription = undefined;
  this._domeSurfaceMaterial = undefined;
  this._domeSurfaceMaterialSubscription = undefined;
  this._showDomeSurfaces = undefined;
  this._showDomeSurfacesSubscription = undefined;
  this._ellipsoidSurfaceMaterial = undefined;
  this._ellipsoidSurfaceMaterialSubscription = undefined;
  this._showEllipsoidSurfaces = undefined;
  this._showEllipsoidSurfacesSubscription = undefined;

  this._portionToDisplay = undefined;
  this._portionToDisplaySubscription = undefined;
  this._intersectionColor = undefined;
  this._intersectionColorSubscription = undefined;
  this._intersectionWidth = undefined;
  this._intersectionWidthSubscription = undefined;
  this._showIntersection = undefined;
  this._showIntersectionSubscription = undefined;
  this._showThroughEllipsoid = undefined;
  this._showThroughEllipsoidSubscription = undefined;
  this._radius = undefined;
  this._radiusSubscription = undefined;
  this._show = undefined;
  this._showSubscription = undefined;

  this._environmentConstraint = undefined;
  this._environmentConstraintSubscription = undefined;
  this._showEnvironmentOcclusion = undefined;
  this._showEnvironmentOcclusionSubscription = undefined;
  this._environmentOcclusionMaterial = undefined;
  this._environmentOcclusionMaterialSubscription = undefined;
  this._showEnvironmentIntersection = undefined;
  this._showEnvironmentIntersectionSubscription = undefined;
  this._environmentIntersectionColor = undefined;
  this._environmentIntersectionColorSubscription = undefined;
  this._environmentIntersectionWidth = undefined;
  this._environmentIntersectionWidthSubscription = undefined;

  this._definitionChanged = new Event();

  this.merge(defaultValue(options, defaultValue.EMPTY_OBJECT));
}

Object.defineProperties(CustomPatternSensorGraphics.prototype, {
  /**
   * Gets the event that is raised whenever a new property is assigned.
   * @memberof CustomPatternSensorGraphics.prototype
   *
   * @type {Event}
   * @readonly
   */
  definitionChanged: {
    get: function () {
      return this._definitionChanged;
    },
  },

  /**
   * A {@link Property} which returns an array of {@link Spherical} instances representing the sensor's projection.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  directions: createPropertyDescriptor("directions"),

  /**
   * Gets or sets the {@link MaterialProperty} specifying the the sensor's appearance.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {MaterialProperty}
   */
  lateralSurfaceMaterial: createMaterialPropertyDescriptor(
    "lateralSurfaceMaterial"
  ),

  /**
   * Gets or sets the boolean {@link Property} specifying the visibility of the lateral surfaces defining the sensor volume.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  showLateralSurfaces: createPropertyDescriptor("showLateralSurfaces"),

  /**
   * Gets or sets the {@link MaterialProperty} specifying the the cone's ellipsoid horizon surface appearance.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {MaterialProperty}
   */
  ellipsoidHorizonSurfaceMaterial: createMaterialPropertyDescriptor(
    "ellipsoidHorizonSurfaceMaterial"
  ),

  /**
   * Gets or sets the boolean {@link Property} specifying the visibility of the ellipsoid horizon surfaces defining the sensor volume.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  showEllipsoidHorizonSurfaces: createPropertyDescriptor(
    "showEllipsoidHorizonSurfaces"
  ),

  /**
   * Gets or sets the {@link MaterialProperty} specifying the the surface appearance of the sensor's dome.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {MaterialProperty}
   */
  domeSurfaceMaterial: createMaterialPropertyDescriptor("domeSurfaceMaterial"),

  /**
   * Gets or sets the boolean {@link Property} specifying the visibility of the dome surfaces defining the sensor volume.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  showDomeSurfaces: createPropertyDescriptor("showDomeSurfaces"),

  /**
   * Gets or sets the {@link MaterialProperty} specifying the the cone's ellipsoid surface appearance.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {MaterialProperty}
   */
  ellipsoidSurfaceMaterial: createMaterialPropertyDescriptor(
    "ellipsoidSurfaceMaterial"
  ),

  /**
   * Gets or sets the boolean {@link Property} specifying the visibility of the ellipsoid surfaces defining the sensor volume.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  showEllipsoidSurfaces: createPropertyDescriptor("showEllipsoidSurfaces"),

  /**
   * Gets or sets the {@link SensorVolumePortionToDisplay} specifying the portion of the sensor to display.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {SensorVolumePortionToDisplay}
   */
  portionToDisplay: createPropertyDescriptor("portionToDisplay"),

  /**
   * Gets or sets the {@link Color} {@link Property} specifying the color of the line formed by the intersection of the sensor and other central bodies.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  intersectionColor: createPropertyDescriptor("intersectionColor"),

  /**
   * Gets or sets the numeric {@link Property} specifying the width of the line formed by the intersection of the sensor and other central bodies.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  intersectionWidth: createPropertyDescriptor("intersectionWidth"),

  /**
   * Gets or sets the boolean {@link Property} specifying the visibility of the line formed by the intersection of the sensor and other central bodies.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  showIntersection: createPropertyDescriptor("showIntersection"),

  /**
   * Gets or sets the boolean {@link Property} specifying whether a sensor intersecting the ellipsoid is drawn through the ellipsoid and potentially out to the other side.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  showThroughEllipsoid: createPropertyDescriptor("showThroughEllipsoid"),

  /**
   * Gets or sets the numeric {@link Property} specifying the radius of the sensor's projection.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  radius: createPropertyDescriptor("radius"),

  /**
   * Gets or sets the boolean {@link Property} specifying the visibility of the sensor.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  show: createPropertyDescriptor("show"),

  /**
   * Gets or sets the boolean {@link Property} determining if a sensor will intersect the environment, e.g. terrain or models,
   * and discard the portion of the sensor that is occluded.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  environmentConstraint: createPropertyDescriptor("environmentConstraint"),

  /**
   * Gets or sets the boolean {@link Property} determining if the portion of the sensor occluded by the environment will be
   * drawn with {@link CustomPatternSensorGraphics#environmentOcclusionMaterial}.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  showEnvironmentOcclusion: createPropertyDescriptor(
    "showEnvironmentOcclusion"
  ),

  /**
   * Gets or sets the {@link MaterialProperty} specifying the surface appearance of the portion of the sensor occluded by the environment.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {MaterialProperty}
   */
  environmentOcclusionMaterial: createMaterialPropertyDescriptor(
    "environmentOcclusionMaterial"
  ),

  /**
   * Gets or sets the boolean {@link Property} that determines if a line is shown where the sensor intersects the environment, e.g. terrain or models.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  showEnvironmentIntersection: createPropertyDescriptor(
    "showEnvironmentIntersection"
  ),

  /**
   * Gets or sets the {@link Color} {@link Property} of the line intersecting the environment.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  environmentIntersectionColor: createPropertyDescriptor(
    "environmentIntersectionColor"
  ),

  /**
   * Gets or sets the {@link Property} that approximate width in meters of the line intersecting the environment.
   * @memberof CustomPatternSensorGraphics.prototype
   * @type {Property}
   */
  environmentIntersectionWidth: createPropertyDescriptor(
    "environmentIntersectionWidth"
  ),
});

/**
 * Duplicates a CustomPatternSensorGraphics instance.
 *
 * @param {CustomPatternSensorGraphics} [result] The object onto which to store the result.
 * @returns {CustomPatternSensorGraphics} The modified result parameter or a new instance if one was not provided.
 */
CustomPatternSensorGraphics.prototype.clone = function (result) {
  if (!defined(result)) {
    result = new CustomPatternSensorGraphics();
  }
  result.directions = this.directions;
  result.radius = this.radius;
  result.show = this.show;
  result.showIntersection = this.showIntersection;
  result.intersectionColor = this.intersectionColor;
  result.intersectionWidth = this.intersectionWidth;
  result.showThroughEllipsoid = this.showThroughEllipsoid;
  result.lateralSurfaceMaterial = this.lateralSurfaceMaterial;
  result.showLateralSurfaces = this.showLateralSurfaces;
  result.ellipsoidHorizonSurfaceMaterial = this.ellipsoidHorizonSurfaceMaterial;
  result.showEllipsoidHorizonSurfaces = this.showEllipsoidHorizonSurfaces;
  result.domeSurfaceMaterial = this.domeSurfaceMaterial;
  result.showDomeSurfaces = this.showDomeSurfaces;
  result.ellipsoidSurfaceMaterial = this.ellipsoidSurfaceMaterial;
  result.showEllipsoidSurfaces = this.showEllipsoidSurfaces;
  result.portionToDisplay = this.portionToDisplay;
  result.environmentConstraint = this.environmentConstraint;
  result.showEnvironmentOcclusion = this.showEnvironmentOcclusion;
  result.environmentOcclusionMaterial = this.environmentOcclusionMaterial;
  result.showEnvironmentIntersection = this.showEnvironmentIntersection;
  result.environmentIntersectionColor = this.environmentIntersectionColor;
  result.environmentIntersectionWidth = this.environmentIntersectionWidth;
  return result;
};

/**
 * Assigns each unassigned property on this object to the value
 * of the same property on the provided source object.
 *
 * @param {CustomPatternSensorGraphics} source The object to be merged into this object.
 */
CustomPatternSensorGraphics.prototype.merge = function (source) {
  //>>includeStart('debug', pragmas.debug);
  if (!defined(source)) {
    throw new DeveloperError("source is required.");
  }
  //>>includeEnd('debug');

  this.directions = defaultValue(this.directions, source.directions);
  this.radius = defaultValue(this.radius, source.radius);
  this.show = defaultValue(this.show, source.show);
  this.showIntersection = defaultValue(
    this.showIntersection,
    source.showIntersection
  );
  this.intersectionColor = defaultValue(
    this.intersectionColor,
    source.intersectionColor
  );
  this.intersectionWidth = defaultValue(
    this.intersectionWidth,
    source.intersectionWidth
  );
  this.showThroughEllipsoid = defaultValue(
    this.showThroughEllipsoid,
    source.showThroughEllipsoid
  );
  this.lateralSurfaceMaterial = defaultValue(
    this.lateralSurfaceMaterial,
    source.lateralSurfaceMaterial
  );
  this.showLateralSurfaces = defaultValue(
    this.showLateralSurfaces,
    source.showLateralSurfaces
  );
  this.ellipsoidHorizonSurfaceMaterial = defaultValue(
    this.ellipsoidHorizonSurfaceMaterial,
    source.ellipsoidHorizonSurfaceMaterial
  );
  this.showEllipsoidHorizonSurfaces = defaultValue(
    this.showEllipsoidHorizonSurfaces,
    source.showEllipsoidHorizonSurfaces
  );
  this.domeSurfaceMaterial = defaultValue(
    this.domeSurfaceMaterial,
    source.domeSurfaceMaterial
  );
  this.showDomeSurfaces = defaultValue(
    this.showDomeSurfaces,
    source.showDomeSurfaces
  );
  this.ellipsoidSurfaceMaterial = defaultValue(
    this.ellipsoidSurfaceMaterial,
    source.ellipsoidSurfaceMaterial
  );
  this.showEllipsoidSurfaces = defaultValue(
    this.showEllipsoidSurfaces,
    source.showEllipsoidSurfaces
  );
  this.portionToDisplay = defaultValue(
    this.portionToDisplay,
    source.portionToDisplay
  );
  this.environmentConstraint = defaultValue(
    this.environmentConstraint,
    source.environmentConstraint
  );
  this.showEnvironmentOcclusion = defaultValue(
    this.showEnvironmentOcclusion,
    source.showEnvironmentOcclusion
  );
  this.environmentOcclusionMaterial = defaultValue(
    this.environmentOcclusionMaterial,
    source.environmentOcclusionMaterial
  );
  this.showEnvironmentIntersection = defaultValue(
    this.showEnvironmentIntersection,
    source.showEnvironmentIntersection
  );
  this.environmentIntersectionColor = defaultValue(
    this.environmentIntersectionColor,
    source.environmentIntersectionColor
  );
  this.environmentIntersectionWidth = defaultValue(
    this.environmentIntersectionWidth,
    source.environmentIntersectionWidth
  );
};
export default CustomPatternSensorGraphics;
