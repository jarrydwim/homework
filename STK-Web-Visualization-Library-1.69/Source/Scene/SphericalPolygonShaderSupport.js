import Cartesian3 from "../Core/Cartesian3.js";
import defined from "../Core/defined.js";

var stride = 7;
var normalsOffset = 0;

/**
 * @private
 * @ionsdk
 */
function SphericalPolygonShaderSupport() {}

function kDopFacetNormalName(i, j) {
  return "u_kDopFacetNormal_" + i + "_" + j;
}

function convexHullImplicitSurfaceFunction(
  numberOfVertices,
  vertices,
  name,
  depth,
  normals,
  useUniformsForNormals
) {
  var length = vertices.length;
  var glsl = "";
  var result = "";
  var even = depth % 2 === 0;
  var sign = even ? "+" : "-";
  var oppositeSign = even ? "-" : "+";
  var count = depth === 0 ? length : length - 1;
  for (var i = 0; i < count; ++i) {
    var j = i + 1 === length ? 0 : i + 1;
    var initialIndex = vertices[i];
    var finalIndex = vertices[j];
    var uniform =
      initialIndex < finalIndex
        ? kDopFacetNormalName(initialIndex, finalIndex)
        : kDopFacetNormalName(finalIndex, initialIndex);
    var argument;
    if (useUniformsForNormals) {
      argument = (initialIndex < finalIndex ? sign : oppositeSign) + uniform;
    } else {
      var normal = normals[uniform];
      argument =
        (initialIndex < finalIndex ? sign : oppositeSign) +
        "vec3(" +
        normal.x +
        ", " +
        normal.y +
        ", " +
        normal.z +
        ")";
    }
    if (i === 0) {
      result += "\tfloat value = dot(direction, " + argument + ");\n";
    } else {
      result += "\tvalue = max(value, dot(direction, " + argument + "));\n";
    }
  }
  glsl +=
    "\nfloat " +
    name +
    "(vec3 direction)\n{\n" +
    result +
    "\treturn value;\n}\n";
  return glsl;
}

function sphericalPolygonImplicitSurfaceFunction(
  numberOfVertices,
  hull,
  name,
  depth,
  normals,
  useUniformsForNormals
) {
  var result = "";
  if (defined(hull.holes)) {
    var deeper = depth + 1;
    for (var h = 0; h < hull.holes.length; ++h) {
      var functionName = name + "_" + h;
      result += sphericalPolygonImplicitSurfaceFunction(
        numberOfVertices,
        hull.holes[h],
        functionName,
        deeper,
        normals,
        useUniformsForNormals
      );
    }
  }
  result += convexHullImplicitSurfaceFunction(
    numberOfVertices,
    hull,
    name,
    depth,
    normals,
    useUniformsForNormals
  );
  return result;
}

function getNormals(directions, floats, hull, normals) {
  var numberOfVertices = directions.length;

  if (defined(hull.holes)) {
    for (var h = 0; h < hull.holes.length; ++h) {
      getNormals(directions, floats, hull.holes[h], normals);
    }
  }
  var length = hull.length;
  for (var i = 0; i < length; ++i) {
    var j = i + 1 === length ? 0 : i + 1;
    var initialIndex = hull[i];
    var finalIndex = hull[j];

    var lastDirection = directions[initialIndex];
    var direction = directions[finalIndex];
    var name =
      initialIndex < finalIndex
        ? kDopFacetNormalName(initialIndex, finalIndex)
        : kDopFacetNormalName(finalIndex, initialIndex);

    if (!defined(normals[name])) {
      var difference =
        initialIndex < finalIndex
          ? finalIndex - initialIndex
          : finalIndex + numberOfVertices - initialIndex;
      var temp = new Cartesian3();
      if (difference === 1) {
        temp = Cartesian3.fromArray(
          floats,
          finalIndex * stride + normalsOffset,
          temp
        );
        temp = initialIndex < finalIndex ? temp : Cartesian3.negate(temp, temp);
      } else {
        temp =
          initialIndex < finalIndex
            ? Cartesian3.cross(direction, lastDirection, temp)
            : Cartesian3.cross(lastDirection, direction, temp);
      }
      normals[name] = temp;
    }
  }
}

function aggregateFunction(hull, functionName, variableName) {
  var result =
    "\tfloat " + variableName + " = " + functionName + "(direction);\n";
  if (defined(hull.holes)) {
    for (var i = 0; i < hull.holes.length; ++i) {
      var variable = variableName + "_" + i;
      var name = functionName + "_" + i;
      var hole = hull.holes[i];
      result += "\tfloat " + variable + " = -" + name + "(direction);\n";
      if (defined(hole.holes)) {
        for (var j = 0; j < hole.holes.length; ++j) {
          var v = variable + "_" + j;
          result += aggregateFunction(hole.holes[j], name + "_" + j, v);
          result += "\t" + variable + " = min(" + variable + ", " + v + ");\n";
        }
      }
      result +=
        "\t" +
        variableName +
        " = max(" +
        variableName +
        ", " +
        variable +
        ");\n";
    }
  }
  return result;
}

function returnUniform(value) {
  return function () {
    return value;
  };
}

SphericalPolygonShaderSupport.uniforms = function (sphericalPolygon) {
  var directions = sphericalPolygon._directions;
  var floats = sphericalPolygon._normalsAndBisectorsWithMagnitudeSquared;
  var convexHull = sphericalPolygon.convexHull;

  var normals = {};
  getNormals(directions, floats, convexHull, normals);

  var uniforms = {};
  for (var normal in normals) {
    if (normals.hasOwnProperty(normal)) {
      uniforms[normal] = returnUniform(normals[normal]);
    }
  }
  return uniforms;
};

SphericalPolygonShaderSupport.implicitSurfaceFunction = function (
  sphericalPolygon,
  useUniformsForNormals
) {
  var directions = sphericalPolygon._directions;
  var floats = sphericalPolygon._normalsAndBisectorsWithMagnitudeSquared;
  var convexHull = sphericalPolygon.convexHull;

  var normals = {};
  getNormals(directions, floats, convexHull, normals);

  var glsl = "\n";
  if (useUniformsForNormals) {
    // Emit uniforms.
    for (var normal in normals) {
      if (normals.hasOwnProperty(normal)) {
        glsl += "uniform vec3 " + normal + ";\n";
      }
    }
  }

  // Emit convex hull functions.
  var functionName = "convexHull";
  var variableName = "value";
  var depth = 0;
  glsl += sphericalPolygonImplicitSurfaceFunction(
    directions.length,
    convexHull,
    functionName,
    depth,
    normals,
    useUniformsForNormals
  );
  // Emit implicit surface function.
  var result = aggregateFunction(convexHull, functionName, variableName);
  glsl +=
    "\nfloat sensorSurfaceFunction(vec3 displacement)\n{\n\tvec3 direction = normalize(displacement);\n" +
    result +
    "\treturn " +
    variableName +
    ";\n}\n";
  return glsl;
};
export default SphericalPolygonShaderSupport;
