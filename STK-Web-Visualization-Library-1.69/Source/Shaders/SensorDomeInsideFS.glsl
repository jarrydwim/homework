#if defined(SHOW_ENVIRONMENT_INTERSECTION)
uniform float u_environmentIntersectionWidth;
uniform vec4 u_environmentIntersectionColor;
#endif

void main()
{
#ifdef ONLY_WIRE_FRAME
	gl_FragColor = getMaterialColor();
	return;
#endif

    vec3 sensorVertexEC = czm_modelView[3].xyz;

    // Ray from eye to fragment in eye coordinates
    czm_ray ray;
    if (!czm_isZeroMatrix(czm_inverseProjection))
    {
        ray = czm_ray(vec3(0.0), normalize(v_positionEC));
    }
    else
    {
        ray = czm_ray(vec3(v_positionEC.xy, 0.0), vec3(0.0, 0.0, -1.0));
    }

    czm_raySegment sphereInterval = raySphereIntersectionInterval(ray, sensorVertexEC, u_sensorRadius);
    if (czm_isEmpty(sphereInterval))
    {
        discard;
    }

    vec3 stopEC = czm_pointAlongRay(ray, sphereInterval.stop);
    vec3 stopWC = (czm_inverseView * vec4(stopEC, 1.0)).xyz;

    vec4 stopCC = czm_projection * vec4(stopEC, 1.0);
    float stopZ = stopCC.z / stopCC.w;

    // Discard in case surface is behind far plane due to depth clamping.
    if ((stopZ < -1.0) || (stopZ > 1.0))
    {
        discard;
    }

    float ellipsoidValue = ellipsoidSurfaceFunction(u_inverseRadii, stopWC);
    float halfspaceValue = ellipsoidHorizonHalfspaceSurfaceFunction(u_q, u_inverseRadii, stopWC);

#if defined(ABOVE_ELLIPSOID_HORIZON)
    float horizonValue = ellipsoidHorizonSurfaceFunction(u_q, u_inverseRadii, stopWC);
    if (horizonValue < 0.0)
    {
        discard;
    }
#elif defined(BELOW_ELLIPSOID_HORIZON)
    float horizonValue = ellipsoidHorizonSurfaceFunction(u_q, u_inverseRadii, stopWC);
    if (horizonValue > 0.0)
    {
        discard;
    }
#if !defined(SHOW_THROUGH_ELLIPSOID)
    if (ellipsoidValue < 0.0)
    {
        discard;
    }
    if (halfspaceValue < 0.0)
    {
        discard;
    }
#endif
#else //defined(COMPLETE)
#if !defined(SHOW_THROUGH_ELLIPSOID)
    if (ellipsoidValue < 0.0)
    {
        discard;
    }
    float horizonValue = ellipsoidHorizonSurfaceFunction(u_q, u_inverseRadii, stopWC);
    if (halfspaceValue < 0.0 && horizonValue < 0.0)
    {
        discard;
    }
#endif
#endif
    vec3 stopMC = (czm_inverseModelView * vec4(stopEC, 1.0)).xyz;
    float sensorValue = sensorSurfaceFunction(stopMC);
    if (sensorValue > 0.0)
    {
        discard;
    }
#if !defined(SHOW_THROUGH_ELLIPSOID)
    vec3 cameraVertexWC;
    if (!czm_isZeroMatrix(czm_inverseProjection))
    {
        cameraVertexWC = czm_inverseView[3].xyz;
    }
    else
    {
        cameraVertexWC = (czm_inverseView * vec4(v_positionEC.xy, 0.0, 1.0)).xyz;
    }
    if (inEllipsoidShadow(u_inverseRadii * cameraVertexWC, u_inverseRadii, stopWC))
    {
        discard;
    }
#endif
#if (defined(ENVIRONMENT_CONSTRAINT) && !defined(SHOW_ENVIRONMENT_OCCLUSION)) || defined(SHOW_ENVIRONMENT_INTERSECTION)
    float depth;
    bool isInShadow = getShadowVisibility(stopEC, depth);
#endif
#if defined(ENVIRONMENT_CONSTRAINT) && !defined(SHOW_ENVIRONMENT_OCCLUSION)
    if (isInShadow)
    {
        discard;
    }
#endif
#if defined(SHOW_ENVIRONMENT_INTERSECTION)
    if (showShadowIntersectionPoint(stopEC, depth, u_environmentIntersectionWidth))
    {
        gl_FragColor = u_environmentIntersectionColor;
        setDepth(stopEC);
        return;
    }
#endif
#if defined(SHOW_INTERSECTION) && !defined(ABOVE_ELLIPSOID_HORIZON)
    if (isOnBoundary(ellipsoidValue, czm_epsilon3))
    {
        gl_FragColor = getIntersectionColor();
    }
    else
    {
        gl_FragColor = getSurfaceColor(stopMC, stopEC);
    }
#else
    gl_FragColor = getSurfaceColor(stopMC, stopEC);
#endif
    setDepth(stopEC);
}
