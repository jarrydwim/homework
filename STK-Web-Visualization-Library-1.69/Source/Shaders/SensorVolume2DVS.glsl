attribute vec4 position;
attribute vec2 cartographic;

varying vec3 v_positionEC;
varying vec2 v_cartographic;

void main()
{
    gl_Position = czm_modelViewProjection * position;
    v_positionEC = (czm_modelView * position).xyz;
    v_cartographic = cartographic;
}