//This file is automatically rebuilt by the Cesium build process.
export default "#ifdef WRITE_DEPTH\n\
#ifdef GL_EXT_frag_depth\n\
#extension GL_EXT_frag_depth : enable\n\
#endif\n\
#endif\n\
\n\
void setDepth(vec3 pointEC)\n\
{\n\
    vec4 pointCC = czm_projection * vec4(pointEC, 1.0);\n\
#ifdef LOG_DEPTH\n\
    czm_writeLogDepth(1.0 + pointCC.w);\n\
#else\n\
#ifdef WRITE_DEPTH\n\
#ifdef GL_EXT_frag_depth\n\
    float z = pointCC.z / pointCC.w;\n\
\n\
    float n = czm_depthRange.near;\n\
    float f = czm_depthRange.far;\n\
\n\
    gl_FragDepthEXT = (z * (f - n) + f + n) * 0.5;\n\
#endif\n\
#endif\n\
#endif\n\
}\n\
";
