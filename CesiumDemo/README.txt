To compile this sample application with Ant:
  * Download STK Web Visualization Library from 
    https://support.agi.com/downloads/ and unpack the contents of
    the Build\Cesium directory into the web\public\Cesium directory.
  * Copy your AGI.Foundation.lic file into the src directory.
  * Run "ant package".  

The application will be compiled, packaged into a jar, and placed in the dist 
directory.  You can then double-click the CesiumDemo.jar file to run the 
application, or, simply run "ant run".

Then, connect to http://localhost:4567 in your browser to run the application.
