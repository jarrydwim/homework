/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["exports","./when-283ff6c4"],function(t,r){"use strict";function e(t){var r;this.name="RuntimeError",this.message=t;try{throw new Error}catch(t){r=t.stack}this.stack=r}r.defined(Object.create)&&((e.prototype=Object.create(Error.prototype)).constructor=e),e.prototype.toString=function(){var t=this.name+": "+this.message;return r.defined(this.stack)&&(t+="\n"+this.stack.toString()),t},t.RuntimeError=e});
