/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["exports","./Math-6bc5bd67"],function(r,P){"use strict";var t={computePositions:function(r,t,e,a,i){var n,o=.5*r,s=-o,c=a+a,u=new Float64Array(3*(i?2*c:c)),f=0,h=0,y=i?3*c:0,M=i?3*(c+a):3*a;for(n=0;n<a;n++){var b=n/a*P.CesiumMath.TWO_PI,d=Math.cos(b),m=Math.sin(b),v=d*e,l=m*e,p=d*t,C=m*t;u[h+y]=v,u[h+y+1]=l,u[h+y+2]=s,u[h+M]=p,u[h+M+1]=C,u[h+M+2]=o,h+=3,i&&(u[f++]=v,u[f++]=l,u[f++]=s,u[f++]=p,u[f++]=C,u[f++]=o)}return u}};r.CylinderGeometryLibrary=t});
