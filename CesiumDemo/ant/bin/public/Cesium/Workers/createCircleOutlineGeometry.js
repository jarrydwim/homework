/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["./when-283ff6c4","./Check-5faa3364","./Math-6bc5bd67","./Cartesian2-5374041e","./Transforms-05b6f507","./RuntimeError-73ca5813","./WebGLConstants-cf9e59a1","./ComponentDatatype-9728ab6a","./GeometryAttribute-3cde56b0","./GeometryAttributes-7ab0fc8a","./IndexDatatype-462102f2","./GeometryOffsetAttribute-b3fcb1c2","./EllipseGeometryLibrary-07af34b4","./EllipseOutlineGeometry-3d658f35"],function(l,e,i,n,t,r,s,o,a,u,c,d,m,p){"use strict";function y(e){var i=(e=l.defaultValue(e,l.defaultValue.EMPTY_OBJECT)).radius,t={center:e.center,semiMajorAxis:i,semiMinorAxis:i,ellipsoid:e.ellipsoid,height:e.height,extrudedHeight:e.extrudedHeight,granularity:e.granularity,numberOfVerticalLines:e.numberOfVerticalLines};this._ellipseGeometry=new p.EllipseOutlineGeometry(t),this._workerName="createCircleOutlineGeometry"}y.packedLength=p.EllipseOutlineGeometry.packedLength,y.pack=function(e,i,t){return p.EllipseOutlineGeometry.pack(e._ellipseGeometry,i,t)};var f=new p.EllipseOutlineGeometry({center:new n.Cartesian3,semiMajorAxis:1,semiMinorAxis:1}),G={center:new n.Cartesian3,radius:void 0,ellipsoid:n.Ellipsoid.clone(n.Ellipsoid.UNIT_SPHERE),height:void 0,extrudedHeight:void 0,granularity:void 0,numberOfVerticalLines:void 0,semiMajorAxis:void 0,semiMinorAxis:void 0};return y.unpack=function(e,i,t){var r=p.EllipseOutlineGeometry.unpack(e,i,f);return G.center=n.Cartesian3.clone(r._center,G.center),G.ellipsoid=n.Ellipsoid.clone(r._ellipsoid,G.ellipsoid),G.height=r._height,G.extrudedHeight=r._extrudedHeight,G.granularity=r._granularity,G.numberOfVerticalLines=r._numberOfVerticalLines,l.defined(t)?(G.semiMajorAxis=r._semiMajorAxis,G.semiMinorAxis=r._semiMinorAxis,t._ellipseGeometry=new p.EllipseOutlineGeometry(G),t):(G.radius=r._semiMajorAxis,new y(G))},y.createGeometry=function(e){return p.EllipseOutlineGeometry.createGeometry(e._ellipseGeometry)},function(e,i){return l.defined(i)&&(e=y.unpack(e,i)),e._ellipseGeometry._center=n.Cartesian3.clone(e._ellipseGeometry._center),e._ellipseGeometry._ellipsoid=n.Ellipsoid.clone(e._ellipseGeometry._ellipsoid),y.createGeometry(e)}});
