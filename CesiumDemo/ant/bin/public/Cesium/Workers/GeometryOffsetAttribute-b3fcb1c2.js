/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["exports","./when-283ff6c4","./Check-5faa3364"],function(e,c,t){"use strict";var f=Object.freeze({NONE:0,TOP:1,ALL:2});e.GeometryOffsetAttribute=f,e.arrayFill=function(e,t,f,a){if("function"==typeof e.fill)return e.fill(t,f,a);for(var r=e.length>>>0,n=c.defaultValue(f,0),i=n<0?Math.max(r+n,0):Math.min(n,r),l=c.defaultValue(a,r),u=l<0?Math.max(r+l,0):Math.min(l,r);i<u;)e[i]=t,i++;return e}});
