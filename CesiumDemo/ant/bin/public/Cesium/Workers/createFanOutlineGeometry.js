/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["./when-283ff6c4","./Check-5faa3364","./Math-6bc5bd67","./Cartesian2-5374041e","./Transforms-05b6f507","./RuntimeError-73ca5813","./WebGLConstants-cf9e59a1","./ComponentDatatype-9728ab6a","./GeometryAttribute-3cde56b0","./GeometryAttributes-7ab0fc8a","./IndexDatatype-462102f2","./VertexFormat-de1e6064"],function(v,e,t,D,w,r,a,A,x,G,_,n){"use strict";var g=new D.Cartesian3;return function(e){var t,r,a,n,i,o,s=e._radius,m=v.defined(e._perDirectionRadius)&&e._perDirectionRadius,p=e._directions,u=e._vertexFormat,f=e._numberOfRings,c=0,d=p.length,y=new G.GeometryAttributes;if(u.position){for(r=0,n=3*d*f,o=new Float64Array(n),a=0;a<f;a++)for(t=0;t<d;t++){g=D.Cartesian3.fromSpherical(p[t],g);var b=m?D.Cartesian3.magnitude(g):s,C=b/f*(a+1);g=D.Cartesian3.normalize(g,g),o[r++]=g.x*C,o[r++]=g.y*C,o[r++]=g.z*C,c=Math.max(c,b)}y.position=new x.GeometryAttribute({componentDatatype:A.ComponentDatatype.DOUBLE,componentsPerAttribute:3,values:o})}for(r=0,n=2*d*f,i=_.IndexDatatype.createTypedArray(n/3,n),a=0;a<f;a++){var h=a*d;for(t=0;t<d-1;t++)i[r++]=t+h,i[r++]=t+1+h;i[r++]=t+h,i[r++]=0+h}return new x.Geometry({attributes:y,indices:i,primitiveType:x.PrimitiveType.LINES,boundingSphere:new w.BoundingSphere(D.Cartesian3.ZERO,c)})}});
