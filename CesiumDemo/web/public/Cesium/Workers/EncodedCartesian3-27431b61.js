/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["exports","./when-283ff6c4","./Check-5faa3364","./Cartesian2-5374041e"],function(e,h,n,i){"use strict";function a(){this.high=i.Cartesian3.clone(i.Cartesian3.ZERO),this.low=i.Cartesian3.clone(i.Cartesian3.ZERO)}a.encode=function(e,n){var i;return h.defined(n)||(n={high:0,low:0}),0<=e?(i=65536*Math.floor(e/65536),n.high=i,n.low=e-i):(i=65536*Math.floor(-e/65536),n.high=-i,n.low=e+i),n};var r={high:0,low:0};a.fromCartesian=function(e,n){h.defined(n)||(n=new a);var i=n.high,o=n.low;return a.encode(e.x,r),i.x=r.high,o.x=r.low,a.encode(e.y,r),i.y=r.high,o.y=r.low,a.encode(e.z,r),i.z=r.high,o.z=r.low,n};var t=new a;a.writeElements=function(e,n,i){a.fromCartesian(e,t);var o=t.high,h=t.low;n[i]=o.x,n[i+1]=o.y,n[i+2]=o.z,n[i+3]=h.x,n[i+4]=h.y,n[i+5]=h.z},e.EncodedCartesian3=a});
