/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["exports","./when-283ff6c4","./Check-5faa3364","./Math-6bc5bd67","./Cartesian2-5374041e","./Transforms-05b6f507"],function(n,i,a,e,o,t){"use strict";function s(n,a){this.normal=o.Cartesian3.clone(n),this.distance=a}s.fromPointNormal=function(n,a,e){var t=-o.Cartesian3.dot(a,n);return i.defined(e)?(o.Cartesian3.clone(a,e.normal),e.distance=t,e):new s(a,t)};var r=new o.Cartesian3;s.fromCartesian4=function(n,a){var e=o.Cartesian3.fromCartesian4(n,r),t=n.w;return i.defined(a)?(o.Cartesian3.clone(e,a.normal),a.distance=t,a):new s(e,t)},s.getPointDistance=function(n,a){return o.Cartesian3.dot(n.normal,a)+n.distance};var c=new o.Cartesian3;s.projectPointOntoPlane=function(n,a,e){i.defined(e)||(e=new o.Cartesian3);var t=s.getPointDistance(n,a),r=o.Cartesian3.multiplyByScalar(n.normal,t,c);return o.Cartesian3.subtract(a,r,e)};var l=new o.Cartesian3;s.transform=function(n,a,e){return t.Matrix4.multiplyByPointAsVector(a,n.normal,r),o.Cartesian3.normalize(r,r),o.Cartesian3.multiplyByScalar(n.normal,-n.distance,l),t.Matrix4.multiplyByPoint(a,l,l),s.fromPointNormal(l,r,e)},s.clone=function(n,a){return i.defined(a)?(o.Cartesian3.clone(n.normal,a.normal),a.distance=n.distance,a):new s(n.normal,n.distance)},s.equals=function(n,a){return n.distance===a.distance&&o.Cartesian3.equals(n.normal,a.normal)},s.ORIGIN_XY_PLANE=Object.freeze(new s(o.Cartesian3.UNIT_Z,0)),s.ORIGIN_YZ_PLANE=Object.freeze(new s(o.Cartesian3.UNIT_X,0)),s.ORIGIN_ZX_PLANE=Object.freeze(new s(o.Cartesian3.UNIT_Y,0)),n.Plane=s});
