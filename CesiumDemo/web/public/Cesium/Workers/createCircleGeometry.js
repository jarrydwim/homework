/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["./when-283ff6c4","./Check-5faa3364","./Math-6bc5bd67","./Cartesian2-5374041e","./Transforms-05b6f507","./RuntimeError-73ca5813","./WebGLConstants-cf9e59a1","./ComponentDatatype-9728ab6a","./GeometryAttribute-3cde56b0","./GeometryAttributes-7ab0fc8a","./AttributeCompression-fb133ccf","./GeometryPipeline-96353589","./EncodedCartesian3-27431b61","./IndexDatatype-462102f2","./IntersectionTests-bb4b429d","./Plane-fcb3bbc4","./GeometryOffsetAttribute-b3fcb1c2","./VertexFormat-de1e6064","./EllipseGeometryLibrary-07af34b4","./GeometryInstance-26789d1e","./EllipseGeometry-345f7e80"],function(o,e,t,n,i,r,a,s,l,m,d,c,u,p,y,_,h,G,x,f,g){"use strict";function b(e){var t=(e=o.defaultValue(e,o.defaultValue.EMPTY_OBJECT)).radius,i={center:e.center,semiMajorAxis:t,semiMinorAxis:t,ellipsoid:e.ellipsoid,height:e.height,extrudedHeight:e.extrudedHeight,granularity:e.granularity,vertexFormat:e.vertexFormat,stRotation:e.stRotation,shadowVolume:e.shadowVolume};this._ellipseGeometry=new g.EllipseGeometry(i),this._workerName="createCircleGeometry"}b.packedLength=g.EllipseGeometry.packedLength,b.pack=function(e,t,i){return g.EllipseGeometry.pack(e._ellipseGeometry,t,i)};var v=new g.EllipseGeometry({center:new n.Cartesian3,semiMajorAxis:1,semiMinorAxis:1}),E={center:new n.Cartesian3,radius:void 0,ellipsoid:n.Ellipsoid.clone(n.Ellipsoid.UNIT_SPHERE),height:void 0,extrudedHeight:void 0,granularity:void 0,vertexFormat:new G.VertexFormat,stRotation:void 0,semiMajorAxis:void 0,semiMinorAxis:void 0,shadowVolume:void 0};return b.unpack=function(e,t,i){var r=g.EllipseGeometry.unpack(e,t,v);return E.center=n.Cartesian3.clone(r._center,E.center),E.ellipsoid=n.Ellipsoid.clone(r._ellipsoid,E.ellipsoid),E.height=r._height,E.extrudedHeight=r._extrudedHeight,E.granularity=r._granularity,E.vertexFormat=G.VertexFormat.clone(r._vertexFormat,E.vertexFormat),E.stRotation=r._stRotation,E.shadowVolume=r._shadowVolume,o.defined(i)?(E.semiMajorAxis=r._semiMajorAxis,E.semiMinorAxis=r._semiMinorAxis,i._ellipseGeometry=new g.EllipseGeometry(E),i):(E.radius=r._semiMajorAxis,new b(E))},b.createGeometry=function(e){return g.EllipseGeometry.createGeometry(e._ellipseGeometry)},b.createShadowVolume=function(e,t,i){var r=e._ellipseGeometry._granularity,o=e._ellipseGeometry._ellipsoid,n=t(r,o),a=i(r,o);return new b({center:e._ellipseGeometry._center,radius:e._ellipseGeometry._semiMajorAxis,ellipsoid:o,stRotation:e._ellipseGeometry._stRotation,granularity:r,extrudedHeight:n,height:a,vertexFormat:G.VertexFormat.POSITION_ONLY,shadowVolume:!0})},Object.defineProperties(b.prototype,{rectangle:{get:function(){return this._ellipseGeometry.rectangle}},textureCoordinateRotationPoints:{get:function(){return this._ellipseGeometry.textureCoordinateRotationPoints}}}),function(e,t){return o.defined(t)&&(e=b.unpack(e,t)),e._ellipseGeometry._center=n.Cartesian3.clone(e._ellipseGeometry._center),e._ellipseGeometry._ellipsoid=n.Ellipsoid.clone(e._ellipseGeometry._ellipsoid),b.createGeometry(e)}});
