/**
 * STK Web Visualization Library
 *
 * Copyright 2018-2020 Analytical Graphics, Inc.
 * All rights reserved.
 *
 * Cesium Analytics SDK
 *
 * Copyright 2012-2020 Cesium GS, Inc.
 * All rights reserved.
 *
 * Patents US9153063B2 US9865085B1 US9449424B2 US10592242
 * Patents pending US15/829,786 US16/850,266 US16/851,958
 *
 * Portions licensed separately.
 * See https://github.com/CesiumGS/cesium/blob/master/LICENSE.md for open-source Cesium license.
 */
define(["./when-283ff6c4","./Check-5faa3364","./Math-6bc5bd67","./Cartesian2-5374041e","./Transforms-05b6f507","./RuntimeError-73ca5813","./WebGLConstants-cf9e59a1","./ComponentDatatype-9728ab6a","./GeometryAttribute-3cde56b0","./GeometryAttributes-7ab0fc8a","./AttributeCompression-fb133ccf","./GeometryPipeline-96353589","./EncodedCartesian3-27431b61","./IndexDatatype-462102f2","./IntersectionTests-bb4b429d","./Plane-fcb3bbc4","./GeometryInstance-26789d1e","./arrayRemoveDuplicates-b54b230e","./EllipsoidTangentPlane-d414d41e","./OrientedBoundingBox-d0cc8276","./CoplanarPolygonGeometryLibrary-9deb248b","./ArcType-3b6f404e","./EllipsoidRhumbLine-0c37b6de","./PolygonPipeline-ecd6cf40","./PolygonGeometryLibrary-b6d8f34b"],function(a,e,t,l,p,r,n,s,u,d,o,b,i,m,y,c,f,g,h,P,G,v,L,C,T){"use strict";function E(e){for(var t=e.length,r=new Float64Array(3*t),n=m.IndexDatatype.createTypedArray(t,2*t),o=0,a=0,i=0;i<t;i++){var y=e[i];r[o++]=y.x,r[o++]=y.y,r[o++]=y.z,n[a++]=i,n[a++]=(i+1)%t}var c=new d.GeometryAttributes({position:new u.GeometryAttribute({componentDatatype:s.ComponentDatatype.DOUBLE,componentsPerAttribute:3,values:r})});return new u.Geometry({attributes:c,indices:n,primitiveType:u.PrimitiveType.LINES})}function k(e){var t=(e=a.defaultValue(e,a.defaultValue.EMPTY_OBJECT)).polygonHierarchy;this._polygonHierarchy=t,this._workerName="createCoplanarPolygonOutlineGeometry",this.packedLength=T.PolygonGeometryLibrary.computeHierarchyPackedLength(t)+1}k.fromPositions=function(e){return new k({polygonHierarchy:{positions:(e=a.defaultValue(e,a.defaultValue.EMPTY_OBJECT)).positions}})},k.pack=function(e,t,r){return r=a.defaultValue(r,0),t[r=T.PolygonGeometryLibrary.packPolygonHierarchy(e._polygonHierarchy,t,r)]=e.packedLength,t};var H={polygonHierarchy:{}};return k.unpack=function(e,t,r){t=a.defaultValue(t,0);var n=T.PolygonGeometryLibrary.unpackPolygonHierarchy(e,t);t=n.startingIndex,delete n.startingIndex;var o=e[t];return a.defined(r)||(r=new k(H)),r._polygonHierarchy=n,r.packedLength=o,r},k.createGeometry=function(e){var t=e._polygonHierarchy,r=t.positions;if(!((r=g.arrayRemoveDuplicates(r,l.Cartesian3.equalsEpsilon,!0)).length<3)&&G.CoplanarPolygonGeometryLibrary.validOutline(r)){var n=T.PolygonGeometryLibrary.polygonOutlinesFromHierarchy(t,!1);if(0!==n.length){for(var o=[],a=0;a<n.length;a++){var i=new f.GeometryInstance({geometry:E(n[a])});o.push(i)}var y=b.GeometryPipeline.combineInstances(o)[0],c=p.BoundingSphere.fromPoints(t.positions);return new u.Geometry({attributes:y.attributes,indices:y.indices,primitiveType:y.primitiveType,boundingSphere:c})}}},function(e,t){return a.defined(t)&&(e=k.unpack(e,t)),e._ellipsoid=l.Ellipsoid.clone(e._ellipsoid),k.createGeometry(e)}});
